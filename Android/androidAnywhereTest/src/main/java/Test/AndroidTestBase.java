package Test;

	
import io.appium.java_client.AppiumDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

	public class AndroidTestBase {
	 
	  AppiumDriver driver = null;
	         
	  @BeforeMethod
	   public void setup() throws Exception {
		  
	  File appDir = new File("C://androidSDK/sdk/build-tools/android-4.4.2");
	  File app = new File(appDir, "DStvAnywhere-release-intro-video.apk");
	  DesiredCapabilities capabilities = new DesiredCapabilities();
	  capabilities.setCapability("device","Android");
	  capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
	  capabilities.setCapability(CapabilityType.VERSION, "4.2.2");
	  capabilities.setCapability(CapabilityType.PLATFORM, "WINDOWS");
	                // Here we mention the app's package name, to find the package name we  have to convert .apk file into java class files
	  capabilities.setCapability("appPackage","com.dstv.dm.dstvanywhere");
	                //Here we mention the activity name, which is invoked initially as app's first page.
	  capabilities.setCapability("appActivity","activities.SplashScreenActivity");
	  //capabilities.setCapability("app-wait-activity","LoginActivity,NewAccountActivity");
	  capabilities.setCapability("app", app.getAbsolutePath());

	 
	  driver = new AppiumDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
	  driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);  

	         }

	 
	       @AfterMethod
	        public void tearDown() {
	               driver.quit();
	        }

	}


	
	
	
	