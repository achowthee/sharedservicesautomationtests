package Test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ClickOk extends AndroidTestBase{
	
	@BeforeMethod
	public void before() throws Exception {
		

	}

  @Test
  public void homeClickOk() throws Exception {
	//locate the Text on the calculator by using By.name()
	  WebElement two=driver.findElement(By.name("Ok"));
	  two.click();
	  
	//click On menu item
	  
	  WebElement watch=driver.findElement(By.name("Watch Now"));
	  watch.click();
	  
	  WebElement yest=driver.findElement(By.name("Yesterday"));
	  yest.click();
	  
	  WebElement results=driver.findElement(By.tagName("EditText"));
      //Check the calculated value on the edit box
      assert results.getText().equals("6"):"Actual value is : "+results.getText()+" did not match with expected value: 6";
	  
	  
	  //WebElement menu=driver.findElement(By.xpath("/FrameLayout/LinearLayout/FrameLayout/View/LinearLayout/FrameLayout/ImageView"));
	  //menu.click();
	  
  }
  
  
}
