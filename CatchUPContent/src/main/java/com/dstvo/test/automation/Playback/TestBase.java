package com.dstvo.test.automation.Playback;

import org.sikuli.script.Screen;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.*;

import java.net.URL;


public class TestBase {
	  //  private static ChromeDriverService service;
	    public RemoteWebDriver driver;  //was private, changed to use in other tests       
	   // public Screen ply;
       // public patterns image = new patterns();
 
	    @BeforeClass
		public void before() throws Exception {
	   // driver = new RemoteWebDriver(new URL("http://197.80.203.161:4457/wd/hub"), DesiredCapabilities.firefox());

	    /**To run tests on autBox using chrome */ 
	    //driver = new RemoteWebDriver(new URL("http://197.80.203.161:4466/wd/hub"), DesiredCapabilities.chrome());
	   // driver = new RemoteWebDriver(new URL("http://10.40.4.6:4444/wd/hub"), DesiredCapabilities.chrome());
	 	//System.setProperty("webdriver.chrome.driver", "C://ProgramFiles(x86)//chromedriver_win32/chromedriver.exe");

	    
//	    /**To run tests on autBox using chrome */ 
//	    driver = new RemoteWebDriver(new URL("http://197.80.203.161:4466/wd/hub"), DesiredCapabilities.chrome());
	    
	 	/**To run tests locally using chrome*/
//	 	/* WINDOWS */
       System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe"); 
       
//	    /* MAC */
//	 	System.setProperty("webdriver.chrome.driver", "///Applications/chromedriver");
//	    
		driver = new ChromeDriver();
		/**END To run tests locally using chrome*/
	 	 
	 	//driver.manage().window().maximize();


	      /* FirefoxProfile profile = new FirefoxProfile();
	  	   driver = new FirefoxDriver(profile);*/
	
	        
	 }
	    		
	        
	    @AfterClass
		public void after() {
	        if (driver != null) {
	        driver.quit();
	        }
	    }
	    
	    protected String getSiteURL (){
	        String URL = System.getProperty("testUrl");
	        if (URL == null){

	            return "http://now.dstv.com";

	        	
	        
	        }
	        else{
	                return URL;
	        }
	        
	    }
	        
  	    protected RemoteWebDriver getWebDriver() {
	        return driver;
	    }
	    
	    
	}
