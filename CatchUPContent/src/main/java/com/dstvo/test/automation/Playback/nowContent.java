package com.dstvo.test.automation.Playback;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.sikuli.script.SikuliScript;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;


public class nowContent extends TestBase {
 
@BeforeClass
public void before() throws Exception{
	super.before();
	getWebDriver().get(getSiteURL());
	}

@Test
public void selectAsset() throws Exception{
	driver.manage().window().maximize();
	ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/a", "login");
	ConnectControl.connectLiteLoginUsernameEmail(driver, "dstvoandroidqa@gmail.com", "android123", "");
	Thread.sleep(5000);
	WebDriverWait wait = new WebDriverWait(driver, 60);
	WebElement catchUpLink = wait.until(ExpectedConditions.elementToBeClickable((By.id("menu-item-catch-up"))));
	ReusableFunc.ClickByID(driver, "menu-item-catch-up", "go to catchUp page");
	WebElement catchUpPage = wait.until(ExpectedConditions.elementToBeClickable(By.id("category-series")));
	ReusableFunc.ClickByID(driver, "category-series", "go to series tab");

	 
	 Thread.sleep(3000);
	 int trow = 1;	
	 int count=0;
	 List<WebElement> TotalCatchUpItems=driver.findElements(By.xpath(" .//*[@id='catch-up-results']/div"));
	 System.out.println("No. of new CatchUp episodes: "+TotalCatchUpItems.size());
	 
	 for(WebElement rowElement:TotalCatchUpItems){
	
	WebElement catchUpplay = wait.until(ExpectedConditions.elementToBeClickable(By.xpath((".//*[@id='catch-up-results']/div[" + trow +"]/a/div/img"))));
	ReusableFunc.ClickByXpath(driver, ".//*[@id='catch-up-results']/div["+ trow +"]/a/div/img", "catchup play page");
	Thread.sleep(10000);
 	Screen ply =new Screen();
	ply.capture();
	ply.setAutoWaitTimeout(60.0);
	ply.click("C:/Users/Belinda.ndlovu/Documents/workspace-sts-3.6.3.RELEASE/CatchUPContent/src/main/resources/images/play.sikuli/play.png");
    ply.exists("C:/Users/Belinda.ndlovu/Documents/workspace-sts-3.6.3.RELEASE/CatchUPContent/src/main/resources/images/play.sikuli/auth.png");
	System.out.println("truing to play assert");
	System.out.println(ply.getAutoWaitTimeout());
	ply.setAutoWaitTimeout(30.0);
	if(ply.exists("C:/Users/Belinda.ndlovu/Documents/workspace-sts-3.6.3.RELEASE/CatchUPContent/src/main/resources/images/play.sikuli/warn.png") != null){
		System.out.println("the assert is playing for reaaaaaalz");
	}
	
	else {
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		// copyFile
		
		FileUtils.copyFile(scrFile, new File("c:\\catchUpContent\\screenshot_" + count + ".png" ));
		WebElement videoMeta = driver.findElementById("video-meta");
		System.out.println(videoMeta.getText());
		WebElement downloadsize = driver.findElementById("video-action");
		System.out.println(downloadsize.getText());
		
	}
	count++;	
	driver.navigate().back();
	trow++;
  
	 }
	
}

}
