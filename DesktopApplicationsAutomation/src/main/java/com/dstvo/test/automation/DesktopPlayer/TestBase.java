package com.dstvo.test.automation.DesktopPlayer;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.sikuli.script.App;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Screen;
import org.sikuli.script.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.*;

<<<<<<< HEAD
//import com.gargoylesoftware.htmlunit.javascript.host.Screen;

=======
import com.dstvo.test.automation.DesktopPlayer.ImagePatterns;

import java.awt.Image;
import java.net.MalformedURLException;
>>>>>>> 724dd46c7299d3a7976b08eac56ea70eb746bf83
import java.net.URL;


public class TestBase {
	
	public RemoteWebDriver driver;
	public Screen desktopPlayer = new Screen();
	public ImagePatterns image = new ImagePatterns();
	protected String userName = "iccmigr03";
	protected String password = "123456";
	
	@BeforeClass
	public void before() throws Exception {
			App.open(getAppLocation());
		
	}
	
	@AfterClass
	public void after() throws Exception {
		
		App.close(getAppLocation());
	}
	
	protected String getAppLocation (){
		String location = "C:\\Program Files (x86)\\DStv Desktop Player\\DStv Desktop Player.exe";
		//String location = "C:\\Program Files (x86)\\DStv Desktop Player\\backup\\DStv Desktop Player.exe";
		return location;  
	}
	
	protected void instantiateWebriver(String siteURL) throws MalformedURLException{
		
		/*driver = new RemoteWebDriver(new URL("http://197.80.203.161:4466/wd/hub"), DesiredCapabilities.chrome());
		System.setProperty("webdriver.chrome.driver", "C://ProgramFiles(x86)//chromedriver_win32/chromedriver.exe");
		*/
		
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		getWebDriver().get(siteURL);

		/*driver = new RemoteWebDriver(new URL("http://197.80.203.161:4445/wd/hub"), DesiredCapabilities.firefox());
    	FirefoxProfile profile = new FirefoxProfile();
	    profile.setPreference("network.proxy.type", 1);
	    profile.setPreference("network.proxy.http", "03rnb-proxy06");
	    profile.setPreference("network.proxy.http_port", 8080);
	    profile.setPreference("network.proxy.ssl", "03rnb-proxy06");
	    profile.setPreference("network.proxy.ssl_port", 8080);
	    profile.setPreference("network.proxy.ftp", "03rnb-proxy06");
	    profile.setPreference("network.proxy.ftp_port", 8080);
	    profile.setPreference("network.proxy.Socks", "03rnb-proxy06");
	    profile.setPreference("network.proxy.Socks_port", 8080);
	    profile.setPreference("network.proxy.no_proxies_on","aut.boxoffice.dstv.com");
	    driver = new FirefoxDriver(profile);*/
	}
	
	protected RemoteWebDriver getWebDriver() {
        return driver;
    }
	
	//Login Reusable 
	protected boolean isLogin() throws Exception{
		Thread.sleep(10000);
		if(desktopPlayer.exists(image.getLoggedInAs()) != null){
			return true;
		}
		else
			return false;			
	}
	
	protected void logIn() throws Exception{
		desktopPlayer.wait(image.getLoginPopup(), 60);
		desktopPlayer.exists(image.getLoginWithYourConnectID());
		desktopPlayer.paste(image.getEnterUsername(), userName);
		desktopPlayer.paste(image.getEnterPassword(), password);
		desktopPlayer.click(image.getUntickKeepMeLoggedIn());
		desktopPlayer.click(image.getLogin());
		desktopPlayer.exists(image.getLoggedInAs(),1000);
	}
}

