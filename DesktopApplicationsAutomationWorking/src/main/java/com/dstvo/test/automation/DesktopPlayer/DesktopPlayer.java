package com.dstvo.test.automation.DesktopPlayer;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class DesktopPlayer extends TestBase{
	
	@Test
	public void desktopPlayerLogin() throws Exception{
		if(!isLogin())
			logIn();
	}
	
	
	@Test(priority = 2, dependsOnMethods = { "desktopPlayerLogin" })
	public void downloadFromCatchUp() throws Exception{
		//desktopPlayer.click(image.getCatchupdownload());
		Thread.sleep(5000);
		
		instantiateWebriver("http://now.dstv.com/");
		ReusableFunc.VerifyTextOnPage(driver, "About DStv Now");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/a", "login");
		ReusableFunc.VerifyTextOnPage(driver,"Login with your Connect ID");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("username"), userName);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("password"), password);
		ReusableFunc.ClickByID(driver, "login", "Login Button");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[1]/span[1]"), "iccmigr03");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='header-nav']/ul/li[4]/a", "Select Catchup");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='filter-header']/div/div[1]/div/div[2]/a", "Select Series");
		int randomSeries = Utilities.generateRandomInt(1, 10);
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='catch-up-results']/div["+randomSeries+"]/a", "Select One Series");
		Thread.sleep(5000);
		ReusableFunc.waitForElementToDisplay(driver, By.id("download-catchUp-link"), 20);
		ReusableFunc.ClickByID(driver, "download-catchUp-link", "download-catchUp-link");
		Thread.sleep(5000);
		desktopPlayer.wait(image.getDownloadPopup());
		desktopPlayer.click(image.getDownloadPopupOkButton());
		Thread.sleep(3000);
		driver.quit();
		//App.focus(getAppLocation());
		Thread.sleep(5000);
		desktopPlayer.click(image.getDownloadQueue());
		desktopPlayer.click(image.getPauseDownload());
		Thread.sleep(2000);
		desktopPlayer.click(image.getResumeDownload());
		desktopPlayer.click(image.getLogo());
		desktopPlayer.waitVanish(image.getZeroBytes());
		Thread.sleep(2000);
		while (desktopPlayer.exists(image.getResumeDownload()) != null || desktopPlayer.exists(image.getPauseDownload()) != null){
			Thread.sleep(10000);
			
			if (desktopPlayer.exists(image.getResumeDownload()) != null) {
				desktopPlayer.click(image.getResumeDownload());
			}
			Thread.sleep(3000);
			desktopPlayer.click(image.getLogo());
		}
		//desktopPlayer.waitVanish(image.getPauseDownloadBlack());
		//desktopPlayer.wait(image.getYourDawnLoadListIsEmpty(),600);

	}
	
	@Test(priority = 3, dependsOnMethods = { "downloadFromCatchUp" })
	public void playDownloadedCatchUpVideo() throws Exception{
		desktopPlayer.click(image.getReadyToWatch());
		desktopPlayer.click(image.getWhatchNow());
		desktopPlayer.wait(image.getBackToDownloadList(),180);
		Thread.sleep(60000);
		desktopPlayer.click(image.getBackToDownloadList());
		desktopPlayer.exists(image.getVerifyVideoWasPlayed());
	}
	
	
	@Test(priority = 4, dependsOnMethods = { "downloadFromCatchUp" })
	public void deleteDownloadedCatupContent() throws Exception{
		while (desktopPlayer.exists(image.getYourDawnLoadListIsEmpty()) == null){
			Thread.sleep(1000);
			
			if (desktopPlayer.exists(image.getYourDawnLoadListIsEmpty()) == null) {
				desktopPlayer.click(image.getDeleteDownloadedContent());
				desktopPlayer.click(image.getDeleteDownloadedContentConfirm());
				Thread.sleep(3000);
			}
		}
	}
	
	
	@Test(priority = 5, dependsOnMethods = {"deleteDownloadedCatupContent"})
	public void desktopPlayerLogout() throws Exception{
		desktopPlayer.click(image.getLogout());
		desktopPlayer.click(image.getConfirmLogout());
	}
	
}
