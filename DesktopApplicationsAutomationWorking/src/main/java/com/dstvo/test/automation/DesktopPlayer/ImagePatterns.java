package com.dstvo.test.automation.DesktopPlayer;

import org.sikuli.script.Pattern;

public class ImagePatterns {
	//logging in
	private Pattern LoginPopup = new Pattern("src/main/resources/Images/LoginPopup/LoginPopup.png");
	private Pattern EnterPassword = new Pattern("src/main/resources/Images/LoginPopup/EnterPassword.png");
	private Pattern EnterUsername = new Pattern("src/main/resources/Images/LoginPopup/EnterUsername.png");
	private Pattern Login = new Pattern("src/main/resources/Images/LoginPopup/Login.png");
	private Pattern LoginWithYourConnectID = new Pattern("src/main/resources/Images/LoginPopup/LoginWithYourConnectID.png");
	private Pattern UntickKeepMeLoggedIn = new Pattern("src/main/resources/Images/LoginPopup/UntickKeepMeLoggedIn.png");
	private Pattern LoggedInAs = new Pattern("src/main/resources/Images/LoginPopup/LoggedInAs.png");
	//Downloading Catchup
	private Pattern catchupdownload = new Pattern("src/main/resources/Images/Download Catchup/catchupdownload.png");
	private Pattern downloadPopup = new Pattern("src/main/resources/Images/Download Catchup/downloadPopup.png");
	private Pattern downloadPopupOkButton = new Pattern("src/main/resources/Images/Download Catchup/downloadPopupOkButton.png");
	private Pattern downloadQueue = new Pattern("src/main/resources/Images/Download Catchup/downloadQueue.png");
	private Pattern PauseDownloadBlack = new Pattern("src/main/resources/Images/Download Catchup/PauseDownloadBlack.png");
	private Pattern ReadyToWatch = new Pattern("src/main/resources/Images/Watch Catchup Content/ReadyToWatch.png");
	private Pattern whatchNow= new Pattern("src/main/resources/Images/Watch Catchup Content/whatchNow.png");
	private Pattern PauseDownload= new Pattern("src/main/resources/Images/Download Catchup/PauseDownload.png");
	private Pattern ResumeDownload= new Pattern("src/main/resources/Images/Download Catchup/ResumeDownload.png");
	private Pattern logo= new Pattern("src/main/resources/Images/Download Catchup/logo.png");
	private Pattern ZeroBytes= new Pattern("src/main/resources/Images/Download Catchup/0bytes.png");
	//Watching Catchup
	private Pattern verifyVideoWasPlayed= new Pattern("src/main/resources/Images/Watch Catchup Content/VerifyVideoWasPlayed.png");
	private Pattern YourDawnLoadListIsEmpty= new Pattern("src/main/resources/Images/Watch Catchup Content/YourDawnLoadListIsEmpty.png");
	private Pattern CatchUpContentPlaying= new Pattern("src/main/resources/Images/Watch Catchup Content/CatchUpContentPlaying.png");
	private Pattern backToDownloadList= new Pattern("src/main/resources/Images/Watch Catchup Content/backToDownloadList.png");
	//delete downloaded content
	private Pattern DeleteDownloadedContent= new Pattern("src/main/resources/Images/delete catchup/DeleteDownloaded Content.png");
	private Pattern DeleteDownloadedContentConfirm= new Pattern("src/main/resources/Images/delete catchup/DeleteDownloaded Content-Confirm.png");
	//logging out
	private Pattern logout= new Pattern("src/main/resources/Images/logout/logout.png");
	private Pattern confirmLogout= new Pattern("src/main/resources/Images/logout/logout-confirm.png");
	//Settings
	private Pattern settings = new Pattern("src/main/resources/Images/SettingsPage/settings.png");
	private Pattern changeDownloadFolder = new Pattern("src/main/resources/Images/SettingsPage/ChangeDownloadFolder.png");
	private Pattern openLogs = new Pattern("src/main/resources/Images/SettingsPage/OpenLogs.png");
	private Pattern sortBoxOfficeToCatchUp = new Pattern("src/main/resources/Images/SettingsPage/sortBoxOfficeToCatchUp.png");
	private Pattern sortCatchUpToBoxOffice = new Pattern("src/main/resources/Images/SettingsPage/sortCatchUpToBoxOffice.png");
	private Pattern verifySettings = new Pattern("src/main/resources/Images/SettingsPage/VerifySettings.png");

	public Pattern getZeroBytes() {
		return ZeroBytes;
	}

	public void setZeroBytes(Pattern zeroBytes) {
		ZeroBytes = zeroBytes;
	}
	
	public Pattern getDeleteDownloadedContent() {
		return DeleteDownloadedContent;
	}

	public void setDeleteDownloadedContent(Pattern deleteDownloadedContent) {
		DeleteDownloadedContent = deleteDownloadedContent;
	}

	public Pattern getDeleteDownloadedContentConfirm() {
		return DeleteDownloadedContentConfirm;
	}

	public void setDeleteDownloadedContentConfirm(
			Pattern deleteDownloadedContentConfirm) {
		DeleteDownloadedContentConfirm = deleteDownloadedContentConfirm;
	}

	public Pattern getLogout() {
		return logout;
	}

	public void setLogout(Pattern logout) {
		this.logout = logout;
	}

	public Pattern getConfirmLogout() {
		return confirmLogout;
	}

	public void setConfirmLogout(Pattern confirmLogout) {
		this.confirmLogout = confirmLogout;
	}

	public Pattern getLogo() {
		return logo;
	}

	public void setLogo(Pattern logo) {
		this.logo = logo;
	}

	public Pattern getPauseDownload() {
		return PauseDownload;
	}

	public void setPauseDownload(Pattern pauseDownload) {
		PauseDownload = pauseDownload;
	}

	public Pattern getResumeDownload() {
		return ResumeDownload;
	}

	public void setResumeDownload(Pattern resumeDownload) {
		ResumeDownload = resumeDownload;
	}

	
	
	public Pattern getVerifyVideoWasPlayed() {
		return verifyVideoWasPlayed;
	}

	public void setVerifyVideoWasPlayed(Pattern verifyVideoWasPlayed) {
		this.verifyVideoWasPlayed = verifyVideoWasPlayed;
	}

	public Pattern getBackToDownloadList() {
		return backToDownloadList;
	}

	public void setBackToDownloadList(Pattern backToDownloadList) {
		this.backToDownloadList = backToDownloadList;
	}

	public Pattern getCatchUpContentPlaying() {
		return CatchUpContentPlaying;
	}

	public void setCatchUpContentPlaying(Pattern catchUpContentPlaying) {
		CatchUpContentPlaying = catchUpContentPlaying;
	}

	public Pattern getYourDawnLoadListIsEmpty() {
		return YourDawnLoadListIsEmpty;
	}

	public void setYourDawnLoadListIsEmpty(Pattern yourDawnLoadListIsEmpty) {
		YourDawnLoadListIsEmpty = yourDawnLoadListIsEmpty;
	}

	public Pattern getWhatchNow() {
		return whatchNow;
	}

	public void setWhatchNow(Pattern whatchNow) {
		this.whatchNow = whatchNow;
	}
	
	public Pattern getReadyToWatch() {
		return ReadyToWatch;
	}

	

	public void setReadyToWatch(Pattern readyToWatch) {
		ReadyToWatch = readyToWatch;
	}

	
	
	public Pattern getPauseDownloadBlack() {
		return PauseDownloadBlack;
	}

	public void setPauseDownloadBlack(Pattern pauseDownloadBlack) {
		PauseDownloadBlack = pauseDownloadBlack;
	}

	public Pattern getDownloadPopup() {
		return downloadPopup;
	}

	public void setDownloadPopup(Pattern downloadPopup) {
		this.downloadPopup = downloadPopup;
	}

	public Pattern getDownloadPopupOkButton() {
		return downloadPopupOkButton;
	}

	public void setDownloadPopupOkButton(Pattern downloadPopupOkButton) {
		this.downloadPopupOkButton = downloadPopupOkButton;
	}

	public Pattern getDownloadQueue() {
		return downloadQueue;
	}

	public void setDownloadQueue(Pattern downloadQueue) {
		this.downloadQueue = downloadQueue;
	}

	
	public Pattern getCatchupdownload() {
		return catchupdownload;
	}

	public void setCatchupdownload(Pattern catchupdownload) {
		this.catchupdownload = catchupdownload;
	}

	public Pattern getLoggedInAs() {
		return LoggedInAs;
	}

	public void setLoggedInAs(Pattern loggedInAs) {
		LoggedInAs = loggedInAs;
	}

	public Pattern getEnterPassword() {
		return EnterPassword;
	}

	public void setEnterPassword(Pattern enterPassword) {
		EnterPassword = enterPassword;
	}

	public Pattern getEnterUsername() {
		return EnterUsername;
	}

	public void setEnterUsername(Pattern enterUsername) {
		EnterUsername = enterUsername;
	}

	public Pattern getLogin() {
		return Login;
	}

	public void setLogin(Pattern login) {
		Login = login;
	}

	public Pattern getLoginWithYourConnectID() {
		return LoginWithYourConnectID;
	}

	public void setLoginWithYourConnectID(Pattern loginWithYourConnectID) {
		LoginWithYourConnectID = loginWithYourConnectID;
	}

	public Pattern getUntickKeepMeLoggedIn() {
		return UntickKeepMeLoggedIn;
	}

	public void setUntickKeepMeLoggedIn(Pattern untickKeepMeLoggedIn) {
		UntickKeepMeLoggedIn = untickKeepMeLoggedIn;
	}

	public Pattern getLoginPopup() {
		return LoginPopup;
	}

	public void setLoginPopup(Pattern loginPopup) {
		LoginPopup = loginPopup;
	}

	public Pattern getSettings() {
		return settings;
	}

	public void setSettings(Pattern settings) {
		this.settings = settings;
	}

	public Pattern getChangeDownloadFolder() {
		return changeDownloadFolder;
	}

	public void setChangeDownloadFolder(Pattern changeDownloadFolder) {
		this.changeDownloadFolder = changeDownloadFolder;
	}

	public Pattern getOpenLogs() {
		return openLogs;
	}

	public void setOpenLogs(Pattern openLogs) {
		this.openLogs = openLogs;
	}

	public Pattern getSortBoxOfficeToCatchUp() {
		return sortBoxOfficeToCatchUp;
	}

	public void setSortBoxOfficeToCatchUp(Pattern sortBoxOfficeToCatchUp) {
		this.sortBoxOfficeToCatchUp = sortBoxOfficeToCatchUp;
	}

	public Pattern getSortCatchUpToBoxOffice() {
		return sortCatchUpToBoxOffice;
	}

	public void setSortCatchUpToBoxOffice(Pattern sortCatchUpToBoxOffice) {
		this.sortCatchUpToBoxOffice = sortCatchUpToBoxOffice;
	}

	public Pattern getVerifySettings() {
		return verifySettings;
	}

	public void setVerifySettings(Pattern verifySettings) {
		this.verifySettings = verifySettings;
	}
}
