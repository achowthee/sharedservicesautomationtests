package com.dstvo.test.automation.DesktopPlayer;

import org.testng.annotations.Test;

public class SettingsPage extends TestBase {

	
		@Test//(dependsOnMethods = {"desktopPlayerLogin"})
		public void sortDownloadsBoxOfficeToCatchUp() throws Exception{
			desktopPlayer.click(image.getSettings());
			if(desktopPlayer.exists(image.getVerifySettings()) != null){
				desktopPlayer.click(image.getSortBoxOfficeToCatchUp());
			}
			desktopPlayer.click(image.getBackToDownloadList());
		}
		
		@Test//(dependsOnMethods = {"desktopPlayerLogin"})
		public void sortDownloadsCatchUpToBoxOffice() throws Exception{
			desktopPlayer.click(image.getSettings());
			if(desktopPlayer.exists(image.getVerifySettings()) != null){
				desktopPlayer.click(image.getSortCatchUpToBoxOffice());
			}
			desktopPlayer.click(image.getBackToDownloadList());
		}
}
