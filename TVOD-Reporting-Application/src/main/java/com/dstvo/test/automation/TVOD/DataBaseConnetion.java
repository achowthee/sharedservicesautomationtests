package com.dstvo.test.automation.TVOD;

import java.sql.*;

public class DataBaseConnetion {
	 // JDBC driver name and database URL
	 static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	 static final String DBURL = "jdbc:mysql://10.40.4.170:3306/BoxOffice";

	 //  Database credentials
	 static final String USER = "boxoffice";
	 static final String PASS = "boxoffice";
	 
	 public int getNumberOfCallsFromDB(String sqlQuery) {
	 Connection conn = null;
	 Statement stmt = null;
	 int numberOfCalls = 0;
	 try{
	    //Register JDBC driver
	    Class.forName("com.mysql.jdbc.Driver");
	    //Open a connection
	    System.out.println("Connecting to database...");
	    conn = DriverManager.getConnection(DBURL,USER,PASS);
	    //Execute a query
	    stmt = conn.createStatement();
	    ResultSet rs = stmt.executeQuery(sqlQuery);
	    while(rs.next()){
	    	numberOfCalls = rs.getInt("numberOfCalls");
	    }
	    //Clean-up environment
	    rs.close();
	    stmt.close();
	    conn.close();
	 }catch(SQLException se){
	    //Handle errors for JDBC
	    se.printStackTrace();
	 }catch(Exception e){
	    //Handle errors for Class.forName
	    e.printStackTrace();
	 }finally{
	    //finally block used to close resources
	    try{
	       if(stmt!=null)
	          stmt.close();
	    }catch(SQLException se2){
	    }// nothing we can do
	    try{
	       if(conn!=null)
	          conn.close();
	    }catch(SQLException se){
	       se.printStackTrace();
	    }
	 }
	 System.out.println("Goodbye!");
	return numberOfCalls;
	}
}

