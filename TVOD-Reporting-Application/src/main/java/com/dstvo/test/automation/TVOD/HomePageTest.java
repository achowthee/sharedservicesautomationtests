package com.dstvo.test.automation.TVOD;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class HomePageTest  extends TestBase {

	
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}

	@Test
	public void programHomePage()throws Exception {
		ReusableFunc.VerifyTextOnPage(driver, "Box Office: TVOD Reporting Application");
		//TBC
	}
	
}

