package com.dstvo.test.automation.TVOD;

import org.sikuli.script.Pattern;

public class ImagePatterns {
	//logging in
	private Pattern profileManagerBar = new Pattern("src/main/resources/Images/ProfileManager.png");
	private Pattern BoxOfficeBar = new Pattern("src/main/resources/Images/BoxOffice.png");
	private Pattern updateProfileRequestBar = new Pattern("src/main/resources/Images/UpdateProfileRequest.png");
	private Pattern getProfileByMSISDNRequestBar = new Pattern("src/main/resources/Images/GetProfileByMSISDNRequest.png");
	private Pattern GetProfileByCustomerNumberRequestBar = new Pattern("src/main/resources/Images/GetProfileByCustomerNumberRequest.png");
	private Pattern supportRentalRequestBar = new Pattern("src/main/resources/Images/SupportRentalRequest.png");
	private Pattern getCountryRequestBar = new Pattern("src/main/resources/Images/GetCountryRequest.png");
	
	
	public Pattern getGetCountryRequestBar() {
		return getCountryRequestBar;
	}

	public void setGetCountryRequestBar(Pattern getCountryRequestBar) {
		this.getCountryRequestBar = getCountryRequestBar;
	}

	public Pattern getSupportRentalRequestBar() {
		return supportRentalRequestBar;
	}

	public void setSupportRentalRequestBar(Pattern supportRentalRequestBar) {
		this.supportRentalRequestBar = supportRentalRequestBar;
	}

	public Pattern getGetProfileByCustomerNumberRequestBar() {
		return GetProfileByCustomerNumberRequestBar;
	}

	public void setGetProfileByCustomerNumberRequestBar(
			Pattern getProfileByCustomerNumberRequestBar) {
		GetProfileByCustomerNumberRequestBar = getProfileByCustomerNumberRequestBar;
	}

	public Pattern getGetProfileByMSISDNRequestBar() {
		return getProfileByMSISDNRequestBar;
	}

	public void setGetProfileByMSISDNRequestBar(Pattern getProfileByMSISDNRequest) {
		this.getProfileByMSISDNRequestBar = getProfileByMSISDNRequest;
	}

	public Pattern getUpdateProfileRequestBar () {
		return updateProfileRequestBar ;
	}
	public void setUpdateProfileRequestBar(Pattern updateProfileRequest) {
		this.updateProfileRequestBar  = updateProfileRequest;
	}

	public Pattern getBoxOfficeBar() {
		return BoxOfficeBar;
	}

	public void setBoxOfficeBar(Pattern boxOfficeBar) {
		BoxOfficeBar = boxOfficeBar;
	}

	public Pattern getProfileManagerBar() {
		return profileManagerBar;
	}

	public void setProfileManagerBar(Pattern profileManagerBar) {
		this.profileManagerBar = profileManagerBar;
	}

	
	
}
