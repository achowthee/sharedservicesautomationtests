package com.dstvo.test.automation.TVOD;

import org.openqa.selenium.By;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class NumberOfCallsPerMethodPerService_BoxOffice extends TestBase {

	
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}
	
	@Test
	public void numberOfCallsPerServicePerMethodHome() throws Exception{
		ReusableFunc.ClickByID(driver, "menuForm:reportsMethodsMenuLink", "numberOfCallsPerMethodPerService");
		ReusableFunc.ClickDropdownElement(driver, By.id("j_idt19:serviceName_label"), By.xpath(".//*[@id='j_idt19:serviceName_panel']/div/ul/li[3]"));
		ReusableFunc.ClickByID(driver, "j_idt19:j_idt25", "Submit Button");
//		ReusableFunc.VerifyTextOnPage(driver, "SupportRentalReques");
//		ReusableFunc.VerifyTextOnPage(driver, "GetCountryRequest");
	}
	@Test(dependsOnMethods={"numberOfCallsPerServicePerMethodHome"})
	public void numberOfCallsPerServicePerMethod_BoxOffice_SupportRentalRequest()throws Exception {
		int numberOfCallsFromDB = getNumberOfCallsPerOperatonFromDB("SupportRentalRequest");
		int numberOfCalls = getNumberOfCallsPerOperatonFromGraph(image.getSupportRentalRequestBar());
		org.testng.Assert.assertEquals(numberOfCallsFromDB,  numberOfCalls);	
	}
	
	@Test(dependsOnMethods={"numberOfCallsPerServicePerMethodHome"})
	public void numberOfCallsPerServicePerMethod_BoxOffice_getCountryRequest()throws Exception {
		int numberOfCallsFromDB = getNumberOfCallsPerOperatonFromDB("GetCountryRequest");
		int numberOfCalls = getNumberOfCallsPerOperatonFromGraph(image.getGetCountryRequestBar());
		org.testng.Assert.assertEquals(numberOfCallsFromDB,  numberOfCalls);	
	}
	
	private int getNumberOfCallsPerOperatonFromDB(String OperationName){
		String sqlQuery = "select count(*) as numberOfCalls from BoxOffice.Conversation where serviceName = \'BoxOffice\'and operation = \'" + OperationName + "\'" ;
		int numberOfCallsFromDB = dbConn.getNumberOfCallsFromDB(sqlQuery);
		System.out.println("numberOfCallsFromDB: " + numberOfCallsFromDB);
		return numberOfCallsFromDB; 
	}
	
	private int getNumberOfCallsPerOperatonFromGraph(Pattern graphImage) throws FindFailed {
		GraphBars.click(graphImage); 
		String numberOfCallsStr = ReusableFunc.getTextFromWebElement(driver, By.className("jqplot-highlighter-tooltip"));
		System.out.println("NumberOfCalls: " + numberOfCallsStr);
		String[] numberOfCallsArray = numberOfCallsStr.split(", ");
		int numberOfCalls = Integer.parseInt(numberOfCallsArray[0]);
		return numberOfCalls;
	}
	
	
}
