package com.dstvo.test.automation.TVOD;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class NumberOfCallsPerService extends TestBase {

	
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}

	@Test(priority = -10)
	public void numberOfCallsPerService_ProfileManager()throws Exception {
		String sqlQuery = "select count(*) as numberOfCalls from BoxOffice.Conversation where serviceName = \'ProfileManager\'";
		int numberOfCallsFromDB = dbConn.getNumberOfCallsFromDB(sqlQuery);
		System.out.println("numberOfCallsFromDB: " + numberOfCallsFromDB);
		
		ReusableFunc.ClickByID(driver, "menuForm:reportsServicesMenuLink", "numberOfCallsPerService");
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='j_idt19:j_idt21']/canvas[2]"), "Graph");
	
		GraphBars.click(image.getProfileManagerBar());
		String numberOfCallsStr = ReusableFunc.getTextFromWebElement(driver, By.xpath("//*[@id='j_idt19:j_idt21']/div[5]"));
		System.out.println("NumberOfCalls: " + numberOfCallsStr);
		String[] numberOfCallsArray = numberOfCallsStr.split(", ");
		int numberOfCalls = Integer.parseInt(numberOfCallsArray[1]);
		
		org.testng.Assert.assertEquals(numberOfCallsFromDB,  numberOfCalls);	
	}
	
	@Test(priority = -9)
	public void numberOfCallsPerService_BoxOffice()throws Exception {
		String sqlQuery = "select count(*) as numberOfCalls from BoxOffice.Conversation where serviceName = \'BoxOffice\'";
		int numberOfCallsFromDB = dbConn.getNumberOfCallsFromDB(sqlQuery);
		System.out.println("numberOfCallsFromDB: " + numberOfCallsFromDB);
		
		ReusableFunc.ClickByID(driver, "menuForm:reportsServicesMenuLink", "numberOfCallsPerService");
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='j_idt19:j_idt21']/canvas[2]"), "Graph");

		GraphBars.click(image.getBoxOfficeBar());
		String numberOfCallsStr = ReusableFunc.getTextFromWebElement(driver, By.xpath("//*[@id='j_idt19:j_idt21']/div[5]"));
		System.out.println("NumberOfCalls: " + numberOfCallsStr);
		String[] numberOfCallsArray = numberOfCallsStr.split(", ");
		int numberOfCalls = Integer.parseInt(numberOfCallsArray[1]);
		
		org.testng.Assert.assertEquals(numberOfCallsFromDB,  numberOfCalls);	
	}
	
	
}
