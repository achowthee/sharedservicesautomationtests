package com.dstvo.test.automation.amodaut;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class AmodConnectValidationsTest extends TestBase{
	
	
	@BeforeMethod
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}
	
	@Test
	public void validateEmailAddressRegister() throws Exception{
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form1']/nav/div/div[2]/div/div[1]/a[2]", "Connect Register link");
		Thread.sleep(2000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("main_ctl00_txtSignUpEmailAddress"), "asdf@asdf.com");
		ReusableFunc.ClickByID(driver, "txtSignUpPassword");
		ReusableFunc.clearTextBox(driver, By.id("main_ctl00_txtSignUpEmailAddress"));
		
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("main_ctl00_txtSignUpEmailAddress"), "me@");
		ReusableFunc.ClickByID(driver, "txtSignUpPassword");
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='main_ctl00_signupEmailAddressRegex']/span/div[2]"), "Please Enter valid email message");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form1']/nav/div/div[2]/div/div[1]/a[2]", "Connect Register link");
		Thread.sleep(2000);
		
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("main_ctl00_txtSignUpEmailAddress"), "@example.com");
		ReusableFunc.ClickByID(driver, "txtSignUpPassword");
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='main_ctl00_signupEmailAddressRegex']/span/div[2]"), "Please Enter valid email message");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form1']/nav/div/div[2]/div/div[1]/a[2]", "Connect Register link");
		Thread.sleep(2000);
		
		
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("main_ctl00_txtSignUpEmailAddress"), "@example.com");
		ReusableFunc.ClickByID(driver, "txtSignUpPassword");
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='main_ctl00_signupEmailAddressRegex']/span/div[2]"), "Please Enter valid email message");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form1']/nav/div/div[2]/div/div[1]/a[2]", "Connect Register link");
		Thread.sleep(2000);
		
//		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("main_ctl00_txtSignUpEmailAddress"), "me.@example.com");
//		ReusableFunc.ClickByID(driver, "txtSignUpPassword");
//		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='main_ctl00_signupEmailAddressRegex']/span/div[2]"), "Please Enter valid email message");
//		ReusableFunc.ClickByXpath(driver, ".//*[@id='form1']/nav/div/div[2]/div/div[1]/a[2]", "Connect Register link");
//		Thread.sleep(2000);
		
//		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("main_ctl00_txtSignUpEmailAddress"), ".me@example.com");
//		ReusableFunc.ClickByID(driver, "txtSignUpPassword");
//		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='main_ctl00_signupEmailAddressRegex']/span/div[2]"), "Please Enter valid email message");
//		ReusableFunc.ClickByXpath(driver, ".//*[@id='form1']/nav/div/div[2]/div/div[1]/a[2]", "Connect Register link");
//		Thread.sleep(2000);
		
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("main_ctl00_txtSignUpEmailAddress"), "me@example..com");
		ReusableFunc.ClickByID(driver, "txtSignUpPassword");
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='main_ctl00_signupEmailAddressRegex']/span/div[2]"), "Please Enter valid email message");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form1']/nav/div/div[2]/div/div[1]/a[2]", "Connect Register link");
		Thread.sleep(2000);
		
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("main_ctl00_txtSignUpEmailAddress"), "me\\@example.com");
		ReusableFunc.ClickByID(driver, "txtSignUpPassword");
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='main_ctl00_signupEmailAddressRegex']/span/div[2]"), "Please Enter valid email message");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form1']/nav/div/div[2]/div/div[1]/a[2]", "Connect Register link");
		Thread.sleep(2000);
		

	}
	
	

}
