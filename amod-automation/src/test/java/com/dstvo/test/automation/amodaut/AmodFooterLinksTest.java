package com.dstvo.test.automation.amodaut;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class AmodFooterLinksTest extends TestBase {
	
	@BeforeMethod
		@Override
	    public  void before() throws Exception {
	        super.before();        
	        getWebDriver().get(getSiteURL());
	       
	    } 
	@Test
    public void canAccessMultichoiceWebsiteFooter() throws Exception {
		
		String parentHandle = driver.getWindowHandle();
    	
     	ReusableFunc.ClickByXpath(driver, ".//*[@id='form1']/footer/div/div/div/div[4]/div[2]/a[1]", "MultiChoice Website");
     	
     	
     	for (String popUpHandle : driver.getWindowHandles()) {
      	  if(!popUpHandle.equals(parentHandle)){
      	    driver.switchTo().window(popUpHandle);
      	    if(driver.getCurrentUrl().equalsIgnoreCase("http://www.multichoice.co.za/multichoice/view/multichoice/en/page44115")){
       	    		 
      	    	ReusableFunc.VerifyPageTitle(driver, "Multichoice | Home |");   
      	  
      	    }
      	  }
      	}
     
      	driver.switchTo().window(parentHandle);
     	
      }
      
      
	@Test
    public void canAccessMultichoiceWebsiteviaLogoFooter() throws Exception {
		//click MultiChoice Footer logo
    	ReusableFunc.ClickByXpath(driver, ".//*[@id='form1']/footer/div/div/div/div[4]/div[1]/a/img", "MultiChoice");
    	Thread.sleep(2000);	   
    	
    	    	
    	String parentHandle = driver.getWindowHandle();
    	
    	for (String popUpHandle : driver.getWindowHandles()) {
        	  if(!popUpHandle.equals(parentHandle)){
        	    driver.switchTo().window(popUpHandle);
        	    if(driver.getCurrentUrl().equalsIgnoreCase("http://www.multichoice.co.za/multichoice/view/multichoice/en/page44115")){
         	    		 
        	    	ReusableFunc.VerifyPageTitle(driver, "Multichoice | Home |");   
        	  
        	    }
        	  }
        	}
       
        	driver.switchTo().window(parentHandle);
       	
        }
	
	
	@Test
	public void canAccessTermsAndConditions() throws Exception{
		
		ReusableFunc.ClickByLinkText(driver, "Terms & Conditions");
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, "These Terms and Conditions are the general");
		ReusableFunc.VerifyTextOnPage(driver, "By accessing the Website");
		
	}
	
	
	@Test
	public void canAccessPrivacyPolicy() throws Exception{
		
		ReusableFunc.ClickByLinkText(driver, "Privacy Policy");
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, "Your privacy is important to us");
		ReusableFunc.VerifyTextOnPage(driver, "This privacy policy");
	}
	

	
	
	
	
	
    		    
}