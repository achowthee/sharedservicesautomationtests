package com.dstvo.test.automation.amodaut;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class AmodHomePageTest extends TestBase {

	@BeforeMethod
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}

	
	@Test
    public void canAccessHomePageViaLogo() throws Exception {
    	
    	//click Amod logo
    	ReusableFunc.ClickByXpath(driver, ".//*[@id='form1']/nav/div/div[1]/a[3]/img", "Amod logo");
    	Thread.sleep(2000);	   
    	
    	ReusableFunc.VerifyTextOnPage(driver, "Hottest");
    	ReusableFunc.ContainsPageTitle(driver, "AMOD");
    	
  	    
	} 
}
