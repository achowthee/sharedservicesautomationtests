package com.dstvo.test.automation.amodaut;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.dstvo.test.automation.dstvfunctionlib.*;



public class AmodSiteMapTest extends TestBase {


    @BeforeMethod
	@Override
    public  void before() throws Exception {
        super.before();        
        getWebDriver().get(getSiteURL());
       
    } 
    

    @Test
    public void canAccessMoviesViaSiteMap() throws Exception {
    	
     	ReusableFunc.ClickByXpath(driver, ".//*[@id='form1']/footer/div/div/div/div[1]/ul/li[1]/a", "Movies");//click Movies	
     	Thread.sleep(2000);
    	
    	ReusableFunc.VerifyTextOnPage(driver, "Genre");
    	ReusableFunc.VerifyTextOnPage(driver, "Language");
    	ReusableFunc.ContainsPageTitle(driver, "AMOD - Browse Movies");
    		
    }
    
   
    
    
    @Test
    public void canAccessSeriesViaSiteMap() throws Exception {
    	
     	ReusableFunc.ClickByXpath(driver, ".//*[@id='form1']/footer/div/div/div/div[1]/ul/li[2]/a", "Series");//click Series	
     	Thread.sleep(2000);
    	
    	ReusableFunc.VerifyTextOnPage(driver, "Genre");
    	ReusableFunc.VerifyTextOnPage(driver, "Action");
    	ReusableFunc.ContainsPageTitle(driver, "AMOD - Browse Series");
    	
	
    }
    
   
  
    

     
}
