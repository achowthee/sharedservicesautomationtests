package com.dstvo.test.automation.bbaut;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

@Test
  public class  HomePageTest extends TestBase{

	    @BeforeMethod
		@Override
	    public  void before() throws Exception {
	        super.before();       
	       getWebDriver().get(getSiteURL());
	    
	       
	       
	    } 
	    
	    
	    @Test
	    public void canAccessHomePageViaLogo() throws Exception {
	    	
	    	//click bbm logo
	    	ReusableFunc.ClickByXpath(driver, ".//*[@id='headContainer']/div[2]/div/div[1]/a/img", "bbmlogo");
	    	Thread.sleep(4000);	   
	    	String expectedurl = "http://aut.bigbrothermzansi.dstv.com/";
	  	    String actualurl = driver.getCurrentUrl();
	  	    AssertJUnit.assertEquals(actualurl, expectedurl);
	    	
	    		    } 
	    

	    @Test
	    public void canAccessHighlightsViaSiteMap() throws Exception {
	    
	    	String parentHandle = driver.getWindowHandle();
	    	   	
	   	
	    	//click Highlights
	    	WebElement ele = driver.findElement(By.linkText("Highlights"));
	    	Thread.sleep(3000);
	    	ele.click();
	    	Thread.sleep(3000);
	    	
	    	 //after you have pop ups
	    	for (String popUpHandle : driver.getWindowHandles()) {
	    	  if(!popUpHandle.equals(parentHandle)){
	    	    driver.switchTo().window(popUpHandle);
	    	    if(driver.getCurrentUrl().equalsIgnoreCase("http://www.dstv.com/Highlights/")){
	    	    	ReusableFunc.VerifyTextOnPage(driver, "General Entertainment"); 	    	 
	    	
	    	    }
	    	  }
	    	}
		
	    }
	    
	    @Test
	    public void canAccessBBMMobiViaSiteMap() throws Exception {
	    
	    	String parentHandle = driver.getWindowHandle();
	    	   	
	   	
	    	//click Highlights
	    	WebElement ele = driver.findElement(By.linkText("BBM Mobile"));
	    	Thread.sleep(3000);
	    	ele.click();
	    	Thread.sleep(3000);
	    	
	    	 //after you have pop ups
	    	for (String popUpHandle : driver.getWindowHandles()) {
	    	  if(!popUpHandle.equals(parentHandle)){
	    	    driver.switchTo().window(popUpHandle);
	    	    if(driver.getCurrentUrl().equalsIgnoreCase("http://bigbrothermzansiwap.dstv.com/")){
	    	    	System.out.println("It's the mobisite!");	 
	    	
	    	    }
	    	  }
	    	}
		
	    }
	    
	    
	    @Test
	    public void canAccessFacebookViaSiteMap() throws Exception{
	    	
	    	//before any pop ups are open
	    	String parentHandle = driver.getWindowHandle();
	    	
	    	//click on Facebook 
	    	ReusableFunc.ClickByXpath(driver, ".//*[@id='headContainer']/div[1]/div[3]/a[1]", "Facebook");
	      	Thread.sleep(2000);


	    	//after you have pop ups
	    	for (String popUpHandle : driver.getWindowHandles()) {
	    	  if(!popUpHandle.equals(parentHandle)){
	    	    driver.switchTo().window(popUpHandle);
	    	    if(driver.getCurrentUrl().equalsIgnoreCase("https://www.facebook.com/BBmzansi")){
	    	    	ReusableFunc.VerifyTextOnPage(driver, "likes"); 	    	 
	    	    	ReusableFunc.VerifyPageTitle(driver, "Big Brother Mzansi | Facebook");   
	    	  
	    	    }
	    	  }
	    	}
	   
	    	driver.switchTo().window(parentHandle);
	   	
	    }
	    
	    

	     
	}

