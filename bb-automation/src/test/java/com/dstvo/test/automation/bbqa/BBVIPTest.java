package com.dstvo.test.automation.bbqa;

import org.openqa.selenium.WebElement;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class BBVIPTest extends TestBase {
	

	@BeforeMethod
	@Override
    public  void before() throws Exception {
        super.before();        
        getWebDriver().get(getSiteURL());
                
	    } 
	
	
	@Test
	public void canAccessVIPPageViaMenu() throws Exception {
		    	
			ReusableFunc.ClickByLinkText(driver, "VIP");   
			ReusableFunc.ContainsPageTitle(driver, "BBM VIP | Big Brother Mzansi: Secrets");
	        String expectedurl = "http://qa-bigbrothermzansi.dstv.com/VIP/About.aspx";
		  	String actualurl = driver.getCurrentUrl();
		  	org.testng.Assert.assertEquals(actualurl, expectedurl);
		  	ReusableFunc.VerifyTextOnPage(driver, "Backstage & Bye Bye");
			ReusableFunc.VerifyTextOnPage(driver, "Rant & Rave");
			ReusableFunc.VerifyTextOnPage(driver, "Uncut");
			
	  }
	
	@Test
	public void becomeAVipNotLoggedIn() throws Exception {
		ReusableFunc.ClickByLinkText(driver, "VIP");  
		ReusableFunc.ClickByXpath(driver, ".//*[@id='headContainer']/div[5]/div/a[1]", "vip");
		ConnectControl.connectLogin(driver,"bnplou@gmail.com","123456"); 
		 String expectedurl = "http://qa-bigbrothermzansi.dstv.com/VIP/PurchaseInfo.aspx";
		 String actualurl = driver.getCurrentUrl();
		 org.testng.Assert.assertEquals(actualurl, expectedurl);
		 ReusableFunc.VerifyTextOnPage(driver, "Confirm Subscription");
		 ReusableFunc.ClickByXpath(driver, " .//*[@id='ctl00_BodyWrapper_ImageButton1']", "continue");
		
		
		
		
		
		
		
		
	}
	
		
	
}