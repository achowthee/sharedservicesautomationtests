package com.dstvo.test.automation.bbqa;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;


public class BBVotePageTest extends TestBase {
	
	 @BeforeMethod
		@Override
	    public  void before() throws Exception {
	        super.before();        
	        getWebDriver().get(getSiteURL());
	       
	    } 

	@Test
  
  public void canAccessVotePageViaMenu() throws Exception {
	    	
		ReusableFunc.ClickByLinkText(driver, "Vote & Win");   
		ReusableFunc.ContainsPageTitle(driver, "Vote and Win | Big Brother Mzansi: Secrets");
        String expectedurl = "http://qa-bigbrothermzansi.dstv.com/vote/";
	  	String actualurl = driver.getCurrentUrl();
	  	AssertJUnit.assertEquals(actualurl, expectedurl);
	  	Thread.sleep(3000);
		ReusableFunc.VerifyTextOnPage(driver, "Cast your vote");
		ReusableFunc.VerifyTextOnPage(driver, "SMS");
		ReusableFunc.VerifyTextOnPage(driver, "Prizes");
		
  }
	
	/* @Test
	    public void clickFacebookRecommend() throws Exception{
		 ReusableFunc.ClickByLinkText(driver, "Vote & Win");   
		 Thread.sleep(3000);
	    	//before any pop ups are open
	    	String parentHandle = driver.getWindowHandle();
	    	
	    	//click on Facebook 
	    	ReusableFunc.ClickByXpath(driver, ".//*[@id='u_0_1']/div/div[1]/div/div/div/div/button","submit");

	    	//after you have pop ups
	    	for (String popUpHandle : driver.getWindowHandles()) {
	    	  if(!popUpHandle.equals(parentHandle)){
	    	    driver.switchTo().window(popUpHandle);
	    	   }
	    	}
	   
	    	driver.switchTo().window(parentHandle);
	   	
	    }*/
	 
	//Vote 
	 @Test
	
	 public void voteAlreadyLoggedIn() throws Exception{
	 ConnectControl.connectLogin(driver,"bnplou@gmail.com","123456"); 
	 ReusableFunc.ClickByLinkText(driver, "Vote & Win"); 
	 Thread.sleep(5000);
	 ReusableFunc.ClickByXpath(driver, ".//*[@id='contestantContainer']/ul/li[1]/a/div","Vote");
	 Thread.sleep(3000);
	 ReusableFunc.ClickByXpath(driver, "./html/body/form/div[5]/div[6]/div/div/div/div/div/div/div[4]/div/div[3]/a/img", "voteYes");
	 ReusableFunc.VerifyTextOnPage(driver, "Thank you for voting for");
	 
	 }

	 @Test
	 public void voteNotLoggedIn() throws Exception{
		 ReusableFunc.ClickByLinkText(driver, "Vote & Win"); 
		 Thread.sleep(5000);
		 ReusableFunc.ClickByXpath(driver, ".//*[@id='contestantContainer']/ul/li[1]/a/div","Vote");
		 ConnectControl.connectLogin(driver,"bnptonylou@yahoo.com","123456"); 
		 Thread.sleep(5000);
		 ReusableFunc.ClickByXpath(driver, ".//*[@id='contestantContainer']/ul/li[2]/a/div","Vote");
		 ReusableFunc.ClickByXpath(driver, "./html/body/form/div[5]/div[6]/div/div/div/div/div/div/div[4]/div/div[3]/a/img", "voteYes");
		 ReusableFunc.VerifyTextOnPage(driver, "Thank you for voting for"); 
		 
	 }
	 
}

