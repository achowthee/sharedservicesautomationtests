package com.dstvo.test.automation.boAngola;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class EnglishHomePageTest extends TestBase{
	
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}
	
	@Test
	public void HomeClickPortuguese() throws Exception {
		ReusableFunc.VerifyPageTitle(driver, "BoxOffice");
		ReusableFunc.verifyElementIsDisplayed(driver, By.cssSelector(".backNavigation>div"), "language div");
		ReusableFunc.ClickByLinkText(driver, "Português");
		//ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div[2]/div[1]/div[1]/a[2]", "Português");
		ReusableFunc.VerifyTextOnPage(driver, "Registo");
		ReusableFunc.ClickByLinkText(driver, "English");
		//ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div[2]/div[1]/div[1]/a[1]", "English");
		ReusableFunc.VerifyTextOnPage(driver, "Registration");
		
		
		
	}
	
	@Test(dependsOnMethods={"HomeClickPortuguese"})
	public void ClickRegisterEnglish() throws Exception{
		driver.get("http://10.10.14.68:8080//tvod-web/wap");
		ReusableFunc.ClickByLinkText(driver, "Registration");
		ReusableFunc.VerifyPageTitle(driver, "BoxOffice Registration");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "submit");
		ReusableFunc.VerifyTextOnPage(driver, "screen");
		ReusableFunc.VerifyTextOnPage(driver, "re-enter");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("smartCard"), "1234567890");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("msisdn"), "244741340432");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "submit");
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, "We are processing your Registration Request");

	}
	
	@Test(dependsOnMethods={"ClickRegisterEnglish"})
	public void ClickRentalEnglish() throws Exception{
		driver.get("http://10.10.14.68:8080//tvod-web/wap");
		ReusableFunc.ClickByLinkText(driver, "Rental");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='rentalForm']/div[7]/div/input", "submit");
		ReusableFunc.VerifyTextOnPage(driver, "18 digit");
		ReusableFunc.VerifyTextOnPage(driver, "re-enter");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("rentalCode"), "454567674567456723");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("msisdn"), "244741340432");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='rentalForm']/div[7]/div/input", "submit");
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, "We are processing your Rental Request");
			
	}
	
	@Test(dependsOnMethods={"ClickRentalEnglish"})
	public void InvalidSmartCard() throws Exception{
		driver.get("http://10.10.14.68:8080//tvod-web/wap");
		ReusableFunc.ClickByLinkText(driver, "Registration");
		ReusableFunc.VerifyPageTitle(driver, "BoxOffice Registration");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "submit");
		ReusableFunc.VerifyTextOnPage(driver, "screen");
		ReusableFunc.VerifyTextOnPage(driver, "re-enter");
		//1digit
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("smartCard"), "1");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("msisdn"), "244741340432");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "submit");
		ReusableFunc.VerifyTextOnPage(driver, "screen");
		
		//12digits
		ReusableFunc.clearTextBox(driver, By.id("smartCard"));
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("smartCard"), "211234567890");
		ReusableFunc.ClickByXpath(driver, "//*[@id='registrationForm']/div[7]/div/input", "submit");
		ReusableFunc.VerifyTextOnPage(driver, "displayed");
			
		//valid 11 digits
		ReusableFunc.clearTextBox(driver, By.id("smartCard"));
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("smartCard"), "12345678901");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "submit");
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, "We are processing your Registration Request");
		
	
	}
	
	@Test(dependsOnMethods={"ClickRentalEnglish"})
	public void InvalidMobileNum() throws Exception{
		driver.get("http://10.10.14.68:8080//tvod-web/wap");
		ReusableFunc.ClickByLinkText(driver, "Registration");
		ReusableFunc.VerifyPageTitle(driver, "BoxOffice Registration");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "submit");
		ReusableFunc.VerifyTextOnPage(driver, "screen");
		ReusableFunc.VerifyTextOnPage(driver, "re-enter");
		//1digit
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("smartCard"), "1234567890");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("msisdn"), "1");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "submit");
		ReusableFunc.VerifyTextOnPage(driver, "re-enter");
		
		//11digits
		/*ReusableFunc.clearTextBox(driver, By.id("msisdn"));
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("msisdn"), "24474134043");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "submit");
		ReusableFunc.VerifyTextOnPage(driver, "re-enter");*/
		
		//13digits
		/*ReusableFunc.clearTextBox(driver, By.id("msisdn"));
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("msisdn"), "2447413404322");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "submit");
		ReusableFunc.VerifyTextOnPage(driver, "We are processing your Registration Request");*/
		
		
		
		//19digits
		ReusableFunc.clearTextBox(driver, By.id("msisdn"));
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("msisdn"), "2447413404324345619");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "submit");
		ReusableFunc.VerifyTextOnPage(driver, "re-enter");
		
		//characters
		ReusableFunc.clearTextBox(driver, By.id("msisdn"));
	    ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("msisdn"), "+24474134043");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "submit");
		ReusableFunc.VerifyTextOnPage(driver, "re-enter");
			
		//valid 
		ReusableFunc.clearTextBox(driver, By.id("msisdn"));
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("msisdn"), "00244741340432");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "submit");
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, "We are processing your Registration Request");
		
	
	}
	
	
	
}