package com.dstvo.test.automation.boAngola;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class PortuegueseHomePageTest extends TestBase{
	
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}
	
	@Test
	public void HomeClickPortuguese() throws Exception {
		ReusableFunc.VerifyPageTitle(driver, "BoxOffice");
		ReusableFunc.verifyElementIsDisplayed(driver, By.cssSelector(".backNavigation>div"), "language div");
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div[2]/div[1]/div[1]/a[2]", "Português");
		ReusableFunc.VerifyTextOnPage(driver, "Registo");
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div[2]/div[1]/div[1]/a[1]", "English");
		ReusableFunc.VerifyTextOnPage(driver, "Registration");
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div[2]/div[1]/div[1]/a[2]", "Português");
		ReusableFunc.VerifyTextOnPage(driver, "Registo");
		
		
	}
	
	@Test(dependsOnMethods={"HomeClickPortuguese"})
	public void ClickRegisterPortuguese() throws Exception{
		//driver.get("http://bo.dstv.com");
		ReusableFunc.ClickByLinkText(driver, "Registo");
		ReusableFunc.VerifyPageTitle(driver, "Registo BoxOffice");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "Submeter");
		ReusableFunc.VerifyTextOnPage(driver, "exibido");
		ReusableFunc.VerifyTextOnPage(driver, "válido");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("smartCard"), "1234567890");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("msisdn"), "244741340432");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "submit");
		ReusableFunc.VerifyTextOnPage(driver, "Estamos a processar o seu registo");
		Thread.sleep(2000);
		ReusableFunc.ClickByLinkText(driver, "Novo aluguer");

	}
	
	@Test(dependsOnMethods={"ClickRegisterPortuguese"})
	public void ClickRentalPotuguese() throws Exception{
		driver.get("http://10.10.14.68:8080//tvod-web/wap");
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div[2]/div[1]/div[1]/a[2]", "Português");
		ReusableFunc.ClickByLinkText(driver, "Aluguer");
		ReusableFunc.VerifyPageTitle(driver, "Aluguer de BoxOffice");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='rentalForm']/div[7]/div/input", "Submeter");
		ReusableFunc.VerifyTextOnPage(driver, "18 dígitos");
		ReusableFunc.VerifyTextOnPage(driver, "novamente");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("rentalCode"), "454567674567456723");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("msisdn"), "244741340432");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='rentalForm']/div[7]/div/input", "Submeter");
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, "Estamos a processar o seu pedido de aluguer");
			
	}
	
	@Test(dependsOnMethods={"ClickRentalPotuguese"})
	public void InvalidSmartCardPot() throws Exception{
		driver.get("http://10.10.14.68:8080//tvod-web/wap");
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div[2]/div[1]/div[1]/a[2]", "Português");
		ReusableFunc.ClickByLinkText(driver, "Registo");
		ReusableFunc.VerifyPageTitle(driver, "Registo BoxOffice");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "Submeter");
		ReusableFunc.VerifyTextOnPage(driver, "exibido");
		ReusableFunc.VerifyTextOnPage(driver, "válido");
		//1digit
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("smartCard"), "1");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("msisdn"), "244741340432");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "Submeter");
		ReusableFunc.VerifyTextOnPage(driver, "exibido");
		//12digits
		ReusableFunc.clearTextBox(driver, By.id("smartCard"));
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("smartCard"), "211234567890");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "Submeter");
		ReusableFunc.VerifyTextOnPage(driver, "exibido");
		//valid 
		ReusableFunc.clearTextBox(driver, By.id("smartCard"));
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("smartCard"), "12345678901");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "Submeter");
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, "Estamos a processar o seu registo");
		
	
	}
	
	@Test(dependsOnMethods={"ClickRentalPotuguese"})
	public void InvalidMobileNumReg() throws Exception{
		driver.get("http://10.10.14.68:8080//tvod-web/wap");
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div[2]/div[1]/div[1]/a[2]", "Português");
		ReusableFunc.ClickByLinkText(driver, "Registo");
		ReusableFunc.VerifyPageTitle(driver, "Registo BoxOffice");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "Submeter");
		ReusableFunc.VerifyTextOnPage(driver, "tela");
		ReusableFunc.VerifyTextOnPage(driver, "fornecido");
		//1digit
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("smartCard"), "1234567890");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("msisdn"), "1");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "Submeter");
		ReusableFunc.VerifyTextOnPage(driver, "novamente");
			
		//19digits
		ReusableFunc.clearTextBox(driver, By.id("msisdn"));
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("msisdn"), "2447413404324141519");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "Submeter");
		ReusableFunc.VerifyTextOnPage(driver, "fornecido");
		
		//characters
		ReusableFunc.clearTextBox(driver, By.id("msisdn"));
	    ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("msisdn"), "+24474134043");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "Submeter");
		ReusableFunc.VerifyTextOnPage(driver, "fornecido");		
		
		//characters 00
		/*ReusableFunc.clearTextBox(driver, By.id("msisdn"));
	    ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("msisdn"), "0024474134043");
	    ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "Submeter");
		ReusableFunc.VerifyTextOnPage(driver, "fornecido");		*/
		
		//valid 
		ReusableFunc.clearTextBox(driver, By.id("msisdn"));
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("msisdn"), "00244741340432");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='registrationForm']/div[7]/div/input", "Submeter");
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, "Estamos a processar o seu registo");
		
	
	}
	
	
	
	
}