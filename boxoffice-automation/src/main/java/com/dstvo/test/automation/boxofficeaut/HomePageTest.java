package com.dstvo.test.automation.boxofficeaut;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.dstvo.test.automation.dstvfunctionlib.*;


public class HomePageTest extends TestBase{
	
	public static String[] strBOMovieTitles = new String[21];
	
	public HomePageTest(){
		
	}
	
	@BeforeMethod
	@Override
	public void before() throws Exception{
		super.before();
		getWebDriver().get(getSiteURL());
	}
	

	/*
	
	//These tests are for a user who is not logged in.
	
	@Test
	public void pvrIconRedirectsToPvrBrowsePage() throws Exception{
	
		//Act
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form1']/div[6]/div[2]/a[2]/div[1]", "pvr icon");
		//Assert
		ReusableFunc.VerifyTextOnPage(driver, "Movies on PVR");
		ReusableFunc.ContainsPageTitle(driver, "Browse Movies");
	}
	
	@Test
	public void pcIconRedirectsToOnlineMoviesBrowsePage() throws Exception{
		//Act
		ReusableFunc.ClickByXpath(driver, ".//div[@class=\"type pc\"]", "pc icon");
		//Assert
		ReusableFunc.VerifyTextOnPage(driver, "Movies Now");
		ReusableFunc.ContainsPageTitle(driver, "Browse Movies");
	}
	
	@Test
	public void seeMoreLinkRedirectsToBrowsePage() throws Exception{
		//Act
		ReusableFunc.ClickByLinkText(driver, "See more >>"); 
		//Assert
		ReusableFunc.VerifyTextOnPage(driver, "Movies Now");
		ReusableFunc.VerifyTextOnPage(driver, "Next");
		ReusableFunc.ContainsPageTitle(driver, "Browse Movies");
	}

	@Test
	public void verifyCarouselListandNextButton() throws Exception{
		
			
		int i=1;
		String[] strMovieTitles = new String[21];
		BoxOfficeFunction.setStrMovieTitle(driver);
		
		ReusableFunc.ClickByID(driver, "logo");
		Utilities.fluentWait(By.xpath(".//*[@id='main_wucTopPicksMovieTileList_divCategory']/div[1]/h1"), driver);
		
		int k=1;
		while(i<=5){
			
			WebElement hoverElement = driver.findElementByXPath("//div[@class='item active']//div[@class='span2 tileContainer']["+i+"]//a[@class='js-movietile-image']/img");
			WebElement nameElement = driver.findElementByXPath("//div[@class='jttip']["+k+"]//a[@class='js-movietile-hover-title']/h2");
		
			Actions builder = new Actions(driver);
			//builder.moveToElement(hoverElement).build().perform();
			builder.moveToElement(hoverElement).clickAndHold().perform();
			Thread.sleep(3000);

			strMovieTitles[k] = nameElement.getText();
			AssertJUnit.assertEquals("Movies not matching  :"+strBOMovieTitles[k]+ "---"+strMovieTitles[k], BoxOfficeFunction.getStrMovieTitle(k), strMovieTitles[k]);
				
			//ReusableFunc.ClickByID(driver, "logo");
			builder.moveToElement(driver.findElementById("logo")).clickAndHold().perform();
			Thread.sleep(3000);
			k++;
			
			if(i==5 && k<20){
				i=1;
			}
			else{
				i++;
			}
			
			if(k==6){
				ReusableFunc.ClickByXpath(driver, ".//*[@id='main_wucTopPicksMovieTileList_myCarousel']/a[2]", "Carousel Next Button");
				Thread.sleep(1000);
			}
			if(k==11){
				ReusableFunc.ClickByXpath(driver, ".//*[@id='main_wucTopPicksMovieTileList_myCarousel']/a[2]", "Carousel Next Button");
				Thread.sleep(1000);
			}
			if(k==16){
				ReusableFunc.ClickByXpath(driver, ".//*[@id='main_wucTopPicksMovieTileList_myCarousel']/a[2]", "Carousel Next Button");
				Thread.sleep(1000);
			}
			
			
		}
	

	
	}
	
	
	@Test
	public void canAccessWatchMovieNowFromCarousel() throws Exception{
	
		String[] strMovieTitles = new String[21];
		int k=1,i=1;
		
		while(i<=5){
			WebElement hoverElement = driver.findElementByXPath("//div[@class='item active']//div[@class='span2 tileContainer']["+i+"]//a[@class='js-movietile-image']/img");
														//       //            item active                span2 tileContainer
			WebElement nameElement = driver.findElementByXPath("//div[@class='jttip']["+k+"]//a[@class='js-movietile-hover-title']/h2");
		
			Actions builder = new Actions(driver);
			//builder.moveToElement(hoverElement).build().perform();
			builder.moveToElement(hoverElement).clickAndHold().perform();
			Thread.sleep(2000);
			strMovieTitles[k] = nameElement.getText();
			nameElement.click();
			Thread.sleep(3000);
			ReusableFunc.VerifyTextOnPage(driver, "Movies rented online can only be viewed on the BoxOffice website");
			driver.getPageSource().contains(strMovieTitles[k]);
			//ReusableFunc.VerifyTextOnPage(driver, strMovieTitles[k]);
			ReusableFunc.ContainsPageTitle(driver, strMovieTitles[k]);
			
			ReusableFunc.ClickByID(driver, "logo");
			Thread.sleep(3000);
			k++;
		
			if(i==5 && k<20){
				i=1;
			}
			else{
				i++;
			}
			
			if(k>5 && k<=20){
				ReusableFunc.ClickByXpath(driver, ".//*[@id='main_wucTopPicksMovieTileList_myCarousel']/a[2]", "Carousel Next Button");
				Thread.sleep(1000);
			}
			if(k>10 && k<=20){
				ReusableFunc.ClickByXpath(driver, ".//*[@id='main_wucTopPicksMovieTileList_myCarousel']/a[2]", "Carousel Next Button");
				Thread.sleep(1000);
			}
			if(k>15 && k<=20){
				ReusableFunc.ClickByXpath(driver, ".//*[@id='main_wucTopPicksMovieTileList_myCarousel']/a[2]", "Carousel Next Button");
				Thread.sleep(1000);
			}
		}
		
	}
	
*/

}

		

	

