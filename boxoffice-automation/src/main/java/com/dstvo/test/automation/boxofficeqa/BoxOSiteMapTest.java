package com.dstvo.test.automation.boxofficeqa;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.By;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class BoxOSiteMapTest extends TestBase{

	@BeforeMethod
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}

    
    
    @Test
    public void clickOnMoviesOnPVR() throws Exception{
    	
    	ReusableFunc.ClickByID(driver, "siteMap_HyperLink4");
    	Thread.sleep(2000);
    
    }
    
    
    
     
    
    @Test
    public void canAccessMoviesNowViaSiteMap() throws Exception{
        // Act
    	if(Utilities.isTextOnPage(driver, "Logged in as")){
    		ReusableFunc.ClickByLinkText(driver, "Logout");
    	   	Utilities.fluentWait(By.linkText("Login"), driver);
    	}
    	//Click Movies Now
    	ReusableFunc.ClickByLinkText(driver, "Movies Now");

        // Assert
        ReusableFunc.VerifyTextOnPage(driver, "Movies Now");
        
    }
    
    @Test
    public void canAccessMoviesOnPvrViaSiteMap() throws Exception{
        // Act
    	if(Utilities.isTextOnPage(driver, "Logged in as")){
    		ReusableFunc.ClickByLinkText(driver, "Logout");
    	   	Utilities.fluentWait(By.linkText("Login"), driver);
    	}
    	//Click Movies Now
    	ReusableFunc.ClickByLinkText(driver, "Movies on PVR");

        // Assert
        ReusableFunc.VerifyTextOnPage(driver, "Movies on PVR");
        	    	
    }
    
    @Test
    public void canAccessDesktopPlayerViaSiteMap() throws Exception{
    	//Act
    	if(Utilities.isTextOnPage(driver, "Logged in as")){
    		ReusableFunc.ClickByLinkText(driver, "Logout");
    	   	Utilities.fluentWait(By.linkText("Login"), driver);
    	}
    	//click Desktop Player
    	ReusableFunc.ClickByLinkText(driver, "Desktop player");
    	//Assert
    	ReusableFunc.VerifyTextOnPage(driver, "DStv Desktop Player");
    	ReusableFunc.VerifyTextOnPage(driver, "Entertainment whenever and wherever you want it");
    }
    
    @Test
    public void canAccessFAQsViaSiteMap() throws Exception{
    	//Act
    	if(Utilities.isTextOnPage(driver, "Logged in as")){
    		ReusableFunc.ClickByLinkText(driver, "Logout");
    	   	Utilities.fluentWait(By.linkText("Login"), driver);
    	}
    	//click
    	ReusableFunc.ClickByLinkText(driver, "FAQs");    	
    	//Assert
    	ReusableFunc.VerifyTextOnPage(driver, "Frequently Asked Questions");
    	ReusableFunc.VerifyTextOnPage(driver, "What is BoxOffice Online?");
    }
    
    @Test
    public void canAccessContactUsViaSiteMap() throws Exception{

    	//click
    	ReusableFunc.ClickByLinkText(driver, "Contact us");    	

    	//Assert
    	ReusableFunc.VerifyTextOnPage(driver, "Got a question, comment or complaint?");
    	ReusableFunc.VerifyTextOnPage(driver, "Fill in the form below");
    }
    
    @Test
    public void canAccessLikeFBViaSiteMap() throws Exception{
    	
    	ReusableFunc.ClickByLinkText(driver, "Like us on Facebook");
    	Thread.sleep(2000);
    	 for(String winHandle : driver.getWindowHandles()){
             driver.switchTo().window(winHandle);
         }
    	ReusableFunc.ContainsPageTitle(driver, "BoxOffice | Facebook");
	
    }
    
}
