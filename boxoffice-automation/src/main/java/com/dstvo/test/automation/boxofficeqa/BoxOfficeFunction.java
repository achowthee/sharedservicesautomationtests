package com.dstvo.test.automation.boxofficeqa;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class BoxOfficeFunction {

	public static String[] strBOMovieTitles = new String[21];
	
	
	public static String[] getAllStrMovieTitle() throws Exception{
		return strBOMovieTitles;
	}
	
	public static String getStrMovieTitle(int k) throws Exception{
		return strBOMovieTitles[k];
	}

	
	public static String[] setStrMovieTitle(RemoteWebDriver rwd) throws Exception{
		
		
		ReusableFunc.ClickByXpath(rwd, ".//div[@class=\"type pc\"]", "pc icon");
		Thread.sleep(3000);
		int k=1;
		for(int j=1;j<=4;j++){
			
			for(int i=1;i<=5;i++){
			
				WebElement hoverElement = rwd.findElementByXPath("//div[@class='row']["+j+"]//div[@class='span2b tileContainer']["+i+"]//a[@class='js-movietile-image']/img");
				WebElement nameElement = rwd.findElementByXPath("//div[@class='jttip']["+k+"]//a[@class='js-movietile-hover-title']/h2");
				//WebElement hoverAwayElement = rwd.findElementByXPath(".//*[@id='form1']/div[5]/div[4]/div[2]/div[2]/div/ul/li[1]/a");
				WebElement hoverAwayElement = rwd.findElementByXPath(".//div[@class='pagination']/ul/li[1]/a");
				
		    	Actions builder = new Actions(rwd);
		    	builder.moveToElement(hoverElement).clickAndHold().perform();
		    	Thread.sleep(3000);
		    	strBOMovieTitles[k] = nameElement.getText();
		    	
		    	builder.moveToElement(hoverAwayElement).build().perform();
		    	
		    	k++;
				
			}
		}	
	return strBOMovieTitles;
	}
	

	public static String getRandomMovieFromBoxOffice(RemoteWebDriver rwd) throws Exception{

			Random rand = new Random();
			int k=rand.nextInt(5)+1;
			
			ReusableFunc.ClickByID(rwd, "logo");
			Thread.sleep(2000);
			ReusableFunc.VerifyTextOnPage(rwd, "Now Showing");
			
			WebElement hoverElement = rwd.findElementByXPath("//div[@class='item active']//div[@class='span2 tileContainer']["+k+"]//a[@class='js-movietile-image']/img");
			WebElement nameElement = rwd.findElementByXPath("//div[@class='jttip']["+k+"]//a[@class='js-movietile-hover-title']/h2");
		
			Actions builder = new Actions(rwd);
			builder.moveToElement(hoverElement).clickAndHold().perform();
			Thread.sleep(3000);
	    	return nameElement.getText();
		
		
	}
	
	
    public static void BoxOfficeLogin(RemoteWebDriver rwd, String strUserName, String strPassword) throws Exception{
  	   //Selenium@mailinator.com, 123456
     	
 	   	if(Utilities.isTextOnPage(rwd, "Logged in as")){
 			ReusableFunc.ClickByLinkText(rwd, "Logout");
 		   	Utilities.fluentWait(By.linkText("Login"), rwd);
 		}
    	//click login
    	ReusableFunc.ClickByLinkText(rwd, "Login");
    	Thread.sleep(2000);
    	//Switch control to the iframe
    	rwd.switchTo().frame(0); 	
    	
    	ReusableFunc.VerifyTextOnPage(rwd, "Login with your DStv Connect details");
    	//Enter username as automation2@mailinator.com   	
    	ReusableFunc.EnterTextInTextBox(rwd, rwd.findElement(By.id("Main_Login1_EmailAddressTextBox")), strUserName);
    	//Enter password as 123456
    	ReusableFunc.EnterTextInTextBox(rwd, rwd.findElement(By.id("Main_Login1_PasswordTextBox")), strPassword);
    	//Click login
    	ReusableFunc.ClickByID(rwd, "Main_Login1_LoginImageButton");
    	Thread.sleep(2000);
    	//switch control back to main window
    	rwd.switchTo().defaultContent();
    	ReusableFunc.VerifyTextOnPage(rwd,"Logged in as");

     }
    
    public static void BoxOfficeLogout(RemoteWebDriver rwd) throws Exception{
    	
 	   	if(Utilities.isTextOnPage(rwd, "Logged in as")){
 			ReusableFunc.ClickByLinkText(rwd, "Logout");
 			Thread.sleep(2000);
 		   	Utilities.fluentWait(By.linkText("Login"), rwd);
 		}
    	
    	
    }

	
}
