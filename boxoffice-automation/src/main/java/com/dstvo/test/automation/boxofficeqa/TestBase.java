package com.dstvo.test.automation.boxofficeqa;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.net.URL;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.*;


public class TestBase {
	  //  private static ChromeDriverService service;
	    public RemoteWebDriver driver;  //was private, changed to use in other tests  
	    

 
	    @BeforeMethod
		public void before() throws Exception {
	    	driver = new RemoteWebDriver(new URL("http://197.80.203.161:4445/wd/hub"), DesiredCapabilities.firefox());
	    	
	    	/*FirefoxProfile profile = new FirefoxProfile();
	    	    profile.setPreference("network.proxy.type", 1);
	    	    profile.setPreference("network.proxy.http", "03rnb-proxy06");
	    	    profile.setPreference("network.proxy.http_port", 8080);
	    	    profile.setPreference("network.proxy.ssl", "03rnb-proxy06");
	    	    profile.setPreference("network.proxy.ssl_port", 8080);
	    	    profile.setPreference("network.proxy.ftp", "03rnb-proxy06");
	    	    profile.setPreference("network.proxy.ftp_port", 8080);
	    	    profile.setPreference("network.proxy.Socks", "03rnb-proxy06");
	    	    profile.setPreference("network.proxy.Socks_port", 8080);
	    	    profile.setPreference("network.proxy.no_proxies_on","qa-boxoffice.dstv.com");
	     	    driver = new FirefoxDriver(profile);
	    	    	*/
	    }

	    @AfterMethod
		public void after() {
	        if (driver != null) {
	        driver.quit();
	        }
	    }
	    
	    protected String getSiteURL (){
	        String URL = System.getProperty("testUrl");
	        if (URL == null){
	                return "http://qa-boxoffice.dstv.com";
	        
	        }
	        else{
	                return URL;
	        }
	        
	    }
	    
	    protected RemoteWebDriver getWebDriver() {
	        return driver;
	    }
	}
