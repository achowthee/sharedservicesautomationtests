package com.dstvo.test.automation.samigrationboxofficeaut;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class Functions extends TestBase {
	
	@BeforeMethod
	@Override
	public void before() throws Exception{
		super.before();
		getWebDriver().get(getSiteURL());
	}
	
	
	public void logIn()  throws Exception{
		ReusableFunc.ClickByLinkText(driver, "Login");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='username']"), "boxoselfservice04@mailinator.com");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='password']"), "boxoselfservice04");
		Thread.sleep(3000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='login']", "Login");
		ReusableFunc.VerifyTextOnPage(driver, "Logout");
	}

}
