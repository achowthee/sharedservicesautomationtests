package com.dstvo.test.automation.samigrationboxofficeaut;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class RentAMovie extends TestBase {

	@BeforeMethod
	@Override
	public void before() throws Exception{
		super.before();
		getWebDriver().get(getSiteURL());
	}
	
	//@Test
	public void logIn()  throws Exception{
		ReusableFunc.ClickByLinkText(driver, "Login");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='username']"), "boxoselfservice04@mailinator.com");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='password']"), "boxoselfservice04");
		Thread.sleep(3000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='login']", "Login");
		ReusableFunc.VerifyTextOnPage(driver, "Logout");
	}
	
	//@Test
	public void seeMoreLinkRedirectsToBrowsePage() throws Exception{
		//Act
		ReusableFunc.ClickByLinkText(driver, "See more >>"); 
		//Assert
		ReusableFunc.VerifyTextOnPage(driver, "Movies Now");
		ReusableFunc.VerifyTextOnPage(driver, "Next");
		ReusableFunc.ContainsPageTitle(driver, "Browse Movies");
	}
	
	//@Test
	public void selectRandomMovieFromList() throws Exception{
		//seeMoreLinkRedirectsToBrowsePage();
		ReusableFunc.ClickByXpath(driver, "//*[@id=\"main_wucTopPicksMovieTileListLoggedin_videoPagesRepeater_myCarousel_1\"]/div/div/div[2]", "Select a movie");
		// ReusableFunc.ClickSubNav(driver, By.xpath(".//*[@id='form1']/div[6]/div[4]/div[6]/div[3]/div[3]"), By.xpath(".//*[@id='jttip-43324_main_videoGallery_videoRowRepeater_videoRepeater_2']/div/div/div[1]/a[2]"));
		//rentNow();
		
	}
	
	@Test
	public void rentMovie() throws Exception{
		logIn();
		seeMoreLinkRedirectsToBrowsePage();
		selectRandomMovieFromList();
		Thread.sleep(3000);
		rentNow();
		creditCardOption();
	}
	
	public void creditCardOption() throws Exception{
		ReusableFunc.ClickByXpath(driver, "//*[@id='creditCardTab']/span/img", "choose voucher");
		//ReusableFunc.ClickByLinkText(driver, "Continue");   //.ClickByXpath(driver, "/html/body/form/div[3]/div/div/div[1]/div[3]/div/div[2]/div[2]/a/span", "");
		Thread.sleep(5000);
		
	}
	
	public void rentNow() throws Exception{
		ReusableFunc.ClickByXpath(driver, ".//div[@id='main_newVideoPlayerControl_divMoviedetails']/div[3]/div/div/a/span", "rentNow");
		
	}
}
