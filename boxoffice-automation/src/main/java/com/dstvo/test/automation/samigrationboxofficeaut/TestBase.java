package com.dstvo.test.automation.samigrationboxofficeaut;

import java.net.URL;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;


import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.*;


public class TestBase {
	  //  private static ChromeDriverService service;
	    public RemoteWebDriver driver;  //was private, changed to use in other tests  
	    

 
	    @BeforeMethod
		public void before() throws Exception {
	    	//driver = new RemoteWebDriver(new URL("http://197.80.203.161:4445/wd/hub"), DesiredCapabilities.firefox());
	    	/*FirefoxProfile profile = new FirefoxProfile();
    	    profile.setPreference("network.proxy.type", 1);
    	    profile.setPreference("network.proxy.http", "03rnb-proxy06");
    	    profile.setPreference("network.proxy.http_port", 8080);
    	    profile.setPreference("network.proxy.ssl", "03rnb-proxy06");
    	    profile.setPreference("network.proxy.ssl_port", 8080);
    	    profile.setPreference("network.proxy.ftp", "03rnb-proxy06");
    	    profile.setPreference("network.proxy.ftp_port", 8080);
    	    profile.setPreference("network.proxy.Socks", "03rnb-proxy06");
    	    profile.setPreference("network.proxy.Socks_port", 8080);
    	    profile.setPreference("network.proxy.no_proxies_on","aut.boxoffice.dstv.com");
    	    driver = new FirefoxDriver(profile);*/
    	    
	          /*driver = new RemoteWebDriver(new URL("http://197.80.203.161:4466/wd/hub"), DesiredCapabilities.chrome());
	           System.setProperty("webdriver.chrome.driver", "C://ProgramFiles(x86)//chromedriver_win32/chromedriver.exe");*/
	          /**To run tests locally using chrome*/
	          System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
	          driver = new ChromeDriver();
	           driver.manage().window().maximize();

	    	
	    
     
	    }

	    @AfterMethod
		public void after() {
	        if (driver != null) {
	        driver.quit();
	        }
	    }
	    
	    protected String getSiteURL (){
	        String URL = System.getProperty("testUrl");
	        if (URL == null){
	                return "http://staging-boxoffice.dstv.com/";
	        
	        }
	        else{
	                return URL;
	        }
	        
	    }
	    
	    protected RemoteWebDriver getWebDriver() {
	        return driver;
	    }
	}
