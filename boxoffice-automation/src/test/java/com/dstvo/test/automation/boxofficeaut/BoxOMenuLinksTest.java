package com.dstvo.test.automation.boxofficeaut;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.By;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;


public class BoxOMenuLinksTest extends TestBase{
	
	public class BoxOMenuLinksTests{
		
	}

    @BeforeMethod
	@Override
    public  void before() throws Exception {
        super.before();        
       // getWebDriver().get("http://aut.boxoffice.dstv.com");
        getWebDriver().get(getSiteURL()); 
    } 
    
    @Test
    public void canAccessHomePageViaLogo() throws Exception {       
        

        // Act
    	if(Utilities.isTextOnPage(driver, "Logged in as")){
    		ReusableFunc.ClickByLinkText(driver, "Logout");
    	   	Utilities.fluentWait(By.linkText("Login"), driver);
    	}
    	//Click Logo
        ReusableFunc.ClickByID(driver, "logo");    
        // Assert
        ReusableFunc.VerifyTextOnPage(driver, "Now Showing");
        ReusableFunc.VerifyTextOnPage(driver, "Coming Soon");
        
    }  
    
    @Test
    public void canAccessHomePageViaMenu() throws Exception {       
        

        // Act
    	if(Utilities.isTextOnPage(driver, "Logged in as")){
    		ReusableFunc.ClickByLinkText(driver, "Logout");
    	   	Utilities.fluentWait(By.linkText("Login"), driver);
    	}
    	//Click Logo
        ReusableFunc.ClickByXpath(driver, ".//*[@id='boxOfficeMenuSelected_mnuHome']/a", "Home button");  
        // Assert
        ReusableFunc.VerifyTextOnPage(driver, "Now Showing");
        ReusableFunc.VerifyTextOnPage(driver, "Coming Soon");
        ReusableFunc.ContainsPageTitle(driver, "BoxOffice");  
    }  
    
    
    @Test
    public void canAccessDesktopPlayerViaMenu() throws Exception{
    	//Act
    	if(Utilities.isTextOnPage(driver, "Logged in as")){
    		ReusableFunc.ClickByLinkText(driver, "Logout");
    	   	Utilities.fluentWait(By.linkText("Login"), driver);
    	}
    	//click Desktop Player
    	ReusableFunc.ClickByLinkText(driver, "Desktop Player");
	
    	//Assert
    	ReusableFunc.VerifyTextOnPage(driver, "DStv Desktop Player");
    	ReusableFunc.VerifyTextOnPage(driver, "Entertainment whenever and wherever you want it");
	
    }
    
    @Test
    public void canAccessHelpViaMenu() throws Exception{
    	//Act
    	if(Utilities.isTextOnPage(driver, "Logged in as")){
    		ReusableFunc.ClickByLinkText(driver, "Logout");
    	   	Utilities.fluentWait(By.linkText("Login"), driver);
    	}
    	//click
    	ReusableFunc.ClickByLinkText(driver, "Help");    	

    	//Assert
    	ReusableFunc.VerifyTextOnPage(driver, "Frequently Asked Questions");
    	ReusableFunc.VerifyTextOnPage(driver, "What is BoxOffice Online?");
   	
    	
    }
    
   
    
}
