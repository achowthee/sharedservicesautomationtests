package com.dstvo.test.automation.boxofficeqa;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;


public class NGABoxOSiteMapTest extends TestBase{
	
	@BeforeMethod
	@Override
    public  void before() throws Exception {
        super.before();        
        getWebDriver().get(getSiteURL());
       // getWebDriver().get("http://aut.boxoffice.dstv.com");
       
    } 
    
        
    @Test
    public void canAccessMoviesNowViaSiteMap() throws Exception{
    	
    	BoxOfficeFunction.BoxOfficeLogin(driver, "Nigeria@mailinator.com", "123456");
    	//Click Movies Now
    	ReusableFunc.FailIfTextOnPage(driver, "Movies on PVR");
    	ReusableFunc.ClickByLinkText(driver, "Movies Now");

        // Assert
        ReusableFunc.VerifyTextOnPage(driver, "Online");
        BoxOfficeFunction.BoxOfficeLogout(driver);
    }
    

    
    @Test
    public void canAccessDesktopPlayerViaSiteMap() throws Exception{

    	BoxOfficeFunction.BoxOfficeLogin(driver, "Nigeria@mailinator.com", "123456");
    	
    	//click Desktop Player
    	ReusableFunc.ClickByLinkText(driver, "Desktop player");
    	//Assert
    	ReusableFunc.VerifyTextOnPage(driver, "DStv Desktop Player");
    	ReusableFunc.VerifyTextOnPage(driver, "Entertainment whenever and wherever you want it");
    	
    	BoxOfficeFunction.BoxOfficeLogout(driver);
    }
    
    
    @Test
    public void canAccessFAQsViaSiteMap() throws Exception{

    	BoxOfficeFunction.BoxOfficeLogin(driver, "Nigeria@mailinator.com", "123456");
    	
    	//click
    	ReusableFunc.ClickByLinkText(driver, "FAQs");    	
    	//Assert
    	ReusableFunc.VerifyTextOnPage(driver, "Frequently Asked Questions");
    	ReusableFunc.VerifyTextOnPage(driver, "What is BoxOffice Online?");
    	ReusableFunc.FailIfTextOnPage(driver, "BoxOffice on PVR");
    	
    	BoxOfficeFunction.BoxOfficeLogout(driver);
    }
    
    @Test
    public void canAccessContactUsViaSiteMap() throws Exception{

    	BoxOfficeFunction.BoxOfficeLogin(driver, "Nigeria@mailinator.com", "123456");
    	//click
    	ReusableFunc.ClickByLinkText(driver, "Contact us");    	

    	//Assert
    	ReusableFunc.VerifyTextOnPage(driver, "Got a question, comment or complaint?");
    	ReusableFunc.VerifyTextOnPage(driver, "Fill in the form below");
    	ReusableFunc.FailIfTextOnPage(driver, "BoxOffice on PVR");
    	
    	BoxOfficeFunction.BoxOfficeLogout(driver);
    }
    
    @Test
    public void canAccessLikeFBViaSiteMap() throws Exception{
    	
    	BoxOfficeFunction.BoxOfficeLogin(driver, "Nigeria@mailinator.com", "123456");
    	ReusableFunc.ClickByLinkText(driver, "Like us on Facebook");
    	Thread.sleep(2000);
    	 for(String winHandle : driver.getWindowHandles()){
             driver.switchTo().window(winHandle);
         }
    	ReusableFunc.ContainsPageTitle(driver, "BoxOffice | Facebook");
    	
    	BoxOfficeFunction.BoxOfficeLogout(driver);
	
    }
    

}
