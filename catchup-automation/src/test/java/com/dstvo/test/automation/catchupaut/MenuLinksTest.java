package com.dstvo.test.automation.catchupaut;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class MenuLinksTest extends TestBase {

	public MenuLinksTest() {

	}

	@BeforeMethod
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}

	@Test
	public void canAccessHomePageViaMenu() throws Exception {

		ReusableFunc.ClickByLinkText(driver, "Home");
		Thread.sleep(3000);
		ReusableFunc.VerifyPageTitle(driver, "DStv Catch Up");
		ReusableFunc.VerifyTextOnPage(driver, "Latest Releases");
		ReusableFunc.VerifyTextOnPage(driver, "Last Chance");

	}

	@Test
	public void canAccessMoviesViaMenu() throws Exception {

		ReusableFunc.ClickByLinkText(driver, "Movies");
		Thread.sleep(3000);
		ReusableFunc.VerifyTextOnPage(driver, "Movies");
		

	}
	
	@Test
	public void canAccessSeriesViaMenu() throws Exception {

		ReusableFunc.ClickByLinkText(driver, "Series");
		Thread.sleep(3000);
		ReusableFunc.VerifyTextOnPage(driver, "Series");
	

	}


	@Test
	public void canAccessActualityViaMenu() throws Exception {

		ReusableFunc.ClickByLinkText(driver, "Actuality");
		Thread.sleep(3000);
		ReusableFunc.VerifyTextOnPage(driver, "Actuality/Information");

	}

	@Test
	public void canAcessMusicViaMenu() throws Exception {

		ReusableFunc.ClickByLinkText(driver, "Music");
		Thread.sleep(3000);
		ReusableFunc.VerifyTextOnPage(driver, "All Music");

	}

	@Test
	public void canAccessSportViaMenu() throws Exception {

		Thread.sleep(2000);
		ReusableFunc.ClickByLinkText(driver, "Sport");
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, "All Magazine");
		ReusableFunc.VerifyTextOnPage(driver, "Catch Up");

	}

	@Test
	public void canAccessChannelsViaMenu() throws Exception {

		ReusableFunc.ClickByLinkText(driver, "Channels");
		Thread.sleep(3000);
		ReusableFunc.VerifyTextOnPage(driver, "All Channels");

	}

	@Test
	public void canAccessKidsViaMenu() throws Exception {

		ReusableFunc.ClickByLinkText(driver, "Kids");
		Thread.sleep(3000);
		ReusableFunc.VerifyTextOnPage(driver, "Youth/Children");

	}

}
