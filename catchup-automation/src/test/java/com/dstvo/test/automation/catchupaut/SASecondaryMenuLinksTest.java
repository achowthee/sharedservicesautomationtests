package com.dstvo.test.automation.catchupaut;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class SASecondaryMenuLinksTest extends TestBase{
	@BeforeMethod
	@Override
    public  void before() throws Exception {
        super.before();        
        getWebDriver().get(getSiteURL());
       
            
	    }     
	    
	    @Test
	    public void canAccessInstallDeskPlayer() throws Exception{
	    	
	    	ReusableFunc.ClickByXpath(driver, ".//*[@id='ctl00_wucProfileStatusBar_MyProfileHyperLink']", "Install Desktop Player");
	    	ReusableFunc.VerifyTextOnPage(driver, "DStv Desktop Player BETA");
	    	ReusableFunc.VerifyTextOnPage(driver, "Is my computer ready?");
	    	
	    	
	    }
	    
	    @Test
	   public void canAccessFindOutMore() throws Exception{
		   
	    	ReusableFunc.ClickByXpath(driver, ".//*[@id='A1']", "Find out more");
		   	//ReusableFunc.ClickByLinkText(driver, "Find out more");
		    ReusableFunc.VerifyTextOnPage(driver, "Get to know the DStv Explora");
		   //ReusableFunc.VerifyTextOnPage(driver, "Minimum system requirements");
		   
	   }
	    
	    @Test
		   public void canAccessFAQ() throws Exception{
			   
		    	ReusableFunc.ClickByLinkText(driver, "FAQ");
		    	Thread.sleep(3000);
			    ReusableFunc.VerifyTextOnPage(driver, "All about DStv Catch Up");
			    
			   
			   
		   }
	    
	    
	    @Test
		   public void canAccessContactUs() throws Exception{
			   
		    	ReusableFunc.ClickByLinkText(driver, "Contact Us");
			    ReusableFunc.VerifyTextOnPage(driver, "CONTACT US");
			    ReusableFunc.VerifyTextOnPage(driver, "Live Chat Support");
			   
			   
		   }
}
	   
