package com.dstvo.test.automation.catchupqa;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class CatchUpTermAndCsTest extends TestBase{
	
	
    @BeforeMethod
	@Override
    public  void before() throws Exception {
        super.before();        
        getWebDriver().get(getSiteURL());
        
    }  
    
    
    @Test
    public void verifySupportEmail() throws Exception{
    	
    	Thread.sleep(2000);
    	ReusableFunc.ClickByLinkText(driver, "DStv Catch Up Website Terms & Conditions");
    	Thread.sleep(2000);
    	ReusableFunc.VerifyTextOnPage(driver, "support@dstvcatchup.com");
    }

}