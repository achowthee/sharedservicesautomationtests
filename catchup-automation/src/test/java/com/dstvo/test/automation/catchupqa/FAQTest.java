package com.dstvo.test.automation.catchupqa;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.By;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class FAQTest extends TestBase{
	
	@BeforeMethod
	@Override
	public void before() throws Exception{
		
		super.before();
		getWebDriver().get(getSiteURL());
	}
	
	@Test
	public void canAccessFAQ() throws Exception{
		
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='ctl00_wucProfileStatusBar_LogoutHyperLink']", "FAQ");
		ReusableFunc.VerifyTextOnPage(driver, "All about DStv Catch Up");
		ReusableFunc.VerifyTextOnPage(driver, "What is DStv Catch Up");
		//ReusableFunc.ContainsPageTitle(driver, "Frequently Asked Questions");
		
	}
	
	@Test
	public void canAccessFAQiPad() throws Exception{
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='ctl00_wucProfileStatusBar_LogoutHyperLink']", "FAQ");
		ReusableFunc.VerifyTextOnPage(driver, "All about DStv Catch Up");
		ReusableFunc.ClickByLinkText(driver, "DStv Catch Up iPad app");
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='CU_AboutiPad3']/div"), "iPad Element");

	}
	
	
	@Test
	public void canAccessDesktopPlayer() throws Exception{

		ReusableFunc.ClickByXpath(driver, ".//*[@id='ctl00_wucProfileStatusBar_LogoutHyperLink']", "FAQ");
		ReusableFunc.VerifyTextOnPage(driver, "All about DStv Catch Up");
		
		ReusableFunc.ClickByLinkText(driver, "Desktop Player");
		
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='CU_DesktopPlayer1']/b"), "Where can i find the desktop player");
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='CU_DesktopPlayer5']/div"), "Explain the Desktop Player layout");
		
		
	}
	
	@Test
	public void whereCanIFindDeskPlayerLink() throws Exception{
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='ctl00_wucProfileStatusBar_LogoutHyperLink']", "FAQ");
		ReusableFunc.VerifyTextOnPage(driver, "All about DStv Catch Up");
		
		ReusableFunc.ClickByLinkText(driver, "Desktop Player");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='CU_DesktopPlayer1']/div/a", "1st Here");
		Thread.sleep(2000);
    	ReusableFunc.VerifyTextOnPage(driver, "DStv Desktop Player BETA");
    	ReusableFunc.VerifyTextOnPage(driver, "Is my computer ready?");
    	
		ReusableFunc.ClickByXpath(driver, ".//*[@id='ctl00_wucProfileStatusBar_LogoutHyperLink']", "FAQ");
		ReusableFunc.VerifyTextOnPage(driver, "All about DStv Catch Up");
		
		ReusableFunc.ClickByLinkText(driver, "Desktop Player");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='CU_DesktopPlayer2']/div/a", "2nd Here");
		Thread.sleep(2000);
    	ReusableFunc.VerifyTextOnPage(driver, "DStv Desktop Player BETA");
    	ReusableFunc.VerifyTextOnPage(driver, "Is my computer ready?");
    	  	
		
	}
	
	
	@Test
	public void missingTitlesOnPVR() throws Exception{
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='ctl00_wucProfileStatusBar_LogoutHyperLink']", "FAQ");
		ReusableFunc.VerifyTextOnPage(driver, "All about DStv Catch Up");
		
		ReusableFunc.ClickByLinkText(driver, "Missing titles on PVR?");
		
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='CU_MissingTitlesPVR1']/div"), "missing title");

	}
	
	
	
	@Test
	public void isMySystemReady() throws Exception{
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='ctl00_wucProfileStatusBar_LogoutHyperLink']", "FAQ");
		ReusableFunc.VerifyTextOnPage(driver, "All about DStv Catch Up");
		
		ReusableFunc.ClickByLinkText(driver, "Is my system ready?");
		
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='CU_SystemReq']/b"), "Minimum system requirements");
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='CU_SystemCookies']/div/img[1]"), "internet options image");
		
	}

}
