package com.dstvo.test.automation.catchupqa;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class FindOutMoreTest extends TestBase{
	
	
	@BeforeMethod
	@Override
	public void before() throws Exception{
		super.before();
		getWebDriver().get(getSiteURL());
		
	}
	
	
	@Test
	public void canAccessFindOutMore() throws Exception{
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='A1']", "Find Out More");
		ReusableFunc.VerifyTextOnPage(driver, "Explora");

		
	}
	

}
