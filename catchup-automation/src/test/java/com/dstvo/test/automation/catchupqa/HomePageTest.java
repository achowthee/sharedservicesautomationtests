package com.dstvo.test.automation.catchupqa;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.By;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class HomePageTest extends TestBase{
	
	
    @BeforeMethod
	@Override
    public  void before() throws Exception {
        super.before();        
        getWebDriver().get(getSiteURL());
        
    }     

    
    
    @Test
    public void verifyStreamOrDownloadTextDisplayed() throws Exception{
    	
    	Thread.sleep(5000);
    	ReusableFunc.verifyTextInWebElement(driver,By.xpath(".//*[@id='SA3']/h2"), "Stream or download over 900 titles of series, movies and sport highlights right now");
    	ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@class='promoList-CU']/ul/li[1]"), "Hundreds of movies and series titles");
    	ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@class='promoList-CU']/ul/li[2]"), "The latest sports highlights");
    	ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@class='promoList-CU']/ul/li[4]"), "A great selection of magazine and actuality shows");
    	ReusableFunc.VerifyTextOnPage(driver, "All in one place and exclusive to DStv Premium subscribers for free");
    }
    
    @Test
    public void verifyHomeToolTip() throws Exception{
    	
    	ReusableFunc.verifyAttributeInWebElement(driver, By.id("ctl00_ctl00_wucOnDemandNavBar_lv_ctrl0_HyperLink1"), "title", "Catch Up Home");
    }
    
    @Test
    public void verifyFooterLinks() throws Exception{
    	
    	ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='footer_ctrlFooter_mfooter_za']/div/div[2]/a[1]"), "DStv Catch Up Website Terms & Conditions");
    	ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='footer_ctrlFooter_mfooter_za']/div/div[2]/a[2]"), "MultiChoice Website");
    	ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='footer_ctrlFooter_mfooter_za']/div/div[2]/a[3]"), "Terms & Conditions");
    	ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='footer_ctrlFooter_mfooter_za']/div/div[2]/a[4]"), "Privacy Policy");
    	ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='footer_ctrlFooter_mfooter_za']/div/div[2]/a[5]"), "Copyright");
    	ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='footer_ctrlFooter_mfooter_za']/div/div[2]/a[6]"), "Glossary");
    	ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='footer_ctrlFooter_mfooter_za']/div/div[2]/a[7]"), "DStv-i");
    	ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='footer_ctrlFooter_mfooter_za']/div/div[2]/a[8]"), "Email Scam");
    	ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='footer_ctrlFooter_mfooter_za']/div/div[2]/a[9]"), "DMMA");
    	ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='footer_ctrlFooter_mfooter_za']/div/div[2]/a[10]"), "Site Map");
    	ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='footer_ctrlFooter_mfooter_za']/div/div[2]/a[11]"), "Contact us");
    	ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='footer_ctrlFooter_mfooter_za']/div/div[2]/a[12]"), "Careers");
    }
    
    @Test
    public void verifySiteMapToolTip() throws Exception{
    	
    	ReusableFunc.verifyAttributeInWebElement(driver, By.linkText("Site Map"), "title", "Catch Up Site Map");
    	
    }


    
}
