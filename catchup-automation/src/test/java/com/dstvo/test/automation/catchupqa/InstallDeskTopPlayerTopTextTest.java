package com.dstvo.test.automation.catchupqa;

import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class InstallDeskTopPlayerTopTextTest extends TestBase{
	
	
    @BeforeMethod
	@Override
    public  void before() throws Exception {
        super.before();        
        getWebDriver().get(getSiteURL());
        
    }     
    
    @Test
    public void canAccessInstallDskPlayer() throws Exception{
    	
    	ReusableFunc.ClickByXpath(driver, ".//*[@id='ctl00_wucProfileStatusBar_MyProfileHyperLink']", "Install Desktop Player");
    	
    	ReusableFunc.VerifyTextOnPage(driver, "DStv Desktop Player BETA");
    	ReusableFunc.VerifyTextOnPage(driver, "Is my computer ready?");
    	
    	
    }
    
    @Test
   public void canAccessFindOutMore() throws Exception{
	   
    	ReusableFunc.ClickByXpath(driver, ".//*[@id='ctl00_wucProfileStatusBar_MyProfileHyperLink']", "Install Desktop Player");
	   ReusableFunc.ClickByLinkText(driver, "Find out more");
	   
	   ReusableFunc.VerifyTextOnPage(driver, "Frequently Asked Questions");
	   ReusableFunc.VerifyTextOnPage(driver, "Minimum system requirements");
	   
   }
   
    @Test
   public void canAccessCheckNow() throws Exception{
	   int intFlag=0;
	   ReusableFunc.ClickByXpath(driver, ".//*[@id='ctl00_wucProfileStatusBar_MyProfileHyperLink']", "Install Desktop Player");
	   ReusableFunc.ClickByLinkText(driver, "Check Now");
	   
	   
	   ReusableFunc.VerifyTextOnPage(driver, "Is my computer ready?");
	   ReusableFunc.VerifyTextOnPage(driver, "Operating system");
	   ReusableFunc.VerifyTextOnPage(driver, "Country");
	   
	   try{
		   if(driver.findElementByXPath(".//*[@id='dummy-lightbox']/div/div[5]/div/div[2]/div[2]/div[1]/div[1]").isDisplayed())
			   intFlag = 1;
		   
	   }
	   catch(Exception e){
		   Assert.fail("Can not find check now pop up");
	   }
	   
	   if(intFlag==0)
		   Assert.fail("Can not find check now pop up");
	   
	   
   }
    


}
