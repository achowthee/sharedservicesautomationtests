package com.dstvo.test.automation.dstvfunctionlib;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import java.util.Properties;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import com.google.common.base.Function;

public class ConnectControl {
public static String strEmailAddy;
	
	
	public static void setEmailAddy(String strEmail){
		strEmailAddy = strEmail;
	}
	
	public static String getEmailAddy(){
		return strEmailAddy;
	}
	public static String strFirstname;
	
	public static void setFirstname(String strFirst){
		strFirstname = strFirst;
	}
	public static String getFirstname(){
		return strFirstname;
	}
	public static String strSurname;
	
	public static void setSurname(String strSur){
		strSurname = strSur;
	}
	public static String getSurname(){
		return strSurname;
	}
	public static String strProvince;
	
	public static void setProvince(String strProv){
		strProvince = strProv;
	}
	public static String getProvince(){
		return strProvince;
	}

//******************************************************************************************************************************************************
//Read test data from Properties file

	//static Properties TestCredentials = getProperties("C:\\workspace\\QA-Automated-Tests\\QA-automated-test\\connect-automation\\src\\main\\resources\\TestData.properties");
	//static Properties TestCredentials = getProperties("C:\\TestNG-Workspace\\QA-automated-test\\connect-automation\\src\\main\\resources\\TestData.properties");

/*	static Properties TestCredentials = getProperties("C:\\workspace\\QA-Automated-Tests\\QA-automated-test\\connect-automation\\src\\main\\resources\\TestData.properties");
//	static Properties TestCredentials = getProperties("C:\\TestNG-Workspace\\QA-automated-test\\connect-automation\\src\\main\\resources\\TestData.properties");
>>>>>>> .r144259
	//Third Party login creds
	public static String Password = TestCredentials.getProperty("Password");
	public static String YEmailAddress = TestCredentials.getProperty("YahooEmailAddress");
	public static String MobileNumber = TestCredentials.getProperty("MobileNumber");
	public static String GEmailAddress = TestCredentials.getProperty("GmailEmailAddress");
	public static String GUserName = TestCredentials.getProperty("GmailUsername");
	public static String MailinatorAddress = TestCredentials.getProperty("MailinatorAddress");
	public static String ForgotPasswordMobile = TestCredentials.getProperty("ForgotPasswordMobile");
	public static String GoogleUsername = TestCredentials.getProperty("GoogleUsername");
	public static String YahooUsername = TestCredentials.getProperty("YahooUsername");
	public static String GPassword = TestCredentials.getProperty("GPassword");
	public static String YPassword = TestCredentials.getProperty("YPassword");
	//QA
	public static String QAYUsername = TestCredentials.getProperty("QAYahooUsername");
	public static String QAUsername = TestCredentials.getProperty("QAUsername");
	public static String QAURL = TestCredentials.getProperty("QAURL");
	//Staging
	public static String STYUsername = TestCredentials.getProperty("StagingYahooUsername");
	public static String STUsername = TestCredentials.getProperty("StagingUsername");
	public static String StagingURL = TestCredentials.getProperty("StagingURL");
	//Production
	public static String LiveYUsername = TestCredentials.getProperty("LiveYahooUsername");
	public static String LiveUsername = TestCredentials.getProperty("LiveUsername");
	public static String LiveURL = TestCredentials.getProperty("LiveURL");
	public static String LiveEmail = TestCredentials.getProperty("LiveEmail");
	//Common XPaths
	public static String ResetPasswordButton = TestCredentials.getProperty("ResetPasswordButton");
	public static String ForgotPasswordPageOkButton = TestCredentials.getProperty("ForgotPasswordPageOkButton");
	public static String ForgotPasswordSendCodeButton = TestCredentials.getProperty("ForgotPasswordSendCodeButton");
	public static String SmartcardLinkButton = TestCredentials.getProperty("SmartcardLinkButton");
	public static String SmartcardUnlinkButton = TestCredentials.getProperty("SmartcardUnlinkButton");*/
	
//******************************************************************************************************************************************************	
	public static void register(RemoteWebDriver rwd) throws Exception{
		
    	WebElement textBoxEle;
    	String strUserName;

	   	if(Utilities.isTextOnPage(rwd, "Logged in as")){
			ReusableFunc.ClickByLinkText(rwd, "Logout");
		   	Utilities.fluentWait(By.linkText("Login"), rwd);
		}
	   	
	   	
	   	//Click Register
	   	ReusableFunc.ClickByLinkText(rwd, "Register");
	   	//ReusableFunc.ClickByXpath(rwd, ".//*[contains(@id, 'RegisterHyperLink')]", "Register button");
	   	Thread.sleep(1000);
	   	ReusableFunc.VerifyTextOnPage(rwd, "Create your Connect ID");
	  
	   	
	   do{

	   		textBoxEle=rwd.findElement(By.xpath(".//*[contains(@id, 'ctrlSignIn_txtSignUpEmailAddress')]"));
	   		//Generate random 4 char string
	   		RandomString rs = new RandomString(4);

	    	strUserName="Automation"+rs.nextString();
	    	ConnectControl.setEmailAddy(strUserName + "@mailinator.com");
	    	textBoxEle.clear();
	   		ReusableFunc.EnterTextInTextBox(rwd,textBoxEle, ConnectControl.getEmailAddy());
	   		rwd.findElementById("txtSignUpPassword").clear();
	   		ReusableFunc.EnterTextInTextBox(rwd, rwd.findElementById("txtSignUpPassword"), "Password123");
	   		
	   		//check if checkbox is checked
	   		if(!rwd.findElementById("chkAcceptTerms").isSelected())
	   			ReusableFunc.CheckCheckBox(rwd, rwd.findElementById("chkAcceptTerms"));
	   		
	   		ReusableFunc.ClickByXpath(rwd, ".//*[contains(@id, 'ctrlSignIn_btnRegister')]", "Cannot locate Register button");
	   		Thread.sleep(8000);
	
	   		
	   	}while(rwd.findElementById("signupEmailAddressCustomValidator").isDisplayed());
	   	Thread.sleep(2000);
	   	//ReusableFunc.VerifyTextOnPage(rwd, "A verification link has been sent to your email.");
	   	ReusableFunc.ClickByXpath(rwd, ".//*[contains(@id, 'ctrlSignIn_btnBacktoSite')]", "Back to site");
	   
	   	
	}
	
	public static void registerWithGmail(RemoteWebDriver rwd) throws Exception{
		
    	WebElement textBoxEle;

	   	if(Utilities.isTextOnPage(rwd, "Logged in as")){
			ReusableFunc.ClickByLinkText(rwd, "Logout");
		   	Utilities.fluentWait(By.linkText("Login"), rwd);
		}
	   	
	   	
	   	//Click Register
	   	ReusableFunc.ClickByLinkText(rwd, "Register");
	   	ReusableFunc.VerifyTextOnPage(rwd, "Create your DStv Connect account");
	   	ReusableFunc.VerifyTextOnPage(rwd, "This will allow you to login to all");
	   	
	   do{

	   		textBoxEle=rwd.findElementById("AuthenticationMenu1_ctrlSignIn_txtSignUpEmailAddress");
	    	ConnectControl.setEmailAddy("automationselenium187@gmail.com");
	    	textBoxEle.clear();
	   		ReusableFunc.EnterTextInTextBox(rwd,textBoxEle, ConnectControl.getEmailAddy());
	   		rwd.findElementById("txtSignUpPassword").clear();
	   		ReusableFunc.EnterTextInTextBox(rwd, rwd.findElementById("txtSignUpPassword"), "Password123");
	   		
	   		//check if checkbox is checked
	   		if(!rwd.findElementById("chkAcceptTerms").isSelected())
	   			ReusableFunc.CheckCheckBox(rwd, rwd.findElementById("chkAcceptTerms"));
	   		
	   		ReusableFunc.ClickByID(rwd, "AuthenticationMenu1_ctrlSignIn_btnRegister");
	   		Thread.sleep(5000);
	
	   		
	   	}while(rwd.findElementById("signupEmailAddressCustomValidator").isDisplayed());
	   	
	   	ReusableFunc.VerifyTextOnPage(rwd, "A verification link has been sent to your email.");
	}
	
    public static void canVerifyEmail(RemoteWebDriver rwd) throws Exception {  
    	
     	WebElement mailTextBoxEle;
     	mailTextBoxEle=rwd.findElement(By.id("check_inbox_field"));
     	ReusableFunc.EnterTextInTextBox(rwd, mailTextBoxEle, ConnectControl.getEmailAddy());
     	ReusableFunc.ClickByXpath(rwd, ".//*[@id='checkInbox']/form/input[2]", "Go! button");
     	ReusableFunc.VerifyTextOnPage(rwd, "dstv: activate your account for");
     	ReusableFunc.ClickByLinkText(rwd, "dstv: activate your account for unlimite");
     	ReusableFunc.VerifyTextOnPage(rwd, "Thank you for registering with DStv");
     	Thread.sleep(4000);
     	
     	//click Activate your account
     	//ReusableFunc.ClickByXpath(rwd, ".//*[@id='message']/a[1]/img", "Activate your account");
     	ReusableFunc.ClickByXpath(rwd, "//img[@alt='Activate your account']", "Activate your account");
     	Thread.sleep(4000);
     	ReusableFunc.VerifyTextOnPage(rwd, "Thank you! Your account has been verified");
     	ReusableFunc.VerifyTextOnPage(rwd, "You are not currently subscribed to any newsletter");
     	
     	ReusableFunc.ClickByID(rwd, "AuthenticationMenu1_LogoutHyperLink");	
     	//Syncronization ,...waiting for page to display
     	Utilities.fluentWait(By.linkText("Login"), rwd);

    }
    
    
    public static void canVerifyViaCMS(RemoteWebDriver rwd) throws Exception {  
    	
    	WebElement mailEle = rwd.findElementById("txtUsername");
    	WebElement passEle = rwd.findElementById("txtPassword");
    	
    	mailEle.clear();
    	passEle.clear();
    	
    	ReusableFunc.EnterTextInTextBox(rwd, mailEle, "kreasen.naidoo@dstvo.com");
    	ReusableFunc.EnterTextInTextBox(rwd, passEle, "kreasen187");
    	ReusableFunc.ClickByXpath(rwd, ".//*[@id='divLogin']/span", "Login button");
    	
    	
    	Thread.sleep(3000);
		WebElement hoverElement = rwd.findElementByXPath(".//*[@id='mainContainer']/div/ul/li[1]/span");
		WebElement nameElement = rwd.findElementByXPath(".//*[@id='ctl00_role_UserManagement']");
	
		Actions builder = new Actions(rwd);
		//builder.moveToElement(hoverElement).build().perform();
		builder.moveToElement(hoverElement).clickAndHold().perform();
		Thread.sleep(1000);
		nameElement.click();
		
		ReusableFunc.EnterTextInTextBox(rwd, rwd.findElementById("ctl00_ContentPlaceHolder1_TextBox_Email_Filter"), ConnectControl.getEmailAddy());
    	ReusableFunc.ClickByID(rwd, "ctl00_ContentPlaceHolder1_FilterButton");
    	ReusableFunc.ClickByLinkText(rwd, "Base Profile");
    	Thread.sleep(2000);
    	
    	WebElement statusEle = rwd.findElementById("ctl00_ContentPlaceHolder1_FormView_User_StatusIDTextBox");
    	statusEle.clear();
    	ReusableFunc.EnterTextInTextBox(rwd, statusEle, "1");
    	ReusableFunc.ClickByLinkText(rwd, "Update");
    	Thread.sleep(2000);
    	

    }
    
   
    
    public static void canLogin(RemoteWebDriver rwd, String strUserName, String strPassword) throws Exception{
 	   //Selenium@mailinator.com, 123456
    	
	   if(Utilities.isTextOnPage(rwd, "Logged in as")){
			ReusableFunc.ClickByLinkText(rwd, "Logout");
		   	Utilities.fluentWait(By.linkText("Login"), rwd);
		}
		//click login
	   	Thread.sleep(2000);
	   	WebElement eleLogin = rwd.findElementByLinkText("Login");
	   	Thread.sleep(2000);
		eleLogin.click();
		
		//Enter username as automation2@mailinator.com   	
		ReusableFunc.EnterTextInTextBox(rwd, rwd.findElement(By.xpath(".//*[@class='EmailAddressTextBoxSelector con_input']")), strUserName);
		//Enter password as 123456
		ReusableFunc.EnterTextInTextBox(rwd, rwd.findElement(By.xpath(".//*[@class='PasswordTextBoxSelector con_input']")), strPassword);
		//Click login
		ReusableFunc.ClickByXpath(rwd, ".//*[@class='con_modEnable']", "Login");

		ReusableFunc.VerifyTextOnPage(rwd,"You are logged in as");
		//click logout
		
		Thread.sleep(1000);
		rwd.manage().window().maximize();
		Thread.sleep(2000);
	 	//ReusableFunc.ClickByID(rwd, "ctl00_AuthenticationMenu_LogoutHyperLink");	
		//ReusableFunc.ClickByLinkText(rwd, "Logout");
	 	//Syncronization ,...waiting for page to display
	 	//Utilities.fluentWait(By.linkText("Login"), rwd);
    }
    

    public static void connectLogin(RemoteWebDriver rwd, String strUserName, String strPassword) throws Exception{
	   	if(Utilities.isTextOnPage(rwd, "Logged in as")){
			ReusableFunc.ClickByLinkText(rwd, "Logout");
		   	Utilities.fluentWait(By.linkText("Login"), rwd);
		}
		//click login
	   	WebElement eleLogin = rwd.findElement(By.xpath(".//*[contains(@id, 'LoginHyperLink')]"));
	   	Thread.sleep(1000);
		eleLogin.click();
		
		//Enter username 
		ReusableFunc.EnterTextInTextBox(rwd, rwd.findElement(By.xpath(".//*[contains(@id, 'Login_txtLoginEmailAddress')]")), strUserName);
		//Enter password
		ReusableFunc.EnterTextInTextBox(rwd, rwd.findElement(By.xpath(".//*[contains(@id, 'Login_txtPassword')]")), strPassword);
		//Click login
		ReusableFunc.ClickByXpath(rwd, ".//*[contains(@id, 'Login_btnLogin')]", "Login");
		Thread.sleep(1000);

		//ReusableFunc.VerifyTextOnPage(rwd,"You are logged in as");
		//click logout
		//Thread.sleep(1000);
		//rwd.manage().window().maximize();
		//Thread.sleep(1000);

      }
     
    
    public static void connectLogout(RemoteWebDriver rwd) throws Exception{
    	
    	if(Utilities.isTextOnPage(rwd, "Logged in as")){
			ReusableFunc.ClickByLinkText(rwd, "Logout");
			Thread.sleep(1000);
		   	Utilities.fluentWait(By.linkText("Login"), rwd);
 		}
    	
    	
    }
    
	public static void connectVerifyMyProfilePage (RemoteWebDriver rwd) throws Exception{
		//Click the My Profile link to go to the My Profile page
		ReusableFunc.ClickByLinkText(rwd, "My Profile");
		//Verify the country for the logged in user is displayed
    	ReusableFunc.VerifyTextOnPage(rwd, "South Africa");
    	//Verify the My Newsletters link is available on the My Details page
    	ReusableFunc.VerifyTextOnPage(rwd, "My Newsletters");
    	//Verify the My Linked Smartcards field is available on the My Details page
    	ReusableFunc.VerifyTextOnPage(rwd, "My linked Smartcards");
	}
	
	
	
	public static void connectUpdateMyDetails (RemoteWebDriver rwd, String strFirstName, String strSurName, String strProvnce) throws Exception{
			
			WebElement eleFnameTxtBox, eleSnameTxtBox, elePvinceTxtBox;
	    	//Go to My Details Page
	    	ReusableFunc.ClickByLinkText(rwd, "My Profile");
	    	//ReusableFunc.VerifyTextOnPage(rwd, "Selenium");
	    	ReusableFunc.ClickByLinkText(rwd, "My Details");
	    	ReusableFunc.VerifyTextOnPage(rwd, "Username");
	    	Thread.sleep(3000);
	    	//Input Firstname
	    	eleFnameTxtBox=rwd.findElement(By.xpath(".//*[contains(@id, 'Details1_txtFirstname')]"));
	    	eleFnameTxtBox.clear();
	    	ConnectControl.setFirstname(strFirstName);
	   		ReusableFunc.EnterTextInTextBox(rwd,eleFnameTxtBox, ConnectControl.getFirstname());
	   		//Input Surname
	   		eleSnameTxtBox=rwd.findElement(By.xpath(".//*[contains(@id, 'Details1_txtSurname')]"));
	   		eleSnameTxtBox.clear();
	    	ConnectControl.setSurname(strSurName);
	   		ReusableFunc.EnterTextInTextBox(rwd,eleSnameTxtBox, ConnectControl.getSurname());
			//Input a Province
	   		elePvinceTxtBox=rwd.findElement(By.xpath(".//*[contains(@id, 'Details1_ddlProvince')]"));
	    	ConnectControl.setProvince(strProvnce);
	   		ReusableFunc.EnterTextInTextBox(rwd,elePvinceTxtBox, ConnectControl.getProvince());
			//Save
			ReusableFunc.ClickByXpath(rwd, ".//*[contains(@id, 'Details1_btnSave')]", "Unable to locate Save Details button");
			
			}


	public static void connectVerifyMyDetails(RemoteWebDriver rwd) throws Exception{
		
	    	//Get the Firstname, Surname and Province from the My Details page
			//ReusableFunc.VerifyTextOnPage(rwd, "Selenium");
	    	String strFullName = ReusableFunc.getTextFromWebElement(rwd, By.xpath("#ctl00_BodyWrapper_Details1_txtFirstname"));
	    	String strPVince = ReusableFunc.getTextFromWebElement(rwd, By.xpath(".//*[@id='pubinboxfield']/btn"));
	    	//Go to My Profile Page
	    	org.testng.Assert.assertTrue(strFullName.contains(ConnectControl.getFirstname()));
	    	org.testng.Assert.assertTrue(strFullName.contains(ConnectControl.getSurname()));
	    	org.testng.Assert.assertEquals("The text does not match "+strProvince+"---"+strProvince, ConnectControl.getProvince(), strPVince);
	    	}
	
	
	//********************************************************************************************************************************************
	
	public static void UpdateMyDetails(RemoteWebDriver rwd, String strFirstName, String strSurName) throws Exception{
		
		WebElement eleFnameTxtBox, eleSnameTxtBox, elePvinceTxtBox;
    	//Go to My Details Page
    	ReusableFunc.ClickByLinkText(rwd, "My Profile");
    	//ReusableFunc.VerifyTextOnPage(rwd, "Selenium");
    	ReusableFunc.ClickByLinkText(rwd, "My Details");
    	ReusableFunc.VerifyTextOnPage(rwd, "Username");
    	Thread.sleep(3000);
    	//Input Firstname
    	eleFnameTxtBox=rwd.findElement(By.xpath(".//*[contains(@id, 'Details1_txtFirstname')]"));
    	eleFnameTxtBox.clear();
    	ConnectControl.setFirstname(strFirstName);
   		ReusableFunc.EnterTextInTextBox(rwd,eleFnameTxtBox, ConnectControl.getFirstname());
   		//Input Surname
   		eleSnameTxtBox=rwd.findElement(By.xpath(".//*[contains(@id, 'Details1_txtSurname')]"));
   		eleSnameTxtBox.clear();
    	ConnectControl.setSurname(strSurName);
   		ReusableFunc.EnterTextInTextBox(rwd,eleSnameTxtBox, ConnectControl.getSurname());	
		}


	
	
	
	 public static void responsiveConnectLogin(RemoteWebDriver driver, String strUserName, String strPassword) throws Exception{
		   //click login
		  	//ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'ctl26_HyperLink6')]", "login");
		  	ReusableFunc.ClickByLinkText(driver, "Login");
		  			
			//Enter username as automation2@mailinator.com   	
			Thread.sleep(1000);
			ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.xpath(".//*[contains(@id, 'Login_txtLoginEmailAddress')]")), strUserName);
			//Enter password as 123456
			ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.xpath(".//*[contains(@id, 'Login_txtPassword')]")), strPassword);
			//Click login
			Thread.sleep(1000);
			ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'Login_btnLogin')]", "Login");
			
			Thread.sleep(2000);
			ReusableFunc.VerifyTextOnPage(driver,"You are logged in as");
			//click logout
			/*Thread.sleep(1000);
			driver.manage().window().maximize();
			Thread.sleep(1000);*/

	      }
	 
	 
	 
	 public static void responsiveConnectRegister(RemoteWebDriver driver) throws Exception{
			
	    	WebElement textBoxEle;
	    	String strUserName;

		   	if(Utilities.isTextOnPage(driver, "Logged in as")){
				ReusableFunc.ClickByLinkText(driver, "Logout");
			   	Utilities.fluentWait(By.linkText("Login"), driver);
			}
		   	
		   	
		   	//Click Register
		   	ReusableFunc.ClickByLinkText(driver, "Register");
		   	//ReusableFunc.VerifyTextOnPage(driver, "Create your Connect ID");
		   	//ReusableFunc.VerifyTextOnPage(driver, "This will allow you to login to all");
		   	
		   do{

		   		textBoxEle=driver.findElement(By.xpath(".//*[contains(@id, 'ctrlSignIn_txtSignUpEmailAddress')]"));
		   		//Generate random 4 char string
		   		RandomString rs = new RandomString(4);

		    	strUserName="Automation"+rs.nextString();
		    	ConnectControl.setEmailAddy(strUserName + "@mailinator.com");
		    	textBoxEle.clear();
		   		ReusableFunc.EnterTextInTextBox(driver,textBoxEle, ConnectControl.getEmailAddy());
		   		driver.findElementById("txtSignUpPassword").clear();
		   		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("txtSignUpPassword"), "Password123");
		   		
		   		//check if checkbox is checked
		   		if(!driver.findElementById("chkAcceptTerms").isSelected())
		   			ReusableFunc.CheckCheckBox(driver, driver.findElementById("chkAcceptTerms"));
		   		
		   		ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'ctrlSignIn_btnRegister')]", "Cannot locate Register button");
		   		Thread.sleep(8000);
		
		   		
		   	}while(driver.findElementById("signupEmailAddressCustomValidator").isDisplayed());
		   	
		   	ReusableFunc.VerifyTextOnPage(driver, "To verify your email address ");
		   	ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'SignIn_btnBacktoSite')]", "Back to Site");
		}
	 
	 public static void ConnectRegisterIgnoreTerms(RemoteWebDriver driver) throws Exception{
			
	    	WebElement textBoxEle;
	    	String strUserName;

		   	if(Utilities.isTextOnPage(driver, "Logged in as")){
				ReusableFunc.ClickByLinkText(driver, "Logout");
			   	Utilities.fluentWait(By.linkText("Login"), driver);
			}
		   	
		   	
		   	//Click Register
		   	ReusableFunc.ClickByLinkText(driver, "Register");
		   	ReusableFunc.VerifyTextOnPage(driver, "Create your Connect ID");
		   	ReusableFunc.VerifyTextOnPage(driver, "This will allow you to login to all");
		   	
		   do{

		   		textBoxEle=driver.findElement(By.xpath(".//*[contains(@id, 'ctrlSignIn_txtSignUpEmailAddress')]"));
		   		//Generate random 4 char string
		   		RandomString rs = new RandomString(4);

		    	strUserName="Automation"+rs.nextString();
		    	ConnectControl.setEmailAddy(strUserName + "@mailinator.com");
		    	textBoxEle.clear();
		   		ReusableFunc.EnterTextInTextBox(driver,textBoxEle, ConnectControl.getEmailAddy());
		   		driver.findElementById("txtSignUpPassword").clear();
		   		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("txtSignUpPassword"), "Password123");
		   		
		   		//check if checkbox is checked
		   		if(!driver.findElementById("chkAcceptTerms").isSelected())
		   			//ReusableFunc.CheckCheckBox(driver, driver.findElementById("chkAcceptTerms"));
		   			
		   		
		   		ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'ctrlSignIn_btnRegister')]", "Cannot locate Register button");
		   		Thread.sleep(8000);
		
		   		
		   	}while(driver.findElementById("signupEmailAddressCustomValidator").isDisplayed());
		   	
		   	ReusableFunc.VerifyTextOnPage(driver, "Please accept terms");
		   	System.out.println("You cannot Register without accepting the terms and conditions");
		 
		}
	 
	 
	 
	 public static void  ConnectRegisterLoginBox(RemoteWebDriver rwd) throws Exception{
			
	    	WebElement textBoxEle;
	    	String strUserName;

		   	if(Utilities.isTextOnPage(rwd, "Logged in as")){
				ReusableFunc.ClickByLinkText(rwd, "Logout");
			   	Utilities.fluentWait(By.linkText("Login"), rwd);
			}
		   	
		   	
		   	//Click Login
		   	
		   	ReusableFunc.ClickByLinkText(rwd, "Register");
		   	ReusableFunc.VerifyTextOnPage(rwd, "Create your Connect ID");
		   	ReusableFunc.VerifyTextOnPage(rwd, "This will allow you to login to all");
		   	
		   do{

		   		textBoxEle=rwd.findElement(By.xpath(".//*[contains(@id, 'ctrlSignIn_txtSignUpEmailAddress')]"));
		   		//Generate random 4 char string
		   		RandomString rs = new RandomString(4);

		    	strUserName="Automation"+rs.nextString();
		    	ConnectControl.setEmailAddy(strUserName + "@mailinator.com");
		    	textBoxEle.clear();
		   		ReusableFunc.EnterTextInTextBox(rwd,textBoxEle, ConnectControl.getEmailAddy());
		   		rwd.findElementById("txtSignUpPassword").clear();
		   		ReusableFunc.EnterTextInTextBox(rwd, rwd.findElementById("txtSignUpPassword"), "Password123");
		   		
		   		//check if checkbox is checked
		   		if(!rwd.findElementById("chkAcceptTerms").isSelected())
		   			ReusableFunc.CheckCheckBox(rwd, rwd.findElementById("chkAcceptTerms"));
		   		
		   		ReusableFunc.ClickByXpath(rwd, ".//*[contains(@id, 'ctrlSignIn_btnRegister')]", "Cannot locate Register button");
		   		Thread.sleep(10000);
		
		   		
		   	}while(rwd.findElementById("signupEmailAddressCustomValidator").isDisplayed());
		   	
		   	ReusableFunc.VerifyTextOnPage(rwd, "To verify your email address ");
		}
		
		
	 public static void resetPassword(RemoteWebDriver driver, String emailAdd) throws Exception {
		 
		 //ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'ctl30_HyperLink7')]", "Forgot Password");
		 ReusableFunc.ClickByLinkText(driver, "Forgot password");
		 Thread.sleep(3000);
		 ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.xpath(".//*[contains(@id, 'ForgotPassword_txtFPEmailAddress')]")), emailAdd);
		 ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'ForgotPassword_btnSubmit')]", "Submit");
		 Thread.sleep(3000);
		 ReusableFunc.VerifyTextOnPage(driver,"Reset password mail sent");
		 ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'ctrlForgotPassword_btnBacktoSite')]", "Back To Site");
	 		 		 
	 
			}
	 
	 
	 @Test
		public static void loginWithFacebook(RemoteWebDriver driver, String strUserName, String strPassword , String Text) throws Exception {
			//click login
		    ReusableFunc.ClickByLinkText(driver, "Login");
		   	
		 // before any pop ups are open
			String parentHandle = driver.getWindowHandle();

			// click on link that will open in a new window
			ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'Login_lnkFacebook')]", "Login Using Facebook");
			Thread.sleep(3000);
		   	
		   	
			
			// after you have pop ups
			for (String popUpHandle : driver.getWindowHandles()) {
				if (!popUpHandle.equals(parentHandle)) {
					driver.switchTo().window(popUpHandle);
					if(driver.getTitle().equalsIgnoreCase("Facebook"))	
						Thread.sleep(2000);
						//Enter username 
						ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.xpath(".//*[contains(@id, 'email')]")), strUserName);
						//Enter password as 123456
						Thread.sleep(1000);
						ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.xpath(".//*[contains(@id, 'pass')]")), strPassword);
						Thread.sleep(1000);
						//Click Facebook login
						ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'u_0_1')]", "Facebook Login");
						Thread.sleep(2000);
						
						
					}
				}
			

			driver.switchTo().window(parentHandle);
			Thread.sleep(2000);
		    ReusableFunc.VerifyTextOnPage(driver, Text);
				
			
		}
           
	  
	
	 @Test
		public static void loginWithGmail(RemoteWebDriver driver, String strUserName, String strPassword, String Text) throws Exception {
			//click login
		    Thread.sleep(1000);
		    ReusableFunc.ClickByLinkText(driver, "Login");
		   	//ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'AuthenticationMenu1_ctl26_HyperLink6')]", "login");
		   	
			// click on Gmail link
			ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'Login_lnkGoogle')]", "Login Using Gmail");
			Thread.sleep(1000);
			//Enter username 
			ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.xpath(".//*[contains(@id, 'Email')]")), strUserName);
			//Enter password
			ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.xpath(".//*[contains(@id, 'Passwd')]")), strPassword);
			//Click Sing in to login with Gmail
			ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'signIn')]", "Gmail Sign in");
			Thread.sleep(1000);
		    ReusableFunc.VerifyTextOnPage(driver, "Text");
								
		}
        
	 @Test
	   public static void responsiveCanLinkSmartCard(RemoteWebDriver driver, String smartcardNumber, String strSurname) throws Exception{
	    	                 
	    	 
            for (int i=1;i<=2;i++){
                          
           ReusableFunc.ClickByLinkText(driver, "My Profile");
           ReusableFunc.VerifyTextOnPage(driver, "My Profile");
           ReusableFunc.ClickByLinkText(driver, "My Smartcards");
           ReusableFunc.VerifyTextOnPage(driver, "Link Smartcards");
           
          
           
           WebElement smartcard = driver.findElementByXPath(".//*[contains(@id, 'MyAccount1_txtSmartCardNumber')]");
           smartcard.sendKeys(smartcardNumber);
           
           WebElement surname = driver.findElementByXPath(".//*[contains(@id, 'MyAccount1_txtAccountSurname')]");
           surname.sendKeys(strSurname);
           

           ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'MyAccount1_btnAccountSave')]", "Save");
           
           Thread.sleep(2000);
           ReusableFunc.CheckCheckBox(driver, driver.findElementByXPath(".//*[contains(@id, 'MyAccount1_repSmartCards_c1_0')]"));
           
                             
           WebElement description = driver.findElementByXPath(".//*[contains(@id, 'MyAccount1_repSmartCards_tbxDescription_0')]");
           description.sendKeys("home");
           
           ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'MyAccount1_btnSaveSmartcards')]", "Link Smartcard");
           System.out.println("Linked smartcard "+i+" times");
           ReusableFunc.VerifyTextOnPage(driver, smartcardNumber);
           
           
           ReusableFunc.ClickByLinkText(driver, "My Smartcards");
           ReusableFunc.VerifyTextOnPage(driver, "Link Smartcards");
           ReusableFunc.CheckCheckBox(driver, driver.findElementByXPath(".//*[contains(@id, 'MyAccount1_repSmartCards_c1_0')]"));
           ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'MyAccount1_btnSaveSmartcards')]", "Link Smartcard");
           
           System.out.println("Unlinked smartcard "+i+" times");
           
           ReusableFunc.VerifyTextOnPage(driver, "My Profile");
           ReusableFunc.FailIfTextOnPage(driver, smartcardNumber);
	        }
	                  
            ReusableFunc.ClickByLinkText(driver, "Logout");
	       }
	  
//***********************************************************************************************************************************	
	 //ConnectLite Controls
	 
	 public static void connectLiteLoginEmail(RemoteWebDriver driver, String strEmail, String strPassword, String text) throws Exception{

			//Enter username
			ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.xpath(".//*[@id='email']")), strEmail);
			//Enter password as 123456
			ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.xpath(".//*[@id='password']")), strPassword);
			//Click login
			Thread.sleep(2500);
		//	ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'login')]", "Login");
			ReusableFunc.verifyElementIsDisplayed(driver, By.id("login"), "Login");
			ReusableFunc.ClickByID(driver, "login");
//			Utilities.fluentWait(By.linkText(text), driver);
			
			ReusableFunc.VerifyTextOnPage(driver,text);
			
	      }
	 
	 public static void connectLiteLoginMobile(RemoteWebDriver driver, String strMobileNumber, String strPassword, String text) throws Exception{

			//Enter username
			ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.xpath(".//*[contains(@id, 'mobilenumber')]")), strMobileNumber);
			//Enter password as 123456
			ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.xpath(".//*[contains(@id, 'password')]")), strPassword);
			//Click login
			Thread.sleep(1000);
			ReusableFunc.ClickByID(driver, "login");
			Thread.sleep(2000);
			ReusableFunc.VerifyTextOnPage(driver,text);
			
	      }
	 
	 public static void connectLiteLoginFacebook(RemoteWebDriver driver, String strUserName, String strPassword, String text) throws Exception{

			//Enter username
			ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.xpath(".//*[contains(@id, 'email')]")), strUserName);
			//Enter password as 123456
			ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.xpath(".//*[contains(@id, 'pass')]")), strPassword);
			//Click login
			Thread.sleep(20000);
			ReusableFunc.ClickByID(driver, "loginbutton");
			Thread.sleep(2000);
			ReusableFunc.VerifyTextOnPage(driver,text);
			
	      }
	 
	 public static void connectLiteLoginGoogle(RemoteWebDriver driver, String strUserName, String strPassword, String text) throws Exception{

			//Enter username
		 	Thread.sleep(1500);
			ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.xpath(".//*[contains(@id, 'Email')]")), strUserName);
			Thread.sleep(1500);
			//Enter password as 123456
			ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.xpath(".//*[contains(@id, 'Passwd')]")), strPassword);
			//Click login
			Thread.sleep(1000);
			ReusableFunc.ClickByID(driver, "signIn");
			Thread.sleep(2000);
			ReusableFunc.VerifyTextOnPage(driver,text);
			
	      }
	 
	 public static void connectLiteLoginYahoo(RemoteWebDriver driver, String strUserName, String strPassword) throws Exception{

			//Enter username
		 	Thread.sleep(1500);
			ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.xpath(".//*[contains(@id, 'login-username')]")), strUserName);
			Thread.sleep(1500);
			//Enter password as 123456
			ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.xpath(".//*[contains(@id, 'login-passwd')]")), strPassword);
			//Click login
			Thread.sleep(1000);
			ReusableFunc.ClickByID(driver, "login-signin");
			
	      }
	 
	 public static void connectLiteLogout(RemoteWebDriver rwd) throws Exception{
	    		
		 		Thread.sleep(1500);
		 		ReusableFunc.verifyElementIsDisplayed(rwd, By.xpath(".//*[@id='connect']/div/div[2]/div/div[6]/a"), "Logout");
		 		ReusableFunc.verifyElementIsDisplayed(rwd, By.linkText("Logout"), "Logout");
		 		ReusableFunc.verifyTextInWebElement(rwd, By.xpath(".//*[@id='connect']/div/div[2]/div/div[6]/a"), "Logout");
		 		Thread.sleep(1000);
	    		ReusableFunc.ClickByLinkText(rwd, "Logout");
	    		Thread.sleep(2000);
//				ReusableFunc.ClickByXpath(rwd, ".//*[@id='connect']/div/div[2]/div/div[6]/a", "Unable to locate Logout button");
				ReusableFunc.VerifyTextOnPage(rwd, "Log in with your Connect ID");
	    	
	    	
	    }
	 
	 @Test
	   public static void connectLiteCanLinkSmartCard(RemoteWebDriver driver, String smartcardNumber, String strSurname, String idCountryList, String xpathCountry) throws Exception{
	    	                 
	    	 
         // for (int i=1;i<=2;i++){
                        
         ReusableFunc.VerifyTextOnPage(driver, "Smartcards");
         ReusableFunc.ClickByLinkText(driver, "Smartcards");
         ReusableFunc.VerifyTextOnPage(driver, "✓ DStv subscription customer number:");
         
        
         
         WebElement smartcard = driver.findElementByXPath(".//*[contains(@id, 'Number')]");
         smartcard.sendKeys(smartcardNumber);
         
         WebElement surname = driver.findElementByXPath(".//*[contains(@id, 'Surname')]");
         surname.sendKeys(strSurname);
         
         ReusableFunc.ClickDropdownElement(driver, By.id(idCountryList), By.xpath(xpathCountry));

         ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'save-details')]", "Save");
         
       
                           
         /*WebElement description = driver.findElementByXPath(".//*[contains(@id, 'MyAccount1_repSmartCards_tbxDescription_0')]");
         description.sendKeys("home");
         
         ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'MyAccount1_btnSaveSmartcards')]", "Link Smartcard");
         System.out.println("Linked smartcard "+i+" times");
         ReusableFunc.VerifyTextOnPage(driver, smartcardNumber);*/
         
         
        /* ReusableFunc.ClickByLinkText(driver, "My Smartcards");
         ReusableFunc.VerifyTextOnPage(driver, "Link Smartcards");
         ReusableFunc.CheckCheckBox(driver, driver.findElementByXPath(".//*[contains(@id, 'MyAccount1_repSmartCards_c1_0')]"));
         ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'MyAccount1_btnSaveSmartcards')]", "Link Smartcard");
         
         System.out.println("Unlinked smartcard "+i+" times");
         
         ReusableFunc.VerifyTextOnPage(driver, "My Profile");
         ReusableFunc.FailIfTextOnPage(driver, smartcardNumber);
	        }
	                  
          ReusableFunc.ClickByLinkText(driver, "Logout");*/
	       }
	 public static void connectLiteClickUsername(RemoteWebDriver rwd, String strUsername) throws Exception{
//	    	ReusableFunc.VerifyPageTitle(rwd, "DStv Connect");
		 	Thread.sleep(1500);
	    	ReusableFunc.VerifyTextOnPage(rwd, strUsername);
	    	ReusableFunc.ClickByXpath(rwd, ".//*[@id='connect']/div/div[1]/span[1]", strUsername);
	 		}
	// ********************************************************************************************************************************	  
	 public static void connectLiteVerifyPageIsLoaded(RemoteWebDriver rwd, String strPageTitle) throws Exception{
		 int timeOut = 2000;
		 
		Utilities.isTitlePresentAfterWait(strPageTitle, rwd);
		
//		Utilities.fluentWait(By.className("btn btn-progress btn-full btn-submit"), rwd);
		Utilities.waitForElement(rwd, By.id("login"), timeOut).isEnabled();
		
		if (Utilities.waitForElement(rwd, By.id("login"), timeOut).isEnabled()){
			
			ReusableFunc.verifyElementIsDisplayed(rwd, By.id("login"), "Login button");
			System.out.print("Success! Login button was enabled");
		}
		else
		{
			rwd.navigate().refresh();
			System.out.print("Login button was not enabled...page has been refreshed!");
		
		}
		 
	 		}
	//*********************************************************************************************************************************
	 public static void connectLiteYahooLoginIcon(RemoteWebDriver driver, String strUserName, String strPassword, String text) throws Exception{

		 	ReusableFunc.ClickByXpath(driver, "html/body/div[2]/div[2]/form/div[9]/div/div[2]/a[1]/img", "Unable to locate Yahoo link");
			ReusableFunc.VerifyPageTitle(driver, "Yahoo - login");
			ReusableFunc.VerifyTextOnPage(driver, "Sign in to your account");
			Thread.sleep(5000);
			ConnectControl.connectLiteLoginYahoo(driver, strUserName, strPassword);
			ReusableFunc.VerifyPageTitle(driver, "Yahoo");
			Thread.sleep(1500);
			ReusableFunc.ClickByXpath(driver, ".//*[@id='oauth2-agree']", "Agree");
			ReusableFunc.VerifyTextOnPage(driver, text);
			Thread.sleep(2000);
			
	      }
	 //********************************************************************************************************************************
	 public static void connectLiteYahooLoginIconLoggedIn(RemoteWebDriver driver, String text) throws Exception{

		 	ReusableFunc.ClickByXpath(driver, "html/body/div[2]/div[2]/form/div[9]/div/div[2]/a[1]/img", "Unable to locate Yahoo link");
			ReusableFunc.VerifyPageTitle(driver, "Yahoo");
			ReusableFunc.ClickByXpath(driver, ".//*[@id='oauth2-agree']", "Agree");
			ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[1]/span[1]"), text);
			//Logout of Connect account
			ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[1]/span[1]"), text);
			Thread.sleep(2500);
			
	      }
	 //********************************************************************************************************************************
	 public static void connectLiteYahooLogout(RemoteWebDriver driver, String text) throws Exception{

		 	driver.get("https://us-mg4.mail.yahoo.com/neo/launch?.rand=52fn4vqd76nu4");
			ReusableFunc.VerifyTextOnPage(driver, text);
			ReusableFunc.navigateToURL(driver, "https://login.yahoo.com/config/login;_ylt=AhKumcZfFTwgob00XWTcGbm.ulI6?logout=1&.direct=2&.done=https://www.yahoo.com&.src=cdgm&.intl=us&.lang=en-US");
			ReusableFunc.VerifyTextOnPage(driver, "Sign in");
			
	      }
	 //********************************************************************************************************************************
	 public static void connectLiteGoogleLoginIcon(RemoteWebDriver driver, String strUserName, String strPassword, String text) throws Exception{

		 	ReusableFunc.ClickByXpath(driver, "html/body/div[2]/div[2]/form/div[9]/div/div[2]/a[2]/img", "Unable to locate Google link");
			ReusableFunc.VerifyTextOnPage(driver, "Sign in with your Google Account");
			Thread.sleep(5000);
			ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[contains(@id, 'Email')]"), strUserName);
			ReusableFunc.ClickByXpath(driver, ".//*[@id='next']", "Next");
			ReusableFunc.VerifyTextOnPage(driver, "Password");
			ReusableFunc.waitForElementToDisplay(driver, By.xpath("html/body/div[1]/div[2]/div[2]/div[1]/form/div[2]/div/div[1]/span"), 7);
			ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='back-arrow']"), "back-arrow");
//			ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='email-display']"), "ashenychowtheey@gmail.com");
			ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[contains(@id, 'Passwd')]"), strPassword);
			ReusableFunc.ClickByXpath(driver, ".//*[@id='signIn']", "SignIn");
			ReusableFunc.VerifyTextOnPage(driver, text);
			Thread.sleep(2000);
			
	      }
	//********************************************************************************************************************************
	 public static void connectLiteGoogleLoginIconLoggedIn(RemoteWebDriver driver, String text) throws Exception{

		 	ReusableFunc.ClickByXpath(driver, "html/body/div[2]/div[2]/form/div[9]/div/div[2]/a[2]/img", "Unable to locate Google link");
			ReusableFunc.VerifyPageTitle(driver, "DStv Connect");
			ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[1]/span[1]"), text);
			Thread.sleep(2500);
			
	      }
	 //********************************************************************************************************************************
	 public static void connectLiteGoogleLogout(RemoteWebDriver driver, String text) throws Exception{

		 	driver.get("http://www.googlemail.com");
			ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='gb']/div[1]/div[1]/div[1]/div/span"), text);
			ReusableFunc.ClickByXpath(driver, ".//*[@id='gb']/div[1]/div[1]/div[2]/div[4]/div[1]/a/span", "Google drop down");
			Thread.sleep(1500);
			ReusableFunc.ClickByXpath(driver, ".//*[@id='gb_71']", "Sign out");
			
	      }
	//********************************************************************************************************************************
	 public static void connectLiteFacebookLoginIcon(RemoteWebDriver driver, String strUserName, String strPassword, String text) throws Exception{

		 	ReusableFunc.ClickByXpath(driver, "html/body/div[2]/div[2]/form/div[9]/div/div[2]/a[3]/img", "Unable to locate Facebook link");
		 	ReusableFunc.VerifyPageTitle(driver, "Log into Facebook | Facebook");
			ReusableFunc.VerifyTextOnPage(driver, "Log into Facebook");
			ConnectControl.connectLiteLoginFacebook(driver, strUserName, strPassword, text);
			ReusableFunc.VerifyTextOnPage(driver, text);
			Thread.sleep(2500);
			
	      }
	//********************************************************************************************************************************
	 public static void connectLiteFacebookLoginIconLoggedIn(RemoteWebDriver driver, String text) throws Exception{

		 	ReusableFunc.VerifyPageTitle(driver, "DStv Connect");
			ReusableFunc.ClickByXpath(driver, "html/body/div[2]/div[2]/form/div[9]/div/div[2]/a[3]/img", "Unable to locate Facebook link");
			ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[3]/div/div/form/div[2]/span/span"), text);
			
	      }
	 //*******************************************************************************************************************************
	 public static void connectLiteFacebookLogout(RemoteWebDriver driver, String text) throws Exception{

		 	driver.get("http://www.facebook.com");
			ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='u_0_1']/div[1]/div[1]/div/a/span"), text);
			ReusableFunc.ClickByXpath(driver, ".//*[@id='userNavigationLabel']", "Account Settings");
			Thread.sleep(1500);
			ReusableFunc.ClickByLinkText(driver, "Log Out");
			
	      }
	//*******************************************************************************************************************************
	 public static void connectSearchMailinatorForForgotPasswordEmail(RemoteWebDriver driver, String text, String strEmailAddress) throws Exception{

		 	ReusableFunc.navigateToURL(driver, "http://www.mailinator.com");
			ReusableFunc.VerifyTextOnPage(driver, "Check An Inbox!");
			ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='inboxfield']"), strEmailAddress);
			ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div/div[1]/div[2]/div/div[2]/div/div/btn", "Check it");
			ReusableFunc.VerifyTextOnPage(driver, "Inbox for:");
			ReusableFunc.VerifyTextOnPage(driver, text);
			
	      }
	//*******************************************************************************************************************************
	 public static void connectGenerateRandomMailinatorEmailAddress(RemoteWebDriver driver, int connectEmailLength) throws Exception{

		 String strEmail = RandomStringGen.generateRandomString(connectEmailLength, RandomStringGen.Mode.ALPHANUMERIC);
			ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='email']"), strEmail+"@mailinator.com");
			
	      }
	 //******************************************************************************************************************************
	 public static void connectGenerateAndEnterRandomNameAndSurname(RemoteWebDriver driver, int connectFirstNameLength, int connectLastNameLength) throws Exception{

		 	driver.findElementByXPath(".//*[@id='FirstName']").clear();
			String fName=RandomStringGen.generateRandomString(connectFirstNameLength, RandomStringGen.Mode.ALPHA);
			ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("FirstName")),fName);
			driver.findElementByXPath(".//*[@id='Surname']").clear();
			String sName=RandomStringGen.generateRandomString(connectLastNameLength, RandomStringGen.Mode.ALPHA);
			ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("Surname")),sName);
			
	      }
	//******************************************************************************************************************************
	 public static void connectClickProfileMenuOption(RemoteWebDriver driver) throws Exception{
		 	
		 	Thread.sleep(1000);
		 	ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[2]/div/div[6]/a"), "Logout");
		 	ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[2]/div/div[2]/a/div/span"), "My Profile");
			ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[2]/a/div", "Could not locate Profile AuthMenu link");
			
	      }
	//******************************************************************************************************************************
	 public static void connectClickAboutMeMenuOption(RemoteWebDriver driver) throws Exception{

			ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div/div[1]/div[1]/div/a[2]/h4"), "About Me");
			ReusableFunc.ClickByXpath(driver, "html/body/div[4]/div/div[1]/div[1]/div/a[2]/h4", "About Me");
			
	      }
	//******************************************************************************************************************************
	 public static void connectClickMobileMenuOption(RemoteWebDriver driver) throws Exception{

		 	ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div/ul/li[2]/div/a[2]"), "Mobile");
			ReusableFunc.ClickByXpath(driver, "html/body/div[4]/div/ul/li[2]/div/a[1]", "Mobile");
			
	      }
	//******************************************************************************************************************************
	 public static void connectClickEmailMenuOption(RemoteWebDriver driver) throws Exception{

		 	ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div/ul/li[3]/div/a[2]"), "Email");
			ReusableFunc.ClickByXpath(driver, "html/body/div[4]/div/ul/li[3]/div/a[1]", "Email");
			
	      }
	//******************************************************************************************************************************
	 public static void connectClickPasswordMenuOption(RemoteWebDriver driver) throws Exception{

		 	ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div/ul/li[4]/div/a[2]"), "Password");
			ReusableFunc.ClickByXpath(driver, "html/body/div[4]/div/ul/li[4]/div/a[1]", "Password");
			
	      }
	//******************************************************************************************************************************
	 public static void connectClickDStvSubscriptionsMenuOption(RemoteWebDriver driver) throws Exception{
		 	
		 	Thread.sleep(1000);
		 	ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[2]/div/div[6]/a"), "Logout");
			ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[2]/div/div[3]/a/div/span"), "My DStv");
			ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[3]/a/div", "Could not locate My DStv AuthMenu link");
				
		      }
	//******************************************************************************************************************************
	 public static void connectClickOverviewMenuOption(RemoteWebDriver driver) throws Exception{

			ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[2]/div/div[2]/a/div/span"), "Overview");
			ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[2]/a/div/span", "Could not locate Overview AuthMenu link");
					
			      }
	//******************************************************************************************************************************
	 public static void connectClickNewsletterMenuOption(RemoteWebDriver driver) throws Exception{
		 	
		 	Thread.sleep(1000);
		 	ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[2]/div/div[6]/a"), "Logout");
			ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[2]/div/div[4]/a/div/span"), "My Newsletters");
			ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[4]/a/div", "Could not locate Newsletters AuthMenu link");
					
			      }
	//******************************************************************************************************************************
	 public static void connectClickMyPayuWalletMenuOption(RemoteWebDriver driver) throws Exception{
			 
		 	Thread.sleep(2000);
			ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[2]/div/div[6]/a"), "Logout");
			ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[2]/div/div[5]/a/div/span"), "My PayU Wallet");
			ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[5]/a/div/span", "Could not locate My PayU Wallet AuthMenu link");
				
		      }
	//******************************************************************************************************************************
	// Waiting 40 seconds for an element to be present on the page, checking
	   // for its presence once every 3 seconds.
	   
	 public static WebElement connectLoginJavaScriptfluentWait(final By locator, String strTitle, RemoteWebDriver rwd){
		 ReusableFunc.VerifyPageTitle(rwd, strTitle);
		 	
		      Wait<WebDriver> wait = new FluentWait<WebDriver>(rwd)
		              .withTimeout(40, TimeUnit.SECONDS)
		              .pollingEvery(3, TimeUnit.SECONDS)
		              .ignoring(NoSuchElementException.class);

		      WebElement foo = wait.until(
		    		  new Function<WebDriver, WebElement>() 
		    		  {
		    			  
		    			   public WebElement apply(WebDriver driver) 
		    			   {
		    				  driver.navigate().refresh();
		                      return driver.findElement(locator);
		    			  }
		              }
		    	);
		        return  foo;              
		        };
	//******************************************************************************************************************************
	//Read test data from properties file
		        public static Properties getProperties (String strFilePath){
		        File file = new File(strFilePath);
		  	  
				FileInputStream fileInput = null;
				try {
					fileInput = new FileInputStream(file);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				Properties prop = new Properties();
				
				//load properties file
				try {
					prop.load(fileInput);
				} catch (IOException e) {
					e.printStackTrace();
				}
				return prop;
		        }
		        
	//******************************************************************************************************************************
		        public static Boolean boolVerifyTextOnPage(RemoteWebDriver rwd, String text) throws Exception {
		       boolean boolIsSmartcardLinked = Utilities.isTextOnPage(rwd, text);
					return boolIsSmartcardLinked;
		    	}
		        
    //******************************************************************************************************************************
	public static void connectVerifySmartCardNotLinked(RemoteWebDriver rwd,
			String strLoginEmailAddress, String strLoginPassword,
			String strUsername, String strSmartCardDescription)
			throws Exception {

		ConnectControl.connectLoginJavaScriptfluentWait(By.id("login"),
				"DStv Connect", rwd);
		ReusableFunc.VerifyTextOnPage(rwd, "Log in with your Connect ID");
		ConnectControl.connectLiteLoginEmail(rwd, strLoginEmailAddress,
				strLoginPassword, "My DStv");

		if (ConnectControl.boolVerifyTextOnPage(rwd, strSmartCardDescription) != false) {

			System.out.print("Smartcard was already linked");
			ConnectControl.connectLiteClickUsername(rwd, strUsername);
			ConnectControl.connectClickDStvSubscriptionsMenuOption(rwd);
			ReusableFunc
					.VerifyTextOnPage(
							rwd,
							"These are your DStv Subscriptions. Linking your DStv Subscriptions to your online profile will enable you to manage your accounts, make payments online and enter exclusive competitions. Premium subscribers are also able watch live sport, rent movies and catch up with shows online.");
			ReusableFunc.ClickByXpath(rwd,
					"html/body/div[4]/div[3]/div/div/form/div[1]/div/button",
					"Unlink");
			ReusableFunc.verifyTextInWebElement(rwd,
					By.xpath("html/body/div[4]/div[2]/div/ul/li"),
					"Smartcard unlinked successfully");
			ConnectControl.connectLiteClickUsername(rwd, strUsername);
			ConnectControl.connectLiteLogout(rwd);
		} else {
			System.out.print("Smartcard was not linked");
			ConnectControl.connectLiteClickUsername(rwd, strUsername);
			ConnectControl.connectLiteLogout(rwd);

		}

	

		        	if (ConnectControl.boolVerifyTextOnPage(rwd, strSmartCardDescription)!=false){
		        		
		        		System.out.print("Smartcard was already linked");
		        		ConnectControl.connectLiteClickUsername(rwd, strUsername);
		    			ConnectControl.connectClickDStvSubscriptionsMenuOption(rwd);
		    			ReusableFunc.VerifyTextOnPage(rwd, "These are your DStv Subscriptions. Linking your DStv Subscriptions to your online profile will enable you to manage your accounts, make payments online and enter exclusive competitions. Premium subscribers are also able watch live sport, rent movies and catch up with shows online.");
		    			//ReusableFunc.ClickByXpath(rwd, , "Unlink");
		    			ReusableFunc.verifyTextInWebElement(rwd, By.xpath("html/body/div[4]/div[2]/div/ul/li"), "Smartcard unlinked successfully");
		    			ConnectControl.connectLiteClickUsername(rwd, strUsername);
		    			ConnectControl.connectLiteLogout(rwd);
		        	}
		        	else
		        	{ 
		        		System.out.print("Smartcard was not linked");
	    				ConnectControl.connectLiteClickUsername(rwd, strUsername);
	    				ConnectControl.connectLiteLogout(rwd);
		        		
		        	}
		        			
		        	}
		    		

	//*******************************************************************************************************************************
	public static void connectVerifyNewsletterNotSubscribed(
			RemoteWebDriver rwd, String strLoginEmailAddress,
			String strLoginPassword, String strUsername, String strNewsletter)
			throws Exception {

		ConnectControl.connectLoginJavaScriptfluentWait(By.id("login"),
				"DStv Connect", rwd);
		ReusableFunc.VerifyTextOnPage(rwd, "Log in with your Connect ID");
		ConnectControl.connectLiteLoginEmail(rwd, strLoginEmailAddress,
				strLoginPassword, "My Newsletters");

		if (ConnectControl.boolVerifyTextOnPage(rwd, strNewsletter) != false) {

			System.out.print("Newsletter was already subscribed to");
			ConnectControl.connectLiteClickUsername(rwd, strUsername);
			ConnectControl.connectClickNewsletterMenuOption(rwd);
			ReusableFunc.VerifyTextOnPage(rwd, "Newsletters");
			ReusableFunc.ClickByLinkText(rwd, "Unsubscribe");
			ReusableFunc
					.verifyTextInWebElement(rwd,
							By.xpath("html/body/div[4]/div[3]/div/ul/li"),
							"Successfully unsubscribed from the DStv email newsletter.");
			ConnectControl.connectLiteClickUsername(rwd, strUsername);
			ConnectControl.connectLiteLogout(rwd);
		} else {
			System.out.print("Newsletter was not already subscribed to");
			ConnectControl.connectLiteClickUsername(rwd, strUsername);
			ConnectControl.connectLiteLogout(rwd);

		}

	}
	//***********************************************************************************************************************************
	public static void selectRadioButton(RemoteWebDriver driver,
			String specifyIfClassOrid, String InputIdOrClassName,
			String EnterClassOrIdValue) throws Exception {

		driver.findElement(
				By.cssSelector("input[" + specifyIfClassOrid + "='"
						+ InputIdOrClassName + "']" + "[value='"
						+ EnterClassOrIdValue + "']")).click();
	}

	//***********************************************************************************************************************************
	public static void waitForLoginButton(RemoteWebDriver driver, String strTitle) {
		ReusableFunc.VerifyPageTitle(driver, strTitle);
		driver.findElement(By.xpath(".//*[@id='login']"));
		// Wait for element to be clickable
		WebDriverWait wait = new WebDriverWait(driver, 5);
//		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#login")));
		for (int i=0; i<=5; i++){
			
		if (driver.findElementByCssSelector("#login").isEnabled()==false)
		{
			driver.navigate().refresh();
			ReusableFunc.VerifyPageTitle(driver, strTitle);
			driver.findElementByCssSelector("#login").isEnabled();
//			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#login")));
		}
		}
		System.out.print("Login button is now clickable");
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#login")));
		// driver.findElement(By.cssSelector("#login")).click();
	}

}
	 
	 

