package com.dstvo.test.automation.dstvfunctionlib;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class ReusableFunc {
	private static final RemoteWebDriver Id = null;
	private static final By By = null;
	private static final RemoteWebDriver RemoteWebDriver = null;
	
	private static int rowIndex;
	private static int columnIndex;
	
	public static void ClickMenuItem(WebDriver driver, String menuLink)
			throws Exception {

		// WebElement element =
		// driver.findElement(By.className("global_nav")).findElement(By.linkText(menuLink));

		WebElement element = driver.findElement(By.className("global_nav"));
		element = element.findElement(By.linkText(menuLink));

		if (element.isDisplayed()) {
			element.click();
		} else {
			throw new Exception("Link can not be found" + menuLink);
		}
	}
	
	public static void clickOptionFromMenuList(WebDriver driver, String menuClassName, int navigationOption) 
	{		
		List<WebElement> menuElements = driver.findElements(By.className("menuClassName"));
		menuElements.get(navigationOption).click();
	}

	public static int Login(WebDriver driver, String emailAdd, String password) {

		WebElement element = driver.findElement(By.linkText("Login"));
		element.click();

		element = Utilities.waitForElement(driver,
				By.className("EmailAddressTextBoxSelector"), 5000);
		element.sendKeys(emailAdd);// "yuvesheng@gmail.com");

		element = driver.findElement(By.className("PasswordTextBoxSelector"));
		element.sendKeys(password); // "yuveshen1982");

		element = driver.findElement(By
				.xpath("//div[@class='connectform']//input[@type='image']"));
		element.click();

		element = driver.findElement(By.linkText("Logout"));
		if (element.isDisplayed()) {
			driver.close();
			Utilities.log("You were sucessfully logged in");
			return 1;
		}
		throw new RuntimeException("Could not Login");
	}

	public static int LoginLightBox(WebDriver driver, String emailAdd,
			String password) {

		WebElement element = driver.findElement(By.linkText("Login"));
		element.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException ex) {
			Logger.getLogger(ReusableFunc.class.getName()).log(Level.SEVERE,
					null, ex);
		}
		// Switch to LightBox
		element = Utilities.waitForElement(driver,By.xpath("//div[@class='jquery-lightbox-move']//iframe[@src='/login.aspx']"),5000);
		driver.switchTo().frame(element);
		@SuppressWarnings("unused")
		WebElement editable = driver.switchTo().activeElement();

		element = Utilities.waitForElement(driver,By.className("EmailAddressTextBoxSelector"), 5000);
		element.sendKeys(emailAdd);// ("yuvesheng@gmail.com");

		element = driver.findElement(By.className("PasswordTextBoxSelector"));
		element.sendKeys(password); // ("yuveshen1982");

		element = driver.findElement(By.xpath("//div[@class='connectform']//input[@type='image']"));
		element.click();

		// Switch back from LightBox
		driver.switchTo().defaultContent();

		element = Utilities.waitForElement(driver, By.linkText("Logout"), 5000);
		// element = driver.findElement(By.linkText("Logout"));
		if (element.isDisplayed()) {
			Utilities.log("You were sucessfully logged in");
			element.click();
			return 1;
		}
		throw new RuntimeException("Could not Login");
	}

	public static void ResetPassword(WebDriver driver, String emailAdd,
			String menuLink) throws Exception {

		WebElement element = driver
				.findElement(By.linkText("Forgot Password?"));
		element.click();

		// Switch to LightBox
		element = Utilities
				.waitForElement(
						driver,
						By.xpath("//div[@class='jquery-lightbox-move']//iframe[@src='/forgotten.aspx']"),
						5000);
		driver.switchTo().frame(element);
		@SuppressWarnings("unused")
		WebElement editable = driver.switchTo().activeElement();

		// element = DriverExtentions.waitForElement(driver,
		// By.className("connectinputBoxes"), 5000);
		element = Utilities
				.waitForElement(
						driver,
						By.xpath("//div[@class='connectinputBoxes']//input[@type='text']"),
						5000);
		element.sendKeys(emailAdd); // ("yuvesheng@gmail.com");

		element = driver.findElement(By
				.xpath("//div[@class='connectform']//input[@type='image']"));
		element.click();

		// If this is there then email invalid : Email address is incorrect.

		org.testng.Assert.assertFalse(
				Utilities.isTextOnPage(driver, "Email address is incorrect"),
				"Invalid Email Address");

		// Switch back from LightBox
		driver.switchTo().defaultContent();

		// junit.framework.Assert.assertTrue("Could not see Find Out More.",
		// DriverExtentions.isTextOnPage(driver, "How do I get DStv"));
		element = Utilities.waitForElement(driver,
				By.className("formThnxHead"), 5000);
		if (element.isDisplayed()) {
			ReusableFunc.ClickMenuItem(driver, menuLink);
			System.out.println("Password Reset Success Screen visible");
		} else {
			Utilities.log("Password Reset Success Screen not visible");
		}

	}

	public static void RegisterFailbyUsername(WebDriver driver, String userName)
			throws Exception {

		WebElement element = driver.findElement(By.linkText("Register"));
		element.click();

		element = Utilities.waitForElement(driver,By.xpath("//div[@class='connectinputBoxes']//input[@tabindex='1']"),5000);
		element.sendKeys(userName); // ("yuvesheng@gmail.com");

		element = driver.findElement(By.xpath("//div[@class='connectform']//input[@src='App_Themes/Connect/Images/btn_checkavail.gif']"));
		// element =
		// driver.findElement(By.xpath("//div[@class='connectform']//input[@type='image']"));
		element.click();
		Utilities.waitForElement(driver, "Chosen username is not available",5000, "Username possible available");
		// junit.framework.Assert.assertTrue("Chosen username is not available",
		// DriverExtentions.isTextOnPage(driver,
		// "Chosen username is not available"));
	}

	public static void Register(WebDriver driver, String userName,String emailAdd, String password) throws Exception {

		WebElement element = driver.findElement(By.linkText("Register"));
		element.click();

		element = Utilities.waitForElement(driver,By.xpath("//div[@class='connectinputBoxes']//input[@tabindex='1']"),5000);
		element.sendKeys(userName);

		element = driver.findElement(By.xpath("//div[@class='connectform']//input[@src='App_Themes/Connect/Images/btn_checkavail.gif']"));
		// element =
		// driver.findElement(By.xpath("//div[@class='connectform']//input[@type='image']"));
		element.click();

		Utilities.waitForElement(driver, "Chosen username is available", 5000,"Username possibly used");
		// junit.framework.Assert.assertFalse("Chosen username is not available",
		// DriverExtentions.isTextOnPage(driver,
		// "Chosen username is not available"));

		element = Utilities.waitForElement(driver,By.xpath("//div[@class='connectinputBoxes']//input[@tabindex='4']"),5000);
		element.sendKeys(emailAdd);

		element = Utilities.waitForElement(driver,By.xpath("//div[@class='connectinputBoxes']//input[@tabindex='5']"),5000);
		element.sendKeys(password);

		element = Utilities.waitForElement(driver,By.xpath("//div[@class='connectinputBoxes']//input[@tabindex='6']"),5000);
		element.sendKeys(password);

		element = Utilities.waitForElement(driver, By.xpath("//div[@class='connectform']//input[@type='checkbox']"),5000);
		element.click();

		element = driver.findElement(By.xpath("//div[@class='connectform']//input[@src='App_Themes/Connect/Images/btn_register.gif']"));
		element.click();

		Utilities.isTextOnPage(driver, "Thank you for registering");
		//
		// element = driver.findElement(By.linkText("Logout"));
		// if (element.isDisplayed())
		// {
		// driver.close();
		// System.out.println("You were sucessfully logged in");
		//
		// }
		// throw new RuntimeException("Could not Login");
	}

	// xpath=//div[matches(@id,'che.*boxes')]
	// (this, of course, would click the div with 'id=checkboxes', or
	// 'id=cheANYTHINGHEREboxes')

	// Function to click any link by text
	public static void ClickByLinkText(RemoteWebDriver driver,String strTextLink) throws Exception {
		Utilities.fluentWait(By.linkText(strTextLink), driver);

		WebElement element = driver.findElement(By.linkText(strTextLink));

		if (element.isDisplayed())
			element.click();
		else
			throw new Exception("Link can not be found " + strTextLink);

	}

	// Function to click any link by XPATH
	// strXpath = xpath of the element to click
	// strIdentifier = Name to easily identify element represented by xpath
	public static void ClickByXpath(RemoteWebDriver driver, String strXpath,String strIdentifier) throws Exception {
		try {
			Utilities.fluentWait(By.xpath(strXpath), driver);
			WebElement element = driver.findElement(By.xpath(strXpath));

			if (element.isDisplayed())
				element.click();
			else
				throw new Exception("Link can not be found " + strIdentifier);
		} catch (Exception e) {
			throw new Exception("Link can not be found " + strIdentifier);
		}

	}

	// Function to click element by ID
	// ********************************************************************************************************************************
	public static void ClickByID(RemoteWebDriver driver, String strID)
			throws Exception {

		Utilities.fluentWait(org.openqa.selenium.By.id(strID), driver);
		WebElement element = driver.findElement(org.openqa.selenium.By.id(strID));

		if (element.isDisplayed())
			element.click();
		else
			throw new Exception("Link can not be found " + strID);

	}
	
	public static void ClickByID(RemoteWebDriver driver, String strID,String strIdentifier)
			throws Exception {

		try {
			Utilities.fluentWait(org.openqa.selenium.By.id(strID), driver);
			WebElement element = driver.findElement(org.openqa.selenium.By.id(strID));

			if (element.isDisplayed())
				element.click();
			else
				throw new Exception("Link can not be found " + strIdentifier);
		} catch (Exception e) {
			throw new Exception("Link can not be found " + strIdentifier);
		}

	}

	// ********************************************************************************************************************************

	public static void ClickByCss(RemoteWebDriver driver, String strSelector)throws Exception {

		Utilities.fluentWait(By.cssSelector(strSelector), driver);
		WebElement element = driver.findElement(By.cssSelector(strSelector));

		if (element.isDisplayed())
			element.click();
		else
			throw new Exception("Link can not be found " + strSelector);
	}
	
	public static void ClickByCss(RemoteWebDriver driver, String strSelector, String strIdentifier)throws Exception {
		
		try {
			Utilities.fluentWait(org.openqa.selenium.By.cssSelector(strSelector), driver);
			WebElement element = driver.findElement(org.openqa.selenium.By.cssSelector(strSelector));

			if (element.isDisplayed())
				element.click();
			else
				throw new Exception("Link can not be found " + strIdentifier);
		} catch (Exception e) {
			throw new Exception("Link can not be found " + strIdentifier);
		}
	}

	// ********************************************************************************************************************************
	public static void VerifyTextOnPage(RemoteWebDriver rwd, String text) throws Exception {

		Utilities.isTextPresentAfterWait(text, rwd);
		org.testng.Assert.assertTrue(Utilities.isTextOnPage(rwd, text), "Text "
				+ text + " not found on Page");
	}

	// ********************************************************************************************************************************
	public static void FailIfTextOnPage(RemoteWebDriver rwd, String strText) throws Exception {
		org.testng.Assert.assertFalse(Utilities.isTextOnPage(rwd, strText),
				"Text ' " + strText + " 'found on page");
	}

	// ********************************************************************************************************************************
	public static void VerifyPageTitle(RemoteWebDriver rwd, String strTitle) {
		Utilities.isTitlePresentAfterWait(strTitle, rwd);
		org.testng.Assert.assertEquals(strTitle, rwd.getTitle());
	}

	// ********************************************************************************************************************************
	public static void ContainsPageTitle(RemoteWebDriver rwd, String strTitle) {

		Utilities.containsTitleAfterWait(strTitle, rwd);
		org.testng.Assert.assertTrue(rwd.getTitle().contains(strTitle));

	}

	// ********************************************************************************************************************************
	public static void EnterTextInTextBox(RemoteWebDriver rwd,WebElement eleTextBox, String strText) {
		eleTextBox.clear();
		eleTextBox.sendKeys(strText);
	}

	// ********************************************************************************************************************************

	public static void CheckCheckBox(RemoteWebDriver rwd, WebElement eleCheckBox) {
		eleCheckBox.click();
	}

	// ********************************************************************************************************************************

	public static String getTextFromWebElement(RemoteWebDriver rwd, By by) {

		WebElement ele = rwd.findElement(by);
		Utilities.fluentWait(by, rwd);
		return ele.getText();
	}

	// ********************************************************************************************************************************
	public static void verifyTextInWebElement(RemoteWebDriver rwd, By by,String strText) {

		Utilities.fluentWait(by, rwd);
		WebElement ele = rwd.findElement(by);
		String strResult = ele.getText();
		org.testng.Assert.assertEquals(strResult, strText, "The text does not match " + strText + "---" + strResult);

	}

	// *******************************************************************************************************************************

	public static void verifyAttributeInWebElement(RemoteWebDriver rwd, By by,String elementAttribute, String expectedAttributeName) {

		WebElement ele = rwd.findElement(by);
		String actualAttributeName = ele.getAttribute(elementAttribute);
		org.testng.Assert.assertEquals(actualAttributeName, expectedAttributeName,"The Attribute does not match " + actualAttributeName + "---"+ expectedAttributeName);

	}

	// *******************************************************************************************************************************

	public static void verifyElementIsDisplayed(RemoteWebDriver rwd, By by,	String strElement) {

		Utilities.fluentWait(by, rwd);
		org.testng.Assert.assertTrue(rwd.findElement(by).isDisplayed(),strElement + " is not displayed");
		
		

	}
	

//*******************************************************************************************************************************
	public static void verifyElementAbsent(RemoteWebDriver rwd, By by, String strElement) {
		
		Utilities.fluentWait(by, rwd);
		org.testng.Assert.assertFalse(rwd.findElement(by).isDisplayed(), strElement + " is Present");
		
	}

	// ********************************************************************************************************************************

	public static void verifyIdElementIsDisplayed(RemoteWebDriver driver,WebElement findElementById) {
		Utilities.fluentWait(By, driver);
		org.testng.Assert.assertTrue(driver.findElement(By).isDisplayed(), Id + " is not displayed");

	}

	// ********************************************************************************************************************************

    public static void clearTextBox(RemoteWebDriver rwd,By by){
        
        WebElement ele =rwd.findElement(by);
        ele.clear();
 }


 // ********************************************************************************************************************************

	public static void ClickSubNav(RemoteWebDriver driver, By By, By by) throws Exception {
		WebElement menuElement;
		WebElement submenuElement;

		// Move cursor to the Main Menu Element
		menuElement = driver.findElement(By);
		Actions builder = new Actions(driver);
		builder.moveToElement(menuElement).perform();
		// Giving 5 Secs for submenu to be displayed
		//Thread.sleep(5000);
		Utilities.fluentWait(by, driver);
		// Clicking on the Hidden SubMenu
		submenuElement = driver.findElement(by);
		
		Thread.sleep(3000);
		if (submenuElement.isDisplayed()){
			submenuElement.click();
		}
		else{
			System.out.println("There is no such element");
		}

	}

	// ********************************************************************************************************************************

	
		
	public static void HoverOver(RemoteWebDriver driver, By By, By by, String actualMessage) throws Exception {
		WebElement hova;
		WebElement popupText;

		// Move cursor to the Main Menu Element
		hova = driver.findElement(By);
		Actions builder = new Actions(driver);
		builder.moveToElement(hova).perform();
		// Giving 5 Secs for text to be displayed
		Utilities.fluentWait(by, driver);
		
		
		popupText = driver.findElement(by);
		String expectedText = popupText.getText();
		
		org.testng.Assert.assertEquals(actualMessage, expectedText,"The text does not match " + expectedText + "---" + actualMessage);
		
		/*
		
		Utilities.fluentWait(by, driver);
		Thread.sleep(3000);
		if (submenuElement.isDisplayed()){
			submenuElement.click();
		}
		else{
			System.out.println("There is no such element");
		}*/

	}
	
	// ********************************************************************************************************************************


	public static void CompareUrls(RemoteWebDriver driver, String expectedUrl)
			throws Exception {

		String actualUrl = driver.getCurrentUrl();
		org.testng.AssertJUnit.assertEquals(actualUrl, expectedUrl);

	}
	
	//*********************************************************************************************************************************
	public static void navigateToURL(RemoteWebDriver rwd, String strURL){ 
    	rwd.get(strURL);
    }

	// ********************************************************************************************************************************

	public static void SwitchWindows(RemoteWebDriver driver, By By,String popupUrl, String textOnPage, String pageTitle)throws Exception {

		// before any pop ups are open
		String parentHandle = driver.getWindowHandle();

		WebElement popuplink;

		// click on link that will open in a new window
		popuplink = driver.findElement(By);
		popuplink.click();
		Thread.sleep(2000);

		// after you have pop ups
		for (String popUpHandle : driver.getWindowHandles()) {
			if (!popUpHandle.equals(parentHandle)) {
				driver.switchTo().window(popUpHandle);
				if (driver.getCurrentUrl().equalsIgnoreCase(popupUrl)){
					ReusableFunc.VerifyTextOnPage(driver, textOnPage);
					ReusableFunc.ContainsPageTitle(driver, pageTitle);
				}
			}
		}
		Thread.sleep(2000);
		driver.switchTo().window(parentHandle);
	}
	
	
	public static void megaNavSwitchWindow(RemoteWebDriver driver, By By, By by,String popupUrl, String textOnPage, String pageTitle)throws Exception {
		
		// before any pop ups are open
		String parentHandle = driver.getWindowHandle();
		WebElement menuElement;
		WebElement popuplink;

		// Move cursor to the Main Menu Element
		menuElement = driver.findElement(By);
		Actions builder = new Actions(driver);
		builder.moveToElement(menuElement).perform();
		// Giving 5 Secs for submenu to be displayed
		Thread.sleep(5000);

		// click on link that will open in a new window ie.the link which was hidden
		popuplink = driver.findElement(by);
		popuplink.click();
		Thread.sleep(2000);

		// after you have pop ups
		for (String popUpHandle : driver.getWindowHandles()) {
			if (!popUpHandle.equals(parentHandle)) {
				driver.switchTo().window(popUpHandle);
				if (driver.getCurrentUrl().equalsIgnoreCase(popupUrl)) {
					ReusableFunc.VerifyTextOnPage(driver, textOnPage);
					ReusableFunc.ContainsPageTitle(driver, pageTitle);

				}
			}
		}

		driver.switchTo().window(parentHandle);
	}
	
	
	

	// ********************************************************************************************************************************

public static void ClickMegaNavSwitchWindow(RemoteWebDriver driver, By By, By by,String popupUrl)throws Exception {
		
		// before any pop ups are open
		String parentHandle = driver.getWindowHandle();
	
		
		WebElement menuElement = driver.findElement(By);
		menuElement.click();
		Thread.sleep(2000);

		// click on link that will open in a new window ie.the link which was hidden
		WebElement popupLink = driver.findElement(by);
		popupLink.isDisplayed();
		popupLink.click();	 
		Thread.sleep(2000);

		// after you have a popup
		for (String popUpHandle : driver.getWindowHandles()) {
			if (!popUpHandle.equals(parentHandle)) {
				driver.switchTo().window(popUpHandle);
				if (driver.getCurrentUrl().equalsIgnoreCase(popupUrl)) {
				System.out.println("this is a new window");

				}
			}
		}

		driver.switchTo().window(parentHandle);
	}
	
	
	public static void ClickPagination(RemoteWebDriver driver, By By) {

		List<WebElement> pagination = driver.findElements(By);
		int size = pagination.size();
		System.out.println(pagination.size());
		if (size > 0) {
			System.out.println("pagination exists");
			int numberofpages = pagination.get(1).findElements(By.tagName("li")).size()-2;
			// click on pagination link
			for (int i = 2; i < numberofpages+1; i++) {
				pagination = driver.findElements(By);
				WebElement li = pagination.get(1).findElements(By.tagName("li")).get(i);
				System.out.println(li.getTagName());
				WebElement link = li.findElement(By.tagName("a"));
				System.out.println(link.getText());
				link.click();
				try {
					Thread.sleep(5000);
				} catch (Exception e) {
				}
			}
			driver.quit();
		} else {
			System.out.println("pagination does not exist");
		}
	}
	//html/body/form/div[6]/div[2]/div/div[4]/div/div/ul/li[4]
	// ********************************************************************************************************************************

/*public static void ClickDropdownElement(RemoteWebDriver driver,By by, String optionToSelect) throws Exception{
	
	//First, get the WebElement for the select tag 
	WebElement selectElement = driver.findElement(by); 
	
	Thread.sleep(2000);

	//Then instantiate the Select class with that WebElement 
	Select select = new Select(selectElement); 

	//Get a list of the options 
	List <WebElement> options = select.getOptions(); 

	// For each option in the list, verify if it's the one you want and then click it 
	for (WebElement option: options) { 
	if (option.getText().equals(optionToSelect)){ 
	option.click(); 
	break; 
	} 
	} 
	} */
	
	
	public static void ClickDropdownElement(RemoteWebDriver driver,By By, By by) throws Exception {
		
		WebElement dropdownlist	= driver.findElement(By);
		dropdownlist.click(); 
		Thread.sleep(2000);
		WebElement dropdownElement = driver.findElement(by);
		dropdownElement.isDisplayed();
		dropdownElement.click();	 
	}
	

	 public static void NewCMSLogin(RemoteWebDriver driver, By By, By by, String strID) throws Exception {
	        //Enter username 	
			ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By), "testautomationcms@gmail.com");
			//Enter password
			ReusableFunc.EnterTextInTextBox(driver, driver.findElement(by), "Ch33530mle773");
			//Click login
			ReusableFunc.ClickByID(driver, strID);
						

	 }
	 
	public static void initializeEventLocation(RemoteWebDriver driver,By dayXpath,By tableXp,String time,int pageNumber)throws Exception {
		int currentTime = Integer.parseInt(time);
		Random random = new Random();
		//ReusableFunc.setRowIndex(7);
		//ReusableFunc.setColumnIndex(1);
		
		for(int p = 1; p <= pageNumber; p++){
			WebElement link = driver.findElement(dayXpath);
			if(p == pageNumber){
				WebElement channelTable= driver.findElement(tableXp);
				List<WebElement> rows = channelTable.findElements(org.openqa.selenium.By.tagName("tr"));
				Iterator<WebElement> i = rows.iterator();
				int numberOfEvent = rows.size();
				int rowNum = 1;
				int colNum = 0;
				while(i.hasNext()){
				    WebElement row=i.next();
				    List<WebElement> columns= row.findElements(org.openqa.selenium.By.tagName("td"));
				    Iterator<WebElement> j = columns.iterator();
				    colNum = 1;
				    while(j.hasNext()){
				    	WebElement column1 = j.next();
				    	WebElement column2 = j.next();
				    	String eventTimeStr = column1.getText();
				    	
				    	int eventTime = Integer.parseInt(eventTimeStr.substring(0, 2));
				    	//Events playing today: Check Time constraints
				    	if(pageNumber == 1){
				    		if (eventTime >= currentTime) {
					    		System.out.println("eventTime >= currentTime? Yes");
								int randomEvent = random.nextInt(numberOfEvent - rowNum)+ rowNum;
								System.out.println("numberOfEvent: " + numberOfEvent + " rowNum: "+ rowNum + " randomEvent: "+ randomEvent);
								
								for (int r = rowNum; r <= numberOfEvent; r++) {
									//System.out.println("Current Row:	"+ r + " Target Row: "+ randomEvent );
									if (randomEvent == r) {
										//System.out.println("randomEvent == r? Yes"); 
										ReusableFunc.setRowIndex(r);
										ReusableFunc.setColumnIndex(1);
										return;
									}else{
										//System.out.println("randomEvent == r? No continue Searching");
										continue;
									}	
								}
							}else{
								//System.out.println("eventTime >= currentTime? No continue Searching");
								continue;
							}
				    	}else{//Events From second day On: Check Time constraints
				    		int randomEvent = random.nextInt(numberOfEvent - rowNum)+ rowNum;
							for (int r = rowNum; r <= numberOfEvent; r++) {
								//System.out.println("Current Row:	"+ r + " Target Row: "+ randomEvent );
								if (randomEvent == r) {
									//System.out.println("randomEvent == r? Yes");
									System.out.println("Chosen Event "+ column1.getText() + " " + column2.getText());
									ReusableFunc.setRowIndex(r);
									ReusableFunc.setColumnIndex(1);
									return;
								}else{
									//System.out.println("randomEvent == r? No continue Searching");
									continue;
								}
							}
				    	}
				    	colNum++;
				    }
				    rowNum++;
				}
			}else{//move to the next page
				link.click();
			}	
			Thread.sleep(2000);
		}
	}
	
	public static int getEventPage(){
		int currentPage = 1;
		int lastPage = 4;
		return Utilities.generateRandomInt(currentPage, lastPage);
	}
	
	public static String getCurrentTime(){
		Calendar c = Calendar.getInstance();
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH");
		c.setTime(new Date());
		String currentTimeStr = timeFormat.format(c.getTime());
		return currentTimeStr;
	}
	 
	public static int getRowIndex() {
		return rowIndex;
	}

	public static void setRowIndex(int rowIndex) {
		ReusableFunc.rowIndex = rowIndex;
	}

	public static int getColumnIndex() {
		return columnIndex;
	}
	
	public static void setColumnIndex(int columnIndex) {
		ReusableFunc.columnIndex = columnIndex;
	}
	
	public static int getChannelByBouquet(RemoteWebDriver driver,By by){
		int channel = 1;
		WebElement channelSelector= driver.findElement(by);
		System.out.println("channelSelector.getText()" + channelSelector.getText());
		if(channelSelector.getText().equalsIgnoreCase("DStv Premium"))
			return  Utilities.generateRandomInt(1, 137);
		else if(channelSelector.getText().equalsIgnoreCase("DStv Extra"))
			return  Utilities.generateRandomInt(1, 97);
		else if(channelSelector.getText().equalsIgnoreCase("DStv Compact"))
			return  Utilities.generateRandomInt(1, 84);
		else if(channelSelector.getText().equalsIgnoreCase("DStv Family"))
			return  Utilities.generateRandomInt(1, 63);
		else if(channelSelector.getText().equalsIgnoreCase("DStv Access"))
			return  Utilities.generateRandomInt(1, 50);
		else if(channelSelector.getText().equalsIgnoreCase("DStv EasyView"))
			return  Utilities.generateRandomInt(1, 25);
		else if(channelSelector.getText().equalsIgnoreCase("DStv Indian South"))
			return  Utilities.generateRandomInt(1, 14);
		else if(channelSelector.getText().equalsIgnoreCase("DStv Indian North"))
			return  Utilities.generateRandomInt(1, 17);
		else if(channelSelector.getText().equalsIgnoreCase("DStv Portuguesa"))
			return  Utilities.generateRandomInt(1, 9);
		else if(channelSelector.getText().equalsIgnoreCase("DStv Mobile"))
			return  Utilities.generateRandomInt(1, 16);
		else if(channelSelector.getText().equalsIgnoreCase("DStv Mobile DVBH"))
			return  Utilities.generateRandomInt(1, 17);
		else
			return  channel;
	}
	
	public static int getBouquet(){
		return  Utilities.generateRandomInt(1, 11);
	}
	
	public static void searchBySearchKey(RemoteWebDriver driver,By searchBox, By searchIcon, String searchText) 
	{
		WebElement ele = driver.findElement(searchBox);
		ele.click();
		ele.sendKeys(searchText);
		driver.findElement(searchIcon).click();
		
	}
	
	public static void searchByEnterKey(RemoteWebDriver driver,By searchBox, String searchText) throws Exception
	{
		WebElement ele = driver.findElement(searchBox);
		ele.click();
		ele.sendKeys(searchText);
		ele.sendKeys(Keys.ENTER);
	}
	
	public static String getPartialText(RemoteWebDriver driver,By by) 
	{
		WebElement miniArticleHeading = driver.findElement(by);
		String miniArticleHeadingTxt = miniArticleHeading.getText();
		String articleHeadingSubString = miniArticleHeadingTxt.substring(miniArticleHeadingTxt.length() - 3).equalsIgnoreCase("...")? miniArticleHeadingTxt.substring(0 ,miniArticleHeadingTxt.length()-3)  : miniArticleHeadingTxt;
		return articleHeadingSubString;
	}
	
	public static void  verifyContainsPatialText(RemoteWebDriver driver,By by,String partialText){
		WebElement mainText = driver.findElement(by);
		String mainTestStr = mainText.getText(); 
		org.testng.Assert.assertTrue(mainTestStr.contains(partialText));
	}
	
	public static void  paginateForwardThenBackward(RemoteWebDriver driver, By slides, By  forwardArrow, By BackwardArrow) throws Exception{
		List<WebElement> elements = driver.findElements(slides);
		WebElement forwardLink = driver.findElement(forwardArrow);
		WebElement backwardLink = driver.findElement(BackwardArrow);
		
		int NumberOfSlides = elements.size();
		if (NumberOfSlides > 0) {
			for(int f = 0; f <= NumberOfSlides; f++){
				forwardLink.click();
				Thread.sleep(2000);		
			}
			
			for(int b = 0; b <= NumberOfSlides; b++){
				backwardLink.click();
				Thread.sleep(2000);		
			}
			
		} else 
			System.out.println("Pagination does not exist");
	}
	
	public static void filterSearchResults(RemoteWebDriver driver,By dropdownLink,By dropdownItem) throws Exception{
		WebElement dropdownlist	= driver.findElement(dropdownLink);
		WebElement dropdownElement = driver.findElement(dropdownItem);
		dropdownlist.click();
		if (dropdownElement.isDisplayed()){
			if(dropdownElement.isEnabled()){
				String parentHandle = driver.getWindowHandle();
				dropdownElement.click();
				driver.navigate().to(dropdownElement.getAttribute("value"));
				Thread.sleep(2000);
				for (String popUpHandle : driver.getWindowHandles()) {
					if (!popUpHandle.equals(parentHandle)) {
						driver.switchTo().window(popUpHandle);
						if (driver.getCurrentUrl().equalsIgnoreCase(dropdownElement.getAttribute("value"))){
							ReusableFunc.VerifyTextOnPage(driver, dropdownElement.getText());
							ReusableFunc.ContainsPageTitle(driver, driver.getTitle());
						}
					}
				}
				Thread.sleep(2000);
				driver.switchTo().window(parentHandle);
			}else
				System.out.println("No results are currently available for " +dropdownElement.getText());
		}
		else
			throw new Exception("Link can not be found " + dropdownElement.getText());		
	}
	
	public static void waitForElementToDisplay(RemoteWebDriver driver,By by,int timePeriodInSeconds){
		WebDriverWait wait = new WebDriverWait(driver, timePeriodInSeconds);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}
	
	public static void waitForElementToBeClickable(RemoteWebDriver driver,By by,int timePeriodInSeconds){
		WebDriverWait wait = new WebDriverWait(driver, timePeriodInSeconds);
		wait.until(ExpectedConditions.elementToBeClickable(by));
	}

	public static String getAttributeInAWebElement(RemoteWebDriver driver,By by, String attribute) {
		// TODO Auto-generated method stub
		WebElement el = driver.findElement(by);
		return el.getAttribute(attribute);
		
	}
	
}
	



