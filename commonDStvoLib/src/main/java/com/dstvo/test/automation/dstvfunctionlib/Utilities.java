package com.dstvo.test.automation.dstvfunctionlib;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import com.google.common.base.Function;
public class Utilities {

    public static WebElement waitForElement(WebDriver driver, By by, int timeout) {
        // Sleep until the dv we want is visible or 5 seconds is over
            long end = System.currentTimeMillis() + timeout;
            while (System.currentTimeMillis() < end) {
                // Browsers which render content (such as Firefox and IE) return "RenderedWebElements"
                
                List<WebElement> elements = driver.findElements(by);

                // If results have been returned, the results are displayed in a drop down.
                if (elements.toArray().length > 0) {
                    return elements.get(0);            
                }
            }        
            
            throw new RuntimeException("Could not find element " + by.toString());
    }
    
    public static void waitForElement(WebDriver driver, String text, int timeout,String errorMSG) throws Exception {
        // Sleep until the dv we want is visible or 5 seconds is over
            long end = System.currentTimeMillis() + timeout;
            while (System.currentTimeMillis() < end) {
                                
                if (isTextOnPage(driver, text)) {
                    return;
                }                
            }        
            
            throw new RuntimeException("Could not find text " + text + " - " + errorMSG);
    }
    
    public static boolean isTextOnPage(WebDriver driver, String text) throws Exception {
        return (driver.getPageSource().indexOf(text) >= 0);
    }
    
  
  public static int generateNum(){
   
    Random randomGenerator = new Random();
    //for (int idx = 1; idx <= 10; ++idx){
      int randomInt = randomGenerator.nextInt(10000);
      log("Generated : " + randomInt);
      return randomInt;
    //}
    
   // log("Done.");
  }
  
  
  public static int generateRandomInt(int intMin,int intMax){
	  
	  //int max=9999,min=1000;
	  Random randNum = new Random();
	  
	  
	  return(randNum.nextInt(intMax - intMin+1) + intMin);
  
  }
 
  public static void log(String aMessage){
    System.out.println(aMessage);
  }
  
  public static WebElement fluentWait(final By locator, RemoteWebDriver rwd){
      Wait<WebDriver> wait = new FluentWait<WebDriver>(rwd)
              .withTimeout(40, TimeUnit.SECONDS)
              .pollingEvery(5, TimeUnit.SECONDS)
              .ignoring(NoSuchElementException.class);

      WebElement foo = wait.until(
    		  new Function<WebDriver, WebElement>() {
    			  public WebElement apply(WebDriver driver) {
                      return driver.findElement(locator);
    			  }
              }
    	);
        return  foo;              
        };
        
        
/*        public static Boolean isTextPresentAfterWait(final String strStringToAppear, RemoteWebDriver rwd){
            Wait<WebDriver> wait = new FluentWait<WebDriver>(rwd)
                    .withTimeout(30, TimeUnit.SECONDS)
                    .pollingEvery(2, TimeUnit.SECONDS)
                    .ignoring(NoSuchElementException.class);

            Boolean foo = wait.until(new ExpectedCondition<Boolean>() {
                public Boolean apply(final WebDriver webDriver) {
                	//return webDriver.findElement(locator).getText().contains(strStringToAppear);
                	return (webDriver.getPageSource().indexOf(strStringToAppear) >= 0);
                }
           });
              return  foo;              
              };    
        */

        public static Boolean isTextPresentAfterWait(final String strStringToAppear, RemoteWebDriver rwd){
        	try{
                  Wait<WebDriver> wait = new FluentWait<WebDriver>(rwd)
                          .withTimeout(40, TimeUnit.SECONDS)
                          .pollingEvery(5, TimeUnit.SECONDS)
                          .ignoring(NoSuchElementException.class);

                  Boolean foo = wait.until(new ExpectedCondition<Boolean>() {
                      public Boolean apply(final WebDriver webDriver) {
                      	//return webDriver.findElement(locator).getText().contains(strStringToAppear);
                      	return (webDriver.getPageSource().indexOf(strStringToAppear) >= 0);
                      }
                 });
                    return  foo;              
                      
                    
        }
        catch(TimeoutException e){
        	throw new RuntimeException("Could not find text " + strStringToAppear + " - " + e);
        }
        };
        
        
        public static Boolean isTitlePresentAfterWait(final String strPageTitle, RemoteWebDriver rwd){
        	try{
                  Wait<WebDriver> wait = new FluentWait<WebDriver>(rwd)
                          .withTimeout(40, TimeUnit.SECONDS)
                          .pollingEvery(5, TimeUnit.SECONDS)
                          .ignoring(NoSuchElementException.class);

                  Boolean foo = wait.until(new ExpectedCondition<Boolean>() {
                      public Boolean apply(final WebDriver webDriver) {
                      	//return webDriver.findElement(locator).getText().contains(strStringToAppear);
                      	return ((webDriver.getTitle().equals(strPageTitle)));
                      }
                 });
                    return  foo;              
                      
                    
        }
        catch(TimeoutException e){
        	throw new RuntimeException("Could not find Title " + strPageTitle + " - " + e);
        }
        };
        
        public static Boolean containsTitleAfterWait(final String strPageTitle, RemoteWebDriver rwd){
        	try{
                  Wait<WebDriver> wait = new FluentWait<WebDriver>(rwd)
                          .withTimeout(40, TimeUnit.SECONDS)
                          .pollingEvery(5, TimeUnit.SECONDS)
                          .ignoring(NoSuchElementException.class);

                  Boolean foo = wait.until(new ExpectedCondition<Boolean>() {
                      public Boolean apply(final WebDriver webDriver) {
                      	//return webDriver.findElement(locator).getText().contains(strStringToAppear);
                      	//return ((webDriver.getTitle().equals(strPageTitle)));
                    	  return ((webDriver.getTitle().contains(strPageTitle)));
                      }
                 });
                    return  foo;              
                      
                    
        }
        catch(TimeoutException e){
        	throw new RuntimeException("Could not find Title " + strPageTitle + " - " + e);
        }
        };
   
}
