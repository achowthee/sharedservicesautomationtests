package com.dstvo.test.automation.connectWcf;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.*;
import org.junit.*;
import java.net.URL;


public class TestBase {
	  //  private static ChromeDriverService service;
	    public static RemoteWebDriver driver;  //was private, changed to use in other tests       
	    public RemoteWebDriver mailDriver;
 
	    @Before
	    public void before() throws Exception {
//	    	driver = new RemoteWebDriver(new URL("http://10.10.24.179:4444/wd/hub"), DesiredCapabilities.firefox());
	    	driver = new FirefoxDriver();

	    }

	    @After
	    public void after() {
	        if (driver != null) {
	        driver.quit();
	        }
	    }
	    
	    protected String getSiteURL (){
	        String URL = System.getProperty("testUrl");
	        if (URL == null){
	            return "http://qaconnectlite.dstv.com/4.1";
	        	//return "http://www.dstv.com";
	        
	        }
	        else{
	                return URL;
	        }
	        
	    }
	    
	    protected RemoteWebDriver getWebDriver() {
	        return driver;
	    }
	}

