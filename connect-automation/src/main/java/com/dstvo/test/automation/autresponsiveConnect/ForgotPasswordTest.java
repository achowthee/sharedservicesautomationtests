package com.dstvo.test.automation.autresponsiveConnect;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ForgotPasswordTest extends TestBase {

	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}


	

	@Test
	public void autForgotPassword() throws Exception {

		ConnectControl.resetPassword(driver, "bnplou@gmail.com");
		driver.quit();
	
		
	}
	
	

}
