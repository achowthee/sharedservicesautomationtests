package com.dstvo.test.automation.autresponsiveConnect;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class LoginTest extends TestBase {

	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}

	@Test
	public void autlogin() throws Exception {
		ConnectControl.responsiveConnectLogin(driver, "bnplou@gmail.com", "123456");
		Thread.sleep(1000);
		ConnectControl.connectLogout(driver);
		driver.quit();

	}
} 
