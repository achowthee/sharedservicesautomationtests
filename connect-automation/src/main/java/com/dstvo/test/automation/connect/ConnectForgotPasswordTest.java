package com.dstvo.test.automation.connect;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ConnectForgotPasswordTest extends TestBase{
	
	
    @BeforeMethod
	@Override
    public  void before() throws Exception {
        super.before();
        getWebDriver().get(getSiteURL()); 
    }
    
    @Test
    public void canResetPassword() throws Exception{
    	String strNewPassword;
    	ReusableFunc.ClickByLinkText(driver, "Forgot password");
    	ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.xpath(".//*[contains(@id, 'ForgotPassword_txtFPEmailAddress')]")), "forgotconnect@mailinator.com");
    	ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'ForgotPassword_btnSubmit')]", "Cannot locate the Submit button");
    	
    	AssertJUnit.assertTrue("Reset password mail sent", driver.findElement(By.xpath(".//*[contains(@id, 'ForgotPassword_lblNotifyH4')]")).isDisplayed());
    	driver.close();
    	mailDriver = new RemoteWebDriver(new URL("http://197.80.203.161:4453/wd/hub"), DesiredCapabilities.firefox());
    	mailDriver.get("http://mailinator.com/");

    	
     	WebElement mailTextBoxEle;
     	mailTextBoxEle=mailDriver.findElement(By.id("inboxfield"));
     	ReusableFunc.EnterTextInTextBox(mailDriver, mailTextBoxEle, "forgotconnect@mailinator.com");
     	Thread.sleep(6000);
     	ReusableFunc.ClickByXpath(mailDriver, "/html/body/div[2]/div/div/div[2]/div/div[2]/div/div/btn", "Check it");
     	Thread.sleep(10000);
       	ReusableFunc.VerifyTextOnPage(mailDriver, "Reset your password");
     	ReusableFunc.ClickByXpath(mailDriver, ".//*[@id='mailcontainer']/li[1]/a/div[2]", "Reset your password link");
     	ReusableFunc.VerifyTextOnPage(mailDriver, "You have requested to reset your password.");
     	Thread.sleep(4000);
     	
     	ReusableFunc.ClickByXpath(mailDriver, ".//*[@id='mailshowdiv']/div[3]/div/div/table/tbody/tr/td/table/tbody/tr[6]/td/table/tbody/tr[3]/td[2]/a/img", "Reset your password");
     	Thread.sleep(4000);
     	//generate random password
     	RandomString rs = new RandomString(8);
     	strNewPassword = rs.nextString();
     	ReusableFunc.EnterTextInTextBox(mailDriver, mailDriver.findElement(By.xpath(".//*[contains(@id, 'ForgotPassword_PasswordTextBox')]")), strNewPassword);
     	ReusableFunc.EnterTextInTextBox(mailDriver, mailDriver.findElement(By.xpath(".//*[contains(@id, 'ForgotPassword_RepeatPasswordTextBox')]")), strNewPassword);
     	ReusableFunc.ClickByXpath(mailDriver, ".//*[contains(@id, 'ForgotPassword_btnReset')]", "Unable to locate Reset Password button");

     	
     	AssertJUnit.assertTrue("Your password reset is successful", mailDriver.findElementById("AuthenticationMenu1_ctrlForgotPassword_lblNotifyH4").isDisplayed());
     	     	
    	//#########################################################################
    	mailDriver.close();
    	driver = new RemoteWebDriver(new URL("http://197.80.203.161:4453/wd/hub"), DesiredCapabilities.firefox());
    	driver.get("http://autconnect.dstv.com");
    	Thread.sleep(2000);
    	ConnectControl.canLogin(driver,"forgotconnect@mailinator.com",strNewPassword);
	
    }
    

}
