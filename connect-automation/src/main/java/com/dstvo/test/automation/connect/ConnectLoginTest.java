package com.dstvo.test.automation.connect;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import org.openqa.selenium.By;
import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ConnectLoginTest extends TestBase{


    @BeforeMethod
	@Override
    public  void before() throws Exception {
        super.before();
        getWebDriver().get(getSiteURL()); 
    } 
    
    
  
    
   @Test
    public void canLoginViaConnectControl() throws Exception{

	   	ConnectControl.canLogin(driver,"Selenium@mailinator.com","123456");
    	
   }
   
   @Test
   public void canAccessForgotPassword() throws Exception{
	   
		ReusableFunc.ClickByLinkText(driver, "LoginTest");
		ReusableFunc.VerifyTextOnPage(driver, "Enter your email address");
		ReusableFunc.VerifyTextOnPage(driver, "Enter your registered email address");
		
	   
	   ReusableFunc.ClickByID(driver, "jumptosecondmodal4");
	   Thread.sleep(2000);
	   
	   AssertJUnit.assertTrue("Enter your registered email address - Message NOt Found", driver.findElementById("AuthenticationMenu1_ctrlForgotPassword_txtFPEmailAddress").isDisplayed());

   }
   
   
   @Test
   public void canAccessRegister() throws Exception{
	   
       // Act
   	if(Utilities.isTextOnPage(driver, "Logged in as")){
   		ReusableFunc.ClickByLinkText(driver, "Logout");
   	   	Utilities.fluentWait(By.linkText("LoginTest"), driver);
   	}
	   
		ReusableFunc.ClickByLinkText(driver, "LoginTest");
		ReusableFunc.VerifyTextOnPage(driver, "Enter your email address");
		ReusableFunc.VerifyTextOnPage(driver, "Enter your registered email address");
		
	   
	   ReusableFunc.ClickByID(driver, "jumptosecondmodal");
	   Thread.sleep(2000);
	   
	   ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='divSignup']/h3/span"), "RegisterTest");
	   
	  // org.junit.Assert.assertTrue("LoginTest with your DStv Connect details - Message NOt Found", driver.findElementByXPath(".//*[@id='divSignup']/div[2]/div[1]/h4/span").isDisplayed());

   }

}
