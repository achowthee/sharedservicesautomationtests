package com.dstvo.test.automation.connect;

import org.junit.Before;
import org.junit.Test;
import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;

public class ConnectMyProfileTest extends TestBase{
	

    @Before
    @Override
    public  void before() throws Exception {
        super.before();        
        getWebDriver().get(getSiteURL());
       
    } 
    
    
    @Test
    public void canViewMyDetails() throws Exception{
    	
    		//LoginTest
    	Thread.sleep(2000);
    		ConnectControl.connectLogin(driver, "selenium@mailinator.com", "123456");
    		//Check that the relevant options and links are present on the My Profile page
    		ConnectControl.connectVerifyMyProfilePage(driver);
    		//Logout
    		Thread.sleep(1000);
    		ConnectControl.connectLogout(driver);
    		Thread.sleep(1000);
	    	}
       
    @Test
	public void canEditMyDetails() throws Exception{
    	
    		//LoginTest
    	Thread.sleep(2000);
    		ConnectControl.connectLogin(driver, "selenium@mailinator.com", "123456");
    		//Update My Details
    		ConnectControl.connectUpdateMyDetails(driver, "Ashen", "Chowthee", "Gauteng");
    		//Verify Updates
    		ConnectControl.connectVerifyMyDetails(driver);
    		//Update My Details
    		ConnectControl.connectUpdateMyDetails(driver, "Name", "Surname", "Limpopo");
    		//Logout
    		Thread.sleep(1000);
    		ConnectControl.connectLogout(driver);
    		Thread.sleep(1000);

			}
   }