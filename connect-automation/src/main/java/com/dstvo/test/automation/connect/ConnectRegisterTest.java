package com.dstvo.test.automation.connect;

import java.net.URL;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ConnectRegisterTest extends TestBase{
	
	
    @Before
    @Override
    public  void before() throws Exception {
        super.before();
        getWebDriver().get(getSiteURL()); 
    } 
    
    @Test
    public void canRegisterViaConnectControl() throws Exception{
    	
    	ConnectControl.register(driver);
    	//driver.close();
    	mailDriver = new RemoteWebDriver(new URL("http://197.80.203.161:4453/wd/hub"), DesiredCapabilities.firefox());
    	mailDriver.get("http://sam-dev03/connect/ST/LoginTest.aspx");
    	ConnectControl.canVerifyViaCMS(mailDriver);
    	mailDriver.close();
    	ConnectControl.canLogin(driver,ConnectControl.getEmailAddy(),"Password123");
    }
    
   
    @Test
    public void canAccessTermsAndConditionsLink() throws Exception{
    	
	   	//Click RegisterTest
	   	ReusableFunc.ClickByLinkText(driver, "RegisterTest");
	   	ReusableFunc.VerifyTextOnPage(driver, "Create your DStv Connect account");
	   	ReusableFunc.VerifyTextOnPage(driver, "This will allow you to login to all");
	   	
	   	ReusableFunc.ClickByLinkText(driver, "terms & conditions");
	   	
	   	
	   	 for(String winHandle : driver.getWindowHandles()){
	         driver.switchTo().window(winHandle);
	     }
	//	ReusableFunc.VerifyPageTitle(driver, "DSTV.COM");
		ReusableFunc.VerifyTextOnPage(driver, "These Terms and Conditions are the general");
    }
    

    
    @Test
    public void canAccessAlreadyHaveAccount() throws Exception{
   
	   	//Click RegisterTest
	   	ReusableFunc.ClickByLinkText(driver, "RegisterTest");
	   	ReusableFunc.VerifyTextOnPage(driver, "Create your DStv Connect account");
	   	ReusableFunc.VerifyTextOnPage(driver, "This will allow you to login to all");
	   	
	   	ReusableFunc.ClickByID(driver, "jumptosecondmodal2");
	   	Thread.sleep(10000);
	   	
	   	org.junit.Assert.assertTrue("LoginTest with your DStv Connect details - Message NOt Found", driver.findElementByXPath(".//*[@id='divLogin']/div[2]/div[1]/h4").isDisplayed())		;
	   	
    	
    }
    
    @Test
    public void termsAndConditionsError() throws Exception{
    	
    	WebElement textBoxEle=driver.findElement(By.xpath(".//*[contains(@id, 'ctrlSignIn_txtSignUpEmailAddress')]"));
    	
	   	//Click RegisterTest
	   	ReusableFunc.ClickByLinkText(driver, "RegisterTest");
	   	ReusableFunc.VerifyTextOnPage(driver, "Create your DStv Connect account");
	   	ReusableFunc.VerifyTextOnPage(driver, "This will allow you to login to all");	
    	
	   	ReusableFunc.EnterTextInTextBox(driver,textBoxEle, "Asdf@asdf.com");
   		driver.findElementById("txtSignUpPassword").clear();
   		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("txtSignUpPassword"), "Password123");
   		ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'ctrlSignIn_btnRegister')]", "Cannot locate register button");

   		org.junit.Assert.assertTrue("Cant find text Please accept terms & conditions.", driver.findElement(By.xpath(".//*[contains(@id, 'ctrlSignIn_CustomValidator1')]")).isDisplayed());
    }
    
    


}
