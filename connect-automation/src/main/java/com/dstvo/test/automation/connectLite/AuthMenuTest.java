package com.dstvo.test.automation.connectLite;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class AuthMenuTest extends TestBase {

	@BeforeMethod
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}

	@Test
	public void liteAuthMenuTest() throws Exception {
		//Login
		ReusableFunc.VerifyPageTitle(driver, "DStv Connect");
		ReusableFunc.VerifyTextOnPage(driver, "Login with your Connect ID");
		Thread.sleep(5000);
		ConnectControl.connectLiteLoginUsernameEmail(driver, "ashenchowthee@yahoo.com", "123456", "ashenchowthee02");
		ReusableFunc.VerifyTextOnPage(driver, "ashenchowthee02");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/span[1]", "Unable to locate username link");
		//Click the Profile AuthMenu link
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[2]/div/div[3]/div[1]/a/div/span[1]"), "Profile");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[3]/div[1]/a/div", "Could not locate Profile AuthMenu link");
		//Click About Me
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[2]/div/div[3]/div[2]/div[1]/a/div"), "About me");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[3]/div[2]/div[1]/a/div", "About me");
		ReusableFunc.VerifyTextOnPage(driver, "Username");
		ReusableFunc.VerifyTextOnPage(driver, "ashenchowthee02");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/span[1]", "Unable to locate username link");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[2]/div/div[3]/div[1]/a/div/span[1]"), "Profile");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[3]/div[1]/a/div", "Could not locate Profile AuthMenu link");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[2]/div/div[3]/div[2]/div[2]/a/div"), "Mobile");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[3]/div[2]/div[2]/a/div", "Mobile");
		ReusableFunc.VerifyTextOnPage(driver, "Add mobile number");
		ReusableFunc.VerifyTextOnPage(driver, "ashenchowthee02");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/span[1]", "Unable to locate username link");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[3]/div[1]/a/div", "Could not locate Profile AuthMenu link");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[2]/div/div[3]/div[2]/div[3]/a/div"), "Email");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[3]/div[2]/div[3]/a/div", "Email");
		ReusableFunc.VerifyTextOnPage(driver, "Change email address");
		ReusableFunc.VerifyTextOnPage(driver, "ashenchowthee02");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/span[1]", "Unable to locate username link");
		//Click Profile
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[2]/div/div[3]/div[1]/a/div/span[1]"), "Profile");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[3]/div[1]/a/div", "Could not locate Profile AuthMenu link");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[2]/div/div[3]/div[2]/div[4]/a/div"), "Password");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[3]/div[2]/div[4]/a/div", "Password");
		ReusableFunc.VerifyTextOnPage(driver, "Change password");
		//Click the Smartcards link from the AuthMenu
		ReusableFunc.VerifyTextOnPage(driver, "ashenchowthee02");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/span[1]", "Unable to locate username link");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[4]/a/div/span", "Could not locate DStv Subscriptions AuthMenu link");
		ReusableFunc.VerifyTextOnPage(driver, "Package:");
		ReusableFunc.VerifyTextOnPage(driver, "Device:");
		ReusableFunc.VerifyTextOnPage(driver, "SC no:");
		//Click the Newsletters link from the Authmenu
		ReusableFunc.VerifyTextOnPage(driver, "ashenchowthee02");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/span[1]", "Unable to locate username link");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[5]/a/div/span", "Could not locate Newsletters AuthMenu link");
		ReusableFunc.VerifyTextOnPage(driver, "DStv");
		ReusableFunc.VerifyTextOnPage(driver, "ashenchowthee02");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/span[1]", "Unable to locate username link");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[2]/a/div/span", "Could not locate Overview AuthMenu link");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div/div[1]/div/div/a[1]/h3"), "Profile");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div/div[2]/div[1]/a/div/h3"), "DStv Subscriptions");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div/div/div[1]/span[1]", "Unable to locate UserName link");
		ConnectControl.connectLiteLogout(driver);
		

	}
	
}
