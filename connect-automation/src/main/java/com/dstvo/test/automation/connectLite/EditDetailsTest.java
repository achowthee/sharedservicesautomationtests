package com.dstvo.test.automation.connectLite;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
//import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.RandomStringGen;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class EditDetailsTest extends TestBase {

	@BeforeMethod
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}

	
	@Test
	public void editAboutMe() throws Exception{
		ReusableFunc.VerifyPageTitle(driver, "DStv Connect");
		ReusableFunc.VerifyTextOnPage(driver, "Login with your Connect ID");
		Thread.sleep(5000);
		ConnectControl.connectLiteLoginUsernameEmail(driver, "ashenchowthee@yahoo.com", "123456", "Profile");
		ReusableFunc.ClickByXpath(driver, "html/body/div[2]/ul/li[2]/a", "top profile link");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div/ul/li[1]/div/a[2]"), "About me");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div/ul/li[2]/div/a[2]"), "Mobile");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div/ul/li[3]/div/a[2]"), "Email");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div/ul/li[4]/div/a[2]"), "Password");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div/ul/li[5]/div/a[2]"), "Accounts");
		driver.findElementByXPath(".//*[@id='FirstName']").clear();
//      	RandomString rs = new RandomString(5);
//	 	String firstName="FirstName"+rs.nextString();
		String fName=RandomStringGen.generateRandomString(5, RandomStringGen.Mode.ALPHA);
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("FirstName")),fName);
		driver.findElementByXPath(".//*[@id='Surname']").clear();
//		RandomString rs2 = new RandomString(5);
//	 	String surname="bee"+rs2.nextString();
		String sName=RandomStringGen.generateRandomString(8, RandomStringGen.Mode.ALPHA);
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("Surname")),sName);
		//Enter date Of birth-No format-calendar yet
		driver.findElementByXPath("html/body/div[5]/form/div[6]/div/label/input").clear();
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath("html/body/div[5]/form/div[6]/div/label/input"), "1983-02-14");
		//gender
		ReusableFunc.CheckCheckBox(driver, driver.findElementByXPath("html/body/div[5]/form/div[7]/div[2]/label[1]/input"));
		//select country
//		ReusableFunc.ClickDropdownElement(driver, By.id("Country"), By.xpath("html/body/div[5]/form/div[8]/div/label/select/option[3]"));
//		ReusableFunc.ClickByXpath(driver, "html/body/div[5]/form/div[8]/div/label/select/option[4]", "Country");
		//language
		//ReusableFunc.ClickDropdownElement(driver, By.id("Language"), By.xpath("/html/body/div[4]/form/div[10]/div/label/select/option[3]"));
		//Country telephone number
		//select Province
		ReusableFunc.VerifyTextOnPage(driver, "Province");
		ReusableFunc.ClickByXpath(driver, "html/body/div[5]/form/div[9]/div/label/select/option[5]", "Province");
		//ReusableFunc.ClickDropdownElement(driver, By.xpath("html/body/div[5]/form/div[9]/div/label/select"), By.xpath("html/body/div[5]/form/div[9]/div/label/select/option[5]"));
		//ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("mobileNumber")),"12345678");
		//Thread.sleep(1000);
		//Save
		ReusableFunc.ClickByID(driver,"save-details");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[5]/form/div[2]/div/ul/li"), "Successfully updated profile");
	
					
		
	}
	
	//Confirm that the mobile page and all the relevant text and elements exist
	@Test
	public void viewMobilePage() throws Exception{
		ReusableFunc.VerifyPageTitle(driver, "DStv Connect");
		ReusableFunc.VerifyTextOnPage(driver, "Login with your Connect ID");
		Thread.sleep(5000);
		ConnectControl.connectLiteLoginUsernameEmail(driver, "ashenchowthee@yahoo.com", "123456", "Profile");
		ReusableFunc.ClickByXpath(driver, "html/body/div[2]/ul/li[2]/a", "top profile link");
		//Click the Mobile tab
		ReusableFunc.ClickByXpath(driver, "html/body/div[4]/div/ul/li[2]/div/a[1]", "Could not locate Mobile tab");
		//Confirm text exists
		ReusableFunc.VerifyTextOnPage(driver, "Add mobile number");
		//Confirm text exists
		ReusableFunc.VerifyTextOnPage(driver, "Enter mobile number");
		//Confirm input box exists and user able to input and delete a mobile number
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("html/body/div[5]/form/div[3]/div/div/input"), "Could not locate mobile number text box");
		//Input mobile number
		ReusableFunc.clearTextBox(driver, By.xpath(".//*[@id='mobileNumber']"));
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='mobileNumber']"), "+27825768004");
		//Shift focus off the mobile input box
		ReusableFunc.ClickByXpath(driver, "html/body/div[5]/form/div[1]/div/h3", "Add mobile number");
		//Clear the mobile number input box
		ReusableFunc.clearTextBox(driver, By.xpath(".//*[@id='mobileNumber']"));
		ReusableFunc.ClickByXpath(driver, "html/body/div[5]/form/div[1]/div/h3", "Add mobile number");
		//Verify the input box has been cleared
		ReusableFunc.FailIfTextOnPage(driver, "27825768004");
	}
	
	//Confirm the Email page and all relevant text and element exist
	@Test
	public void viewEmailPage() throws Exception{
		ReusableFunc.VerifyPageTitle(driver, "DStv Connect");
		ReusableFunc.VerifyTextOnPage(driver, "Login with your Connect ID");
		Thread.sleep(5000);
		ConnectControl.connectLiteLoginUsernameEmail(driver, "ashenchowthee@yahoo.com", "123456", "Profile");
		ReusableFunc.ClickByXpath(driver, "html/body/div[2]/ul/li[2]/a", "top profile link");
		//Click the email tab
		ReusableFunc.ClickByXpath(driver, "html/body/div[4]/div/ul/li[3]/div/a[1]", "Could not locate Email tab");
		//Confirm text exists
		ReusableFunc.VerifyTextOnPage(driver, "Change email address");
		//Confirm text exists
//		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[5]/form/div[1]/div/label"), "Current email address");
		//Confirm input box exists
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("html/body/div[5]/form/div[1]/div/input"), "Could not locate current email address text box");
		//Confirm text exists
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[5]/form/div[2]/div/label"), "New email address");
		//Confirm input box exists
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("html/body/div[5]/form/div[2]/div/label/input"), "Could not locate new email address text box");
		//Confirm input box exists
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[5]/form/div[3]/div/div/label"), "Enter your login password");
		//Confirm input box exists
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("html/body/div[5]/form/div[3]/div/div/input[1]"), "Could not locate enter your connect password text box");
	}
	
	//Confirm the Password page and all relevant text and element exist
	@Test
	public void viewPasswordPage() throws Exception{
		ReusableFunc.VerifyPageTitle(driver, "DStv Connect");
		ReusableFunc.VerifyTextOnPage(driver, "Login with your Connect ID");
		Thread.sleep(5000);
		ConnectControl.connectLiteLoginUsernameEmail(driver, "ashenchowthee@yahoo.com", "123456", "Profile");
		ReusableFunc.ClickByXpath(driver, "html/body/div[2]/ul/li[2]/a", "top profile link");
		//Click the email tab
		ReusableFunc.ClickByXpath(driver, "html/body/div[4]/div/ul/li[4]/div/a[1]", "Could not locate Password tab");
		//Confirm text exists
		ReusableFunc.VerifyTextOnPage(driver, "Change password");
		//Confirm text exists
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[5]/form/div[1]/div/div/label"), "Current password");
		//Confirm input box exists
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("html/body/div[5]/form/div[1]/div/div/label/input[1]"), "Could not locate current password text box");
		//Confirm text exists
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[5]/form/div[2]/div/div/label"), "New password");
		//Confirm input box exists
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("html/body/div[5]/form/div[2]/div/div/label/input[1]"), "Could not locate new password text box");
		//Confirm input box exists
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("html/body/div[5]/form/div[3]/div/button"), "Could not locate update password button");
	}

} 
