package com.dstvo.test.automation.connectLite;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class LinkUnlinkSMartCardTest extends TestBase {

	@BeforeMethod
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}

	
		
	@Test
	public void linkSmartCard() throws Exception{
		ReusableFunc.VerifyPageTitle(driver, "DStv Connect");
		ReusableFunc.VerifyTextOnPage(driver, "Login with your Connect ID");
		Thread.sleep(5000);
		ConnectControl.connectLiteLoginUsernameEmail(driver, "ashenchowthee@yahoo.com", "123456", "DStv Subscriptions");
		//Go to Smartcards page and link a smartcard
		ReusableFunc.ClickByLinkText(driver, "DStv Subscriptions");
		ReusableFunc.VerifyTextOnPage(driver, "These are your DStv Subscriptions. Linking your DStv Subscriptions to your online profile will enable you to manage your accounts, make payments online and enter exclusive competitions. Premium subscribers are also able watch live sport, rent movies and catch up with shows online.");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='name-4135299930']"), "test");
		Thread.sleep(1500);
		ReusableFunc.ClickByXpath(driver, "html/body/div[4]/div[3]/div/div/form/div[1]/div/button", "Link");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div[2]/div/ul/li"), "Smartcard linked successfully");
		//Verify the linked smartcard appears in the Overview page
		Thread.sleep(1500);
		ReusableFunc.ClickByLinkText(driver, "Overview");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div/div[2]/div[1]/a/div/div[2]/div/strong"), "test");
		//Go to the Smartcards page and unlink the smartcard
		Thread.sleep(1500);
		ReusableFunc.ClickByXpath(driver, "html/body/div[4]/div/div[2]/div[1]/a/div", "Smartcards summary Overview page");
		Thread.sleep(1500);
		ReusableFunc.ClickByXpath(driver, "html/body/div[4]/div[3]/div/div/form/div[1]/div/button", "Unlink");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div[2]/div/ul/li"), "Smartcard unlinked successfully");
		//Verify the unlinked smartcard no longer appears in the Overview page
		Thread.sleep(1500);
		ReusableFunc.ClickByLinkText(driver, "Overview");
		ReusableFunc.VerifyTextOnPage(driver, "You have not linked any DStv subscriptions to your profile yet.");
	}

} 
