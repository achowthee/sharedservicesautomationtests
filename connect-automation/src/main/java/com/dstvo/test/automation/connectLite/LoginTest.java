package com.dstvo.test.automation.connectLite;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class LoginTest extends TestBase {

	@BeforeMethod
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
		
	}

	@Test
	public void liteLoginUsername() throws Exception {
		//loginWithUsername
		ReusableFunc.VerifyPageTitle(driver, "DStv Connect");
		ReusableFunc.VerifyTextOnPage(driver, "Login with your Connect ID");
		Thread.sleep(5000);
		ConnectControl.connectLiteLoginUsernameEmail(driver, "belz25", "123456", "belz25");
		ReusableFunc.VerifyTextOnPage(driver, "Profile");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[1]/div/div/div[1]"), "belz25");
		Thread.sleep(2500);
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div/div/div[1]/span[1]", "Unable to locate UserName link");
		ConnectControl.connectLiteLogout(driver);
		

	}
	
	@Test
	public void liteLoginEmail() throws Exception{
		ReusableFunc.VerifyPageTitle(driver, "DStv Connect");
		ReusableFunc.VerifyTextOnPage(driver, "Login with your Connect ID");
		Thread.sleep(5000);
		ConnectControl.connectLiteLoginUsernameEmail(driver, "bnptonylou@yahoo.com", "123456", "Newsletters");
		ReusableFunc.VerifyTextOnPage(driver, "belindaBeygencyBeygency");
		Thread.sleep(2500);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/span[1]", "Unable to locate UserName link");
		ConnectControl.connectLiteLogout(driver);
	}
	
	@Test
	public void liteLoginMobile() throws Exception{
		//Login with registered mobile number
		ReusableFunc.VerifyPageTitle(driver, "DStv Connect");
//		ReusableFunc.VerifyTextOnPage(driver, "Login with your Connect ID");
		Thread.sleep(5000);
		ReusableFunc.ClickByLinkText(driver, "Use mobile number");
		Thread.sleep(5000);
		ConnectControl.connectLiteLoginMobile(driver, "825768004", "123456", "ashenchowtheeQA");
		//Logout of Connect account
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[1]"), "ashenchowtheeQA");
		Thread.sleep(2200);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/span[1]", "Unable to locate UserName link");
		ConnectControl.connectLiteLogout(driver);
	}
	
	@Test
	public void liteLoginFacebook() throws Exception{
		//Login with registered Facebook account
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/form/div[9]/div/div/a[3]/img", "Unable to locate Facebook link");
		ReusableFunc.VerifyTextOnPage(driver, "Facebook Login");
		Thread.sleep(5000);
		ConnectControl.connectLiteLoginFacebook(driver, "ashenychowtheey@gmail.com", "ashen123", "ashenychowtheey");
		ReusableFunc.VerifyTextOnPage(driver, "ashenychowtheey");
		Thread.sleep(2500);
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div/div/div[1]/span[1]", "ashenychowtheey");
		ConnectControl.connectLiteLogout(driver);
		//Quick login to Connect account after already signing in to Facebook account
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/form/div[9]/div/div/a[3]/img", "Unable to locate Facebook link");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[3]/div/div/form/div[2]/span/span"), "ashenychowtheey");
		//Logout of Connect account
		ReusableFunc.VerifyTextOnPage(driver, "ashenychowtheey");
		Thread.sleep(2500);
		ReusableFunc.ClickByCss(driver, ".connect-user");
		ConnectControl.connectLiteLogout(driver);
		//Logout of Facebook
		driver.get("http://www.facebook.com");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='blueBarNAXAnchor']/div[1]/div/div/div[1]/ul/li[1]/a/span"), "Asheny");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='pageLoginAnchor']", "Account Settings");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='logout_form']/label/input", "Log Out");
	}
	
	@Test
	public void liteLoginGoogle() throws Exception{
		//Login with registered Google account
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/form/div[9]/div/div/a[2]/img", "Unable to locate Google link");
		ReusableFunc.VerifyTextOnPage(driver, "Sign in with your Google Account");
		Thread.sleep(5000);
		ConnectControl.connectLiteLoginGoogle(driver, "ashenychowtheey@gmail.com", "ashen123", "ashenychowtheey");
		ReusableFunc.VerifyTextOnPage(driver, "ashenychowtheey");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/span[1]", "Unable to locate UserName link");
		ConnectControl.connectLiteLogout(driver);
		//Quick login to Connect account after already signing in to Google account
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/form/div[9]/div/div/a[2]/img", "Unable to locate Google link");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[1]/span[1]"), "ashenychowtheey");
		//Logout of Connect account
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[1]/span[1]"), "ashenychowtheey");
		Thread.sleep(2500);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/span[1]", "Unable to locate UserName link");
		ConnectControl.connectLiteLogout(driver);
		//Logout of Google
		driver.get("http://www.googlemail.com");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='gb']/div[1]/div[1]/div[1]/div/span"), "Asheny");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='gb']/div[1]/div[1]/div[2]/div[4]/div[1]/a/span", "Google drop down");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='gb_71']", "Sign out");
	}
	
	@Test
	public void liteLoginYahoo() throws Exception{
		//Login with registered Yahoo account
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/form/div[9]/div/div/a[1]/img", "Unable to locate Yahoo link");
		ReusableFunc.VerifyPageTitle(driver, "Yahoo - login");
		ReusableFunc.VerifyTextOnPage(driver, "Sign in to your account");
		Thread.sleep(5000);
		ConnectControl.connectLiteLoginYahoo(driver, "ashenchowthee@yahoo.com", "chocolate70");
		ReusableFunc.VerifyPageTitle(driver, "Yahoo - review and continue");
		Thread.sleep(1500);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='mbrMbOAuth']/div[1]/button[1]", "Agree");
		ReusableFunc.VerifyTextOnPage(driver, "ashenchowthee02");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/span[1]", "Unable to locate UserName link");
		ConnectControl.connectLiteLogout(driver);
		//Quick login to Connect account after already signing in to Google account
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/form/div[9]/div/div/a[1]/img", "Unable to locate Yahoo link");
		Thread.sleep(1500);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='mbrMbOAuth']/div[1]/button[1]", "Agree");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[1]/span[1]"), "ashenchowthee02");
		//Logout of Connect account
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[1]/span[1]"), "ashenchowthee02");
		Thread.sleep(2500);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/span[1]", "Unable to locate UserName link");
		ConnectControl.connectLiteLogout(driver);
		//Logout of Yahoo
		driver.get("https://us-mg4.mail.yahoo.com/neo/launch?.rand=52fn4vqd76nu4");
		ReusableFunc.VerifyTextOnPage(driver, "ashen");
		ReusableFunc.navigateToURL(driver, "https://login.yahoo.com/config/login;_ylt=AhKumcZfFTwgob00XWTcGbm.ulI6?logout=1&.direct=2&.done=https://www.yahoo.com&.src=cdgm&.intl=us&.lang=en-US");
		ReusableFunc.VerifyTextOnPage(driver, "Sign in");
	}

} 
