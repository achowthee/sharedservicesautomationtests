package com.dstvo.test.automation.connectLite.Prod;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class AuthMenuTest extends TestBase {

	@BeforeMethod
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}

	@Test
	public void liteAuthMenuTest() throws Exception {
		//Login
		ReusableFunc.VerifyTextOnPage(driver, "Login with your Connect ID");
		ConnectControl.connectLiteLoginUsernameEmail(driver, "ashen.chowthee@dstvo.com", "123456", "AshenchowtheeLive");
		ReusableFunc.VerifyTextOnPage(driver, "AshenchowtheeLive");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/span[1]", "Unable to locate username link");
		Thread.sleep(1000);
		//Click the Profile AuthMenu link
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[3]/div[1]/a/div", "Could not locate Profile AuthMenu link");
		Thread.sleep(1000);
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[2]/div/div[3]/div[2]/div[1]/a/div[2]"), "About me");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[3]/div[2]/div[1]/a/div[2]", "About me");
		ReusableFunc.VerifyTextOnPage(driver, "Username");
		ReusableFunc.VerifyTextOnPage(driver, "AshenchowtheeLive");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/span[1]", "Unable to locate username link");
		Thread.sleep(1000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[3]/div[1]/a/div", "Could not locate Profile AuthMenu link");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[2]/div/div[3]/div[2]/div[2]/a/div[2]"), "Mobile");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[3]/div[2]/div[2]/a/div[2]", "Mobile");
		ReusableFunc.VerifyTextOnPage(driver, "Add or change your mobile number");
		ReusableFunc.VerifyTextOnPage(driver, "AshenchowtheeLive");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/span[1]", "Unable to locate username link");
		Thread.sleep(1000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[3]/div[1]/a/div", "Could not locate Profile AuthMenu link");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[2]/div/div[3]/div[2]/div[3]/a/div[2]"), "Email");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[3]/div[2]/div[3]/a/div[2]", "Email");
		ReusableFunc.VerifyTextOnPage(driver, "Change your email address");
		ReusableFunc.VerifyTextOnPage(driver, "AshenchowtheeLive");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/span[1]", "Unable to locate username link");
		Thread.sleep(1000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[3]/div[1]/a/div", "Could not locate Profile AuthMenu link");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[2]/div/div[3]/div[2]/div[4]/a/div[2]"), "Password");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[3]/div[2]/div[4]/a/div[2]", "Password");
		ReusableFunc.VerifyTextOnPage(driver, "Change your password");
		//Click the Smartcards link from the AuthMenu
		ReusableFunc.VerifyTextOnPage(driver, "AshenchowtheeLive");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/span[1]", "Unable to locate username link");
		Thread.sleep(1000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[4]/a/div/span", "Could not locate DStv Subscriptions AuthMenu link");
		ReusableFunc.VerifyTextOnPage(driver, "Package:");
		ReusableFunc.VerifyTextOnPage(driver, "Device:");
		ReusableFunc.VerifyTextOnPage(driver, "SC no:");
		//Click the Newsletters link from the Authmenu
		ReusableFunc.VerifyTextOnPage(driver, "AshenchowtheeLive");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/span[1]", "Unable to locate username link");
		Thread.sleep(1000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[5]/a/div/span", "Could not locate Newsletters AuthMenu link");
		ReusableFunc.VerifyTextOnPage(driver, "DStv");
		ReusableFunc.VerifyTextOnPage(driver, "AshenchowtheeLive");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/span[1]", "Unable to locate username link");
		Thread.sleep(1000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[2]/a/div/span", "Could not locate Overview AuthMenu link");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div/div[1]/div/div/a[1]/h3"), "Profile");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div/div[2]/div[1]/a/div/h3"), "DStv Subscriptions");
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div/div/div[1]/span[1]", "Unable to locate UserName link");
		ConnectControl.connectLiteLogout(driver);
		

	}
	
}
