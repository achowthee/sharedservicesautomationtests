package com.dstvo.test.automation.connectLite.Prod;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomStringGen;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ForgotPasswordTest extends TestBase {

	@BeforeMethod
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}

	@Test
	public void liteForgotPasswordEmail() throws Exception {
		
		//Clear Mailinator inbox
/*		ReusableFunc.navigateToURL(driver, "http://www.mailinator.com");
		ReusableFunc.VerifyTextOnPage(driver, "Check An Inbox!");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='inboxfield']"), "forgotpasswordtest@mailinator.com");
		ReusableFunc.ClickByXpath(driver, "html/body/div[2]/div/div[1]/div[2]/div/div[2]/div/div/btn", "Check it");
		ReusableFunc.VerifyTextOnPage(driver, "Inbox for:");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='mailcontainer']/li/a/div[2]", "DStv: Reset your password");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='mailshowdivhead']/div[2]/div/div[2]/div/button[3]", "Delete");
		ReusableFunc.VerifyTextOnPage(driver, "[ This Inbox is currently Empty ]");*/
		//Do a Forgot Password
//		ReusableFunc.navigateToURL(driver, "http://qaconnectlite.dstv.com/4.1");
		ReusableFunc.VerifyTextOnPage(driver, "Login with your Connect ID");
		ReusableFunc.ClickByLinkText(driver, "Forgot Password");
		ReusableFunc.VerifyTextOnPage(driver, "Reset your password");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[contains(@id, 'email')]"), "forgotpasswordtest@mailinator.com");
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/form/div[2]/div[4]/div/button", "Could not locate Reset Password button");
		ReusableFunc.VerifyTextOnPage(driver, "A link has been sent to");
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/div[5]/div/a", "OK button");
		ReusableFunc.VerifyTextOnPage(driver, "Login with your Connect ID");
		ReusableFunc.navigateToURL(driver, "http://www.mailinator.com");
		ReusableFunc.VerifyTextOnPage(driver, "Check An Inbox!");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='inboxfield']"), "forgotpasswordtest@mailinator.com");
		ReusableFunc.ClickByXpath(driver, "html/body/div[2]/div/div[1]/div[2]/div/div[2]/div/div/btn", "Check it");
		ReusableFunc.VerifyTextOnPage(driver, "Inbox for:");
		ReusableFunc.VerifyTextOnPage(driver, "DStv: Reset your password");
		
	}
	
	@Test
	public void liteForgotPasswordMobile() throws Exception {
		//Click the Forgot Password link
		ReusableFunc.VerifyTextOnPage(driver, "Login with your Connect ID");
		ReusableFunc.ClickByLinkText(driver, "Forgot Password");
		ReusableFunc.VerifyTextOnPage(driver, "Reset your password");
		//Select Use Mobile Number
		ReusableFunc.VerifyTextOnPage(driver, "Use mobile number");
		Thread.sleep(2000);
		ReusableFunc.ClickByLinkText(driver, "Use mobile number");
		//Input a mobile number so a verification code can be sent
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[contains(@id, 'MobileNumber')]"), "795125956");
		//Click the Send Code button
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/form/div[3]/div[3]/div/button", "Could not locate Send code button");
		//Verify the input box for the verification code is displayed
		ReusableFunc.VerifyTextOnPage(driver, "Enter the verification code sent to your phone");
		//Verify the Submit Code button is displayed
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("html/body/div[3]/form/div[3]/div[3]/div/button"), "Send code");
		

	}


}
