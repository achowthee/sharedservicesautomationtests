package com.dstvo.test.automation.connectLite.Prod;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class LoginTest extends TestBase {

	@BeforeMethod
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}

	@Test
	public void liteLoginUsername() throws Exception {
		//loginWithUsername
		ConnectControl.connectLiteLoginUsernameEmail(driver, "AshenchowtheeLive", "123456", "AshenchowtheeLive");
		ReusableFunc.VerifyTextOnPage(driver, "Profile");
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div/div/div[1]/span[1]", "Unable to locate UserName link");
		ConnectControl.connectLiteLogout(driver);
		

	}
	
	@Test
	public void liteLoginEmail() throws Exception{
		ConnectControl.connectLiteLoginUsernameEmail(driver, "ashen.chowthee@dstvo.com", "123456", "Newsletters");
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div/div/div[1]/span[1]", "Unable to locate UserName link");
		ConnectControl.connectLiteLogout(driver);
	}
	
	@Test
	public void liteLoginMobile() throws Exception{
		//Login with registered mobile number
		ReusableFunc.VerifyTextOnPage(driver, "DStv Connect");
		Thread.sleep(2000);
		ReusableFunc.ClickByLinkText(driver, "Use mobile number");
		ConnectControl.connectLiteLoginMobile(driver, "825768004", "123456", "AshenchowtheeLive");
		ReusableFunc.VerifyTextOnPage(driver, "AshenchowtheeLive");
		//Logout of Connect account
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div/div/div[1]/span[1]", "Unable to locate UserName link");
		ConnectControl.connectLiteLogout(driver);
	}
	
	@Test
	public void liteLoginFacebook() throws Exception{
		//Login with registered Facebook account
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/form/div[9]/div/div/a[3]/img", "Unable to locate Facebook link");
		ReusableFunc.VerifyTextOnPage(driver, "Facebook Login");
		ConnectControl.connectLiteLoginFacebook(driver, "ashenychowtheey@gmail.com", "ashen123", "ashenychowtheey");
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div/div/div[1]/span[1]", "Unable to locate Facebook UserName link");
		ConnectControl.connectLiteLogout(driver);
		//Quick login to Connect account after already signing in to Facebook account
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/form/div[9]/div/div/a[3]/img", "Unable to locate Facebook link");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[1]/span[1]"), "ashenychowtheey");
		//Logout of Connect account
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div/div/div[1]/span[1]", "Unable to locate UserName link");
		ConnectControl.connectLiteLogout(driver);
		//Logout of Facebook
		driver.get("http://www.facebook.com");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='pagelet_welcome_box']/div/div/div/div[2]/a[1]"), "Asheny Chowtheey");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='pageLoginAnchor']", "Account Settings");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='logout_form']/label/input", "Log Out");
	}
	
	@Test
	public void liteLoginGoogle() throws Exception{
		//Login with registered Facebook account
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/form/div[9]/div/div/a[2]/img", "Unable to locate Google link");
		ReusableFunc.VerifyTextOnPage(driver, "Sign in with your Google Account");
		ConnectControl.connectLiteLoginGoogle(driver, "ashenychowtheey@gmail.com", "ashen123", "ashenychowtheey");
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div/div/div[1]/span[1]", "Unable to locate UserName link");
		ConnectControl.connectLiteLogout(driver);
		//Quick login to Connect account after already signing in to Google account
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/form/div[9]/div/div/a[2]/img", "Unable to locate Google link");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='connect']/div/div[1]/span[1]"), "ashenychowtheey");
		//Logout of Connect account
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[1]/div/div/div[1]/span[1]"), "ashenychowtheey");
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div/div/div[1]/span[1]", "Unable to locate UserName link");
		ConnectControl.connectLiteLogout(driver);
		//Logout of Google
		driver.get("http://www.googlemail.com");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='gb']/div[1]/div[1]/div[1]/div/a"), "+Asheny");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='gb']/div[1]/div[1]/div[2]/div[5]/div[1]/a/span", "Google drop down");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='gb_71']", "Sign out");
	}

} 
