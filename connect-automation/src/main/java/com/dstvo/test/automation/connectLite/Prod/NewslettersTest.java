package com.dstvo.test.automation.connectLite.Prod;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class NewslettersTest extends TestBase {

	@BeforeMethod
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}

	@Test
	public void liteNewsletterPageTest() throws Exception {
		//Verify that a user is able to Subscribe and Unsubscribe to Newsletters
		ReusableFunc.VerifyTextOnPage(driver, "Login with your Connect ID");
		ConnectControl.connectLiteLoginUsernameEmail(driver, "ashen.chowthee@dstvo.com", "123456", "AshenchowtheeLive");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div/div[1]/div/div/a[1]/h3"), "Profile");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div/div[2]/div[1]/a/div/h3"), "DStv Subscriptions");
		//Go to the Newsletters page
		ReusableFunc.ClickByLinkText(driver, "Newsletters");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/h2"), "Make the most of your TV viewing by selecting the weekly newsletters you would like to subscribe to.");
		//Subscribe to a Newsletter
		ReusableFunc.ClickByXpath(driver, "html/body/div[4]/div[4]/div[1]/div/div[1]/div/button", "Subscribe");
		ReusableFunc.VerifyTextOnPage(driver, "Successfully subscribed!");
		//Go to the Overview page and verify that the subscribed newsletter appears
		ReusableFunc.ClickByLinkText(driver, "Overview");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div/div[1]/div/div/a[1]/h3"), "Profile");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div/div[2]/div[1]/a/div/h3"), "DStv Subscriptions");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div/div[2]/div[2]/div/div[2]/span[1]"), "DStv");
		//Go to the Newsletter page and Unsubscribe the selected Newsletter
		ReusableFunc.ClickByLinkText(driver, "Newsletters");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/h2"), "Make the most of your TV viewing by selecting the weekly newsletters you would like to subscribe to.");
		ReusableFunc.ClickByXpath(driver, "html/body/div[4]/div[4]/div[1]/div/div[1]/div/button", "Unsubscribe");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div[3]/div/ul/li"), "Successfully unsubscribed!");
		//Verify that there are no Newsletter displayed in the Overview page
		ReusableFunc.ClickByLinkText(driver, "Overview");
		ReusableFunc.FailIfTextOnPage(driver, "Click to view latest");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[1]/div/div/div[1]/span[1]"), "AshenchowtheeLive");
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div/div/div[1]/span[1]", "Unable to locate UserName link");
		ConnectControl.connectLiteLogout(driver);

	}
	
}
