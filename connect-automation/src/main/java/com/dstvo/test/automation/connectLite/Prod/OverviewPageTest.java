package com.dstvo.test.automation.connectLite.Prod;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class OverviewPageTest extends TestBase {

	@BeforeMethod
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}

	@Test
	public void liteOverviewPageTest() throws Exception {
		//Click the "Don't have a Connect ID? Register" link
		ReusableFunc.VerifyTextOnPage(driver, "Login with your Connect ID");
		ConnectControl.connectLiteLoginUsernameEmail(driver, "ashen.chowthee@dstvo.com", "123456", "AshenchowtheeLive");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div/div[1]/div/div/a[1]/h3"), "Profile");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[4]/div/div[2]/div[1]/a/div/h3"), "DStv Subscriptions");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='avatar-link']"), "Add profile image");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[2]/ul/li[2]/a"), "Profile");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[2]/ul/li[3]/a"), "DStv Subscriptions");
		ReusableFunc.VerifyTextOnPage(driver, "AshenchowtheeLive");
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div/div/div[1]/span[1]", "Unable to locate UserName link");
		ConnectControl.connectLiteLogout(driver);

	}
	
}
