package com.dstvo.test.automation.connectLite.Prod;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomStringGen;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class RegistrationTest extends TestBase {

	@BeforeMethod
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}

	@Test
	public void liteRegisterWithEmail() throws Exception {
		//Click the registration link
		ReusableFunc.VerifyTextOnPage(driver, "Login with your Connect ID");
		ReusableFunc.ClickByLinkText(driver, "Don't have a Connect ID? Register");
		ReusableFunc.VerifyTextOnPage(driver, "Login to all DStv websites and apps. Get a Connect ID");
		//Enter details and click the Register button
		String strEmail = RandomStringGen.generateRandomString(5, RandomStringGen.Mode.ALPHANUMERIC);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='email']"), strEmail+"@mailinator.com");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='password']"), "123456");
		ReusableFunc.CheckCheckBox(driver, driver.findElementByXPath(".//*[@id='TermsAndConditions']"));
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/form/div[6]/div/button", "Register");
		ReusableFunc.VerifyTextOnPage(driver, "Thanks for registering.");
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/div[2]/div/a", "Back to site");

	}
	@Test
	public void liteRegisterWithMobile() throws Exception {
		//Click the registration link
		ReusableFunc.VerifyTextOnPage(driver, "Login with your Connect ID");
		ReusableFunc.ClickByLinkText(driver, "Don't have a Connect ID? Register");
		ReusableFunc.VerifyTextOnPage(driver, "Login to all DStv websites and apps. Get a Connect ID");
		//Enter details and click the Register button
		ReusableFunc.VerifyTextOnPage(driver, "Use mobile number");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='tabs']/ul/li[2]/a", "Use mobile number");
		Thread.sleep(2000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='mobilenumber']"), "765112778");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='password']"), "654321");
		ReusableFunc.CheckCheckBox(driver, driver.findElementByXPath(".//*[@id='TermsAndConditions']"));
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/form/div[6]/div/button", "Register");
		ReusableFunc.VerifyTextOnPage(driver, "Verify your mobile number");
		ReusableFunc.ClickByLinkText(driver, "Back");

	}
	


}
