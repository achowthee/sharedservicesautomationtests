package com.dstvo.test.automation.connectLite;

import java.net.URL;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;



public class TestBase {
	  //  private static ChromeDriverService service;
	    public static RemoteWebDriver driver;  //was private, changed to use in other tests       
	    public RemoteWebDriver mailDriver;
 
	    @BeforeMethod
	    public void before() throws Exception {
	   driver = new RemoteWebDriver(new URL("http://10.10.24.179:4444/wd/hub"), DesiredCapabilities.firefox());
	    	
	       
/*		FirefoxProfile profile = new FirefoxProfile();
	    	    profile.setPreference("network.proxy.type", 1);
	    	    profile.setPreference("network.proxy.http", "10.100.12.11");
	    	    profile.setPreference("network.proxy.http_port", 8080);
	    	    profile.setPreference("network.proxy.ssl", "10.100.12.11");
	    	    profile.setPreference("network.proxy.ssl_port", 8080);
	    	    profile.setPreference("network.proxy.ftp", "10.100.12.11");
	    	    profile.setPreference("network.proxy.ftp_port", 8080);
	    	    profile.setPreference("network.proxy.Socks", "10.100.12.11");
	    	    profile.setPreference("network.proxy.Socks_port", 8080);
	    	    profile.setPreference("network.proxy.no_proxies_on","qaconnectlite.dstv.com");
	       	    driver = new FirefoxDriver(profile);*/
	       	    
	       	
	    }

	    @AfterMethod
	  		public void after() {
	  	        if (driver != null) {
	  	        driver.quit();
	  	        }
	  	    }
	  	        
	    
	    protected String getSiteURL (){
	        String URL = System.getProperty("testUrl");
	        if (URL == null){
	            return "http://qaconnectlite.dstv.com/4.1";
	        	
	        
	        }
	        else{
	                return URL;
	        }
	        
	    }
	    
	    protected RemoteWebDriver getWebDriver() {
	        return driver;
	    }
	}

