package com.dstvo.test.automation.connectLite;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ToggleLoginRegisterTest extends TestBase {

	@BeforeMethod
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}

	@Test
	public void liteToggleLoginRegister() throws Exception {
		//Click the "Don't have a Connect ID? Register" link
		ReusableFunc.VerifyTextOnPage(driver, "Login with your Connect ID");
		ReusableFunc.ClickByLinkText(driver, "Don't have a Connect ID? Register");
		ReusableFunc.VerifyTextOnPage(driver, "Login to all DStv websites and apps. Get a Connect ID");
		//Click the "Already registered? Login" link
		ReusableFunc.ClickByLinkText(driver, "Already registered? Login");
		ReusableFunc.VerifyTextOnPage(driver, "Login with your Connect ID");
		

	}
	


}
