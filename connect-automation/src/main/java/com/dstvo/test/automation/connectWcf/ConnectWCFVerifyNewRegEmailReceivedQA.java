package com.dstvo.test.automation.connectWcf;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.io.*;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class ConnectWCFVerifyNewRegEmailReceivedQA extends TestBase {

	@BeforeMethod
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}

	@Test
	public void connectWCFVerifyNewRegEmailReceivedTest() throws Exception {
		//Read email address from text file stored by WCF
		FileReader r = new FileReader("C:\\soapemailqa.txt");
        BufferedReader bfr = new BufferedReader(r); 
        String x ="";
        
        //ReusableFunc.navigateToURL(driver, "http://www.mailinator.com");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath("html/body/div[2]/div/div[1]/div[2]/div/div[2]/div/h2"), "Check An Inbox!");
        
        while((x = bfr.readLine()) != null){
            System.out.println(x);
            ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='inboxfield']"), x);   
            }bfr.close();
            Thread.sleep(4000);
            //Confirm that the verification email has been received after registration
            ReusableFunc.ClickByXpath(driver, "html/body/div[2]/div/div[1]/div[2]/div/div[2]/div/div/btn", "Could not locate check it button");
            ReusableFunc.VerifyTextOnPage(driver, "DStv");
            driver.quit();
	}
}