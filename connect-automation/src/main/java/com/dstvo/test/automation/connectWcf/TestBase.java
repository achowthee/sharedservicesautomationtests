package com.dstvo.test.automation.connectWcf;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.junit.*;

import java.io.IOException;
import java.net.URL;


public class TestBase {
	  //  private static ChromeDriverService service;
	    public static RemoteWebDriver driver;  //was private, changed to use in other tests       
	    public RemoteWebDriver mailDriver;
 
	    @BeforeMethod
	    public void before() throws Exception {
//         	driver = new RemoteWebDriver(new URL("http://197.80.203.161:4457/wd/hub"), DesiredCapabilities.firefox());
	    	driver = new FirefoxDriver();

	    }

	    @AfterMethod
	    public void after() throws IOException, InterruptedException {
	        //if (driver != null) {
	        //driver.quit();
	        Runtime.getRuntime().exec("taskkill /F /IM firefox.exe");
	        Thread.sleep(5000);
	        Runtime.getRuntime().exec("taskkill /F /IM plugin-container.exe");
	        Runtime.getRuntime().exec("taskkill /F /IM WerFault.exe");
	        //}
	    }
	    
	    protected String getSiteURL (){
	        String URL = System.getProperty("testUrl");
	        if (URL == null){
	            return "http://mailinator.com";
	        	//return "http://www.dstv.com";
	        
	        }
	        else{
	                return URL;
	        }
	        
	    }
	    
	    protected RemoteWebDriver getWebDriver() {
	        return driver;
	    }
	}

