package com.dstvo.test.automation.qaresponsiveConnect;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ConnectSSOTest extends TestBase{

 @BeforeMethod
 @Override
 public  void before() throws Exception {
     super.before();
     getWebDriver().get(getSiteURL());
    }
 
 
 @Test
 public void canSingleSignOnDStvdotcom() throws Exception{
	ConnectControl.responsiveConnectLogin(driver, "belinda.ndlovu@dstvo.com", "123456");
    ReusableFunc.VerifyTextOnPage(driver, "You are logged in as");
         
    driver.navigate().to("http://qa.dstv.com");
    ReusableFunc.VerifyTextOnPage(driver, "You are logged in as");
    ConnectControl.connectLogout(driver);
         
          
          }
 
/* @Test
 public void canSingleSignOnBoxOffice() throws Exception{
	ConnectControl.responsiveConnectLogin(driver, "belinda.ndlovu@dstvo.com", "123456");
    ReusableFunc.VerifyTextOnPage(driver, "You are logged in as");
         
    driver.navigate().to("http://qa-boxoffice.dstv.com");
    ReusableFunc.VerifyTextOnPage(driver, "You are logged in as");
    ConnectControl.connectLogout(driver);
         
          
          }
 
 
 @Test
 public void canSingleSignOnBBmazansi() throws Exception{
	ConnectControl.responsiveConnectLogin(driver, "belinda.ndlovu@dstvo.com", "123456");
    ReusableFunc.VerifyTextOnPage(driver, "You are logged in as");
         
    driver.navigate().to("http://qa-bigbrothermzansi.dstv.com");
    ReusableFunc.VerifyTextOnPage(driver, "You are logged in as");
    ConnectControl.connectLogout(driver);
         
          
          }
 
 @Test
 public void canSingleSignOnCatchup() throws Exception{
	ConnectControl.responsiveConnectLogin(driver, "belinda.ndlovu@dstvo.com", "123456");
    ReusableFunc.VerifyTextOnPage(driver, "You are logged in as");
         
    driver.navigate().to("http://qa-catchup.dstv.com");
    ReusableFunc.VerifyTextOnPage(driver, "You are logged in as");
    ConnectControl.connectLogout(driver);
         
          
          }*/
 
  } 
