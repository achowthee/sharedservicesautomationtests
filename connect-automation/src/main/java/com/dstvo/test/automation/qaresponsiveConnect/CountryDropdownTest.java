package com.dstvo.test.automation.qaresponsiveConnect;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class CountryDropdownTest extends TestBase {

	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}

	@Test
	public void countryDropdownAngola() throws Exception {
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='undefined']/div/a"), By.xpath(".//*[@id='undefined']/ul/li[1]/a/label/span"));
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//img [@src='http://www.dstv.com/App_Themes/Dstv/Images/flags/AO.png']"), "Angola");
			
				
	}
	
	@Test
	public void countryDropdownBurkinaFaso() throws Exception {
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='undefined']/div/a"), By.xpath(".//*[@id='undefined']/ul/li[4]/a/label/span"));
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//img [@src='http://www.dstv.com/App_Themes/Dstv/Images/flags/BF.png']"), "BurkinaFaso");
		
	}
	
	@Test
	public void countryDropdownCameron() throws Exception {
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='undefined']/div/a"), By.xpath(".//*[@id='undefined']/ul/li[6]/a/label/span"));
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//img [@src='http://www.dstv.com/App_Themes/Dstv/Images/flags/CM.png']"), "Cameroon");

     }
	
	@Test
	public void countryDropdownCentralAfrican() throws Exception {
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='undefined']/div/a"), By.xpath(".//*[@id='undefined']/ul/li[8]/a/label/span"));
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//img [@src='http://www.dstv.com/App_Themes/Dstv/Images/flags/CF.png']"), "Central African");

	}
	
	@Test
	public void countryDropdownComores() throws Exception {
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='undefined']/div/a"), By.xpath(".//*[@id='undefined']/ul/li[10]/a/label/span"));
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//img [@src='http://www.dstv.com/App_Themes/Dstv/Images/flags/KM.png']"), "Comores");

	}
	

	@Test
	public void countryDropdownCongo() throws Exception {
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='undefined']/div/a"), By.xpath(".//*[@id='undefined']/ul/li[11]/a/label/span"));
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//img [@src='http://www.dstv.com/App_Themes/Dstv/Images/flags/CG.png']"), "Congo");

	}
	
	@Test
	public void countryDropdownDRC() throws Exception {
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='undefined']/div/a"), By.xpath(".//*[@id='undefined']/ul/li[14]/a/label/span"));
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//img [@src='http://www.dstv.com/App_Themes/Dstv/Images/flags/CD.png']"), "Congo");

	}
	
	@Test
	public void countryDropdownEritrea() throws Exception {
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='undefined']/div/a"), By.xpath(".//*[@id='undefined']/ul/li[16]/a/label/span"));
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//img [@src='http://www.dstv.com/App_Themes/Dstv/Images/flags/ER.png']"), "Eritrea");

	}
	
	@Test
	public void countryDropdownGuineaBissau() throws Exception {
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='undefined']/div/a"), By.xpath(".//*[@id='undefined']/ul/li[22]/a/label/span"));
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//img [@src='http://www.dstv.com/App_Themes/Dstv/Images/flags/GW.png']"), "GuineaBissau");

	}
	
	@Test
	public void countryDropdownGuinea() throws Exception {
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='undefined']/div/a"), By.xpath(".//*[@id='undefined']/ul/li[21]/a/label/span"));
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//img [@src='http://www.dstv.com/App_Themes/Dstv/Images/flags/GN.png']"), "Guinea");

	}
	
	@Test
	public void countryDropdownKenya() throws Exception {
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='undefined']/div/a"), By.xpath(".//*[@id='undefined']/ul/li[23]/a/label/span"));
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//img [@src='http://www.dstv.com/App_Themes/Dstv/Images/flags/KE.png']"), "Kenya");

	}
	
	@Test
	public void countryDropdownLiberia() throws Exception {
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='undefined']/div/a"), By.xpath(".//*[@id='undefined']/ul/li[24]/a/label/span"));
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//img [@src='http://www.dstv.com/App_Themes/Dstv/Images/flags/LI.png']"), "Liberia");

	}
	
	@Test
	public void countryDropdownMozambique() throws Exception {
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='undefined']/div/a"), By.xpath(".//*[@id='undefined']/ul/li[30]/a/label/span"));
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//img [@src='http://www.dstv.com/App_Themes/Dstv/Images/flags/MZ.png']"), "Mozambique");

	}
	
	@Test
	public void countryDropdownNiger() throws Exception {
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='undefined']/div/a"), By.xpath(".//*[@id='undefined']/ul/li[32]/a/label/span"));
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//img [@src='http://www.dstv.com/App_Themes/Dstv/Images/flags/NE.png']"), "Niger");

	}
	
	@Test
	public void countryDropdownReunion() throws Exception {
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='undefined']/div/a"), By.xpath(".//*[@id='undefined']/ul/li[34]/a/label/span"));
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//img [@src='http://www.dstv.com/App_Themes/Dstv/Images/flags/RE.png']"), "Reunion");

	}
	
	@Test
	public void countryDropdownSomalia() throws Exception {
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='undefined']/div/a"), By.xpath(".//*[@id='undefined']/ul/li[40]/a/label/span"));
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//img [@src='http://www.dstv.com/App_Themes/Dstv/Images/flags/SO.png']"), "Somalia");

	}
	
	@Test
	public void countryDropdownTogo() throws Exception {
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='undefined']/div/a"), By.xpath(".//*[@id='undefined']/ul/li[47]/a/label/span"));
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//img [@src='http://www.dstv.com/App_Themes/Dstv/Images/flags/TG.png']"), "Togo");

	}
	

	@Test
	public void countryDropdownUganda() throws Exception {
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='undefined']/div/a"), By.xpath(".//*[@id='undefined']/ul/li[48]/a/label/span"));
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//img [@src='http://www.dstv.com/App_Themes/Dstv/Images/flags/UG.png']"), "Uganda");

	}
	
	@Test
	public void countryDropdownZimbabwe() throws Exception {
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='undefined']/div/a"), By.xpath(".//*[@id='undefined']/ul/li[50]/a/label/span"));
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//img [@src='http://www.dstv.com/App_Themes/Dstv/Images/flags/ZW.png']"), "Zimbabwe");

	}
	
}

