package com.dstvo.test.automation.qaresponsiveConnect;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class GmailLoginTest extends TestBase {

	@BeforeMethod
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}

	
	
	@Test
	public void QAGmailLogin() throws Exception {
	 	ConnectControl.loginWithGmail(driver, "bnplou@gmail.com", "on123456", "belz25");
	 	Thread.sleep(2000);
	 	ConnectControl.connectLogout(driver);
		Thread.sleep(1000);
		
		
	}
	
    
	@Test
	public void loginWithFacebook() throws Exception {
		ConnectControl.loginWithFacebook(driver, "belzbee", "on123456", "thequeeenbeee");
	
	}
} 
