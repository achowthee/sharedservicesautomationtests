package com.dstvo.test.automation.qaresponsiveConnect;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class UpdateDetailsTest extends TestBase {

	@BeforeMethod
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}

	@Test
	public void QAUpdateMyDetailsSouthAfrica() throws Exception {
		ConnectControl.responsiveConnectLogin(driver, "belinda.ndlovu@dstvo.com", "123456");
		Thread.sleep(1000);
		ConnectControl.UpdateMyDetails(driver, "belzzzzz", "Ndlovu");
		//Select Country
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[contains(@id, 'Details1_ddlCountry')]"), By.xpath("/html/body/form/div[3]/div/div/div[2]/div[2]/div[2]/div[8]/div/div[2]/select/option[205]"));
		//Input a Province
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[contains(@id, 'Details1_ddlProvince')]"), By.xpath("/html/body/form/div[3]/div/div/div[2]/div[2]/div[2]/div[9]/div/div/select/option[3]"));
		ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'Details1_btnSave')]", "Unable to locate Save Details button");
		Thread.sleep(1000);
		ConnectControl.connectLogout(driver);
		Thread.sleep(1000);
		
	}
	
	@Test
	public void QAUpdateMyDetailsAlgeria() throws Exception {
		ConnectControl.responsiveConnectLogin(driver, "bnptonylou@yahoo.com", "123456");
		Thread.sleep(1000);
		ConnectControl.UpdateMyDetails(driver, "belzzzzz", "Ndlovu");
		//Select Country
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[contains(@id, 'Details1_ddlCountry')]"), By.xpath("/html/body/form/div[3]/div/div/div[2]/div[2]/div[2]/div[8]/div/div[2]/select/option[4]"));
		//Input a Province
		Thread.sleep(1000);
		if(!driver.findElementById("mainContent_Details1_ddlProvince").isDisplayed());
   		System.out.println("Your country does not have provinces to set");
		ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'Details1_btnSave')]", "Unable to locate Save Details button");
		Thread.sleep(1000);
		ConnectControl.connectLogout(driver);
		Thread.sleep(1000);
		
	}
	
	

}
