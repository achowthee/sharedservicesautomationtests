package com.dstvo.test.automation.responsivesiteTests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class HighlightFiltering extends TestBase {

	@BeforeClass
	@Override
	public void before() throws Exception {

		super.before();
		getWebDriver().get(getSiteURL());
		driver.manage().window().maximize();
	}

	@Test
	// test if can filter highlights
	public void MoreHighlights() throws Exception {
		
		System.out.println("=== statring More Highlight Test===");
		WebElement high = driver
				.findElementByXPath("html/body/main/div/section[3]/div[3]/a");
		String high2 = high.getAttribute("href");
		//Thread.sleep(2000);
		driver.get(high2);
		//Thread.sleep(2000);

		ReusableFunc.CompareUrls(driver,
				"http://staging-dstv.dstv.com/en-za/highlights");

		getWebDriver().get(getSiteURL());
		driver.manage().window().maximize();
    
	}

	// able to filter Highlights

	public void filterByGeneralEntertainment() throws Exception {
		System.out.println("=== statring test filter by general entertainment===");
		WebElement high = driver
				.findElementByXPath("html/body/main/div/div[10]/a");
		String high2 = high.getAttribute("href");
		//Thread.sleep(2000);
		driver.get(high2);
		//Thread.sleep(2000);

		ReusableFunc.ClickByXpath(driver,"html/body/main/div/div[3]/div[1]/ul[2]/li[1]/a/b", "Filter");
		//Thread.sleep(2000);

		ReusableFunc.ClickByXpath(driver,"html/body/main/div/div[3]/div[1]/ul[2]/li[1]/ul/li[2]/a","General Entertainment");
		//Thread.sleep(2000);

		ReusableFunc.VerifyTextOnPage(driver, "General Entertainment");
		//Thread.sleep(3000);
		// ReusableFunc.verifyTextInWebElement(driver,
		// By.xpath("html/body/div[2]/div[2]/div[1]/ul[2]/li[1]/a"), "Series");

		getWebDriver().get(getSiteURL());
		driver.manage().window().maximize();

	}

}
