package com.dstvo.test.automation.responsivesiteTests;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
public class ResponsiveConnectTest extends TestBase {
	
	@BeforeClass
	@Override
	public void before() throws Exception {
	
		super.before();
		getWebDriver().get(getSiteURL());
		driver.manage().window().maximize();
	
	}
	
	@Test 
	
	//Test  if user can register by expanding connect control and clicking new connect register
	
	public void RegisterConnect() throws Exception{
	  
		
	
	}
	
	@Test
	
	//This Method Test if users can log in on connect
	
	public void LoginConnect() throws Exception{
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/a", "login");
     	 Thread.sleep(5000);
     	 System.out.println("in 1...");
		  ConnectControl.connectLiteLoginEmail(driver, "snkosi351@gmail.com", "0786470660","login");
		 Thread.sleep(5000);
		 System.out.println("in 2...");
		 boolean ispresent;
		 if ("DStv Connect".equals(driver.getTitle())){
			 
		
			//add error message verification when email is valid but wrong password
			    ispresent = driver.findElements(By.xpath(".//*[@id='div-login-error']/div/ul/li")).size()>0;
			      org.testng.AssertJUnit.assertEquals(true, ispresent);
			
			
			
			 //add negative test step invalid email
		 } 	
		 
	else
		 {
			 ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/");
		 }
		 
		 
		 getWebDriver().get(getSiteURL());
		driver.manage().window().maximize();
		
	}
	
	@Test(dependsOnMethods={"LoginConnect"})
	
	//access My profile
	
	public void AccessMyprofile() throws Exception{
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[1]/span[1]", "My profile");
		 Thread.sleep(2000);
		 System.out.println("in 1...");
		  ReusableFunc.ClickByXpath(driver, ".//*[@id='connect']/div/div[2]/div/div[2]/a/div", "My Profile");
		   Thread.sleep(5000);
		   System.out.println("in 2...");
		     ReusableFunc.CompareUrls(driver, "http://stagingapps.dstv.com/connect/LCC/4.1/en/Overview/?returnUrl=http%3A%2F%2Fstaging-dstv.dstv.com%2F");
		     
		     //\\\ReusableFunc.ClickByXpath(driver, "html/body/div[1]/a", "Back to site");
		     
		     //add verification by profile name
		     
		     //getWebDriver().get(getSiteURL()); 
		driver.manage().window().maximize();
		
	   }
	
	//Test(dependsOnMethods={"AccessMyprofile"})
	
	//test if users can link smart cards
	
	public void canlinkCards() throws Exception{
		
		ReusableFunc.ClickByXpath(driver, "html/body/div[2]/ul/li[2]/a", "My DStv");
		
		ConnectControl.connectLiteCanLinkSmartCard(driver, "1002406104", "Nkosi", "Country'", ".//*[@id='Country']/option[44]");
		
		//ConnectControl.connectVerifySmartCardNotLinked(driver, strLoginEmailAddress, strLoginPassword, strUsername, strSmartCardDescription)
		
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/a", "Back to site");
		Thread.sleep(5000);
		
		getWebDriver().get(getSiteURL()); 
		driver.manage().window().maximize();
	}
	
		
	}
	
	
	
	

