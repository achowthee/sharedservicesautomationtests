package com.dstvo.test.automation.responsivesiteTests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ResponsiveHomePageStripTest extends TestBase{
	
	@BeforeClass
	@Override
	public void before() throws Exception {
	
		super.before();
		getWebDriver().get(getSiteURL());
		  driver.manage().window().maximize();
	}
	
	@Test(priority=1)
	//test if a user clicks on the Text Spotlight is redirected to the news page
	
	public void TestSpotlight() throws Exception{
		
		System.out.println("=== statring spotlight Test===");
		
		WebElement high = driver.findElementByXPath("html/body/main/div/section[1]/div[3]/a");
        String high2 = high.getAttribute("href");
         //Thread.sleep(2000);	
             driver.get(high2);
                //Thread.sleep(2000);	
               
                
                ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/en-za/news");
                
                getWebDriver().get(getSiteURL());
                //Thread.sleep(2000);
                
                driver.manage().window().maximize();
	}
	
	@Test(priority=2)
	
	//test if a user clicks on the Text More Highlight is redirected to the highlights  page
	
	public void TestMoreHighlights() throws Exception{
		
		System.out.println("=== statring More Highlight Test===");
		
		WebElement high = driver.findElementByXPath("html/body/main/div/section[3]/div[3]/a");
        String high2 = high.getAttribute("href");
         //Thread.sleep(2000);	
             driver.get(high2);
                //Thread.sleep(2000);	
             
               ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/en-za/highlights");
               
                getWebDriver().get(getSiteURL());
                 //Thread.sleep(2000);
                 driver.manage().window().maximize();
             
	}
	
	@Test
	
	//test if the user click on the text More catch Up is redirected to the now.dstv.com site
	
	public void TestMoreCatchUp() throws Exception{
		
		System.out.println("=== statring More Catch Up Test===");
		
		ReusableFunc.ClickByXpath(driver, "html/body/main/div/section[4]/div[3]/a", "More Catch Up");
		Thread.sleep(2000);
		
		ReusableFunc.CompareUrls(driver, "http://staging-now.dstv.com/catchup");
		
	    getWebDriver().get(getSiteURL());

	    driver.manage().window().maximize();
	} 
	
	@Test(priority=3)
	//test if a user clicks on the text more BoxOffice is redirected to Box
	
	public void TestMoreBoxOffice() throws Exception{
		
		System.out.println("=== statring More Box Office Test===");
		
		ReusableFunc.ClickByXpath(driver, "html/body/main/div/section[2]/div[3]/a", "More Box Office");
		 Thread.sleep(2000); 
		 ReusableFunc.CompareUrls(driver, "http://boxoffice.dstv.com/");
		 
	getWebDriver().get(getSiteURL());	
	  
	  driver.manage().window().maximize();
		 
	}
	
	
	
}
