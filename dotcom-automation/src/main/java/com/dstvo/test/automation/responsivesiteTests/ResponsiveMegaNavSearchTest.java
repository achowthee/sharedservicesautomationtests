package com.dstvo.test.automation.responsivesiteTests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ResponsiveMegaNavSearchTest extends TestBase {
	
	@BeforeClass
	@Override
	public void before() throws Exception {
	
		super.before();
		getWebDriver().get(getSiteURL());
		  driver.manage().window().maximize();
		
	}
	
	
	@Test
	
	//search using search key
	
	public void canSearchbySearchKey() throws Exception {
	
		ReusableFunc.ClickByID(driver, "searchform");
		//Thread.sleep(2000);
	//	Utilities.fluentWait(By.id("searchbar"), driver);
		ReusableFunc.searchBySearchKey(driver, By.id("searchTerm"), By.id("searchbtn"), "kill");
		//Thread.sleep(2000);
		//verify	
		ReusableFunc.VerifyTextOnPage(driver, "TV Guide");
		 ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/search?searchTerm=kill");
		//Thread.sleep(5000);
		getWebDriver().get(getSiteURL());
		  driver.manage().window().maximize();
		
	}
	
	@Test
	
	//search using enter
	public void canSearchbyEnterKey() throws Exception {
		
		ReusableFunc.ClickByID(driver, "searchform");
		//Thread.sleep(2000);
		//Utilities.fluentWait(By.id("searchbar"), driver);
		ReusableFunc.searchByEnterKey(driver,By.id("searchTerm"), "game");
		Thread.sleep(8000);
		ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/search?searchTerm=game");
		ReusableFunc.VerifyTextOnPage(driver, "TV Guide");
		getWebDriver().get(getSiteURL());
		  driver.manage().window().maximize();
		
		
	}
	
	@Test
	
	//test if search works after landing on a page other than the home page.
	
	public void TestSearchOnHotOnTV()throws Exception{
		
		 String originalHandle = driver.getWindowHandle();//parent window so i close the popu window
		   Actions builder = new Actions(driver);
		
		WebElement hoverover = driver.findElement(By.xpath(".//*[@id='cd-primary-nav']/li[1]/a"));//find the main hover element
		 builder.moveToElement(hoverover).click().build().perform();
		 
		 for(String handle : driver.getWindowHandles()) {
		        if (!handle.equals(originalHandle)) {
		            driver.switchTo().window(handle);
		            driver.close();
		        }
		    }
		  driver.switchTo().window(originalHandle);
		  
		  ReusableFunc.ClickByXpath(driver, "html/body/main/div/section[3]/div[3]/a", "Hot on TV");
		   ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/en-za/highlights");
		   
		    canSearchbyEnterKey();//call the serach method by enter key.	    
		
	}
	
	
	@Test(priority=5)
	
	 //this is to test if search is working after changing the country
	public void SearchAfterCountryChange() throws Exception{
		
		  ReusableFunc.ClickByXpath(driver, ".//*[@id='dstvNavCountries']/a", "Country Drop down");
		  //Thread.sleep(2000);
		       
		      ReusableFunc.ClickByXpath(driver, ".//*[@id='dstvNavCountries']/ul/li[2]/a", "Angola");
		      
		       //Thread.sleep(2000);
		     ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/pt-ao"); 
		     
		     ReusableFunc.ClickByID(driver, "searchform");
				//Thread.sleep(2000);
				//Utilities.fluentWait(By.id("searchbar"), driver);
				ReusableFunc.searchByEnterKey(driver,By.id("searchTerm"), "home");
				//Thread.sleep(2000);
				ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/search?searchTerm=home");
				ReusableFunc.VerifyTextOnPage(driver, "TV Guide");
		
				getWebDriver().get(getSiteURL());
				  driver.manage().window().maximize();
				
	}
	
	
}
