package com.dstvo.test.automation.responsivesiteTests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ResponsiveMegaNavTestTVGuide extends TestBase {
	
	@BeforeClass

	public void before() throws Exception {
        super.before();
		getWebDriver().get(getSiteURL());
		driver.manage().window().maximize();
		
	}


    //TV Guide
	@Test
	public void canAccess14DayGuide() throws Exception {
		
		ReusableFunc.ClickSubNav(driver, By.xpath(".//*[@id='cd-primary-nav']/li[1]/a"), By.xpath(".//*[@id='cd-primary-nav']/li[1]/ul/li[3]/ul/li[3]/a"));
		 Thread.sleep(2000);
		  ReusableFunc.CompareUrls(driver, "http://guide.dstv.com/");

            getWebDriver().get(getSiteURL());
        
        driver.manage().window().maximize();
  
	}
	
	
	//hot on Tv
	@Test
	public void canAccessHighlights() throws Exception {

		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath(".//*[@id='dstv-navbar-collapse-1']/ul/li[1]/a"), By.xpath(".//*[@id='dstv-navbar-collapse-1']/ul/li[1]/ul/li[1]/div/div/ul[1]/li[3]/a"), "http://www.dstv.com/highlights");
		//Thread.sleep(2000);
		//ReusableFunc.CompareUrls(driver, "http://www.dstv.com/highlights");
		getWebDriver().get(getSiteURL());
		 driver.manage().window().maximize();
		
	}
	
	
	//Series Calendar
	@Test
	public void canAccessSeriesCalendar() throws Exception {
		

		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath(".//*[@id='dstv-navbar-collapse-1']/ul/li[1]/a"), By.xpath(".//*[@id='dstv-navbar-collapse-1']/ul/li[1]/ul/li[1]/div/div/ul[1]/li[4]/a"), "http://www.dstv.com/en-za/topic/premium-series-calendar-20150402");
		//Thread.sleep(2000);
		//ReusableFunc.CompareUrls(driver, "http://www.dstv.com/en-za/topic/premium-series-calendar-20150402");
		getWebDriver().get(getSiteURL());
		
		 driver.manage().window().maximize();
	
	}

	// Movies Calendar
	@Test
	public void canAccessMoviesCalendar() throws Exception {
		

		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath(".//*[@id='dstv-navbar-collapse-1']/ul/li[1]/a"), By.xpath(".//*[@id='dstv-navbar-collapse-1']/ul/li[1]/ul/li[1]/div/div/ul[1]/li[5]/a"), "http://www.dstv.com/topic/premium-movies-calendar-20150409");
		//Thread.sleep(2000);
		getWebDriver().get(getSiteURL());
		 driver.manage().window().maximize();
		
	}
	
  
}
