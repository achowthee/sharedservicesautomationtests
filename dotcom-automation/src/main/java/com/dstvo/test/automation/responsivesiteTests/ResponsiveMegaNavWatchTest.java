package com.dstvo.test.automation.responsivesiteTests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ResponsiveMegaNavWatchTest extends TestBase {
	
	@BeforeClass
	@Override
	public void before() throws Exception {
	
		super.before();
		getWebDriver().get(getSiteURL());
		
	}

	@Test
	
	//Test if .com site can redirect to BoxOffice Rent Movies Online
	public void BoxOffice_Rent_Movies_Online() throws Exception {

		
		
	
		ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[2]/a", "Watch");// Expand Watch Level on the Meganav
		  //Thread.sleep(2000);
		     ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[2]/ul/li[1]/div/div/ul[3]/li[3]/a", "Rent Movies online"); // Click on rent Movies Online
		     
		       //Thread.sleep(2000);
		       
		     ReusableFunc.CompareUrls(driver, "http://boxoffice.dstv.com/browse-movies/now");  
		     
		getWebDriver().get(getSiteURL());
		
	}
	
	@Test
	
	//Test if .com site can redirect to BoxOffice available Movies on PVR
	
	public void BoxOffice_Available_On_PVR() throws Exception {
		

		ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[2]/a", "Watch");// Expand Watch Level on the Meganav
		  //Thread.sleep(2000);
		     ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[2]/ul/li[1]/div/div/ul[3]/li[4]/a", "Available on Pvr"); // Click on Avilable on PVR
		     
		       //Thread.sleep(2000);
		       
		     ReusableFunc.CompareUrls(driver, "http://boxoffice.dstv.com/browse-movies/pvr");  
		     
		getWebDriver().get(getSiteURL());
		
		
	}
	

	
	//Test if .com site can redirect to Supersport's live streaming page
	
//	public void Watch_Supersport_Live_Streaming() throws Exception {
//		
//
//	String parentHandle = driver.getWindowHandle();//parent window
//			
//		ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[2]/a", "Watch");// Expand Watch Level on the Meganav
//		  //Thread.sleep(2000);
//		     ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[2]/ul/li[1]/div/div/ul[2]/li[2]/a", "Live Streaming"); // Live Streaming
//		     
//		       //Thread.sleep(2000);
//		       
//		       for (String winHandle : driver.getWindowHandles()) {
//		    	    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
//		    	}
//		 
//		       
//		     ReusableFunc.CompareUrls(driver, "http://www.supersport.com/live-video");  
//		     
//		     driver.close(); // close newly opened window when done with it
//			    driver.switchTo().window(parentHandle);
//		     
//		getWebDriver().get(getSiteURL());
//		
//		
//	}
//	

	
	//Test if .com site can redirect to Supersport's Highlights Page
	
	public void Watch_Supersport_Highlights_Page_Redirect() throws Exception {
	

	String parentHandle = driver.getWindowHandle();//parent window
	
		ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[2]/a", "Watch");// Expand Watch Level on the Meganav
		  //Thread.sleep(2000);
		     ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[2]/ul/li[1]/div/div/ul[2]/li[3]/a", "Highlights"); // click on Highlights
		     
		       //Thread.sleep(2000);
		       
		       for (String winHandle : driver.getWindowHandles()) {
		    	    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		    	}
		 
		       
		     ReusableFunc.CompareUrls(driver, "http://www.supersport.com/video");
		     
		     driver.close(); // close newly opened window when done with it
			    driver.switchTo().window(parentHandle);
		     
		getWebDriver().get(getSiteURL());
		
		
	}

@Test

//Test if .com site can redirect to Now.Dstv.com home page
  public void Watch_DStv_Now_Home_Page_Redirect() throws Exception {
	

	  ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[2]/a", "Watch");// Expand Watch Level on the Meganav
	    //Thread.sleep(2000);
	     ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[2]/ul/li[1]/div/div/ul[1]/li[2]/a", "Now Home Page"); // click on Home
	     
	       //Thread.sleep(2000); 
	     ReusableFunc.CompareUrls(driver, "http://now.dstv.com/");
	getWebDriver().get(getSiteURL());
 }

@Test

 //Test if .com site can redirect to Now.Dstv.com/LiveTv 
   public void Watch_DStv_Now_Live_TV_Page_Redirect() throws Exception {

	  ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[2]/a", "Watch");// Expand Watch Level on the Meganav
	    //Thread.sleep(2000);
	     ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[2]/ul/li[1]/div/div/ul[1]/li[3]/a", "Live TV"); // click on Live Tv
	       //Thread.sleep(2000); 
	        ReusableFunc.CompareUrls(driver, "http://now.dstv.com/LiveTv");
	getWebDriver().get(getSiteURL());
}
	
@Test

//Test if .com site can redirect to Now.Dstv.com/CatchUp#Recomandation 
  public void Watch_DStv_Now_Catch_UP_Page_Redirect() throws Exception {
	
	
	String parentHandle = driver.getWindowHandle();//parent window
	
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[2]/a", "Watch");// Expand Watch Level on the Meganav
	    //Thread.sleep(2000);
	     ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[2]/ul/li[1]/div/div/ul[1]/li[4]/a", "Catch Up"); // click on Catch Up
	       //Thread.sleep(2000); 
	       
	       for (String winHandle : driver.getWindowHandles()) {
	    	    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
	    	}
	 
	        ReusableFunc.CompareUrls(driver, "http://now.dstv.com/CatchUp#Recommended");
	        
		     driver.close(); // close newly opened window when done with it
			    driver.switchTo().window(parentHandle); 
			    
	      getWebDriver().get(getSiteURL());
   }

@Test

//Test if .com site can redirect to get the Best from Dstv now page

public void Watch_DStv_Now_Get_The_Best_From_DStv_Now_Page_Redirect() throws Exception {
	

	String parentHandle = driver.getWindowHandle();//parent window
	
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[2]/a", "Watch");// Expand Watch Level on the Meganav
	    //Thread.sleep(2000);
	     ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[2]/ul/li[1]/div/div/ul[1]/li[6]/a", "Get Best From Dstv Now"); // click on Get The Best from Dstv Now
	       //Thread.sleep(2000); 
	       
	       for (String winHandle : driver.getWindowHandles()) {
	    	    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
	    	}
	       
	        ReusableFunc.CompareUrls(driver, "http://www.dstv.com/topic/dstv-apps-20150313");
	        
	        driver.close(); // close newly opened window when done with it
		    driver.switchTo().window(parentHandle);
		    
	      getWebDriver().get(getSiteURL());
	      
     }

@Test

//Test if .com site can redirect to FAQs on now.DStv.com

public void Watch_DStv_Now_FAQs_Page_Redirect() throws Exception {
	
	
	
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[2]/a", "Watch");// Expand Watch Level on the Meganav
	    //Thread.sleep(2000);
	     ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[2]/ul/li[1]/div/div/ul[1]/li[5]/a", "FAQs "); // click on FAQs

	        ReusableFunc.CompareUrls(driver, "http://now.dstv.com/Home/Help");	    
	      getWebDriver().get(getSiteURL());
  }
		
}
