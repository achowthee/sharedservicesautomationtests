package com.dstvo.test.automation.responsivesiteTests;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ResponsiveMeganavRedirects  extends TestBase{
	
	@BeforeClass
	  @Override
	  public void before() throws Exception{

		  super.before();
		  getWebDriver().get(getSiteURL());
		  driver.manage().window().maximize();
		  
	  }
	
	@Test(priority =8)
	//test if go to redirect to self service
	
	public void ResponsiveClickOnGoTo() throws Exception{

		ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[5]/a", "Services");
		  //Thread.sleep(2000);
		    
		   //click self service
		   ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[5]/ul/li[1]/div/div/ul[1]/li[2]/a", "self Service");
		     //Thread.sleep(2000);
		        ReusableFunc.CompareUrls(driver, "https://selfservice.dstv.com/");
		          getWebDriver().get(getSiteURL());
		          driver.manage().window().maximize();
	}
	
	@Test(priority =2)
	// redirects to packages
	
	public void ResponsiveRedirectToPackages() throws Exception{
			
		ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[5]/a", "expand Services");
		  //Thread.sleep(2000);
		    ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[5]/ul/li[2]/div/div/ul[2]/li[2]/a", "Packages");
		    //Thread.sleep(2000);
		    
		    ReusableFunc.CompareUrls(driver, "https://selfservice.dstv.com/products");
		    //Thread.sleep(2000);
		    getWebDriver().get(getSiteURL());
		    driver.manage().window().maximize();
	}
	
	@Test(priority =3)
	
	//redirects to decoders/device page
	
	public void ResponsiveRedirectToDecoders() throws Exception{
		

		ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[5]/a", "expand Services");
		  //Thread.sleep(2000);
		    ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[5]/ul/li[2]/div/div/ul[1]/li[2]/a", "Decoders");
		      //Thread.sleep(2000);
		      
		      
			    ReusableFunc.CompareUrls(driver, "https://selfservice.dstv.com/devices");
			    
			    getWebDriver().get(getSiteURL()); 
			    driver.manage().window().maximize();
	}
	
	@Test(priority = 4)
	
	 //redirect to fixing errors
	
	public void ResponsiveRedirectToFixingErrors() throws Exception{

		

		ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[5]/a", "expand Services");
		  //Thread.sleep(2000);
		    ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[5]/ul/li[2]/div/div/ul[1]/li[3]/a", "Fixing Errors");
		      //Thread.sleep(2000);
		      
		      
			    ReusableFunc.CompareUrls(driver, "https://selfservice.dstv.com/MyDStv/FixErrors#/decoders/clearerrorcode");
			    
			    getWebDriver().get(getSiteURL()); 
			    driver.manage().window().maximize();
	}
	
	@Test(priority = 5)
	
	 //redirect to Help How to pay
	
	public void ResponsiveRedirectToHowToPay() throws Exception{
		

		
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[5]/a", "expand Services");
		  //Thread.sleep(2000);
		    ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[5]/ul/li[2]/div/div/ul[1]/li[4]/a", "How to Pay");
		      //Thread.sleep(2000);
		      
		      
			    ReusableFunc.CompareUrls(driver, "https://selfservice.dstv.com/how-to-pay");
			    
			    getWebDriver().get(getSiteURL()); 
			    driver.manage().window().maximize();
	}
	
	@Test(priority = 6)
	
	 //redirect to FAQs
   public void ResponsiveRedirectToFAQs() throws Exception{
		

		
	
		
		String parentHandle = driver.getWindowHandle();//parent window
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[5]/a", "expand Services");
		  //Thread.sleep(2000);
		    ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[5]/ul/li[2]/div/div/ul[1]/li[5]/a", "FAQs");
		      //Thread.sleep(2000);
		      
		      for (String winHandle : driver.getWindowHandles()) {
		    	    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		    	}
		 
			    ReusableFunc.CompareUrls(driver, "https://selfservice.dstv.com/faqs/");
			    
			    driver.close(); // close newly opened window when done with it
			    driver.switchTo().window(parentHandle);
			    
			    getWebDriver().get(getSiteURL());
			    driver.manage().window().maximize();
	}
	
	
	@Test(priority = 7)
	
	 //redirect to Find Installer
  public void ResponsiveRedirectToFindInstaller() throws Exception{

		ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[5]/a", "expand Services");
		  //Thread.sleep(2000);
		    ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[5]/ul/li[2]/div/div/ul[3]/li[2]/a", "Find Installer");
		      //Thread.sleep(2000);
		      
		      
			    ReusableFunc.CompareUrls(driver, "https://selfservice.dstv.com/InstallationHelp/Installers#/findInstaller");
			    
			    getWebDriver().get(getSiteURL()); 
			    driver.manage().window().maximize();
	
	}
   @Test(priority = 1)
	//redirect to General Installation Information
     public void ResponsiveRedirectToGeneralInstallationInfo() throws Exception{
	   
	 
		ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[5]/a", "expand Services");
		  //Thread.sleep(2000);
		    ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-navbar-collapse-1']/ul/li[5]/ul/li[2]/div/div/ul[3]/li[3]/a", "General Installation Info");
		      //Thread.sleep(2000);
		      
		      
			    ReusableFunc.CompareUrls(driver, "http://www.dstv.com/topic/general-installation-information-20150813");
			    
			    getWebDriver().get(getSiteURL()); 
			    driver.manage().window().maximize();
	}
   
}
