package com.dstvo.test.automation.responsivesiteTests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;


public class ResponsivemeganavChangeCountry extends TestBase {
 
  @BeforeClass
  public void before() throws Exception {
      super.before();
	  getWebDriver().get(getSiteURL());
	  driver.manage().window().maximize();
  }
  
  //test if you can change country to angola
  
  @Test 
  public void canChangeCountry() throws Exception{

	  ReusableFunc.ClickByXpath(driver, ".//*[@id='dstvNavCountries']/a", "Country Drop down");
	    Thread.sleep(2000);
	       
	      ReusableFunc.ClickByXpath(driver, ".//*[@id='dstvNavCountries']/ul/li[2]/a", "Angola");
	      
	       Thread.sleep(2000);
	     ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/pt-ao");  
	  
	  
//	  ReusableFunc.ClickSubNav(driver, By.xpath(".//*[@id='dstvNavCountries']/a/img"), By.xpath(".//*[@id='dstvNavCountries']/ul/li[2]/a"));
//	   //Thread.sleep(2000);
//	   ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/pt-ao");
//	   
    }

  
   @Test (dependsOnMethods ="canChangeCountry")
   
   //can change angola Language to English
   
   public void ChangeLanguageToEnglish() throws Exception{

	   ReusableFunc.ClickByXpath(driver, ".//*[@id='dstvNavLanguages']/a", "change Language");
	   Thread.sleep(2000);
	   
	       ReusableFunc.ClickByXpath(driver, ".//*[@id='dstvNavLanguages']/ul/li[2]/a", "english");  
	       Thread.sleep(2000);
	        
	        
	    ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/en-ao");   
	    Thread.sleep(2000);
	    
	    ReusableFunc.ClickByXpath(driver, ".//*[@id='dstvNavLanguages']/a", "change Language");
	    Thread.sleep(2000);
	       ReusableFunc.ClickByXpath(driver, ".//*[@id='dstvNavLanguages']/ul/li[3]/a", "Portugues");  
	       Thread.sleep(2000);
	       
	       ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/pt-ao");   
		
		getWebDriver().get(getSiteURL());
		
		  driver.manage().window().maximize();
		
		
   }
   
   @Test (dependsOnMethods = "ChangeLanguageToEnglish")
   
   /** change country to Mozambique after changing angola language to english this test verifies that for mozmbiques it will change to 
    * Default Language (Portugues)*/
   
   public void ChangeToMozambique() throws Exception{
	   
	   ReusableFunc.ClickByXpath(driver,".//*[@id='dstvNavCountries']/a", "country drop down");
	     Thread.sleep(2000);
	       ReusableFunc.ClickByXpath(driver, ".//*[@id='dstvNavCountries']/ul/li[31]/a", "select Mozambique");
	          Thread.sleep(2000);
	          
	          /** this assertion checks that our site change country from angola and also changed language to the default
	           language of Mozambique (Portugues)
	           */
	          ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/pt-mz");
	          
	          getWebDriver().get(getSiteURL());
	     
	          driver.manage().window().maximize();
   }
   
@Test (dependsOnMethods = "ChangeToMozambique")
   
   /**change from a country with portugues as default language to a country with english as default language for
    * this Test will change to south africa  */
   
public void changeToEnglishSpeakingCountry() throws Exception{
	 ReusableFunc.ClickByXpath(driver,".//*[@id='dstvNavCountries']/a", "country drop down");
     Thread.sleep(2000);
       ReusableFunc.ClickByXpath(driver, ".//*[@id='dstvNavCountries']/ul/li[41]/a", "South Africa");
          Thread.sleep(2000);
          
         
         ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/en-za");
         
         getWebDriver().get(getSiteURL());
         driver.manage().window().maximize();
   }

@Test(dependsOnMethods = "changeToEnglishSpeakingCountry")


/**check if can change Country to Nigiara  */

public void changeToNigeria() throws Exception{
	 ReusableFunc.ClickByXpath(driver,".//*[@id='dstvNavCountries']/a", "country drop down");
  Thread.sleep(2000);
    ReusableFunc.ClickByXpath(driver, ".//*[@id='dstvNavCountries']/ul/li[34]/a", "Nigeria");
       Thread.sleep(2000);
       
      
      ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/en-ng");
      
      getWebDriver().get(getSiteURL());
      driver.manage().window().maximize();
}

   
}
