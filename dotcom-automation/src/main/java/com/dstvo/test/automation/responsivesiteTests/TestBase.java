package com.dstvo.test.automation.responsivesiteTests;

import java.net.URL;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.*;


public class TestBase {
	  //  private static ChromeDriverService service;
	    public RemoteWebDriver driver;  //was private, changed to use in other tests    
	 

 
	    @BeforeClass
		public void before() throws Exception {
	    	
	  driver = new RemoteWebDriver(new URL("http://10.10.4.161:4474/wd/hub"), DesiredCapabilities.chrome());

	     //System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe"); 
	    
	   //ChromeOptions profile  = new ChromeOptions();
	   
	
    	  /** profile.setPreference("network.proxy.type", 1);

    	    profile.setPreference("network.proxy.http", "03rnb-proxy06");
    	    profile.setPreference("network.proxy.http_port", 8080);
    	    profile.setPreference("network.proxy.ssl", "03rnb-proxy06");
    	    profile.setPreference("network.proxy.ssl_port", 8080);
    	    profile.setPreference("network.proxy.ftp", "03rnb-proxy06");
    	    profile.setPreference("network.proxy.ftp_port", 8080);
    	    profile.setPreference("network.proxy.Socks", "03rnb-proxy06");
    	    profile.setPreference("network.proxy.Socks_port", 8080);
    	    profile.setPreference("network.proxy.no_proxies_on","aut.responsive.dstv.com");
    	    
    	    **/
    	    
	  // driver = new ChromeDriver(profile);    	    
    	
    	 
	        
	    }

	    @AfterClass
		public void after() {
	        if (driver != null) {
	        driver.quit();
	        }
	    }
	    
	    protected String getSiteURL (){
	        String URL = System.getProperty("testUrl");
	        if (URL == null){
	                return "http://staging-dstv.dstv.com/";
	        }
	        else{
	                return URL;
	        }
	        
	    }
	        
	    	    
	    
	    protected RemoteWebDriver getWebDriver() {
	        return driver;
	    }
	    
	    
	}
