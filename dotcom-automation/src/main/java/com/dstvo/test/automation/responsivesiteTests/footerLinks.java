package com.dstvo.test.automation.responsivesiteTests;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

/** this class is test that all footer links are working and redirecting to the right place
 *  
 * 
 * @author Sibonginhlahla.Nkosi
 *
 */
public class footerLinks extends TestBase{

	@BeforeClass
	@Override
	public void before() throws Exception {

		super.before();
		getWebDriver().get(getSiteURL());
		driver.manage().window().maximize();
	}
	
	@Test(priority=1)
	
	//verify the Multichoice lin on the footer
	
	public void multichoicefooterLink() throws Exception{
		
	ReusableFunc.ClickByXpath(driver, ".//*[@id='multiChoiceFooterText']/p[2]/a[1]", "multichoice footer link");
		 //thread.sleep(6000);
		 
		   ReusableFunc.CompareUrls(driver, "http://www.multichoice.co.za/");
		   getWebDriver().get(getSiteURL());
		   driver.manage().window().maximize();
	}
	
	@Test(priority=2)
	
	//verify terms and conditions
	
	public void CustTandCsfooterlink() throws Exception{
		
		 String parentHandler = driver.getWindowHandle();
		ReusableFunc.ClickByXpath(driver, ".//*[@id='multiChoiceFooterText']/p[2]/a[2]", "Customer Terms & Conditions");
		 //thread.sleep(6000);
		 
		 for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); 
			  
			}
		  ReusableFunc.CompareUrls(driver, "http://www.dstv.com/topic/service-customer-terms-and-conditions-20150805");
		  
		  driver.close(); // close newly opened window when done with it
		  driver.switchTo().window(parentHandler);// switch back to the original window
		  
		  getWebDriver().get(getSiteURL());
		  driver.manage().window().maximize();
	}
	
	@Test (priority=3)
	
	//verify the terms of user footer link 
	
	public void TermsOfUse() throws Exception{
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='multiChoiceFooterText']/p[2]/a[3]", "Terms of use footer link");
		 //thread.sleep(5000);
		 
		 ReusableFunc.CompareUrls(driver, "http://www.dstv.com/topic/multichoice-ts-and-cs-20150313");
		
		 getWebDriver().get(getSiteURL());
		 driver.manage().window().maximize();
	}
	
	@Test (priority=4)
	
	//verify the Privacy policy footer link
	
	public void PrivacyPolicyfooterlink() throws Exception {
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='multiChoiceFooterText']/p[2]/a[4]", "Privacy Policy");
		
		  //thread.sleep(5000);
		  
		  ReusableFunc.CompareUrls(driver, "http://www.dstv.com/topic/privacy-policy-20150313");
	
		  getWebDriver().get(getSiteURL());
			 driver.manage().window().maximize();
	
	}
	
	@Test(priority=5)
	
	//verify the Responsible Disclosure Policy footer Link
	
	public void ResponsibleDisclosurePolicyfooterLink() throws Exception{
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='multiChoiceFooterText']/p[2]/a[5]", "Responsible Disclosure policy");
		
		  //thread.sleep(5000);
		   
		   ReusableFunc.CompareUrls(driver, "http://www.dstv.com/topic/multichoice-responsible-disclosure-policy-20151028");
		    
		   getWebDriver().get(getSiteURL());
		   driver.manage().window().maximize();
	}
	
	@Test(priority=6)
	
	//verify the Copyright footer link
	
	public void CopyRightFooterLink() throws Exception{
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='multiChoiceFooterText']/p[2]/a[6]", "Copyright");
		
		  //thread.sleep(5000);
		  
		  ReusableFunc.CompareUrls(driver, "http://www.dstv.com/topic/copyright-20150313");
		  
		  getWebDriver().get(getSiteURL());
		   driver.manage().window().maximize();
	}
	
	@Test(priority=7)
	
	//Verify Open source footer link
	
	public void OpenSourceFooterLink() throws Exception{
		
		String parentHandler = driver.getWindowHandle();
		ReusableFunc.ClickByXpath(driver, ".//*[@id='multiChoiceFooterText']/p[2]/a[7]", "Open Source footer link");
		 //thread.sleep(5000);
		 
		 for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); 
			}
		 ReusableFunc.CompareUrls(driver, "https://selfservice.dstv.com/login?ReturnUrl=%2Fmydstv%2Ffoss");
		 driver.close(); // close newly opened window when done with it
		  driver.switchTo().window(parentHandler);// switch back to the original window
		  
		  getWebDriver().get(getSiteURL());
		  driver.manage().window().maximize();
	}
	
	
	@Test(priority=8)
	
	//Verify iab footer link
	
	public void IABfooterLink() throws Exception{
		
		String parentHandler = driver.getWindowHandle();
		ReusableFunc.ClickByXpath(driver, ".//*[@id='multiChoiceFooterText']/p[2]/a[8]", "iab footer link");
		 //thread.sleep(5000);
		 
		 for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); 
			}
		 ReusableFunc.CompareUrls(driver, "http://iabsa.net/");
		 driver.close(); // close newly opened window when done with it
		  driver.switchTo().window(parentHandler);// switch back to the original window
		  
		  getWebDriver().get(getSiteURL());
		  driver.manage().window().maximize();
	}
	
	@Test(priority=9)
	
	//verify contact us footer link 
	
	public void ContactUsfooterLink() throws Exception{
		
		
		String parentHandler = driver.getWindowHandle();
		ReusableFunc.ClickByXpath(driver, ".//*[@id='multiChoiceFooterText']/p[2]/a[9]", "contuct us");
		 //thread.sleep(5000);
		 
		 for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); 
			}
		 ReusableFunc.CompareUrls(driver, "https://selfservice.dstv.com/contact-us");
		 driver.close(); // close newly opened window when done with it
		  driver.switchTo().window(parentHandler);// switch back to the original window
		  
		  getWebDriver().get(getSiteURL());
		  driver.manage().window().maximize();
	}
	
	@Test(priority=10)
	
	//verify careers footer link
	
	public void CareersFooterLink() throws Exception{
		
		String parentHandler = driver.getWindowHandle();
		ReusableFunc.ClickByXpath(driver, ".//*[@id='multiChoiceFooterText']/p[2]/a[10]", "Careers");
		 //thread.sleep(5000);
		 
		 for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); 
			}
		 ReusableFunc.CompareUrls(driver, "https://multichoice.taleo.net/careersection/mgroup_external_cs1/jobsearch.ftl?lang=en&portal=10105120266");
		 driver.close(); // close newly opened window when done with it
		  driver.switchTo().window(parentHandler);// switch back to the original window
		  
		  getWebDriver().get(getSiteURL());
		  driver.manage().window().maximize();
	}
	
	@Test (priority=11)
	
	//verify the scam alert footer link 
	
	public void ScamAlert() throws Exception{
		
		String parentHandler = driver.getWindowHandle();
		ReusableFunc.ClickByXpath(driver, ".//*[@id='multiChoiceFooterText']/p[2]/a[11]", "Careers");
		 Thread.sleep(5000);
		 
		 for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); 
			}
		 ReusableFunc.CompareUrls(driver, "https://selfservice.dstv.com/announcement/beware-of-these-scams-20150521");
		 driver.close(); // close newly opened window when done with it
		  driver.switchTo().window(parentHandler);// switch back to the original window
		  
		  getWebDriver().get(getSiteURL());
		  driver.manage().window().maximize();
	}
	
}
