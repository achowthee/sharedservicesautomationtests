package com.dstvo.automation.dstvNowWeb;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.RandomStringGen;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class HomePageTest extends TestBase {

	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}

	@Test
	public void HomePage() throws Exception {
		//verify logo exists
		ReusableFunc.ClickByXpath(driver, "//*[@id='logo']/a/img","logo");
	   //verify title
		ReusableFunc.ContainsPageTitle(driver, "DStv Now");
		Thread.sleep(2000);
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//*[@id='site-wrap']/section[1]/div/div/div"), "verify billboards display");
	}
	
	@Test 
	public void Watch() throws Exception{
		System.out.println("starting dstv->watch-DStv Now dropdown test");
	    //click DStv.com on watch
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("//*[@id='dstv-breadcrumbs']/ul/li[2]/a"), By.xpath("//*[@id='dstv-meganav-content']/div[1]/ul/li/a"), "https://www.dstv.com/");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, "//*[@id='dstv-breadcrumbs']/ul/li[1]/a", "collapse");
	    //click BoxOffice on watch
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("//*[@id='dstv-breadcrumbs']/ul/li[2]/a"), By.xpath("//*[@id='dstv-meganav-content']/div[2]/ul/li[1]/a"), "http://boxoffice.dstv.com/");
		ReusableFunc.ClickByXpath(driver, "//*[@id='dstv-breadcrumbs']/ul/li[1]/a", "collapse");
		//click catchUp on watch
		//ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("//*[@id='dstv-breadcrumbs']/ul/li[2]/a"), By.linkText("Catch Up"), "http://catchup.dstv.com/");
	    //ReusableFunc.ClickByXpath(driver, "//*[@id='dstv-breadcrumbs']/ul/li[1]/a", "collapse");
		//click Live sport
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("//*[@id='dstv-breadcrumbs']/ul/li[2]/a"), By.linkText("Live Sport"), "http://www.supersport.com/live-video");
		ReusableFunc.ClickByXpath(driver, "//*[@id='dstv-breadcrumbs']/ul/li[1]/a", "collapse");
		//Click my Account
		//ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("//*[@id='dstv-breadcrumbs']/ul/li[2]/a"), By.linkText("My Account"), "https://selfservice.dstv.com/self-service/my-account/o");
		//ReusableFunc.ClickByXpath(driver, "//*[@id='dstv-breadcrumbs']/ul/li[1]/a", "collapse");
		//Click Digital Magazine link
		//ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("//*[@id='dstv-breadcrumbs']/ul/li[2]/a"), By.linkText("Digital Magazine"), "http://go.dstv.com/digimag/dish_digimag.html");
		//ReusableFunc.ClickByXpath(driver, "//*[@id='dstv-breadcrumbs']/ul/li[1]/a", "collapse");
		//Click news letters
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("//*[@id='dstv-breadcrumbs']/ul/li[2]/a"), By.linkText("Newsletters"), "http://www.dstv.com/Connect/Newsletters/");
		ReusableFunc.ClickByXpath(driver, "//*[@id='dstv-breadcrumbs']/ul/li[1]/a", "collapse");
		//Click Deals
		//ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("//*[@id='dstv-breadcrumbs']/ul/li[2]/a"), By.linkText("Deals"), "http://www.dstv.com/SubPages/Deals%20for%20you/32/114");
		//ReusableFunc.ClickByXpath(driver, "//*[@id='dstv-breadcrumbs']/ul/li[1]/a", "collapse");
		//CLick Winners Circle
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("//*[@id='dstv-breadcrumbs']/ul/li[2]/a"), By.linkText("Winners Circle"), "http://www.dstv.com/content/Winner_s%20Circle/105");
		ReusableFunc.ClickByXpath(driver, "//*[@id='dstv-breadcrumbs']/ul/li[1]/a", "collapse");
		//CLick Competitions
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("//*[@id='dstv-breadcrumbs']/ul/li[2]/a"), By.linkText("Competitions"), "http://www.dstv.com/competitions/");
		ReusableFunc.ClickByXpath(driver, "//*[@id='dstv-breadcrumbs']/ul/li[1]/a", "collapse");
		//Click Payments
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("//*[@id='dstv-breadcrumbs']/ul/li[2]/a"), By.linkText("Make Payments"), "https://selfservice.dstv.com/self-service/my-account/subscriptions/credit-card-payments/");
		ReusableFunc.ClickByXpath(driver, "//*[@id='dstv-breadcrumbs']/ul/li[1]/a", "collapse");
		//Clear errors
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("//*[@id='dstv-breadcrumbs']/ul/li[2]/a"), By.linkText("Clear Errors"), "http://selfservice.dstv.com/self-service/");
		ReusableFunc.ClickByXpath(driver, "//*[@id='dstv-breadcrumbs']/ul/li[1]/a", "collapse");
		//visit self service
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("//*[@id='dstv-breadcrumbs']/ul/li[2]/a"), By.linkText("Visit Self Service"), "http://selfservice.dstv.com/");
		ReusableFunc.ClickByXpath(driver, "//*[@id='dstv-breadcrumbs']/ul/li[1]/a", "collapse");
		System.out.println("dstv->watch-DStv Now dropdown test complete");
		
	}
	
	@Test
	public void topNav() throws Exception{
		System.out.println("starting top nav links next to connect test");
		ReusableFunc.ClickByLinkText(driver, "FAQs");
		ReusableFunc.ClickByID(driver, "testID");
		ReusableFunc.VerifyTextOnPage(driver, "FAQs");
		ReusableFunc.ClickByLinkText(driver, "Home");
		ReusableFunc.ClickByCss(driver, "#desktop-player-link>a");
		Thread.sleep(3000);
		ReusableFunc.VerifyTextOnPage(driver, "Download for Mac OSX");
		ReusableFunc.VerifyTextOnPage(driver, "Download for Windows");
		ReusableFunc.VerifyTextOnPage(driver, "The DStv Desktop Player");
		ReusableFunc.ClickByLinkText(driver, "Home");
		System.out.println("top nav links next to connect test complete");
	}
	

	@Test
	public void NavLinks() throws Exception{
		System.out.println("starting Top Menu links test");
		//TVGuide
		ReusableFunc.SwitchWindows(driver, By.xpath("//*[@id='header-nav']/ul/li[2]/a"), "http://www.dstv.com/guide/", "", "TV Guide");
		//LiveTV
		ReusableFunc.ClickByXpath(driver, "//*[@id='header-nav']/ul/li[3]/a", "Live TV");
		Thread.sleep(2000);
		ReusableFunc.CompareUrls(driver, "http://10.10.24.196/LiveTv");
		System.out.println(driver.findElementByXPath("//*[@id='channel-table']").getText());
		//Utilities.isTextPresentAfterWait("channels", driver);
		ReusableFunc.ClickByLinkText(driver, "Home");
		Thread.sleep(2000);
		//Catch Up
		ReusableFunc.ClickByXpath(driver, "//*[@id='header-nav']/ul/li[4]/a", "CatchUp");
		ReusableFunc.VerifyTextOnPage(driver, "What We're Watching");
		ReusableFunc.ClickByLinkText(driver, "Home");
		//FindOutMore
		ReusableFunc.ClickByXpath(driver, "//*[@id='header-nav']/a[1]", "Find Out more");
		ReusableFunc.CompareUrls(driver, "http://10.10.24.196/Home/About");
		Thread.sleep(2000);
		ReusableFunc.ClickByLinkText(driver, "Home");
		System.out.println("Top Menu links test complete");
			}
	
	@Test
	public void search() throws Exception{
		System.out.println("starting homepage search box test");
		ReusableFunc.searchByEnterKey(driver,By.id("search"), "The");
	    Thread.sleep(2000);
	    ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//*[@id='search-tabs']/li[1]"), "Live TV");
	    ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//*[@id='search-tabs']/li[2]"), "Catch Up");
	    ReusableFunc.ClickByLinkText(driver, "Home");
	    System.out.println("ending homepage search box test");
	}
	
	@Test
	public void WhatWeAreWatching() throws Exception{
		System.out.println("starting what we are watching and express from the US");
		Thread.sleep(2000);
		//verify at least one title is displayed
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//*[@id='site-wrap']/div[2]/div[2]/div/div[1]/div/div[1]/div/a"), "what we are watching 1st item");
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//*[@id='site-wrap']/div[3]/div[2]/div/div[1]/div/div[1]/div/a"), "express from the US 1st item");
		System.out.println("what we are watching and express from the US complete");
	}
	
	//*[@id="site-wrap"]/div[2]/div[2]/div/div[1]/div/div[1]/div/a
	
	@Test
	public void billboardNotLoggedIn() throws Exception{
		System.out.println("starting billboard not logged in-login and register test");
		ReusableFunc.ClickByCss(driver, "#site-wrap > section.billboard-content > div > div > div > div > div > div > a.button.btn-large.primary");
		ReusableFunc.CompareUrls(driver, "http://10.10.24.196/Home/About");
		ReusableFunc.VerifyTextOnPage(driver, "How do I get it?");
		ReusableFunc.ClickByLinkText(driver, "Home");
		ReusableFunc.ClickByCss(driver, "#site-wrap > section.billboard-content > div > div > div > div > div > div > a:nth-child(2)");
		//login
		ConnectControl.connectLiteLoginUsernameEmail(driver, "DstvNowPRMNotLinked@Mailinator.com", "Password123", "DstvNowPRMNotLinked");
		ReusableFunc.ClickByLinkText(driver, "Home");
		//logout
	    ReusableFunc.ClickDropdownElement(driver, By.xpath("//*[@id='connect']/div/div[1]/span[2]"), By.xpath("//*[@id='connect']/div/div[2]/div/div[7]/a"));
		Thread.sleep(2000);
		ReusableFunc.ClickByCss(driver, "#site-wrap > section.billboard-content > div > div > div > div > div > div > a:nth-child(3)");
		//register
		//Enter details and click the Register button
		String strEmail = RandomStringGen.generateRandomString(5, RandomStringGen.Mode.ALPHANUMERIC);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='email']"), strEmail+"@mailinator.com");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='password']"), "123456");
		ReusableFunc.CheckCheckBox(driver, driver.findElementByXPath(".//*[@id='TermsAndConditions']"));
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/form/div[6]/div/button", "Register");
		ReusableFunc.VerifyTextOnPage(driver, "Thanks for registering.");
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/div[2]/div/a", "Back to site");
		Thread.sleep(1000);
		System.out.println("billboard not logged in-login and register test complete");
		
		
	}
	
	@Test
	public void siteMapLinks() throws Exception{
		System.out.println("starting site map links test");
		//liveTV link
		ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-now-nav']/li[2]/a", "LiveTV");
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//*[@id='channels']"), "channels");
		System.out.println(driver.findElementByXPath("//*[@id='channel-table']").getText());
 		ReusableFunc.ClickByLinkText(driver, "Home");
		//TV Guide link
		ReusableFunc.SwitchWindows(driver, By.xpath(".//*[@id='dstv-now-nav']/li[1]/a"), "http://guide.dstv.com/", "", "");
		//CatchUp
		ReusableFunc.ClickByXpath(driver, "//*[@id='dstv-now-nav']/li[3]/a", "CatchUp");
		ReusableFunc.VerifyTextOnPage(driver, "What We're Watching");
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//*[@id='catchUp-content']/div[2]/div[2]/div/div[1]/div/div[1]/div/a"), "express from the US 1st");
		ReusableFunc.ClickByLinkText(driver, "Home");
		//ABout DStv Now link
		ReusableFunc.ClickByCss(driver, "#dstv-now-nav > li:nth-child(4) > a");
		System.out.println(driver.findElementByXPath("//*[@id='selling-points']/div[1]/p").getText());
		ReusableFunc.ClickByLinkText(driver, "Home");
		//click DStv.com
		ReusableFunc.SwitchWindows(driver, By.cssSelector("#dstv-service-nav > li:nth-child(1) > a"), "http://www.dstv.com/", "", "");
		//click support - change to live chat...
		/*ReusableFunc.ClickByCss(driver, "#dstv-service-nav > li:nth-child(2) > a");
		ReusableFunc.CompareUrls(driver, "http://selfservice.dstv.com/");
		driver.get("http://10.10.24.196//");*/
		//Click contact us
		//ReusableFunc.SwitchWindows(driver, By.cssSelector("#dstv-service-nav > li:nth-child(3) > a"), "http://www.dstv.com/contactus/", "", "");
		//Click Ts and Cs
		ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-service-nav']/li[4]/a", "Ts And Cs");
		ReusableFunc.CompareUrls(driver, "http://10.10.24.196/Home/Terms");
		ReusableFunc.VerifyTextOnPage(driver, "Terms and Conditions");
		ReusableFunc.ClickByLinkText(driver, "Home");
		//Click FAQs
		ReusableFunc.ClickByXpath(driver, ".//*[@id='dstv-service-nav']/li[3]/a", "FAQs");
		ReusableFunc.CompareUrls(driver, "http://10.10.24.196/Home/Help");
		//Click Facebook icon
		ReusableFunc.SwitchWindows(driver, By.cssSelector(".facebook>img"), "https://www.facebook.com/DStvSouthAfrica", "DStv", "DStv");
		//click twitter icon
		ReusableFunc.SwitchWindows(driver, By.cssSelector(".twitter>img"), "https://twitter.com/dstv", "", "DStv");
        //click on g+
		ReusableFunc.SwitchWindows(driver, By.cssSelector(".google-plus>img"), "https://plus.google.com/+DstvOfficial/posts", "DStv", "DStv");
 		System.out.println("site map links test complete");
		
	}
	
	@Test
	public void appPlayStoreLinks() throws Exception{
		System.out.println("starting app and play store links test");
		//app store
		ReusableFunc.ClickByCss(driver, "#apps > li:nth-child(1) > a");
		ReusableFunc.VerifyTextOnPage(driver, "DStv Now");
		driver.get("http://10.10.24.196//");
		//Google Playstore
		ReusableFunc.ClickByCss(driver, "#apps > li:nth-child(1) > a");
		ReusableFunc.VerifyTextOnPage(driver, "DStv Now");
		driver.get("http://10.10.24.196");
		System.out.println("app and play store links test complete");
	}
	
	@Test
	public void footerLinksTest() throws Exception{
		System.out.println("starting footer links test");
		Thread.sleep(2000);
		//Multichoice logo
		ReusableFunc.verifyElementIsDisplayed(driver, By.cssSelector("#multichoice-logo > img"), "multichoice logo");
		//click Multichoice website
		ReusableFunc.SwitchWindows(driver, By.cssSelector("#multichoice-links > li:nth-child(1) > a"), "http://www.multichoice.co.za/multichoice/view/multichoice/en/page44115", "", "Multichoice");
		//click Terms of Use
		ReusableFunc.SwitchWindows(driver, By.cssSelector("#multichoice-links > li:nth-child(2) > a"), "http://www.dstv.com/Pages/TermsConditions", "", "");
		//click Privacy policy
		ReusableFunc.SwitchWindows(driver, By.cssSelector("#multichoice-links > li:nth-child(3) > a"), "http://www.dstv.com/PrivacyPolicy/", "", "");
		//click Copyright
		ReusableFunc.SwitchWindows(driver, By.cssSelector("#multichoice-links > li:nth-child(4) > a"), "http://www.dstv.com/Copyright/", "", "");
		//click Glossary
		ReusableFunc.SwitchWindows(driver, By.cssSelector("#multichoice-links > li:nth-child(5) > a"), "http://www.dstv.com/Glossary/", "", "");
        //clickDStv-i-broken link
		//ReusableFunc.SwitchWindows(driver, By.cssSelector("#multichoice-links > li:nth-child(6) > a"), "http://www.dstv.com/DStv-i/", "", "");
        //Email scam link
		ReusableFunc.SwitchWindows(driver, By.cssSelector("#multichoice-links > li:nth-child(7) > a"), "http://www.dstv.com/EmailScam/", "", "");
		//click DMMA link-broken link
		//ReusableFunc.SwitchWindows(driver, By.cssSelector("#multichoice-links > li:nth-child(7) > a"), "http://www.dstv.com/EmailScam/", "", "");
        //click Careers link-different url to dstv website link
		//ReusableFunc.SwitchWindows(driver, By.cssSelector("#multichoice-links > li:nth-child(9) > a"), "http://www.dstv.com/EmailScam/", "", "");

		System.out.println("footer links test complete");

		
		
	}
	

	
} 
