package com.dstvo.automation.dstvNowWeb.Staging;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class ConnectTest extends TestBase {

	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}

	@Test
	public void LoginEmail() throws Exception {
		ReusableFunc.ClickDropdownElement(driver, By.cssSelector("#connect > div > div:nth-child(1) > span"), By.xpath("//*[@id='connect']/div/div[2]/div/div[1]/a/div/span"));
	    ConnectControl.connectLiteLoginUsernameEmail(driver, "DstvNowPRMPVRLinked@mailinator.com", "Password123", "DstvNowPRMPVRLinked");
	    //logout
	    ReusableFunc.ClickDropdownElement(driver, By.xpath("//*[@id='connect']/div/div[1]/span[2]"), By.xpath("//*[@id='connect']/div/div[2]/div/div[7]/a"));
	    Thread.sleep(3000);
	    Assert.assertFalse(Utilities.isTextOnPage(driver, "DstvNowPRMPVRLinked"), "user was not logged out successfully");  
	}
	
	
	public void LoginWithYahoo() throws Exception {
		ReusableFunc.ClickDropdownElement(driver,  By.cssSelector("#connect > div > div:nth-child(1) > span"), By.xpath("//*[@id='connect']/div/div[2]/div/div[2]/div/div/img[1]"));
		ConnectControl.connectLiteLoginYahoo(driver, "testddmqa@yahoo.com", "qa123456");
		//ReusableFunc.VerifyPageTitle(driver, "Yahoo - review and continue");
		Thread.sleep(2500);
		//ReusableFunc.ClickByXpath(driver, ".//*[@id='mbrMbOAuth']/button", "Agree");
		ReusableFunc.VerifyTextOnPage(driver, "testddmqa");
			
	}
	
	
	public void LoginWithGMail() throws Exception {
		ReusableFunc.ClickDropdownElement(driver,  By.cssSelector("#connect > div > div:nth-child(1) > span"), By.xpath("//*[@id='connect']/div/div[2]/div/div[2]/div/div/img[2]"));
		ConnectControl.connectLiteLoginGoogle(driver, "belzng@gmail.com", "on1234567", "belzng@gmail.com");
					
	}
	
	
	
	
	
	

}
