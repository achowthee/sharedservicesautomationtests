package com.dstvo.automation.dstvNowWeb.Staging;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class connectUserBillboardsScenarios extends TestBase {

	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}

	@Test
	public void LoggedInLinkedNotPremium() throws Exception {
		//not logged in
		System.out.println(driver.findElementByCssSelector("#site-wrap > section.billboard-content > div > div > div > div > div").getText().endsWith("Register"));
		//login
		ReusableFunc.ClickDropdownElement(driver, By.cssSelector("#connect > div > div:nth-child(1) > span"), By.xpath("//*[@id='connect']/div/div[2]/div/div[1]/a/div/span"));
	    ConnectControl.connectLiteLoginUsernameEmail(driver, "DstvNowCompact@mailinator.com", "Password123", "DstvNowCompact");
	    //upgrage to premium
	    ReusableFunc.verifyElementIsDisplayed(driver, By.linkText("Upgrade to DStv Premium"), "Upgrade to Premium");
	    //logout
	    ReusableFunc.ClickDropdownElement(driver, By.xpath("//*[@id='connect']/div/div[1]/span[2]"), By.xpath("//*[@id='connect']/div/div[2]/div/div[7]/a"));
	    Thread.sleep(3000);
	    Assert.assertFalse(Utilities.isTextOnPage(driver, "DstvNowCompact"), "user was not logged out successfully");  
	}
	
    @Test
   //broken page after login
	public void LoggedInNotLinkedPremium() throws Exception {
		//not logged in
		System.out.println(driver.findElementByCssSelector("#site-wrap > section.billboard-content > div > div > div > div > div").getText().endsWith("Register"));
		//login
		ReusableFunc.ClickByCss(driver, "#connect > div > div:nth-child(1) > a");
	    ConnectControl.connectLiteLoginUsernameEmail(driver, "DstvNowPRMNotLinked@Mailinator.com", "Password123", "DstvNowPRMNotLinked");
	   //broken page after login
	    //logout
	    ReusableFunc.ClickDropdownElement(driver, By.xpath("//*[@id='connect']/div/div[1]/span[2]"), By.xpath("//*[@id='connect']/div/div[2]/div/div[7]/a"));
	    Thread.sleep(3000);
	    Assert.assertFalse(Utilities.isTextOnPage(driver, "DstvNowCompact"), "user was not logged out successfully");  
	}
	
	@Test
	public void LoggedInNotLinkedNOTPremium() throws Exception {
		//not logged in
		System.out.println(driver.findElementByCssSelector("#site-wrap > section.billboard-content > div > div > div > div > div").getText().endsWith("Register"));
		//login
		ReusableFunc.ClickDropdownElement(driver, By.cssSelector("#connect > div > div:nth-child(1) > span"), By.xpath("//*[@id='connect']/div/div[2]/div/div[1]/a/div/span"));
	    ConnectControl.connectLiteLoginUsernameEmail(driver, "DstvNowCompactNotLinked@mailinator.com", "Password123", "DstvNowCompactNotLinked");
	    //upgrage to premium
	    ReusableFunc.verifyElementIsDisplayed(driver, By.linkText("Link DStv Account"), "Link DStv Account");
	   
	    //logout
	    ReusableFunc.ClickDropdownElement(driver, By.xpath("//*[@id='connect']/div/div[1]/span[2]"), By.xpath("//*[@id='connect']/div/div[2]/div/div[7]/a"));
	    Thread.sleep(3000);
	    Assert.assertFalse(Utilities.isTextOnPage(driver, "DstvNowCompact"), "user was not logged out successfully");  
	}
	
	@Test
	public void LoggedInLinkePremiumPVR() throws Exception {
		//not logged in
		System.out.println(driver.findElementByCssSelector("#site-wrap > section.billboard-content > div > div > div > div > div").getText().endsWith("Register"));
		//login
		ReusableFunc.ClickDropdownElement(driver, By.cssSelector("#connect > div > div:nth-child(1) > span"), By.xpath("//*[@id='connect']/div/div[2]/div/div[1]/a/div/span"));
	    ConnectControl.connectLiteLoginUsernameEmail(driver, "DstvNowPRMPVRLinked@mailinator.com", "Password123", "DstvNowPRMPVRLinked");
		//verify watch now link displayed
	    System.out.println(driver.findElementByCssSelector("#site-wrap > section.billboard-content > div > div > div.swiper-slide.billboard-related-slide.swiper-slide-visible.swiper-slide-active.slide-loaded > div:nth-child(1) > div > div.clear.action-button-wrap > a").getText().endsWith("Watch Now"));
	    //logout
	    ReusableFunc.ClickDropdownElement(driver, By.xpath("//*[@id='connect']/div/div[1]/span[2]"), By.xpath("//*[@id='connect']/div/div[2]/div/div[7]/a"));
	    Thread.sleep(3000);
	    Assert.assertFalse(Utilities.isTextOnPage(driver, "DstvNowCompact"), "user was not logged out successfully");  
	}
	
	

}
