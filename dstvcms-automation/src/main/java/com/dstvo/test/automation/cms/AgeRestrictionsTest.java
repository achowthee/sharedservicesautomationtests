package com.dstvo.test.automation.cms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class AgeRestrictionsTest extends TestBase{
		
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}
	@Test(priority = -6)
	public void ageRestrictionHomePage() throws Exception{
		System.out.println("Starting...Age Restrcition Test");
		ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
		Thread.sleep(2000);
		//Click Global settings first, then Age Restrictions
		navigateToAgeRestrictionsPage();
		Thread.sleep(2000);
	}
	
	@Test(priority = -5, dependsOnMethods = { "ageRestrictionHomePage" })
	public void addAgeRestrictionBlankEntry() throws Exception{
		/** Create an entry leaving the field blank */
//		System.out.println("Start creating an entry leaving the field blank");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div/div[3]/button", "save");
		Thread.sleep(1000);
		ReusableFunc.verifyAttributeInWebElement(driver, By.xpath("//*[@id='form']/div/div[1]/div"), "class","row-fluid control-group error");
//		System.out.println("END creating an entry leaving the field blank");
	  }
	
	@Test(priority = -4, dependsOnMethods = { "ageRestrictionHomePage" })
	public void addAgeRestriction() throws Exception{
		  //1 character
		  RandomString rs1 = new RandomString(1);
	 	  String name="00AgeRes"+rs1.nextString();
	 	  
	 	 RandomString rs = new RandomString(1);
		  String name1="00AgeAut"+rs.nextString();
	 	  
		  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("ageRestrictedToAge")),name);
		  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("dvbPGRatingCode")),name1);
		  //ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div/div[3]/button", "Clicking on save");
		  save(name);
		  Thread.sleep(2000);
		  //System.out.println("Age restrcition test Complete");
		  
	  }
	   
  @Test(priority = -4, dependsOnMethods = { "addAgeRestriction" })
  	public void editAgeRestriction() throws Exception{
	  navigateBackToAgeRestrictionsPage();
	  ReusableFunc.ClickByLinkText(driver, "Age Restrictions");
	  Thread.sleep(2000); //Wait for the page to reload
	  ReusableFunc.ClickByCss(driver, ".icon-pencil.bigger-130"); 
	  Thread.sleep(1000);
	  //driver.findElementByXPath(".//*[@id='ageRestrictedToAge']").clear();
	  String name = "00 Automation Edit";
	  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("ageRestrictedToAge")),name);
	  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("dvbPGRatingCode")),"Automation Edit DVB PG");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div/div[3]/button", "button");
	  Thread.sleep(2000);
	  save(name);
  }
  
  @Test(priority = -3, dependsOnMethods={"editAgeRestriction"})
  	public void deleteAgeRestriction() throws Exception{
	  navigateBackToAgeRestrictionsPage();
	  ReusableFunc.ClickByCss(driver, ".icon-trash.bigger-130");
	  Thread.sleep(1000);
	  ReusableFunc.VerifyTextOnPage(driver, "recovered");
	  Thread.sleep(1000);
	  ReusableFunc.ClickByCss(driver, ".icon-remove.bigger-130"); //this will cancel the delete
	  Thread.sleep(1000);
	  ReusableFunc.ClickByCss(driver, ".icon-trash.bigger-130"); //Click on delete again
	  Thread.sleep(1000);
	  ReusableFunc.ClickByCss(driver, ".btn.btn-minier.btn-danger.ajax-submit"); //Confirm deletion
	  Thread.sleep(1000);
	  System.out.println(".......Age Restrcition Test Completed");
  }
  
	private void navigateToAgeRestrictionsPage() throws Exception {
		  ReusableFunc.ClickByLinkText(driver, "Global Settings");
		  Thread.sleep(2000);
		  ReusableFunc.ClickByLinkText(driver, "Age Restrictions");	
		  Thread.sleep(2000);
	}
	
	private void navigateBackToAgeRestrictionsPage() throws Exception {
		ReusableFunc.ClickByLinkText(driver, "Global Settings");
		navigateToAgeRestrictionsPage();
	}
	
	private void save(String agerestrictionName) throws Exception {
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div/div[3]/button", "Clicking on save");
		Thread.sleep(2000);
		ConfirmSave(agerestrictionName);	
	}
	
	private void ConfirmSave(String agerestrictionName) throws Exception {
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, agerestrictionName);
		
	}
   
}
