package com.dstvo.test.automation.cms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class ArticlesTest extends TestBase{
		
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}
	
	
  
  @Test
  public void CreateNewArtcile() throws Exception {
	  ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
	  System.out.println("Starting Articles test");
	  Thread.sleep(2000);
	  //Click Content first, then Billboard views
	  ReusableFunc.ClickByLinkText(driver, "Content" );
	  ReusableFunc.ClickByLinkText(driver, "Articles");
	  
	  //Click on top Add Article and cancel
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div[1]/div/div[1]/a", "Top Add artcile");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/a", "Click on bottom cancel");
	  
	  //Now capturing an Article using the bottom Add article
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div[2]/a", "Bottom Add article");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[1]/button", "Top save");
	  
	  Utilities.fluentWait(By.xpath(".//*[@id='form']/div[1]/div[1]/div[1]/strong[1]"), driver);
	  ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='form']/div[1]/div[1]/div[1]/strong[2]"), "7 errors");
	  
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("headline"), "Automation test Headline");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("subHeadline"), "Automation test Description");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("shortBlurb"), "Automation test Short blurb");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("longBlurb"), "Automation test long blurb");
	  //ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath("html/body/p"), "Automation test body. just making it slightly longer");
	  
	  WebElement ckeditor_frame = driver.findElementByClassName("cke_wysiwyg_frame");
      this.driver.switchTo().frame(ckeditor_frame);
      WebElement editor_body = driver.findElementByTagName("body");
      editor_body.sendKeys("Selenium Inserted Body Text. CKEditor Body Captured. just adding a little more meat to the body");
      this.driver.switchTo().defaultContent();
      System.out.println("CKEditor Body Captured.");

	  System.out.println("Body");

	  //Search image 4x3 
	  System.out.println("4x3");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("article_images_article-4x3_link_image_asset_search_"), "Koala");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_images_article-4x3_link_image_asset_search']/div[2]/a", "search");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='image_asset_search_form']/tbody/tr/td[1]/a/i", "select A pic");
	  System.out.println("4x3");
	  
	  //Search for image (16x9)
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.id("article_images_article-16x9_link_image_asset_search_")),"automation test");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_images_article-16x9_link_image_asset_search']/div[2]/a", "search");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='image_asset_search_form']/tbody/tr[1]/td[1]/a/i", "select A pic");
	  Utilities.waitForElement(driver, By.id("auto_crop_save_article_16x9"), 20000);
	  Thread.sleep(3000);
	  ReusableFunc.verifyElementIsDisplayed(driver, By.id("auto_crop_save_article_16x9"), "Is the crop and save button displayed");
	  ReusableFunc.ClickByID(driver, "auto_crop_save_article_16x9");
	  Thread.sleep(28000);
	  
	  /*while (driver.findElementByXPath(".//*[@id='edit_image_modal_article_16x9']/div[1]/h3").isDisplayed() == true) {
		 Thread.sleep(1000); 
		 System.out.println("waiting for large image uplaod to complete");
	  }
	  System.out.println("Ok, Go!");*/
	  
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("commentDescriptionValue"), "Editors comment, not sure why its enabled during creation");
	  //ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[1]/div[9]/div/div/div[2]/div/div/div/input[2]", "Click on the tag field");
	  //ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='form']/div[1]/div[1]/div[9]/div/div/div[2]/div/div/div/input[2]"), "Auto tag");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("seoTitle"), "Automation article SEO Title");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("seoDescription"), "Automation article SEO Description");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='form']/div[1]/div[1]/div[10]/div/div/div[2]/div/div[3]/div"), "Automation article SEO Keywords");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagsog_description"), "Automation article SocialMedia Tags Desc");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagsog_url"), "Automation article SocialMedia URL");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagsog_image"), "Automation article SocialMedia Image");
	  
	  
	  ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='socialMediaTagsog_type_chosen']/a/div/b"), By.xpath(".//*[@id='socialMediaTagsog_type_chosen']/div/ul/li[2]"));
	  ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='socialMediaTagsog_type_chosen']/a/div/b"), By.xpath(".//*[@id='socialMediaTagsog_type_chosen']/div/ul/li[3]"));
	  
	  ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='socialMediaTagstwitter_card_chosen']/a/span"), By.xpath(".//*[@id='socialMediaTagstwitter_card_chosen']/div/ul/li[2]"));
	  
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagstwitter_description"), "Automation article Twitter desc");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagstwitter_url"), "Automation article SocialMedia Twitter URL");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagstwitter_image"), "Automation article SocialMedia Twitter Image");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagstwitter_image_src"), "Automation article SocialMedia Twitter large");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagstwitter_site"), "Automation article SocialMedia twitter site");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagstwitter_creator"), "Automation article SocialMedia twitter creator");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagsfb_appid"), "Automation article SocialMedia Fb AppID");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='form']/div[1]/div[1]/div[11]/div/div/div[2]/div/div[15]/div"), "Automation article SocialMedia Admins");
	  
	  //Select a status
	  ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='status_chosen']/a/span"), By.xpath(".//*[@id='status_chosen']/div/ul/li[1]"));
	  Thread.sleep(5000);
	  
	  //Select a publisher
	  
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='s2id_publisher']/a", "Click on Organization dropdown");
	  Utilities.fluentWait(By.id("select2-drop"), driver);
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("select2-drop"), "Au");
	  Utilities.fluentWait(By.xpath(".//*[@id='select2-drop']/ul/li/div"), driver);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='select2-drop']/ul/li/div", "Selecting the automation organization");
	  
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='s2id_author']/a", "Click on Person dropdown");
	  Utilities.fluentWait(By.id("s2id_author"), driver);
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("s2id_author"), "Au");
	  Utilities.fluentWait(By.xpath(".//*[@id='select2-drop']/ul/li[1]/div"), driver);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='select2-drop']/ul/li[1]/div", "Selecting the automation person");
	  
	  /*ReusableFunc.ClickDropdownElement(driver, By.id("publishDate"), by)
	  ReusableFunc.ClickByID(driver, "publishDate");
	  ReusableFunc.ClickByID(driver, "html/body/div[5]/div[1]/table/thead/tr[1]/th[3]");
	  ReusableFunc.ClickByXpath(driver, "html/body/div[5]/div[1]/table/tbody/tr[1]/td[2]", "Selecting a date");*/
	  
	  //Publish on Select and deselect
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[2]/div[7]/div[1]/label", "selecting the 1st Publish on");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[2]/div[7]/div[2]/label", "selecting the 2nd Publish on");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[2]/div[7]/div[3]/label", "selecting the 3rd Publish on");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[2]/div[7]/div[4]/label", "selecting the 4th Publish on");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[2]/div[7]/div[3]/label", "unselecting the 3rd Publish on");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[2]/div[7]/div[4]/label", "unselecting the 4th Publish on");
	  
	  //Selecting the AutoLang
	  ReusableFunc.ClickByID(driver, "language_chosen");
	  Utilities.fluentWait(By.xpath(".//*[@id='language_chosen']/div/div/input"), driver);
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='language_chosen']/div/div/input"), "aut");
	  Utilities.fluentWait(By.xpath(".//*[@id='language_chosen']/div/ul/li[1]"), driver);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='language_chosen']/div/ul/li[1]", "Selecting AutoLang");
	  
	  //Enable/Disable comments
	  //ReusableFunc.ClickByXpath(driver, ".//*[@id='enableComments1']", "enable comments");
	  //ReusableFunc.ClickByXpath(driver, ".//*[@id='enableComments1']", "disable comments");
	  
	  ReusableFunc.ClickByID(driver, "product_select_chosen");
	  Utilities.fluentWait(By.xpath(".//*[@id='product_select_chosen']/ul/li/input"), driver);
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='product_select_chosen']/ul/li/input"), "QA");
	  Thread.sleep(2000);
	  ReusableFunc.ClickDropdownElement(driver, By.id("product_select_chosen"), By.xpath(".//*[@id='product_select_chosen']/div/ul/li"));
	  
	  //Select a category
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='category_select']/div[1]/a", "Select Category");
	  Thread.sleep(2000);
	  //Select 1st Category
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_category_category_014f9f4b-e46d-4f16-b912-a4a9c356fa84']/label[1]/label", "Enable");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_category_category_014f9f4b-e46d-4f16-b912-a4a9c356fa84']/label[1]/label", "Disable");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_category_category_014f9f4b-e46d-4f16-b912-a4a9c356fa84']/label[1]/label", "Enable");
	  //Select another Category and choose another sub category
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_category_product_4bb4f967-0942-4310-94d3-97dba5944b72']/div/ul/li[2]/a", "Select Mobile cateogry");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_category_category_127a6217-6d43-45fd-aabd-c3c0b211b8fb']/label[2]/label", "Select Entertainment");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_category']/div[3]/button", "Close");
	  
	  //Link countries
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='country_select']/div[1]/a", "Click Link country");
	  Thread.sleep(1000);
	  //Select 3 countries in the 1st region
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country_regions_product_4bb4f967-0942-4310-94d3-97dba5944b72']/div[1]/div[1]/a", "Select Oceania region");
	  Thread.sleep(1000);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country_global_check_group_536fb50c-ae6d-4353-82ee-6dc4a2474107']/label[1]/label", "1st country");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country_global_check_group_536fb50c-ae6d-4353-82ee-6dc4a2474107']/label[2]/label", "4th country");
	  //Select another country from the a different region
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country_regions_product_4bb4f967-0942-4310-94d3-97dba5944b72']/div[4]/div[1]/a", "Click Southern African");
	  Thread.sleep(1000);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country_global_check_group_93b310a2-4600-41b6-b1c0-bc920660f103']/label[2]/label", "2nd country");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country_global_check_group_93b310a2-4600-41b6-b1c0-bc920660f103']/label[3]/label", "3rd country");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country']/div[1]/button", "Click X");
	  
  }
  
  /*@Test
  public void EditArtcile() throws Exception {
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='articles-list_filter']/label/input"), "Automation");
  }*/
  
}
  
	  