package com.dstvo.test.automation.cms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class BillboardTest extends TestBase{
		
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}
	
	
  
  @Test
  public void CreateBillboardInBillboardViewNoTitle() throws Exception{
	  ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
	  Thread.sleep(2000);
	  System.out.println("Starting billboards test");
	  //Click Content first, then Billboard views
	  ReusableFunc.ClickByLinkText(driver, "Content" );
	  ReusableFunc.ClickByLinkText(driver, "Billboard Views");
	  ReusableFunc.ClickByLinkText(driver, "QA Automation");
	  ReusableFunc.ClickByLinkText(driver, "Create new Billboard");
	  Utilities.fluentWait(By.id("description"), driver);
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("description"), "Enter a description here");

	  //Search for image (20x9)
	  ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.id("billboard_images_billboard-20x9_image_asset_search_")),"log");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='billboard_images_billboard-20x9_image_asset_search']/div[2]/a", "search");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='image_asset_search_form']/tbody/tr/td[1]/a/i", "select A pic");
	  Thread.sleep(5000);
	  
	  //Search image 19x9 
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("billboard_images_billboard-16x9_image_asset_search_"), "Koala");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='billboard_images_billboard-16x9_image_asset_search']/div[2]/a", "search");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='image_asset_search_form']/tbody/tr/td[1]/a/i", "select A pic");
	  Thread.sleep(5000);
	  
	  //Search image 25x9 
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("billboard_images_billboard-25x9_image_asset_search_"), "Lambo");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='billboard_images_billboard-25x9_image_asset_search']/div[2]/a", "search");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='image_asset_search_form']/tbody/tr/td[1]/a/i", "select A pic");
	  Thread.sleep(5000);
	  
	//Search image 1x1 
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("billboard_images_billboard-1x1_image_asset_search_"), "penguin");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='billboard_images_billboard-1x1_image_asset_search']/div[2]/a", "search");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='image_asset_search_form']/tbody/tr/td[1]/a/i", "select A pic");
	  Thread.sleep(5000);
	  
	  //Related Items linking
	  //Typing a URL
	  ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='billboard_image_relateditem-body']/div[2]/div/a"), "check the related items button is there");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("url-link-textarea"), "Image URL");
	  ReusableFunc.verifyElementIsDisplayed(driver, By.id("url-link-clear-button"), "Verify related items button is gone and clear is displayed");
	  ReusableFunc.ClickByID(driver, "url-link-clear-button");
	  ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='billboard_image_relateditem-body']/div[2]/div/a"), "check the related items button is there");
	  
	  //Adding a related item
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='billboard_image_relateditem-body']/div[2]/div/a", "Click on Related Items");
	  Thread.sleep(2000);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='myTab3']/li[2]/a", "Select Program");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='myTab3']/li[1]/a", "Select Video Meta");
	  
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='s2id_billboard_image_relateditem-product-filter']/a/span[1]", "Select product dropdown");
	  Utilities.fluentWait(By.xpath(".//*[@id='select2-drop']/div/input"), driver);
	  ReusableFunc.clearTextBox(driver, By.xpath(".//*[@id='select2-drop']/div/input"));
	  Thread.sleep(10000);
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='select2-drop']/div/input"), "DSTV Now");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='select2-drop']/ul/li/div", "Selecting DSTV now");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='billboard_image_related-items-search-results_filter']/label/input"), "Stand Up Guys");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='billboard_image_related-items-search-results']/tbody/tr/td[5]/input", "Selecting Stand up guy");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='-billboard_image_link-relateditem']/div[3]/button[2]", "Click save");
	  
	  
	  //Select a billboard type
	  ReusableFunc.CheckCheckBox(driver, driver.findElementById("billboardType2"));
	  ReusableFunc.CheckCheckBox(driver, driver.findElementById("billboardType1"));
	  
	  //Call to Action
	  ReusableFunc.ClickByID(driver, "addCallToActionButtonId");
	  Thread.sleep(2000);
	  //Adding a call to action
	  ReusableFunc.CheckCheckBox(driver, driver.findElementById("displayCallToActions0.callToActionType1"));
	  ReusableFunc.CheckCheckBox(driver, driver.findElementById("displayCallToActions0.callToActionType2"));
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("callToActionTextId_0"), "C2AText");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("callToActionUrlId_0"), "C2AButton");
	  
	  ReusableFunc.ClickByID(driver, "addCallToActionButtonId");
	  Thread.sleep(1000);
	  
	//Adding a 2nd call to action
	  ReusableFunc.CheckCheckBox(driver, driver.findElementById("displayCallToActions0.callToActionType1"));
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("callToActionTextId_1"), "C2AText2");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("callToActionUrlId_1"), "C2AButton2");
	  
	  //Tags
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='billboard-form-body']/div[1]/div[7]/div/div/div[2]/div/div/div/input[2]"), "Tags");
	  
	  //Set a slug
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("slug"), "Slug Auto test");
	  
	  //Setting the status
	  ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='status_chosen']/a/span"), By.xpath(".//*[@id='status_chosen']/div/ul/li[3]"));
	  
	  //Setting the Branding
	  ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='brandingRef_chosen']/a/span"), By.xpath(".//*[@id='brandingRef_chosen']/div/ul/li[5]"));
	  
	  //Setting the publish date
	  ReusableFunc.ClickByID(driver, "publishDate");
	  ReusableFunc.ClickByXpath(driver, "html/body/div[5]/div[1]/table/thead/tr[1]/th[3]", "Change the month");
	  ReusableFunc.ClickByXpath(driver, "html/body/div[5]/div[1]/table/thead/tr[1]/th[1]", "Change the month");
	  ReusableFunc.ClickByXpath(driver, "html/body/div[5]/div[1]/table/tbody/tr[4]/td[3]", "Select a publish date");
	  
	  Thread.sleep(2000);
	  //Setting the publish date
	  ReusableFunc.ClickByID(driver, "expiryDate");
	  ReusableFunc.ClickByXpath(driver, "html/body/div[6]/div[1]/table/tbody/tr[3]/td[5]", "Select an expiry date");
	  
	  //Select platforms to publish on
	  ReusableFunc.CheckCheckBox(driver, driver.findElementById("publishOnList2"));
	  
	  //Select a category
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='category_select']/div[1]/a", "Select Category");
	  Thread.sleep(1000);
	  //Select 1st Category
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_category_category_014f9f4b-e46d-4f16-b912-a4a9c356fa84']/label[1]/label", "Enable");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_category_category_014f9f4b-e46d-4f16-b912-a4a9c356fa84']/label[1]/label", "Disable");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_category_category_014f9f4b-e46d-4f16-b912-a4a9c356fa84']/label[1]/label", "Enable");
	  //Select another Category and choose another sub category
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_category_product_4bb4f967-0942-4310-94d3-97dba5944b72']/div/ul/li[2]/a", "Select Mobile cateogry");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_category_category_127a6217-6d43-45fd-aabd-c3c0b211b8fb']/label[2]/label", "Select Entertainment");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_category']/div[3]/button", "Close");
	  
	  //Link countries
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='country_select']/div[1]/a", "Click Link country");
	  Thread.sleep(1000);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country']/div[2]/div/ul/li[2]/a", "Select country");
	  //Select 3 countries in the 1st region
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country_regions_product_4bb4f967-0942-4310-94d3-97dba5944b72']/div[1]/div[1]/a", "Select Oceania region");
	  Thread.sleep(1000);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country_global_check_group_536fb50c-ae6d-4353-82ee-6dc4a2474107']/label[1]/label", "1st country");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country_global_check_group_536fb50c-ae6d-4353-82ee-6dc4a2474107']/label[2]/label", "4th country");
	  //Select another country from the a different region
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country_regions_product_4bb4f967-0942-4310-94d3-97dba5944b72']/div[4]/div[1]/a", "Click Southern African");
	  Thread.sleep(1000);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country_global_check_group_93b310a2-4600-41b6-b1c0-bc920660f103']/label[2]/label", "2nd country");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country_global_check_group_93b310a2-4600-41b6-b1c0-bc920660f103']/label[3]/label", "3rd country");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country']/div[1]/button", "Click X");
	  
	  //Save and exit
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[3]/div/div/button", "Click on Save");
	  Thread.sleep(3000);
  }
  
  //Remove the compulsory slug and attempt to save. Error should be thrown | Save/Cancel/Clone avaibale at the bottom of the page
  @Test(dependsOnMethods = { "CreateBillboardInBillboardViewNoTitle" })
  public void EditBillboardInBillboardViewNoTitle() throws Exception{
	  //Click on the pencil to edit the billboard
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='billboardsInCollectionListGridId']/ul/li[4]/a/i", "Click on the pencil image");
	 
	  //Clear the text in slug and click save
	  Thread.sleep(1000);
	  ReusableFunc.clearTextBox(driver, By.id("slug"));
	  Thread.sleep(2000);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[1]/button", "Click top save");
	  
	  //Verify correct error message is displayed
	  Thread.sleep(20000);
	  ReusableFunc.verifyTextInWebElement(driver, By.id("slug.errors"), "may not be empty");
	  
	  //Change text in the slug texttbox, add a title and click on the bottom save
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("slug"), "QASlugTest");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("title"), "QA Automation Test");
	  
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[3]/div/div/button", "Click on the bottom Save");
  }



@Test (dependsOnMethods = { "DeleteBillboardInBillboardViewNoTitle" })
  public void CreateBillboardInBillboardListingNoTitle() throws Exception{
	  Thread.sleep(2000);
	  //Click Content first, then Billboard views
	  ReusableFunc.ClickByLinkText(driver, "Billboards");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div[1]/a", "Click + Add a billboard");
	  
	  //Enter a title
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("title"), "QA Automation Test from Billboard listing");
	  //Enter a description
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("description"), "Add a description to the billboard");

	    
	  //Search for image (20x9)
	  ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.id("billboard_images_billboard-20x9_image_asset_search_")),"log");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='billboard_images_billboard-20x9_image_asset_search']/div[2]/a", "search");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='image_asset_search_form']/tbody/tr/td[1]/a/i", "select A pic");
	  Thread.sleep(5000);
	  
	  //Search image 19x9 
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("billboard_images_billboard-16x9_image_asset_search_"), "Koala");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='billboard_images_billboard-16x9_image_asset_search']/div[2]/a", "search");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='image_asset_search_form']/tbody/tr/td[1]/a/i", "select A pic");
	  Thread.sleep(5000);
	  
	  //Search image 25x9 
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("billboard_images_billboard-25x9_image_asset_search_"), "Lambo");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='billboard_images_billboard-25x9_image_asset_search']/div[2]/a", "search");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='image_asset_search_form']/tbody/tr/td[1]/a/i", "select A pic");
	  Thread.sleep(5000);
	  
	  //Search image 1x1 
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("billboard_images_billboard-1x1_image_asset_search_"), "penguin");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='billboard_images_billboard-1x1_image_asset_search']/div[2]/a", "search");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='image_asset_search_form']/tbody/tr/td[1]/a/i", "select A pic");
	  Thread.sleep(5000);
	  
	  ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='billboard_image_relateditem-body']/div[2]/div/a"), "check the related items button is there");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("url-link-textarea"), "Image URL");
	  ReusableFunc.verifyElementIsDisplayed(driver, By.id("url-link-clear-button"), "Verify related items button is gone and clear is displayed");
	  ReusableFunc.ClickByID(driver, "url-link-clear-button");
	  ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='billboard_image_relateditem-body']/div[2]/div/a"), "check the related items button is there");
	  
	  //Adding a related item
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='billboard_image_relateditem-body']/div[2]/div/a", "Click on Related Items");
	  Thread.sleep(2000);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='myTab3']/li[2]/a", "Select Program");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='myTab3']/li[1]/a", "Select Video Meta");
	  
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='s2id_billboard_image_relateditem-product-filter']/a/span[1]", "Select product dropdown");
	  Utilities.fluentWait(By.xpath(".//*[@id='select2-drop']/div/input"), driver);
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='select2-drop']/div/input"), "DSTV Now");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='select2-drop']/ul/li/div", "Selecting DSTV now");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='billboard_image_related-items-search-results_filter']/label/input"), "Stand Up Guys");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='billboard_image_related-items-search-results']/tbody/tr/td[5]/input", "Selecting Stand up guy");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='-billboard_image_link-relateditem']/div[3]/button[2]", "Click save");
	  
	  //Select a billboard type
	  ReusableFunc.CheckCheckBox(driver, driver.findElementById("billboardType2"));
	  ReusableFunc.CheckCheckBox(driver, driver.findElementById("billboardType1"));
	  
	  //Adding 2 Call to Action
	  ReusableFunc.ClickByID(driver, "addCallToActionButtonId");
	  Thread.sleep(1000);
	  //Adding a call to action button
	  ReusableFunc.CheckCheckBox(driver, driver.findElementById("displayCallToActions0.callToActionType1"));
	  ReusableFunc.CheckCheckBox(driver, driver.findElementById("displayCallToActions0.callToActionType2"));
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("callToActionTextId_0"), "C2AText");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("callToActionUrlId_0"), "C2AButton");
	  
	  ReusableFunc.ClickByID(driver, "addCallToActionButtonId");
	  Thread.sleep(1000);
	  //Adding a 2nd call to action link
	  ReusableFunc.CheckCheckBox(driver, driver.findElementById("displayCallToActions0.callToActionType1"));
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("callToActionTextId_1"), "C2AText2");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("callToActionUrlId_1"), "C2AButton2");
	  
	  //Tags
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='billboard-form-body']/div[1]/div[7]/div/div/div[2]/div/div/div/input[2]"), "Tags");
	  
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("slug"), "Slug Auto Test");
	  
	  //Setting the status
	  ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='status_chosen']/a/span"), By.xpath(".//*[@id='status_chosen']/div/ul/li[3]"));
	  
	  //Setting the Branding
	  ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='brandingRef_chosen']/a/span"), By.xpath(".//*[@id='brandingRef_chosen']/div/ul/li[5]"));
	  
	  //Setting the publish date
	  ReusableFunc.ClickByID(driver, "publishDate");
	  
	  ReusableFunc.ClickByXpath(driver, "html/body/div[5]/div[1]/table/thead/tr[1]/th[3]", "Change the month");
	  ReusableFunc.ClickByXpath(driver, "html/body/div[5]/div[1]/table/thead/tr[1]/th[1]", "Change the month");
	  ReusableFunc.ClickByXpath(driver, "html/body/div[5]/div[1]/table/tbody/tr[4]/td[3]", "Select a publish date");
	  Thread.sleep(2000);
	  
	  //Setting the expiry date
	  ReusableFunc.ClickDropdownElement(driver, By.id("expiryDate"), By.xpath("html/body/div[6]/div[1]/table/tbody/tr[5]/td[3]"));
	  
	  //Select platforms to publish on
	  ReusableFunc.CheckCheckBox(driver, driver.findElementById("publishOnList2"));
	  
	  //Select a product
	  Utilities.fluentWait(By.xpath(".//*[@id='transientProducts_chosen']/ul/li/input"), driver);
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='transientProducts_chosen']/ul/li/input"), "qa automation");
	  Thread.sleep(2000);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='transientProducts_chosen']/div/ul/li", "Selecting QA automation");
	  //ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='transientProducts_chosen']/ul"), By.xpath(".//*[@id='transientProducts_chosen']/div/ul/li[6]"));
	  
	  //Select a category
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='category_select']/div[1]/a", "Select Category");
	  Thread.sleep(1000);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_category']/div[2]/div/ul/li[6]/a", "Select QA Automation");
	  Thread.sleep(1000);
	  //Select 1st Category
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_category_category_014f9f4b-e46d-4f16-b912-a4a9c356fa84']/label[1]/label", "Enable");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_category_category_014f9f4b-e46d-4f16-b912-a4a9c356fa84']/label[1]/label", "Disable");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_category_category_014f9f4b-e46d-4f16-b912-a4a9c356fa84']/label[2]/label", "Enable");
	  //Select another Category and choose another sub category
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_category_product_4bb4f967-0942-4310-94d3-97dba5944b72']/div/ul/li[3]/a", "Select Cruza cateogry");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_category_category_dae9bd5d-9b37-4165-9a2c-395601ea38c6']/label[1]/label", "Select Whats new");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_category']/div[3]/button", "Close");
	  
	  //Link countries
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='country_select']/div[1]/a", "Click Link country");
	  Thread.sleep(3000);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country']/div[2]/div/ul/li[7]/a", "Select country list");
	  //Select 3 countries in the 1st region
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country_regions_product_4bb4f967-0942-4310-94d3-97dba5944b72']/div[2]/div[1]/a", "Select Res of Africa region");
	  Thread.sleep(1000);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country_global_check_group_aa8febca-5fef-425e-828f-0efb3a274024']/label[2]/label", "2nd country");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country_global_check_group_aa8febca-5fef-425e-828f-0efb3a274024']/label[3]/label", "3rd country");
	  //Select another country from the a different region
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country_regions_product_4bb4f967-0942-4310-94d3-97dba5944b72']/div[3]/div[1]/a", "Click Southern African");
	  Thread.sleep(1000);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country_global_check_group_d30e3d37-3c3d-4aae-9830-da34cff32a36']/label[1]/label", "1st country");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country_global_check_group_d30e3d37-3c3d-4aae-9830-da34cff32a36']/label[2]/label", "2nd country");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='article_country']/div[1]/button", "Click X");
	  
	  //Save and exit
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "Click on Save");
	  Thread.sleep(5000);
  }



//Remove the compulsory slug and attempt to save. Error should be thrown
  @Test(dependsOnMethods = { "CreateBillboardInBillboardListingNoTitle", "CreateBillboardInBillboardViewNoTitle", "EditBillboardInBillboardViewNoTitle" })
  public void EditBillboardInBillboardListingNoTitle() throws Exception{
	  //Click on the pencil to edit the billboard
	  ReusableFunc.ClickByLinkText(driver, "Billboards");
	  Thread.sleep(5000);
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='billboardListTableId_filter']/label/input"), "QA Automation Test from Billboard listing");
	  
	  //Clear the text in slug and click save
	  Thread.sleep(5000);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='billboardListTableId']/tbody/tr/td[9]/a[1]/i", "Clicking edit on the billboabrd");
	  Thread.sleep(20000);
	  ReusableFunc.clearTextBox(driver, By.id("slug"));
	  ReusableFunc.clearTextBox(driver, By.id("title"));
	  Thread.sleep(2000);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[1]/button", "Click top save");
	  Thread.sleep(20000);
	  
	  //Verify correct error message is displayed
	  //Thread.sleep(20000);
	  ReusableFunc.verifyTextInWebElement(driver, By.id("slug.errors"), "may not be empty");
	  //Change text in the slug textbox and save
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("slug"), "SlugQAAutomation1");
	  
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "Click on the bottom Save");
  }



  	  //Delete a billboard from the billboard listing page
      //This will delete both the billboards created in the listing page and in the billboard view
      //Deleting the 1st billboard
  	  @Test(dependsOnMethods = { "CreateBillboardInBillboardListingNoTitle", "EditBillboardInBillboardListingNoTitle" })
  	  public void DeleteBillboardInBillboardListingNoTitle() throws Exception{
  		  //Filter the billboards so the specific one you want to delete is the only 1 displayed
  		  Thread.sleep(4000);
  		  ReusableFunc.EnterTextInTextBox(driver,driver.findElementByXPath(".//*[@id='billboardListTableId_filter']/label/input"), "SlugQAAutomation1");
  		  Thread.sleep(5000);
  		  ReusableFunc.ClickByCss(driver, ".btn.btn-mini.btn-danger.ajax-nav");
  		  Thread.sleep(2000);
  		  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/a", "Click on cancel");
  		  Thread.sleep(2000);
  		  ReusableFunc.EnterTextInTextBox(driver,driver.findElementByXPath(".//*[@id='billboardListTableId_filter']/label/input"), "SlugQAAutomation1");
  		  Thread.sleep(5000);
  		  ReusableFunc.ClickByCss(driver, ".btn.btn-mini.btn-danger.ajax-nav");
  		  Thread.sleep(2000);
  		  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "click on remove");
  		  
  		  //Deleting the billboard created in the billboard view
  		  Thread.sleep(3000);
  		  ReusableFunc.EnterTextInTextBox(driver,driver.findElementByXPath(".//*[@id='billboardListTableId_filter']/label/input"), "QAslugtest");
  		  Thread.sleep(5000);
  		  ReusableFunc.ClickByCss(driver, ".btn.btn-mini.btn-danger.ajax-nav");
  		  Thread.sleep(2000);
  		  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "click on remove");
  		System.out.println("billboards test complete");
  	  }


  	  //Remove a billboard from a billboard view
  	  @Test(dependsOnMethods = { "CreateBillboardInBillboardViewNoTitle", "EditBillboardInBillboardViewNoTitle" })
  	  public void DeleteBillboardInBillboardViewNoTitle() throws Exception{
  		  //Test to remove a billboard from a billboard view
  		  Thread.sleep(5000);
  		  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("filer-bb-name"), "Automation");
  		  ReusableFunc.ClickByXpath(driver, ".//*[@id='billboardFilterFormId']/div/div[2]/div[3]/button", "Click on filter billboards");
  		  Thread.sleep(3000);
  		  ReusableFunc.ClickByXpath(driver, ".//*[@id='billboards0.selected1']", "Selecting th check box on the 1nd billboard in a collection");
  		  ReusableFunc.ClickByXpath(driver, ".//*[@id='billboard_list_grid_removeButton']", "clicking remove");
  		  Thread.sleep(2000);
  		  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/a", "clicking cancel on the confirm removal");
  		  Thread.sleep(3000);	
  	  	  ReusableFunc.ClickByXpath(driver, ".//*[@id='billboardFilterFormId']/div/div[2]/div[3]/button", "Click on filter billboards");
  	  	  Thread.sleep(2000);
  	  	  ReusableFunc.ClickByXpath(driver, ".//*[@id='billboards0.selected1']", "Selecting th check box on the 1nd billboard in a collection");
  	  	  
  	  	  ReusableFunc.ClickByXpath(driver, ".//*[@id='billboard_list_grid_removeButton']", "clicking remove");
  	  	  Thread.sleep(2000);
  	  	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "click on remove on the convfirm removal screen");
  	  }
  
}