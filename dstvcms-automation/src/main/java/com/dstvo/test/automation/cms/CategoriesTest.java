package com.dstvo.test.automation.cms;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class CategoriesTest extends TestBase {

	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}

	@Test(priority = -5)
	public void categoryProductsListing() throws Exception{
		ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
		Thread.sleep(2000);
		navigateToCategoriesPage();
		Thread.sleep(1000);
		ReusableFunc.VerifyTextOnPage(driver, "Test Cat");
	}
	
	@Test(priority = -4, dependsOnMethods = { "categoryProductsListing" })
	public void addCategory() throws Exception{
		Thread.sleep(2000);
		System.out.println("Starting Category test");
		ReusableFunc.ClickByLinkText(driver, "Test Cat");
		
		//back to Product selection is working
		Thread.sleep(2000);
		Utilities.fluentWait(By.xpath(".//*[@id='page_content']/div[2]/div/div/div/div[3]/div/a"), driver);
		ReusableFunc.verifyContainsPatialText(driver, By.xpath(".//*[@id='page_content']/div[1]/h1"), "Test Cat");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div/div/div[3]/div/a", "Back to Product selection");
		System.out.println("continue Category test");
		ReusableFunc.ClickByLinkText(driver, "Test Cat");
		
		//Add a category
		ReusableFunc.ClickByLinkText(driver, "Add");
		Thread.sleep(2000);
		
		//Save empty category listing
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/button", "Click on the save button");
		Utilities.fluentWait(By.id("name.errors"), driver);
		
		//Verify compulsory fields are identified
		ReusableFunc.verifyTextInWebElement(driver, By.id("name.errors"), "may not be empty");
		ReusableFunc.verifyTextInWebElement(driver, By.id("shortName.errors"), "may not be empty");
		ReusableFunc.verifyTextInWebElement(driver, By.id("permalink.errors"), "may not be empty");
		
		//Generate random 4 char string
	 	RandomString rs = new RandomString(3);
	 	String name="NewCat"+rs.nextString();

		//input category name
	 	ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),name);
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.id("shortName")),"Short name");
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.id("permalink")),"Permalink bus");
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "save");		
	}
	
	@Test(priority = -3 ,dependsOnMethods = { "addCategory" })
	public void addSubCategory() throws Exception{
		//navigateBackToCategoriesPage();
		
		//Generate random 4 char string
	 	RandomString rs = new RandomString(3);
	 	String name="SubCat"+rs.nextString();

		//Click on Add to add a sub category
		ReusableFunc.ClickByCss(driver, ".btn.btn-app.btn-success.btn-mini.radius-4.ajax-nav");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/button", "Click on save for a black sub cat entry");
		
		Utilities.fluentWait(By.id("name.errors"), driver);
		
		//Verify compulsory fields are identified
		ReusableFunc.verifyTextInWebElement(driver, By.id("name.errors"), "may not be empty");
		ReusableFunc.verifyTextInWebElement(driver, By.id("shortName.errors"), "may not be empty");
		ReusableFunc.verifyTextInWebElement(driver, By.id("permalink.errors"), "may not be empty");
		
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),name); 
		driver.findElementByXPath(".//*[@id='seoTitle']").clear();
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='seoTitle']")),"SEOOOOOOOOOO");
		
		driver.findElementByXPath(".//*[@id='seoDescription']").clear();
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='seoDescription']")),"SEOOOOOOOOOO Description");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div[2]/div/button", "save");
	}
	
	@Test(priority = -2, dependsOnMethods = { "addSubCategory" })
	public void editCategory() throws Exception{
		Thread.sleep(2000);
		//navigateBackToCategoriesPage();
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div/div/div[2]/ul/li[1]/div/a[1]/i", "edit");
		
		Thread.sleep(2000);
		
		 driver.findElementByXPath(".//*[@id='name']").clear();
		//Generate random 4 char string
	 	RandomString rs = new RandomString(3);
	 	String name="EditCateogry"+rs.nextString();
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),name);
		  
		driver.findElementByXPath(".//*[@id='seoTitle']").clear();
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='seoTitle']")),"SEOOOOOOOOOO");
		
		driver.findElementByXPath(".//*[@id='seoDescription']").clear();
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='seoDescription']")),"SEOOOOOOOOOO Description");
		//$(".btn.btn-app.btn-success.btn-mini.radius-4.ajax-submit").click();
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/button", "save");
		Thread.sleep(3000);
		System.out.println("done");
		Thread.sleep(3000);
	}
	
	@Test(priority = -1,dependsOnMethods = { "addSubCategory"})
	public void deleteCategory() throws Exception{
		 Thread.sleep(2000);
		 //navigateBackToCategoriesPage();
		 ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div/div/div[2]/ul/li[1]/div/a[2]/i", "delete");
		 ReusableFunc.VerifyTextOnPage(driver, "Are you sure?");
		 ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "Remove");
		System.out.println("Category test complete");
	}
	
	
	private void navigateToCategoriesPage() throws Exception {
		ReusableFunc.ClickByLinkText(driver, "Global Settings");
		Thread.sleep(2000);
		ReusableFunc.ClickByLinkText(driver, "Categories");
		Thread.sleep(2000);
	}
	
	private void navigateBackToCategoriesPage() throws Exception {
		ReusableFunc.ClickByLinkText(driver, "Global Settings");
		navigateToCategoriesPage();
	}
	
}
