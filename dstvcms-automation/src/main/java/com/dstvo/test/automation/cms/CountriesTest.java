package com.dstvo.test.automation.cms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class CountriesTest extends TestBase {
	private String countryName = "Mzansi Africa";
	private String altCountryName = "Afrika Borwa";
	private String countryNotFoundMsg = "No Countries found.";

	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}

	@Test(priority = -5)
	public void addCountry() throws Exception {
		Thread.sleep(2000);
		System.out.println("P1: Start Adding...");
		ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
		System.out.println("Starting Country test");
		Thread.sleep(2000);
		navigateToCountriesPage();
		// Search If county already Exist
		ReusableFunc.EnterTextInTextBox(driver,driver.findElementByXPath("//*[@id='countries-list_filter']/label/input"),countryName);
		Thread.sleep(2000);
		String actualText = driver.findElementByXPath("//*[@id='countries-list']/tbody/tr/td[1]").getText();
		System.out.println("Search Results: " + actualText);
		// Is so, delete it
		if (actualText.contains(countryName)) {
			ReusableFunc.ClickByXpath(driver,"//*[@id='countries-list']/tbody/tr[1]/td[2]/a[2]/i","delete");
			confirmDelete();
		}
		// Otherwise continue adding country
		
		ReusableFunc.ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[1]/div/div[1]/a","add country");
		Thread.sleep(2000);;
		ReusableFunc.VerifyTextOnPage(driver, "Type the country name here");
		Thread.sleep(2000);;
		fillInAddCountyForm(countryName, altCountryName,3,1,2,1);
		addFlag("MzansiFlag");
		saveUsingBottomButton(countryName);
		System.out.println("P1: Added...");
		Thread.sleep(2000);
	}

	@Test(priority = -4, dependsOnMethods = { "addCountry" })
	public void editCountry() throws Exception {
		navigateBackToCountriesPage();
		System.out.println("P2:	Start Editing...");
		String editedCountryName = countryName+" Edited";
		String editedAltCountryName = altCountryName +" Edited";
		Thread.sleep(2000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath("//*[@id='countries-list_filter']/label/input"), countryName);
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver,"//*[@id='countries-list']/tbody/tr[1]/td[2]/a[1]","Edit button");
		System.out.println("P2: Continue Edit");
		Thread.sleep(2000);
		//fill the form
		fillInEditCountyForm(editedCountryName, editedAltCountryName,2,1,1,2);
		removeAndAddTheFlag("AutoFlag");
		editFlag();
		//Save the form
		saveUsingTopRightButton(editedCountryName);
		System.out.println("P2:	Start Edited");
	}

	@Test(priority = -3, dependsOnMethods = { "addCountry" })
	public void deleteCountry() throws Exception {
		navigateBackToCountriesPage();
		Thread.sleep(2000);
		System.out.println("P3: Start Deleting...");
		ReusableFunc.EnterTextInTextBox(driver,driver.findElementByXPath("//*[@id='countries-list_filter']/label/input"),countryName);
		// delete from country from edit page
		ReusableFunc.ClickByXpath(driver,"//*[@id='countries-list']/tbody/tr[1]/td[2]/a[1]","Edit button");
	
		Thread.sleep(2000);;
		ReusableFunc.ClickByXpath(driver,"//*[@id='form']/div[1]/div[2]/div/a[2]", "remove");
		Thread.sleep(2000);
		//confirmDelete
		confirmDelete();
		System.out.println("P3: Deleted");
	}

	private void confirmDelete() throws Exception {
		ReusableFunc.VerifyTextOnPage(driver, " Are you sure?");
		ReusableFunc.ClickByXpath(driver,"//*[@id='form']/div[2]/div/div/button", "Remove");
		Thread.sleep(2000);;
		ReusableFunc.EnterTextInTextBox(driver,driver.findElementByXPath("//*[@id='countries-list_filter']/label/input"),countryName);
		Thread.sleep(2000);;
		ReusableFunc.VerifyTextOnPage(driver, countryNotFoundMsg);
		Thread.sleep(2000);
	}

	private void fillInAddCountyForm(String name, String altName, int lang, int DefaultLang, int timeZone, int activeStatus)throws Exception {
		// input country name
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")), name);
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.xpath(".//*[@id='alternateName']")),altName);

		// Generate random 4 char string
		RandomString rs = new RandomString(2);
		String alpha2 = "" + rs.nextString();
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("iso3166_1Alpha2")), alpha2);

		RandomString rs2 = new RandomString(2);
		String alpha3 = "A" + rs2.nextString();
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("iso3166_1Alpha3")), alpha3);
		Thread.sleep(2000);
		
		// Choose Language
		ReusableFunc.ClickDropdownElement(driver, By.id("languages_chosen"),
		By.xpath(".//*[@id='languages_chosen']/div/ul/li["+lang+"]"));
		Thread.sleep(2000);
//		//Timezone
//		ReusableFunc.ClickDropdownElement(driver, By.id("timezone_chosen"),By.xpath(".//*[@id='timezone_chosen']/div/ul/li"));
//		// Active - ON
//		ReusableFunc.CheckCheckBox(driver,driver.findElementByCssSelector("#active1"));

	}
	
	private void fillInEditCountyForm(String name, String altName, int lang, int DefaultLang, int timeZone, int activeStatus) throws Exception {
		fillInAddCountyForm(name,altName,lang,DefaultLang,timeZone,activeStatus);
	}
	
	private void saveUsingBottomButton(String countryName) throws Exception {
		ReusableFunc.ClickByXpath(driver,"//*[@id='form']/div[2]/div/div/button", "save");
		ConfirmSave(countryName);	
	}
	
	private void saveUsingTopRightButton(String countryName) throws Exception {
		ReusableFunc.ClickByXpath(driver,"//*[@id='form']/div[2]/div/div/button", "save");
		ConfirmSave(countryName);
	}
	
	private void ConfirmSave(String countryName) throws Exception {
		Thread.sleep(2000);
		ReusableFunc.EnterTextInTextBox(driver,driver.findElementByXPath("//*[@id='countries-list_filter']/label/input"),countryName);
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, countryName);
		Thread.sleep(2000);		
	}

	private void addFlag(String flagName) throws Exception {
		// Search for Flag image
		ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.xpath(".//*[@id='image_asset_search_search_filter']")),flagName);
		ReusableFunc.ClickByXpath(driver,".//*[@id='image_asset_search']/div[2]/a", "search");
		// select Image
		ReusableFunc.ClickByXpath(driver,".//*[@id='image_asset_search_form']/tbody/tr/td[1]/a/i","select Flag");
		Thread.sleep(3000);
	}
	
	private void removeAndAddTheFlag(String newFlagName) throws Exception {
		ReusableFunc.ClickByXpath(driver,"//*[@id='image_upload']/div/div[2]/div[4]/a[2]","select Flag");
		Thread.sleep(2000);
		addFlag(newFlagName);
	}
	
	private void editFlag() throws Exception {
		ReusableFunc.VerifyTextOnPage(driver, "Edit Image Instance");
	}
	
	private void navigateToCountriesPage() throws Exception {
		ReusableFunc.ClickByLinkText(driver, "Global Settings");
		Thread.sleep(2000);
		ReusableFunc.ClickByLinkText(driver, "Countries");
		Thread.sleep(2000);
	}
	
	private void navigateBackToCountriesPage() throws Exception {
		ReusableFunc.ClickByLinkText(driver, "Global Settings");
		navigateToCountriesPage();
	}
	
	/*
	 * @Test(dependsOnMethods = { "deleteCountry" }) public void addCountry2()
	 * throws Exception { Thread.sleep(2000); // use bottom +add country button
	 * ReusableFunc
	 * .ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[2]/a",
	 * "add country"); ReusableFunc.VerifyTextOnPage(driver,
	 * "Type the country name here");
	 * 
	 * // input random country name //Generate random 4 char string
	 * //RandomString rs = new RandomString(1); //String
	 * name="1stcountryInAFrica "+rs.nextString();
	 * ReusableFunc.EnterTextInTextBox
	 * (driver,driver.findElement(By.id("name")),"AutoCountry2");
	 * 
	 * RandomString rs2 = new RandomString(4); String
	 * altName="Alt Name is -"+rs2.nextString();
	 * ReusableFunc.EnterTextInTextBox(
	 * driver,driver.findElement(By.id("alternateName")),altName);
	 * 
	 * //Enter Alpha 2 //Generate random 4 char string RandomString rs3 = new
	 * RandomString(2); String alph2=""+rs3.nextString();
	 * ReusableFunc.EnterTextInTextBox
	 * (driver,driver.findElement(By.id("iso3166_1Alpha2")),alph2);
	 * 
	 * RandomString rs4 = new RandomString(2); String
	 * alph3="z"+rs4.nextString();
	 * ReusableFunc.EnterTextInTextBox(driver,driver.
	 * findElement(By.id("iso3166_1Alpha3")),alph3); Thread.sleep(3000); // save
	 * ReusableFunc
	 * .ClickByXpath(driver,".//*[@id='form']/div[2]/div/div/button", "Save");
	 * Thread.sleep(3000);
	 * 
	 * }
	 */
	/*
	 * @Test(dependsOnMethods = { "addCountry2" }) public void deleteCountry2()
	 * throws Exception { Thread.sleep(2000);
	 * ReusableFunc.EnterTextInTextBox(driver,
	 * driver.findElementByXPath("//*[@id='countries-list_filter']/label/input"
	 * ), "AutoCountry2"); Thread.sleep(3000); // delete from countries listing
	 * page ReusableFunc.ClickByXpath(driver,
	 * ".//*[@id='countries-list']/tbody/tr[1]/td[2]/a[2]","clicking delete");
	 * Thread.sleep(2000);; ReusableFunc.VerifyTextOnPage(driver,
	 * "cannot be recovered");
	 * 
	 * //cancel delete ReusableFunc.ClickByLinkText(driver, "Cancel");
	 * 
	 * // delete from products listing page Thread.sleep(2000);;
	 * ReusableFunc.EnterTextInTextBox(driver,
	 * driver.findElementByXPath(".//*[@id='countries-list_filter']/label/input"
	 * ), "AutoCountry2"); Thread.sleep(3000); ReusableFunc.ClickByXpath(driver,
	 * ".//*[@id='countries-list']/tbody/tr[1]/td[2]/a[2]","clicking delete");
	 * Thread.sleep(2000);; ReusableFunc.ClickByXpath(driver,
	 * ".//*[@id='form']/div[2]/div/div/button", "Remove"); Thread.sleep(2000);;
	 * System.out.println("Country test completed"); }
	 */

}
