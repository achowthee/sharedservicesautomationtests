package com.dstvo.test.automation.cms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class CurrenciesTest extends TestBase{
		
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}
	
	
//As a work around for the issue of having the currency ID in the Xpath, I have changed the  script
//So that it creates a currency that starts with a number and then refresh the page so that the currency is at the top
  @Test
  public void addCurrency() throws Exception{
	  ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
	  System.out.println("Starting Currencies test");
	  Thread.sleep(2000);
	  //Click Global settings first, then currencies
	  ReusableFunc.ClickByLinkText(driver, "Global Settings");
	  ReusableFunc.ClickByLinkText(driver, "Currencies");
	  
	  Thread.sleep(2000);
	//Generate random 4 char string
 	  RandomString rs = new RandomString(3);
 	  String name="0Auto"+rs.nextString();
	  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),name);
	  
	  RandomString rs2 = new RandomString(2);
	  String currencyCode="C"+rs2.nextString();
	  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("currencyCode")),currencyCode);
	  
	  RandomString rs3 = new RandomString(2);
	  String symbol="#"+rs3.nextString();
	  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("symbol")),symbol);
	  ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='country_chosen']/a/div/b"), By.xpath(".//*[@id='country_chosen']/div/ul/li[2]"));
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div/div[5]/button", "button");
	  Thread.sleep(2000);
	
  }
  
  
  @Test(dependsOnMethods = { "addCurrency" })
  public void editCurrency() throws Exception{
	  ReusableFunc.ClickByCss(driver, ".icon-pencil.bigger-130");
	  Thread.sleep(2000);
	  driver.findElementById("name").clear();
	  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),"0000AutoCurrencyEdited");
	  ReusableFunc.ClickByCss(driver, ".btn.btn-minier.btn-success.ajax-submit");
	  Thread.sleep(2000);
	  ReusableFunc.VerifyTextOnPage(driver, "0000AutoCurrencyEdited");
	  	  
  }
  

  @Test(dependsOnMethods={"editCurrency"})
  public void deleteCurrency() throws Exception{
	  Thread.sleep(2000);
	  ReusableFunc.verifyElementIsDisplayed(driver, By.cssSelector(".icon-trash.bigger-130"), "Delete button");
	  ReusableFunc.ClickByCss(driver, ".icon-trash.bigger-130");
	  ReusableFunc.VerifyTextOnPage(driver, "recovered");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div/div[2]/a/i", "button");
	  Thread.sleep(2000);
	  ReusableFunc.verifyElementIsDisplayed(driver, By.cssSelector(".icon-trash.bigger-130"), "Delete button");
	  ReusableFunc.ClickByCss(driver, ".icon-trash");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div/div[2]/button", "Delete button");
	  System.out.println("Currencies test complete");
  }
  
 
  
}
