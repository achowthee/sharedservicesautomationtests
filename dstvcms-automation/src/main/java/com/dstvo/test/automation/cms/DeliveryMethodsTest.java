package com.dstvo.test.automation.cms;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class DeliveryMethodsTest extends TestBase{
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	DateFormat dateFormatGetYear = new SimpleDateFormat("yyyy");
	static Date date = new Date();
	
	static String DeliveryMethodId;	
	RandomString rs = new RandomString(3);
	String name="000AutoDelivery"+ dateFormat.format(date) ;
	String editedName = name+" Edited";
	
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}
	
  @Test
  public void addDeliveryMethod() throws Exception{
	  System.out.println("Starting...Delivery methods");
	  ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
	  Thread.sleep(2000);
	  //Click Global settings first, then Age Restrictions
	  navigateToDeliveryMethodsPage();
	  	  
	  Thread.sleep(2000);
	  String deliveryMethodIdBeforeAdd = ReusableFunc.getAttributeInAWebElement(driver,By.xpath(".//*[@id='deliveryMethod_list']/li[2]"),"id") + "_container"; 
	  String actualText = driver.findElementByXPath(".//*[@id='"+deliveryMethodIdBeforeAdd+"']/div[1]").getText();
	  if (actualText.contains("000AutoDelivery" + dateFormatGetYear.format(date))) {
		  ReusableFunc.ClickByXpath(driver,".//*[@id='"+deliveryMethodIdBeforeAdd+"']/div[2]/a[2]" , "AutoDelivery Delete Button"); 
		  Thread.sleep(1000);
		  ConfirmDelete(deliveryMethodIdBeforeAdd);
		  navigateToDeliveryMethodsPage();
		}
	  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),name);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div/div[2]/button", "Clicking on save");
	  navigateBackToDeliveryMethodsPage();
	  DeliveryMethodId = ReusableFunc.getAttributeInAWebElement(driver,By.xpath(".//*[@id='deliveryMethod_list']/li[2]"),"id") + "_container" ;
	  System.out.println(".//*[@id='"+DeliveryMethodId+"']/div[2]/a[1]");
	  confirmAdd();
  }
   
  

@Test(dependsOnMethods = { "addDeliveryMethod" })
  public void editDeliveryMethod() throws Exception{
	 navigateBackToDeliveryMethodsPage();
	 
	 ReusableFunc.ClickByXpath(driver,".//*[@id='"+DeliveryMethodId+"']/div[2]/a[1]" , "AutoDelivery Edit Button");
	 Thread.sleep(1000);
	 driver.findElementById("name").clear();
	 ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),editedName);
	 ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div/div[2]/button", "Clicking on save");
	 ConfirmEdit();
  }
  
 

@Test(dependsOnMethods={"editDeliveryMethod"})
  public void deleteDeliveryMethod() throws Exception{
	  navigateBackToDeliveryMethodsPage();
	  Thread.sleep(2000);
	  //String id = ReusableFunc.getAttributeInAWebElement(driver,By.xpath(".//*[@id='deliveryMethod_list']/li[2]"),"id");
	  ReusableFunc.ClickByXpath(driver,".//*[@id='"+DeliveryMethodId+"']/div[2]/a[2]" , "AutoDelivery Delete Button"); 
	  Thread.sleep(1000);
	  
	  ConfirmDelete(DeliveryMethodId);
	  System.out.println(".....Delivery Methods test Completed");
  }
  
  

private void navigateToDeliveryMethodsPage() throws Exception {
	  ReusableFunc.ClickByLinkText(driver, "Global Settings");
	  Thread.sleep(1000);
	  //ReusableFunc.waitForElementToDisplay(driver, By.id("Program"), 3);
	  ReusableFunc.ClickByLinkText(driver, "Delivery Methods");
	  Thread.sleep(1000);
	  
}

private void navigateBackToDeliveryMethodsPage() throws Exception {
	ReusableFunc.ClickByLinkText(driver, "Global Settings");
	Thread.sleep(1000);
	navigateToDeliveryMethodsPage();
}

private void ConfirmEdit() throws Exception {
	// TODO Auto-generated method stub
	  navigateBackToDeliveryMethodsPage();
	  Thread.sleep(2000);
	  ReusableFunc.waitForElementToDisplay(driver, By.id("deliveryMethod_header_container"), 10);
	  String actualText = driver.findElementByXPath(".//*[@id='"+DeliveryMethodId+"']/div[1]").getText();
	  org.testng.Assert.assertEquals(actualText, editedName);
}

private void confirmAdd() throws Exception {
	// TODO Auto-generated method stub
	navigateBackToDeliveryMethodsPage();
	Thread.sleep(2000);
	ReusableFunc.waitForElementToDisplay(driver, By.id("deliveryMethod_header_container"), 10);
	  String actualText = driver.findElementByXPath(".//*[@id='"+DeliveryMethodId+"']/div[1]").getText();
	  org.testng.Assert.assertEquals(actualText, name);
}

private void ConfirmDelete(String idStr ) throws Exception {
	// TODO Auto-generated method stub
	ReusableFunc.VerifyTextOnPage(driver, "permanently removed");
	ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div/div[2]/button", "Permanently Remove Button");
	//ReusableFunc.ClickByCss(driver, "#form > div > div.span2.action-buttons > a"); //this will cancel the delete
	navigateBackToDeliveryMethodsPage();
	Thread.sleep(2000);
	ReusableFunc.waitForElementToDisplay(driver, By.id("deliveryMethod_header_container"), 10);
	//String actualText = driver.findElementByXPath(".//*[@id='"+idStr+"']/div[1]").getText();
	//org.testng.Assert.assertNotEquals(actualText, editedName);
	String deliveryMethodIdAfterDelete = ReusableFunc.getAttributeInAWebElement(driver,By.xpath(".//*[@id='deliveryMethod_list']/li[2]"),"id") + "_container";
	org.testng.Assert.assertNotEquals(deliveryMethodIdAfterDelete, idStr);
}


}
