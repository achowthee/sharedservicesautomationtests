package com.dstvo.test.automation.cms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class EditorialListCRUDTest extends TestBase  {
	private String editorName = "Noxolo Lekay";
	private String editorNameEdited = editorName + " Edited";

	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}

	@Test(priority = -20)
	public void editorialListHomePage() throws Exception{
		System.out.println("Starting Editorial List");
		ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
		Thread.sleep(2000);
		navigateToEditorialListPage();
		Thread.sleep(2000);
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='page_content']/div[1]/h1"), "Editorial List List");
		Thread.sleep(1000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='editorial-list_filter']/label/input"),editorName);
		Thread.sleep(2000);
		String actualText;
		try {
			actualText = driver.findElementByXPath(".//*[@id='editorial-list']/tbody/tr/td[1]").getText();
			//System.out.println("TRY " + actualText);
		} catch (Exception e) {
			actualText = driver.findElementByXPath(".//*[@id='editorial-list']/tbody/tr/td").getText();
			//System.out.println("Catch " + actualText);
		}
		 
		 //System.out.println("Search Results: " + actualText);
		// If so, delete it
		 if (actualText.contains(editorName)) {
			ReusableFunc.ClickByXpath(driver,".//*[@id='editorial-list']/tbody/tr/td[3]/a[2]", "Delete Button");
			confirmDelete(editorName);
		}
	}
	
	@Test(priority = -19, dependsOnMethods={"editorialListHomePage"})
	public void cancelVidoeAddEditorialList() throws Exception{
		navigateBackToEditorialListPage();
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[2]/a","add editorial item (bottom +add)");
		ReusableFunc.waitForElementToDisplay(driver, By.id("name"), 5);
		ReusableFunc.VerifyTextOnPage(driver, "Region availability");
		String toBeCanceled = editorName + " 2bCanceled";
		// input Editorial name
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='name']")),toBeCanceled);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/a", "Cancel");
		Thread.sleep(2000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='editorial-list_filter']/label/input"), toBeCanceled);
		Thread.sleep(2000);
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='editorial-list']/tbody/tr/td"), "No Editorial Lists found.");
	}

	@Test(priority = -18, dependsOnMethods={"editorialListHomePage"})
	public void addNewEditorialListBlankEntry() throws Exception {
		navigateBackToEditorialListPage();
		ReusableFunc.ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[1]/div/div[1]/a","add editorial item (top +add)");
		ReusableFunc.waitForElementToDisplay(driver, By.id("name"), 5);
		ReusableFunc.VerifyTextOnPage(driver, "Region availability");
		//attempt to save blank entry
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/button", "save blank");
		Thread.sleep(3000);
		ReusableFunc.VerifyTextOnPage(driver, "may not be empty");
		
	}
	
	@Test(priority = -17, dependsOnMethods={"editorialListHomePage"})
	public void addNewEditorialList() throws Exception {
		navigateBackToEditorialListPage();
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div[1]/div/div[1]/a","add editorial item (top +add)");
		ReusableFunc.waitForElementToDisplay(driver, By.id("name"), 5);
		ReusableFunc.VerifyTextOnPage(driver, "Region availability");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("name"), editorName);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("groupname"), "EPG");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("moreLinkUrl"), "http://guide.dstv.com/");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("moreLinkText"), "If you love Jason Statham, you will love him in this high octane action film. In the not too distant future, a reality TV show uses prisoners in a deadly race for entertainment.");
		ReusableFunc.ClickByID(driver, "regionAvailability_chosen", "Region Availability");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='regionAvailability_chosen']/div/div/input"),"01TestRegion21");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='regionAvailability_chosen']/div/ul/li", "01TestRegion21 Dropdown");
		addVideoMeta();
		addProgram();
		addPerson();
		//save
		saveUsingTopRightButton(editorName);
	}
	

	@Test (priority = -16, dependsOnMethods = { "addNewEditorialList" })
	public void editEditorialList() throws Exception{
		navigateBackToEditorialListPage();
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='editorial-list_filter']/label/input"), editorName);
		Thread.sleep(2000);
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='editorial-list']/tbody/tr[1]/td[1]"), editorName);
		ReusableFunc.ClickByXpath(driver,".//*[@id='editorial-list']/tbody/tr[1]/td[3]/a[1]/i", "Edit Button");
		Thread.sleep(3000);
		//ReusableFunc.verifyTextInWebElement(driver, By.id("page_sub_title"), editorName);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("name"), editorNameEdited); //update the Editorial Name
		saveUsingBottomButton(editorNameEdited);
	}
	
	@Test (priority = -15, dependsOnMethods = { "editEditorialList" })
	public void deleteEditorialList() throws Exception{
		navigateBackToEditorialListPage();
		Thread.sleep(2000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='editorial-list_filter']/label/input"),editorName);
		Thread.sleep(2000);
		String actualText = driver.findElementByXPath(".//*[@id='editorial-list']/tbody/tr[1]/td[1]").getText();
		//System.out.println("Search Results: " + actualText);
		// If so, delete it
		if (actualText.contains(editorNameEdited)) {
			ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='editorial-list']/tbody/tr[1]/td[1]"), editorNameEdited);
			ReusableFunc.ClickByXpath(driver,".//*[@id='editorial-list']/tbody/tr[1]/td[3]/a[2]", "Delete Button");
			confirmDelete(editorNameEdited);
		}
		System.out.println("......Editorial List tests Completed");	
	}
	
	private void addPerson() throws Exception{
		// TODO Auto-generated method stub
		ReusableFunc.ClickByID(driver, "person-select-button", "add Person");
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, "Select People");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='persons-modal_filter']/label/input"), "focus");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver,".//*[@id='persons-modal']/tbody/tr/td[3]/input", "Focus Radio Button");
		Thread.sleep(5000);
		ReusableFunc.ClickByID(driver, "savePeople", "savePeople");
		Thread.sleep(2000);
		editPeople();
		
	}

	private void editPeople() throws Exception {
		// TODO Auto-generated method stub
		ReusableFunc.waitForElementToDisplay(driver, By.id("startDate2"), 10);
		ReusableFunc.ClickByID(driver, "startDate2", "startDate2");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("startDate2"), "2015-06-17" );
		ReusableFunc.ClickByID(driver, "startDate2", "startDate2");

		ReusableFunc.waitForElementToDisplay(driver, By.id("expiryDate2"), 10);
		ReusableFunc.ClickByID(driver, "expiryDate2","expiryDate2");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("expiryDate2"), "2016-06-16" );
		ReusableFunc.ClickByID(driver, "expiryDate2","expiryDate2");
	}

	private void addProgram() throws Exception {
		// TODO Auto-generated method stub
		ReusableFunc.ClickByID(driver, "program-select-button", "add Program");
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, "Select Programs");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='programs-modal_filter']/label/input"), "black-ish");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver,".//*[@id='programs-modal']/tbody/tr/td[3]/input", "Black-ish Radio Button");
		Thread.sleep(5000);
		ReusableFunc.ClickByID(driver, "savePrograms", "savePrograms");
		Thread.sleep(2000);
		editProgram();
	}

	private void editProgram() throws Exception {
		// TODO Auto-generated method stub
		ReusableFunc.waitForElementToDisplay(driver, By.id("startDate1"), 10);
		ReusableFunc.ClickByID(driver, "startDate1", "startDate1");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("startDate1"), "2015-06-20" );
		ReusableFunc.ClickByID(driver, "startDate1", "startDate1");

		ReusableFunc.waitForElementToDisplay(driver, By.id("expiryDate1"), 10);
		ReusableFunc.ClickByID(driver, "expiryDate1","expiryDate1");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("expiryDate1"), "2016-06-12" );
		ReusableFunc.ClickByID(driver, "expiryDate1","expiryDate1");
	}

	private void addVideoMeta() throws Exception {
		// TODO Auto-generated method stub
		ReusableFunc.ClickByID(driver, "video-meta-select-button", "add VideoMeta");
		ReusableFunc.VerifyTextOnPage(driver, "Select Video Meta");
		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='video-metas-modal_filter']/label/input"), 15);
		Thread.sleep(3000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='video-metas-modal_filter']/label/input"), "zabalaza");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='video-metas-modal']/tbody/tr[1]/td[6]/input", "zabalaza Radio Button");
		Thread.sleep(5000);
		ReusableFunc.ClickByID(driver, "saveVideoMetas", "saveVideoMetas");
		Thread.sleep(2000);
		editVideoMeta();
	}

	private void editVideoMeta() throws Exception{
		// TODO Auto-generated method stub
		/**
		ReusableFunc.ClickByID(driver, "startDate0", "startDate0");
		for (int i = 0; i < 3; i++) {
			ReusableFunc.ClickByXpath(driver,"/html/body/div[6]/div[1]/table/thead/tr[1]/th[3]/i","increment Month");
		}
		ReusableFunc.ClickByXpath(driver,"/html/body/div[6]/div[1]/table/thead/tr[1]/th[1]/i","decrement Month");
		ReusableFunc.ClickByXpath(driver, "/html/body/div[6]/div[1]/table/tbody/tr[2]/td[2]", "Select Day");
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='expiryDate0']","End Date0");
		for (int i = 0; i < 12; i++) {
			ReusableFunc.ClickByXpath(driver, "/html/body/div[11]/div[1]/table/thead/tr[1]/th[3]/i", "increment Month");
		}
		ReusableFunc.ClickByXpath(driver, "/html/body/div[11]/div[1]/table/thead/tr[1]/th[1]/i", "decrement Month");
		ReusableFunc.ClickByXpath(driver, "/html/body/div[11]/div[1]/table/tbody/tr[2]/td[6]", "Select Day");
		*/
		ReusableFunc.waitForElementToDisplay(driver, By.id("startDate0"), 10);
		ReusableFunc.ClickByID(driver, "startDate0", "startDate0");
		//current date format YYYY-MM-DD
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("startDate0"), "2015-06-16" );
		ReusableFunc.ClickByID(driver, "startDate0", "startDate0");
		//Current Time Format HH:MM:SS.mmm
		//ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("packageProductInstances1.startTime"), "01:30:00.000" );
		
		
		ReusableFunc.waitForElementToDisplay(driver, By.id("expiryDate0"), 10);
		ReusableFunc.ClickByID(driver, "expiryDate0","expiryDate0");
		//current date formart YYYY-MM-DD
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("expiryDate0"), "2016-12-21" );
		ReusableFunc.ClickByID(driver, "expiryDate0","expiryDate0");
		//Current Time Format HH:MM:SS.mmm
		//ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("packageProductInstances1.expiryTime"), "01:30:00.000" );
		
	}

	private void navigateToEditorialListPage() throws Exception {
		ReusableFunc.ClickByLinkText(driver, "Media");
		Thread.sleep(1000);
		//ReusableFunc.waitForElementToDisplay(driver, By.id("editorialList"), 3);
		ReusableFunc.ClickByLinkText(driver, "Editorial List");
		Thread.sleep(1000);
	}
	private void navigateBackToEditorialListPage() throws Exception {
		ReusableFunc.ClickByLinkText(driver, "Media");
		Thread.sleep(1000);
		navigateToEditorialListPage();
	}
	private void saveUsingBottomButton(String name) throws Exception {
		//ReusableFunc.waitForElementToBeClickable(driver, By.xpath(".//*[@id='form']/div[2]/div/div/button"), 3);
		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[1]/div[2]/div/button", "save");
		Thread.sleep(1000);
		ConfirmSave(name);
	}
	private void saveUsingTopRightButton(String name) throws Exception {
		//ReusableFunc.waitForElementToBeClickable(driver, By.xpath(".//*[@id='form']/div[1]/div[2]/div[1]/button"), 3);
		ReusableFunc.waitForElementToBeClickable(driver, By.xpath(".//*[@id='form']/div[1]/div[2]/div/button"), 3);
		
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[2]/div/div/button", "save");
		Thread.sleep(1000);
		ConfirmSave(name);
	}
	private void ConfirmSave(String name) throws Exception {
		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='editorial-list_filter']/label/input"), 11);
		Thread.sleep(2000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='editorial-list_filter']/label/input"), name);
		Thread.sleep(2000);
		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='editorial-list']/tbody/tr[1]/td[1]"), 11);
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='editorial-list']/tbody/tr[1]/td[1]"), name);
		Thread.sleep(1000);
	}
	private void confirmDelete(String name) throws Exception {
		ReusableFunc.VerifyTextOnPage(driver,"Are you sure");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "delete");
		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='editorial-list_filter']/label/input"), 5);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='editorial-list_filter']/label/input"), name);
		Thread.sleep(2000);
		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='editorial-list']/tbody/tr/td"), 5);
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='editorial-list']/tbody/tr/td"), "No Editorial Lists found.");
		Thread.sleep(1000);
	}
}
