package com.dstvo.test.automation.cms;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ImageLibraryCRUDTest extends TestBase{
	private String streamName = "YouTube";
	private String streamNameEdited = streamName + " Edited";

	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}

	@Test(priority = -20)
	public void elementalStreamsHomePage() throws Exception{
		System.out.println("Starting ImageLibrary ");
		ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
		Thread.sleep(2000);
		System.out.println("...... ImageLibrary tests Completed ");
//		navigateToElementalStreamsPage();
//		Thread.sleep(2000);
//		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='page_content']/div[1]/h1"), "Elemental Streams List");
//		Thread.sleep(1000);
//		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='elementalStreams-list_filter']/label/input"),streamName);
//		Thread.sleep(2000);
//		String actualText;
//		try {
//			actualText = driver.findElementByXPath(".//*[@id='elementalStreams-list']/tbody/tr/td[1]").getText();
//			//System.out.println("TRY " + actualText);
//		} catch (Exception e) {
//			actualText = driver.findElementByXPath(".//*[@id='elementalStreams-list']/tbody/tr/td").getText();
//			//System.out.println("Catch " + actualText);
//		}
//		 
//		 //System.out.println("Search Results: " + actualText);
//		// If so, delete it
//		 if (actualText.contains(streamName)) {
//			ReusableFunc.ClickByXpath(driver,".//*[@id='elementalStreams-list']/tbody/tr/td[3]/a[2]", "Delete Button");
//			confirmDelete(streamName);
//		}
	}
	
//	@Test(priority = -19, dependsOnMethods={"elementalStreamsHomePage"})
//	public void cancelAddElementalStreams() throws Exception{
//		navigateBackToElementalStreamsPage();
//		Thread.sleep(2000);
//		ReusableFunc.ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[2]/a","add ElementalStreams (bottom +add)");
//		ReusableFunc.waitForElementToDisplay(driver, By.id("name"), 5);
//		ReusableFunc.VerifyTextOnPage(driver, "Player Url");
//		String toBeCanceled = streamName + " 2bCanceled";
//		// input Editorial name
//		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='name']")),toBeCanceled);
//		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/a", "Cancel");
//		Thread.sleep(2000);
//		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='elementalStreams-list_filter']/label/input"), toBeCanceled);
//		Thread.sleep(2000);
//		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='elementalStreams-list']/tbody/tr/td"), "No Elemental Streams found.");
//	}
//
//	@Test(priority = -18, dependsOnMethods={"elementalStreamsHomePage"})
//	public void addNewElementalStreamsBlankEntry() throws Exception {
//		navigateBackToElementalStreamsPage();
//		ReusableFunc.ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[1]/div/div[1]/a","add ElementalStreams (top +add)");
//		ReusableFunc.waitForElementToDisplay(driver, By.id("name"), 5);
//		ReusableFunc.VerifyTextOnPage(driver, "Player Url");
//		//attempt to save blank entry
//		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/button", "save blank");
//		Thread.sleep(3000);
//		ReusableFunc.VerifyTextOnPage(driver, "may not be empty");
//		
//	}
//	
//	@Test(priority = -17, dependsOnMethods={"elementalStreamsHomePage"})
//	public void addNewElementalStreams() throws Exception {
//		navigateBackToElementalStreamsPage();
//		ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div[1]/div/div[1]/a","add ElementalStreams (top +add)");
//		ReusableFunc.waitForElementToDisplay(driver, By.id("name"), 5);
//		ReusableFunc.VerifyTextOnPage(driver, "Player Url");
//		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("name"),streamName);
//		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("playerURL"), "https://www.youtube.com/watch?v=RvCT9PP0398");
//		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("channelTag"), "Worst Selfies Ever");
//		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='streamType_chosen']/a"), By.xpath(".//*[@id='streamType_chosen']/div/ul/li[2]"));
//		//ReusableFunc.ClickByID(driver, "active1", "Active");
//		//ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[2]/div/div[2]/div/label/span[1]", "Active");
//		//save
//		saveUsingTopRightButton(streamName);
//	}
//	
//	@Test (priority = -16, dependsOnMethods = { "addNewElementalStreams" })
//	public void editElementalStreams() throws Exception{
//		navigateBackToElementalStreamsPage();
//		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='elementalStreams-list_filter']/label/input"), streamName);
//		Thread.sleep(2000);
//		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='elementalStreams-list']/tbody/tr[1]/td[1]"), streamName);
//		ReusableFunc.ClickByXpath(driver,".//*[@id='elementalStreams-list']/tbody/tr[1]/td[5]/a[1]", "Edit Button");
//		Thread.sleep(3000);
//		//ReusableFunc.verifyTextInWebElement(driver, By.id("page_sub_title"), streamName);
//		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("name"), streamNameEdited); //update the Editorial Name
//		saveUsingBottomButton(streamNameEdited);
//	}
//	
//	@Test (priority = -15, dependsOnMethods = { "editElementalStreams" })
//	public void deleteElementalStreams() throws Exception{
//		navigateBackToElementalStreamsPage();
//		Thread.sleep(2000);
//		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='elementalStreams-list_filter']/label/input"),streamName);
//		Thread.sleep(2000);
//		String actualText = driver.findElementByXPath(".//*[@id='elementalStreams-list']/tbody/tr[1]/td[1]").getText();
//		System.out.println("Search Results: " + actualText);
//		// If so, delete it
//		if (actualText.contains(streamNameEdited)) {
//			ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='elementalStreams-list']/tbody/tr[1]/td[1]"), streamNameEdited);
//			ReusableFunc.ClickByXpath(driver,".//*[@id='elementalStreams-list']/tbody/tr[1]/td[5]/a[2]", "Delete Button");
//			confirmDelete(streamNameEdited);
//		}
//		System.out.println("completed... ImageLibrary tests ");	
//	}
//	
//	private void navigateToElementalStreamsPage() throws Exception {
//		ReusableFunc.ClickByLinkText(driver, "Media");
//		Thread.sleep(1000);
//		//ReusableFunc.waitForElementToDisplay(driver, By.id("ElementalStreams"), 3);
//		ReusableFunc.ClickByLinkText(driver, "Elemental Stream");
//		Thread.sleep(1000);
//	}
//	private void navigateBackToElementalStreamsPage() throws Exception {
//		ReusableFunc.ClickByLinkText(driver, "Media");
//		Thread.sleep(1000);
//		navigateToElementalStreamsPage();
//	}
//	private void saveUsingBottomButton(String name) throws Exception {
//		//ReusableFunc.waitForElementToBeClickable(driver, By.xpath(".//*[@id='form']/div[2]/div/div/button"), 3);
//		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[1]/div[2]/div/button", "save");
//		Thread.sleep(1000);
//		ConfirmSave(name);
//	}
//	private void saveUsingTopRightButton(String name) throws Exception {
//		//ReusableFunc.waitForElementToBeClickable(driver, By.xpath(".//*[@id='form']/div[1]/div[2]/div[1]/button"), 3);
//		ReusableFunc.waitForElementToBeClickable(driver, By.xpath(".//*[@id='form']/div[1]/div[2]/div/button"), 3);
//		
//		Thread.sleep(2000);
//		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[2]/div/div/button", "save");
//		Thread.sleep(1000);
//		ConfirmSave(name);
//	}
//	private void ConfirmSave(String name) throws Exception {
//		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='elementalStreams-list_filter']/label/input"), 11);
//		Thread.sleep(2000);
//		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='elementalStreams-list_filter']/label/input"), name);
//		Thread.sleep(2000);
//		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='elementalStreams-list']/tbody/tr[1]/td[1]"), 11);
//		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='elementalStreams-list']/tbody/tr[1]/td[1]"), name);
//		Thread.sleep(1000);
//	}
//	private void confirmDelete(String name) throws Exception {
//		ReusableFunc.VerifyTextOnPage(driver,"Are you sure");
//		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "delete");
//		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='elementalStreams-list_filter']/label/input"), 5);
//		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='elementalStreams-list_filter']/label/input"), name);
//		Thread.sleep(2000);
//		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='elementalStreams-list']/tbody/tr/td"), 5);
//		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='elementalStreams-list']/tbody/tr/td"), "No Elemental Streams found.");
//		Thread.sleep(1000);
//	}
}
