package com.dstvo.test.automation.cms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class LanguagesTest extends TestBase {
  
	private String language = "111st languageTest";
	private String nameISO6391 = "AW";
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}
	 
	 @Test(priority = -5)
	 public void addLanguage() throws Exception{
		 ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
		  System.out.println("Starting Languages test");
		  Thread.sleep(2000);
		  //Click Global settings first, then languages
		  //ReusableFunc.ClickByLinkText(driver, "Global Settings");
		  //ReusableFunc.ClickByLinkText(driver, "Languages");
		  navigateToLanguagesPage();
		  
		  Thread.sleep(3000);
		  
		  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),language);
		  
		 
		  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("iso639_1")),nameISO6391);
		  
		  RandomString rs3 = new RandomString(2);
		  String threeLetters="AZ"+rs3.nextString();
		  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("iso639_2B")),threeLetters);
		  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div/div[4]/button", " save button");
		  Thread.sleep(2000);
		
		  
	 }
	 
	 @Test(priority = -4, dependsOnMethods = { "addLanguage" })
	 public void editLanguage() throws Exception{
		 navigateBackToLanguagesPage();
		 Thread.sleep(2000);
		 ReusableFunc.VerifyTextOnPage(driver, "Languages");
    	 ReusableFunc.ClickByXpath(driver,".//*[@id='language_AW_container']/div[4]/a[1]/i", "Edit");
       	 Thread.sleep(1000);
       	 
       	 driver.findElementByXPath(".//*[@id='name']").clear();
       	 ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),language);
 		  
 		 //driver.findElementByXPath(".//*[@id='iso639_1']").clear();
 		 //RandomString rs2 = new RandomString(1);
		 //String twoELetters="E"+rs2.nextString();
		 ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("iso639_1")),nameISO6391);
		 driver.findElementByXPath(".//*[@id='iso639_2B']").clear();
		 RandomString rs3 = new RandomString(2);
		 String threeELetters="ED"+rs3.nextString();
		 ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("iso639_2B")),threeELetters);
		 // WebElement element = (new WebDriverWait(driver, 20))
			//	   .until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='form']/div/div[4]/button")));
		//element.click();
		  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div/div[4]/button", " save button");
				
	 }
	 
	 @Test(priority = -3, dependsOnMethods={"addLanguage"})
	  public void deleteLanguage() throws Exception{
		 navigateBackToLanguagesPage();
		 WebElement element = (new WebDriverWait(driver, 20)).until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='language_"+nameISO6391+"_container']/div[4]/a[2]/i")));
		  element.click();
		  //ReusableFunc.ClickByXpath(driver, ".//*[@id='language_AW_container']/div[4]/a[2]/i", "delete button");
		  ReusableFunc.VerifyTextOnPage(driver, "recovered");
		  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div/div[2]/button", "delete");
		  Thread.sleep(1000);
		  System.out.println("Languages test completed");
	 }
	 
		private void navigateToLanguagesPage() throws Exception {
			ReusableFunc.ClickByLinkText(driver, "Global Settings");
			Thread.sleep(1000);
			ReusableFunc.ClickByLinkText(driver, "Languages");
			Thread.sleep(1000);
		}
		
		private void navigateBackToLanguagesPage() throws Exception {
			ReusableFunc.ClickByLinkText(driver, "Global Settings");
			navigateToLanguagesPage();
		}
		  	  

}
