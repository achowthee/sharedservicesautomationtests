package com.dstvo.test.automation.cms;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class OrganizationsTest extends TestBase {

	
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}

	@Test()
	public void addOrganization() throws Exception {
		ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
		Thread.sleep(1000);
		System.out.println("Starting Organizations test");
		ReusableFunc.ClickByLinkText(driver, "Global Settings");
		ReusableFunc.ClickByLinkText(driver, "Organizations");
		ReusableFunc.ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[1]/div/div[1]/a","add organization (top +add)");
		ReusableFunc.VerifyTextOnPage(driver, "Type the organization name here");

		// input organization name
		//Generate random 4 char string
	 	RandomString r = new RandomString(2);
	 	String oname="11Organization "+r.nextString();
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),oname);
		
					
		// input address
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='address']")),"272 Pretoria Lane Avenue, Randburg, Johannesburg, South Africa");
		
		//Generate random 880char string to input 1000 characters in the ABout me field
	 	RandomString rs = new RandomString(80);
	 	String about="111Organization is the best organization in the whole wide world....Here's more: "+rs.nextString();
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("about")),about);
		
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("mapUrl"),"https://www.google.com/maps/dir/Exclusive+Books+Clearwater+Mall,+Clearwater+Mall,+Cor+Christiande+Wet+%26+Hendrik+Potgieter+Roads,+Johannesburg+1724,+");
		
		//generate random phone number
		Random generator = new Random();  
         
	    String strippedNum1,strippedNum2, strippedNum3, strippedNum4 ;  
	    int num1 = 0;  
	    int num2 = 0;  
	    int num3 = 0;  
	          
	    num1 = generator.nextInt(600) + 50;//numbers can't include an 8 or 9, can't go below 50.  
	    num2 = generator.nextInt(641) + 50;//number has to be less than 742//can't go below 50.  
	    num3 = generator.nextInt(8999) + 50; // make numbers 0 through 9 for each digit.//can't go below 50.  
	          
	    String string1 = Integer.toString(num1);  
	    strippedNum1 = Integer.toOctalString(num1); 
	         
	    String string2 = Integer.toString(num2);  
	    strippedNum2 = Integer.toOctalString(num2); 
	        
	    String string3 = Integer.toString(num3);  
	    strippedNum3 = Integer.toOctalString(num3); 
	        
	    strippedNum4 =  strippedNum1 + strippedNum2 + strippedNum3;
	    System.out.println("here is the contact number" + strippedNum4); 
	    Thread.sleep(2000);
	    
	    ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("contactNumbersmobile"), strippedNum4);
	    
	    //social media
	    ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaUrlshome"), "http://awesomeorg.com");
	    ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaUrlsfacebook"), "http://facebook.com/rocks");
	    ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaUrlsgoogleplus"), "http://google.com");
	    ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaUrlstwitter"), "http://twitter.com/awesome");  
	       
		//save-top button
	    ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/button", "Save");
	    Thread.sleep(3000);
		
	}

	@Test(dependsOnMethods = { "addOrganization" })
	public void editOrganization() throws Exception {
		ReusableFunc.ClickByXpath(driver, "//*[@id='organizations-list']/tbody/tr[1]/td[2]/a[1]", "Edit");
		//ReusableFunc.VerifyTextOnPage(driver, "111Organization");
		Thread.sleep(2000);
		//Edit Name
        driver.findElementByXPath(".//*[@id='name']").clear();
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.id("name")),"111OrgIsSimplyTheBest");
		
		
		//Delete Address
        driver.findElementById("address").clear();
		
		//Edit About-only have 999 characters
        driver.findElementById("about").clear();
        
       //Generate random 879char string to input 999 characters in the ABout me field
	 	RandomString rs = new RandomString(80);
	 	String about="111Organization is the best organization in the whole wide world...Here's more: "+rs.nextString();
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("about")),about);
		
		//Save with bottom save button
		ReusableFunc.ClickByXpath(driver, "//*[@id='form']/div[1]/div[2]/div/button/i", "Save");
		Thread.sleep(2000);
		
	}

	
	
	@Test(dependsOnMethods = { "editOrganization" })
	public void deleteOrganization() throws Exception {
	
		ReusableFunc.VerifyTextOnPage(driver, "111OrgIsSimplyTheBest");
		// delete from organization edit page
		Thread.sleep(1000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='organizations-list']/tbody/tr[1]/td[2]/a[1]", "Edit");
		WebElement element = (new WebDriverWait(driver, 20))
				   .until(ExpectedConditions.elementToBeClickable(By.linkText("Remove")));
		element.click();
		//ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/a", "Remove");
		Thread.sleep(1000);
		ReusableFunc.VerifyTextOnPage(driver, "sure?");
		
		
		//confirm delete
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "Remove");
		Thread.sleep(1000);

	}

	@Test(dependsOnMethods = { "deleteOrganization" })
	public void addOrganization2() throws Exception {
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[2]/a","add organization (bottom +add)");
		ReusableFunc.VerifyTextOnPage(driver, "Type the organization name here");

		// input organization name
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='name']")),"011 The Org");
					
		// input address
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='address']")),"Ruimsig");
		
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("about")),"011 The Org is too dope.");
		
				
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("mapUrl"),"https://www.google.com/maps");
		
		//generate random phone number
		Random generator = new Random();  
         
	    String strippedNum1,strippedNum2, strippedNum3, strippedNum4 ;  
	    int num1 = 0;  
	    int num2 = 0;  
	    int num3 = 0;  
	          
	    num1 = generator.nextInt(600) + 50;//numbers can't include an 8 or 9, can't go below 50.  
	    num2 = generator.nextInt(641) + 50;//number has to be less than 742//can't go below 50.  
	    num3 = generator.nextInt(8999) + 50; // make numbers 0 through 9 for each digit.//can't go below 50.  
	          
	    String string1 = Integer.toString(num1);  
	    strippedNum1 = Integer.toOctalString(num1); 
	         
	    String string2 = Integer.toString(num2);  
	    strippedNum2 = Integer.toOctalString(num2); 
	        
	    String string3 = Integer.toString(num3);  
	    strippedNum3 = Integer.toOctalString(num3); 
	        
	    strippedNum4 =  strippedNum1 + strippedNum2 + strippedNum3;
	    System.out.println("here is the contact number" + strippedNum4); 
	    Thread.sleep(2000);
	    
	    ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("contactNumbersmobile"), strippedNum4);
	    
	    //social media
	    //ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaUrlshome"), "http://awesomeorg.com");
	    ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaUrlsfacebook"), "facebook");
	    ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaUrlsgoogleplus"), "http://google.com");
	    ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaUrlstwitter"), "http://twitter.com/awesome");  
	       
		//save-bottom button
	    ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "Save");
	    Thread.sleep(3000);
		
	

	}
	
	
	@Test(dependsOnMethods = { "deleteOrganization" })
	public void cancelAddOrganization3() throws Exception {
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[2]/a","add organization (bottom +add)");
		ReusableFunc.VerifyTextOnPage(driver, "Type the organization name here");

		// input organization name
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='name']")),"101Cancel");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/a", "Cancel");
		Thread.sleep(2000);		
	}
		
	
	
	
	
	@Test(dependsOnMethods = { "addOrganization2" })
	public void deleteOrganization2() throws Exception {
		
		// delete from organizations listing page
		ReusableFunc.ClickByXpath(driver,".//*[@id='organizations-list']/tbody/tr[1]/td[2]/a[2]","Simply The Best");
		Thread.sleep(1000);
		ReusableFunc.VerifyTextOnPage(driver, "cannot be recovered");
		
		//cancel delete
		ReusableFunc.ClickByLinkText(driver, "Cancel");
		
		// delete from organizations listing page
		Thread.sleep(1000);
		ReusableFunc.ClickByXpath(driver,".//*[@id='organizations-list']/tbody/tr[1]/td[2]/a[2]","Simply The Best");
		Thread.sleep(1000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "Remove");
		//String bodyText1 = driver.findElement(By.tagName("body")).getText();
		//Assert.assertFalse(bodyText1.contains("011 The Org"), "Org was not deleted!");
		//Thread.sleep(1000);
		System.out.println("Organizations test completed");
	}

}
