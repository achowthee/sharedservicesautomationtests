package com.dstvo.test.automation.cms;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class PeopleTest extends TestBase{
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}

	@Test
	public void addPerson() throws Exception {
		ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
		Thread.sleep(1000);
		System.out.println("Starting Peoples test");
		ReusableFunc.ClickByLinkText(driver, "Global Settings");
		ReusableFunc.ClickByLinkText(driver, "People");
		ReusableFunc.ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[1]/div/div[1]/a","add person");
		ReusableFunc.VerifyTextOnPage(driver, "Person");
		Thread.sleep(2000);
		/*WebElement element = (new WebDriverWait(driver, 20))
				   .until(ExpectedConditions.elementToBeClickable(By.id("name")));
		element.click();*/
		// input person name
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.id("name")),"AutoGuy");

		// input surname
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='surname']")),"Person");

		//Choose Title
		ReusableFunc.ClickDropdownElement(driver, By.id("title_chosen"), By.xpath("/html/body/div[2]/div[2]/div[2]/div[2]/div/form/div/div/div[3]/div/div/ul/li[6]"));
		
		//Alias
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElementById("alias"), "beygency");
		
		
		//dateOfBirth
		ReusableFunc.ClickByID(driver, "dateOfBirth");
		ReusableFunc.ClickByXpath(driver, "html/body/div[6]/div[1]/table/thead/tr[1]/th[2]","year options");
		ReusableFunc.ClickByXpath(driver, "html/body/div[6]/div[2]/table/thead/tr/th[2]", "year ops 2");
		ReusableFunc.ClickByXpath(driver, "html/body/div[6]/div[3]/table/thead/tr/th[1]/i", "click back for more years");
		ReusableFunc.ClickByXpath(driver, "html/body/div[6]/div[3]/table/thead/tr/th[1]/i", "click back for 80s");
		ReusableFunc.ClickByXpath(driver, "html/body/div[6]/div[3]/table/tbody/tr/td/span[10]", "1998");
		
		//Select month
		ReusableFunc.ClickByXpath(driver, "html/body/div[6]/div[2]/table/tbody/tr/td/span[3]", "March");
		Thread.sleep(2000);
		//Select day
		ReusableFunc.ClickByXpath(driver, "html/body/div[6]/div[1]/table/tbody/tr[5]/td[3]", "24");
		Thread.sleep(1000);
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[1]/div[5]/div/div/span/i", "collapse datepicker");
							
		Thread.sleep(1000);
		//Choose gender
	    ReusableFunc.ClickByID(driver,"gender3");
		
		
		
		//Search for image
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='image_asset_search_search_filter']")),"AnImage");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='image_asset_search']/div[2]/a", "search");
	    Thread.sleep(1000);
		  
		//select Image
		ReusableFunc.ClickByXpath(driver, ".//*[@id='image_asset_search_form']/tbody/tr[1]/td[1]/a/i", "select A Face");
	
		
		//input primary email
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("primaryEmail"), "belinda.ndlovu@dstvo.com");
		
		//input secondary email
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("secondaryEmail"), "belinda.ndlovu@dstvdm.com");
		
		//short biography
		//Generate random 255 char string
	 	RandomString rs = new RandomString(255);
	 	String shortbio=""+rs.nextString();
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("shortBiography")),shortbio);
		
		
        //Biography
		//Generate random 1000 char string
	 	RandomString rs1 = new RandomString(1000);
	 	String bio=""+rs.nextString();
	 	//driver.switchTo().frame("iframe.cke_wysiwyg_frame");
	 	WebDriver bioIFrame = (new WebDriverWait(driver, 30))
				   .until(ExpectedConditions.frameToBeAvailableAndSwitchToIt((By.cssSelector("html body.height-limited div.main-container div#main_content.main-content div#page_content.page-content div.row-fluid div.span12 form#form.ajax-submit div.row-fluid div.span8 div.row-fluid div#cke_biography.cke_1 div.cke_inner div#cke_1_contents.cke_contents iframe.cke_wysiwyg_frame"))));
		//driver.switchTo().frame("iframe.cke_wysiwyg_frame");
	 	WebElement biog = (new WebDriverWait(driver, 20))
				   .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".cke_editable.cke_editable_themed.cke_contents_ltr.cke_show_borders")));
		biog.sendKeys(bio);
	 //	ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.xpath(".//*[@id='form']/div[1]/div[1]/div[11]")),bio);
	 	driver.switchTo().defaultContent();
	    
		//Select country
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='country_chosen']/a/span"), By.xpath("/html/body/div[2]/div[2]/div[2]/div[2]/div/form/div/div/div[12]/div/div/ul/li[13]"));

		//Select Organization
		Thread.sleep(1000);
		ReusableFunc.ClickDropdownElement(driver, By.id("organizations_chosen"), By.xpath("/html/body/div[2]/div[2]/div[2]/div[2]/div/form/div/div/div[13]/div/div/ul/li[6]"));
		Thread.sleep(1000);
	   //Select Roles
		ReusableFunc.ClickDropdownElement(driver, By.xpath("//*[@id='roles_chosen']/a"), By.xpath("//*[@id='roles_chosen']/div/ul/li[2]"));

	  //Mobile number
	  //generate random phone number
		Random generator = new Random();  
         
	    String strippedNum1,strippedNum2, strippedNum3, strippedNum4 ;  
	    int num1 = 0;  
	    int num2 = 0;  
	    int num3 = 0;  
	          
	    num1 = generator.nextInt(30) + 0;//numbers can't include an 8 or 9, can't go below 50.  
	    num2 = generator.nextInt(641) + 50;//number has to be less than 742//can't go below 50.  
	    num3 = generator.nextInt(8999) + 50; // make numbers 0 through 9 for each digit.//can't go below 50.  
	          
	    String string1 = Integer.toString(num1);  
	    strippedNum1 = Integer.toOctalString(num1); 
	         
	    String string2 = Integer.toString(num2);  
	    strippedNum2 = Integer.toOctalString(num2); 
	        
	    String string3 = Integer.toString(num3);  
	    strippedNum3 = Integer.toOctalString(num3); 
	        
	    strippedNum4 =  strippedNum1 + strippedNum2 + strippedNum3;
	    System.out.println("here is the contact number" + strippedNum4); 
	    Thread.sleep(2000);
	    
	    ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("contactNumbersmobile"), strippedNum4);
		
	    ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("contactNumbershome"), "0116866692");
	    
	    //Social Media
	    ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaUrlshome"), "dstv.com");
	    ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaUrlsfacebook"), "http://facebook.com/test");
	    ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaUrlsgoogleplus"), "google.co.za/hangouts");
	    ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaUrlstwitter"), "twitter.com/bnp");
	    ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaUrlsimdb"), "imdbbbbbbbbbb");
	    ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaUrlswikipedia"), "wikipedia");
	    
		// Save
		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[2]/div/div/button", "Save");
		Thread.sleep(2000);
		
	}

	@Test(dependsOnMethods = { "addPerson" })
	public void editPerson() throws Exception {
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='people-list_filter']/label/input"), "AutoGuy");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver,".//*[@id='people-list']/tbody/tr[1]/td[3]/a[1]/i","Edit button");
		
        Thread.sleep(1000);
		// Edit Name
        driver.findElementById("name").clear();
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.id("name")),"Autoguy-edited");
		
		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[2]/div/div/button", "Save");
		Thread.sleep(2000);
	}
	
	@Test(dependsOnMethods = { "editPerson" })
	public void deletePerson() throws Exception {
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='people-list_filter']/label/input"), "AutoGuy");
		Thread.sleep(2000);
		
		ReusableFunc.VerifyTextOnPage(driver, "Autoguy-edited");
		// delete from person edit page
		ReusableFunc.ClickByXpath(driver,"//*[@id='people-list']/tbody/tr[1]/td[3]/a[1]","Edit button");
		Thread.sleep(2000);
		
		ReusableFunc.ClickByXpath(driver, "//*[@id='form']/div[1]/div[2]/div/a[2]", "Remove");
		Thread.sleep(1000);
		ReusableFunc.VerifyTextOnPage(driver, " Are you sure?");
		
		//confirm delete
		ReusableFunc.ClickByXpath(driver, "//*[@id='form']/div[2]/div/div/button", "Remove");
		Thread.sleep(2000);
	
	}

	@Test(dependsOnMethods = { "deletePerson" })
	public void cancelDelete() throws Exception {
		// delete from people listing page
		ReusableFunc.ClickByXpath(driver,".//*[@id='people-list']/tbody/tr[2]/td[3]/a[2]/i","Selecting the next available entry");
		Thread.sleep(1000);
		ReusableFunc.VerifyTextOnPage(driver, "Are you sure?");
		
		//cancel delete
		ReusableFunc.ClickByLinkText(driver, "Cancel");
		
		// delete from person edit page
		Thread.sleep(1000);
		ReusableFunc.ClickByXpath(driver,"//*[@id='people-list']/tbody/tr[1]/td[3]/a[1]","Jolie");
		Thread.sleep(1000);
		ReusableFunc.ClickByXpath(driver, "//*[@id='form']/div[1]/div[2]/div/a[2]", "Remove");
		Thread.sleep(1000);
		//cancel delete
		ReusableFunc.ClickByXpath(driver, "//*[@id='form']/div[2]/div/div/a","Cancel");
		System.out.println("Peoples test completed");
	}

}
