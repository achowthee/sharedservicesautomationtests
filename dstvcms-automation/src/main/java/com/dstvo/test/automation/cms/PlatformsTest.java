package com.dstvo.test.automation.cms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class PlatformsTest extends TestBase{
		
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}
	 
  
  @Test(priority = -5)
  public void addPlatform() throws Exception{
	  ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
	  System.out.println("Starting...Platforms test");
	  Thread.sleep(2000);
	  //Click Global settings first, then Platforms
	  navigateToPlatformsPage();
	  Thread.sleep(1000);
	  //Canceling a Platform addition
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div[1]/div/div[1]/a", "Add platform");
	  Thread.sleep(2000);
	  ReusableFunc.ClickByLinkText(driver, "Cancel");
	  
	  //Adding a platform 
 	  ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div[1]/div/div[1]/a", "Add platform");
 	  Utilities.fluentWait(By.id("name"), driver);
	  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),"Auto- Plat");
	  //ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div/div[2]/button", "save");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "Save");
	  Thread.sleep(1000);

  }  
  
  @Test(priority = -4, dependsOnMethods = { "addPlatform" })
  public void editPlatform() throws Exception{
	  navigateBackToPlatformsPage();
	  ReusableFunc.ClickByCss(driver, ".icon-edit.bigger-120"); 
	  Thread.sleep(1000);
	  driver.findElementById("name").clear();
	  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),"Auto - Edited");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "Save button");
 	  
  }
  
  @Test(priority = -3, dependsOnMethods={"addPlatform"})
  public void deletePlatform() throws Exception{
	  navigateBackToPlatformsPage();
	  Thread.sleep(2000);
	  //This will cancel the delete
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath("//*[@id='platforms-list_filter']/label/input"), "Auto");
	  Thread.sleep(1000);
	  ReusableFunc.ClickByCss(driver, "#platforms-list > tbody > tr.odd > td.center > a.btn.btn-mini.btn-danger.ajax-nav");
	  Thread.sleep(2000);
	  ReusableFunc.VerifyTextOnPage(driver, "recovered");
	  ReusableFunc.ClickByCss(driver, "#form > div:nth-child(3) > div > div > a"); //Clicking on the cancel button in the confirm delete screen
	  
	  Thread.sleep(2000);
	  //Now doing the actual delete
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath("//*[@id='platforms-list_filter']/label/input"), "Auto");
	  Thread.sleep(1000);
	  ReusableFunc.ClickByCss(driver, "#platforms-list > tbody > tr:nth-child(1) > td.center > a.btn.btn-mini.btn-danger.ajax-nav");
	  Thread.sleep(2000);
	  ReusableFunc.VerifyTextOnPage(driver, "recovered");
	  ReusableFunc.VerifyTextOnPage(driver, "Auto");
	  ReusableFunc.ClickByCss(driver, "#form > div:nth-child(3) > div > div > button"); //Clicking on the cancel button in the confirm delete screen
	  Thread.sleep(2000);
	  System.out.println("......Platforms test Completed");
  }
  private void navigateToPlatformsPage() throws Exception {

	  ReusableFunc.ClickByLinkText(driver, "Global Settings");
	  ReusableFunc.ClickByLinkText(driver, "Platforms");
	  Thread.sleep(2000);
}
  private void navigateBackToPlatformsPage() throws Exception {
	  ReusableFunc.ClickByLinkText(driver, "Global Settings");
	navigateToPlatformsPage();
}
 
  
}
