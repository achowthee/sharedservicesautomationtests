package com.dstvo.test.automation.cms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ProductsCRUDTest extends TestBase {

	private String productName = "Automation Product" ;
	private String editProductName = productName + " Edited" ;
	
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}

	@Test(priority = -5)
	public void addProduct() throws Exception {
		ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
		System.out.println("Starting Products test");
		Thread.sleep(1000);
		navigateToProductsPage();
		
		
		/** 55555555555555555555555555555555555555555555555555555555555555*/
		// Search If county already Exist
				ReusableFunc.EnterTextInTextBox(driver,driver.findElementByXPath(".//*[@id='products-list_filter']/label/input"),productName);
				Thread.sleep(2000);
				String actualText = driver.findElementByXPath(".//*[@id='products-list']/tbody/tr[1]/td[1]").getText();
				System.out.println("Search Results: " + actualText);
				// Is so, delete it
				if (actualText.contains(productName)) {
					ReusableFunc.ClickByXpath(driver,".//*[@id='products-list']/tbody/tr[1]/td[2]/a[2]","delete");
					confirmDelete();
				}
		
		ReusableFunc.ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[1]/div/div[1]/a","add product");
		ReusableFunc.VerifyTextOnPage(driver, "Type the product name here");
		// input product name
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='name']")),productName);

		// input valid Domain name
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='domainName']")),"ThisIsADomainName.org");

		// input Ad Zone
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElementById("adZone"), "This is an AD Zone");

		// enter title
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElementById("seoTitle"), "SEO Test");

		// Enter SEO description
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElementById("seoDescription"),"Although conventional logic would hold that it's universally wiser to write a good meta description, rather than let the engines scrape a given web page.");

		// Enter Keywords
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElementByXPath(".//*[@id='form']/div[1]/div[1]/div[5]/div/div/div[2]/div/div[3]/div/input[2]"),"Awesome, great, amazeballs, remarkable, outstanding");

		// Social Networks
		ReusableFunc.EnterTextInTextBox(driver,driver.findElementById("socialMediaUrlsfacebook"),"http://facebook.com");
		ReusableFunc.EnterTextInTextBox(driver,driver.findElementById("socialMediaUrlsgoogleplus"),"http://google.com");
		ReusableFunc.EnterTextInTextBox(driver,driver.findElementById("socialMediaUrlstwitter"),"http://twitter.com");

		// social media Type dropdown - book
		//ReusableFunc.ClickDropdownElement(driver,By.xpath(".//*[@id='socialMediaTagsog_type_chosen']/a/div/b"),By.xpath(".//*[@id='socialMediaTagsog_type_chosen']/div/ul/li[5]"));

		// Image name
		ReusableFunc.EnterTextInTextBox(driver,driver.findElementById("socialMediaTagsog_image"),"Image that comes after the social stufffff");

		// Twitter meta cards
		ReusableFunc.ClickDropdownElement(driver,By.xpath(".//*[@id='socialMediaTagstwitter_card_chosen']/a/div/b"),By.xpath(".//*[@id='socialMediaTagstwitter_card_chosen']/div/ul/li[3]"));

		// Analytics
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElementById("analyticsTagsgoogleanalytics"),	"var _gaq = _gaq || []	_gaq.push(['_setAccount', 'UA-178888821485-3']tufffff");

		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[1]/div[1]/div[7]/div/div/div[2]/div/div/ul/li[2]/a","Alexa");
		ReusableFunc.EnterTextInTextBox(driver,driver.findElementById("analyticsTagsalexa"),"var _gaq = _gaq || []	_gaq.push(['_setAccount', 'UA-178888821485-3']tufffff");

		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[1]/div[1]/div[7]/div/div/div[2]/div/div/ul/li[3]/a","Omniture");
		ReusableFunc.EnterTextInTextBox(driver,driver.findElementById("analyticsTagsomniture"),"var _gaq = _gaq || []	_gaq.push(['_setAccount', 'UA-178888821485-3']tufffff");

		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[1]/div[1]/div[7]/div/div/div[2]/div/div/ul/li[4]/a","Effective Measures");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("analyticsTagseffectivemeasures"),"var _gaq = _gaq || []	_gaq.push(['_setAccount', 'UA-178888821485-3']tufffff");

		// HTTP Errors-404
		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[1]/div[1]/div[8]/div/div/div[2]/div/div[1]/ul/li[5]/a","404 error");
		ReusableFunc.EnterTextInTextBox(driver,driver.findElementById("httpErrors404"),"The page is cuurently unavailable. please try again laterrrrrrrrrr");

		// HTTP Errors-5xx
		ReusableFunc.EnterTextInTextBox(driver,driver.findElementById("httpErrors5xx"), "server errors");

		// Save
		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[1]/div[2]/div/button", "Save");
		Thread.sleep(2000);
	
		
		

		}

    @Test(priority = -4, dependsOnMethods = { "addProduct" })
	public void editProduct() throws Exception {
    	navigateBackToProductsPage();
    	System.out.println("Start Editing...");
		Thread.sleep(2000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='products-list_filter']/label/input"), productName);
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver,".//*[@id='products-list']/tbody/tr[1]/td[2]/a[1]/i","Edit button");
		System.out.println("P2: Continue Edit");
		Thread.sleep(2000);
		//fill the form
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.id("name")),editProductName);
		//Save the form
		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[1]/div[2]/div/button", "Save");
		System.out.println("End Edited");
	/*/ ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='products-list_filter']/label/input"), "AAAA");
		//Thread.sleep(2000);
    	ReusableFunc.ClickByCss(driver, "#products-list > tbody > tr:nth-child(1) > td.center > a.btn.btn-mini.btn-info.ajax-nav");
		
		System.out.println(" mama we made it");
		//ReusableFunc.ClickByXpath(driver, "//*[@id='products-list']/tbody/tr[1]/td[2]/a[1]", "edit");
		Thread.sleep(2000);
	
		
		// Edit Name
        driver.findElementById("name").clear();
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.id("name")),"1Automation Producttt");

	
		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[1]/div[2]/div/button", "Save");
		Thread.sleep(3000);*/
		
		}

		
   @Test(priority= -3, dependsOnMethods = { "addProduct" })
	public void deleteProduct() throws Exception {
	   navigateBackToProductsPage();
	   Thread.sleep(2000);
		System.out.println("P3: Start Deleting...");
		ReusableFunc.EnterTextInTextBox(driver,driver.findElementByXPath(".//*[@id='products-list_filter']/label/input"),editProductName);
		Thread.sleep(2000);
		// delete from country from edit page
		ReusableFunc.ClickByXpath(driver,".//*[@id='products-list']/tbody/tr[1]/td[2]/a[1]/i","Edit button");
	
		Thread.sleep(2000);;
		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[1]/div[2]/div/a[2]", "remove");
		Thread.sleep(2000);
		confirmDelete();
		
		System.out.println("Products test completed");
	}

	/*@Test(dependsOnMethods = { "deleteProduct" })
	public void addProduct2() throws Exception {
		// use bottom +add product button
		ReusableFunc.ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[2]/a", "add product");
		ReusableFunc.VerifyTextOnPage(driver, "Type the product name here");

		// input product name
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='name']")), "AAAAA");

		// prompt save
		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[1]/div[2]/div/button", "Save");
		ReusableFunc.VerifyTextOnPage(driver, "may not be empty");
		Thread.sleep(1000);
 
		// input valid Domain name
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.xpath(".//*[@id='domainName']")),"AAAAA.org");
		Thread.sleep(2000);

		// Edit network Ids FB
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.xpath(".//*[@id='socialMediaTagsfb_appid']")),"theAAAAAp");
		
		// Save
		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[1]/div[2]/div/button", "Save");
		Thread.sleep(1000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='products-list_filter']/label/input"), "AAAAA");
		Thread.sleep(1000);
		ReusableFunc.VerifyTextOnPage(driver, "AAAAA");

	}
	@Test(dependsOnMethods = { "addProduct2" })
	public void deleteProduct2() throws Exception {
		//ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='products-list_filter']/label/input"), "AAAAA");
	
		Thread.sleep(1000);
		// delete from products listing page
		//ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='products-list_filter']/label/input"), "AAAAA");
		
		ReusableFunc.ClickByCss(driver, ".btn.btn-mini.btn-danger.ajax-nav");
		ReusableFunc.VerifyTextOnPage(driver, "Are you sure?");
		
		//cancel delete
		ReusableFunc.ClickByLinkText(driver, "Cancel");
		
		// delete from products listing page
		Thread.sleep(1000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='products-list_filter']/label/input"), "AAAAA");
		Thread.sleep(2000);
		ReusableFunc.ClickByCss(driver, ".btn.btn-mini.btn-danger.ajax-nav");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "Remove");
		String bodyText1 = driver.findElement(By.tagName("body")).getText();
		Assert.assertFalse(bodyText1.contains("AAAAA"), "Product was not deleted!");
		Thread.sleep(1000);*/
//		System.out.println("Products test completed");
	//}
   
   private void navigateToProductsPage() throws Exception {
		ReusableFunc.ClickByLinkText(driver, "Global Settings");
		Thread.sleep(2000);
		ReusableFunc.ClickByLinkText(driver, "Products");
		Thread.sleep(2000);
	}
	private void navigateBackToProductsPage() throws Exception {
		ReusableFunc.ClickByLinkText(driver, "Global Settings");
		navigateToProductsPage();
	}
	
	private void confirmDelete() throws Exception {
		ReusableFunc.VerifyTextOnPage(driver, " Are you sure?");
		ReusableFunc.ClickByXpath(driver,"//*[@id='form']/div[2]/div/div/button", "Remove");
		Thread.sleep(2000);;
		ReusableFunc.EnterTextInTextBox(driver,driver.findElementByXPath(".//*[@id='products-list_filter']/label/input"),productName);
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, "No Product's found.");
		Thread.sleep(2000);
	}

	
}
