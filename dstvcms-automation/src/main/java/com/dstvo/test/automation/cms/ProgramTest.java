package com.dstvo.test.automation.cms;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ProgramTest extends TestBase {
	String programName = "TestProgramName";
    String editedProgramName = programName + "Edited";
	
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}

	@Test(priority = 11)
	public void programHomePage()throws Exception {
		ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
		Thread.sleep(1000);
		System.out.println("Starting...Program test");
		navigateToProgramPage();
		Thread.sleep(5000);
		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='page_content']/div[1]/h1"), 10);
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='page_content']/div[1]/h1"), "Program List");
		Thread.sleep(1000);
	     ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='program_List_form_filter']/label/input"), programName);
		 Thread.sleep(2000);
		 String actualText;
		try {
			actualText = driver.findElementByXPath(".//*[@id='program_List_form']/tbody/tr[1]/td[2]").getText();
			//System.out.println("TRY " + actualText);
		} catch (Exception e) {
			actualText = driver.findElementByXPath(".//*[@id='program_List_form']/tbody/tr/td").getText();
			//System.out.println("Catch " + actualText);
		}
		 
		 //System.out.println("Search Results: " + actualText);
		// If so, delete it
		 if (actualText.contains(programName)) {
			ReusableFunc.ClickByXpath(driver,".//*[@id='program_List_form']/tbody/tr[1]/td[4]/a[2]", "Delete Button");
			confirmDelete(programName);
		}
	}
	
	@Test(priority = -10, dependsOnMethods={"programHomePage"})
	public void cancelAddProgram() throws Exception {
		navigateBackToProgramPage();
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[2]/a","add program (bottom +add)");
		ReusableFunc.VerifyTextOnPage(driver, "Short Synopsis");

		String toBeCanceled = programName + " 2bCanceled";
		// input program name
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='name']")),toBeCanceled);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/a", "Cancel");
		Thread.sleep(2000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='program_List_form_filter']/label/input"), toBeCanceled);
		Thread.sleep(2000);
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='program_List_form']/tbody/tr/td"), "No data available in table");
		
//		String bodyText = driver.findElement(By.tagName("body")).getText();
//		Assert.assertFalse(bodyText.contains("101Cancel"), "Create program was not cancelled!");
//		Thread.sleep(1000);
		
		
	}
	
	@Test(priority = -9, dependsOnMethods={"programHomePage"})
	public void addProgramBlankEntry() throws Exception {
		navigateBackToProgramPage();
		ReusableFunc.ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[1]/div/div[1]/a","add program (top +add)");
		ReusableFunc.VerifyTextOnPage(driver, "Short Synopsis");
		//attempt to save blank entry
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[1]/button", "attempt to save");
		Thread.sleep(3000);
		ReusableFunc.VerifyTextOnPage(driver, "may not be empty");
	}
	
     @Test(priority = -8, dependsOnMethods={"programHomePage"})
	public void addProgram() throws Exception {
		navigateBackToProgramPage();
		ReusableFunc.ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[1]/div/div[1]/a","add program (top +add)");
		ReusableFunc.VerifyTextOnPage(driver, "Short Synopsis");
		// input program name
		//Generate random 4 char string
//	 	RandomString r = new RandomString(1);
//	 	String pname="000Program "+r.nextString();
		
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),programName);
		
		//input short synopsis
		//Generate random 115 char string
		RandomString rs = new RandomString(115);
		String shortsyn="Short"+rs.nextString();
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("shortSynopsis")),shortsyn);
		
		//input  synopsis
		//Generate random 1 char string
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("synopsis")),"1");
		
		//Choose Products(s)
		ReusableFunc.ClickDropdownElement(driver, By.id("program_product_select_chosen"), By.xpath(".//*[@id='program_product_select_chosen']/div/ul/li[18]"));
		
		//Choose channel(s)
		//ReusableFunc.ClickByID(driver, "s2id_autogen1");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("s2id_autogen1"), "studio");
		Thread.sleep(3000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='select2-drop']/ul/li[1]/div", "Select 1st Studio universal");
		//ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='s2id_autogen1']/ul"), By.xpath(".//*[@id='channelSelect_chosen']/div/ul/li[2]"));
		
		//PosterImage
		selectPosterImage();
		/** billboard image */
//		selectBillboardImage(); 
		/** Hero image 16:9 */
//		selectHeroImage();
		/** Hero image AMGO (92:61)*/
//	    selectHeroImageAMGO();
        
              
       
        //Seasons
		Thread.sleep(3000);
		ReusableFunc.waitForElementToBeClickable(driver, By.xpath(".//*[@id='addSeasonModalWrapper']/div/a"), 5);
        ReusableFunc.ClickByXpath(driver, ".//*[@id='addSeasonModalWrapper']/div/a", "Add season");
        ReusableFunc.waitForElementToDisplay(driver, By.id("seasonNumber"), 5);
        ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("seasonNumber"),"1");
    	RandomString rs2 = new RandomString(105);
		String shortsynopsis="Short synopsis "+rs2.nextString();
        ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("seasonShortSynopsis"),shortsynopsis);
        
        RandomString rs3 = new RandomString(220);
		String ssynopsis="This is the maxed out synopsis "+rs3.nextString();
        ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("seasonSynopsis"),ssynopsis);
        
        ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("totalNumberOfEpisodes"),"1456");
        ReusableFunc.ClickByID(driver, "saveAddSeason");
        
        Thread.sleep(2000);
       
      //Group Name
      	ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("groupName")),"Group Name AUtoooo");
      		
      	//Link guide event
      	ReusableFunc.ClickByID(driver, "program_epg_event_search-openEPGEvent");
      	Thread.sleep(4000);
      	
      	ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='s2id_program_epg_event_search-epg-country-filter']/a/span[2]/b"), 10);
      	//ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='s2id_program_epg_event_search-epg-country-filter']/a/span[2]/b"), By.xpath(".//*[@id='select2-drop']/ul/li[162]/div"));
      	ReusableFunc.ClickByXpath(driver,".//*[@id='s2id_program_epg_event_search-epg-country-filter']/a/span[2]/b", "Coutry Filter");
      	ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='select2-drop']/div/input"), "South Africa");
      	ReusableFunc.ClickByXpath(driver, ".//*[@id='select2-drop']/ul/li/div", "Select South Africa");
      	Thread.sleep(5000);
      	
      	ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='s2id_program_epg_event_search-epg-bouquet-filter']/a/span[2]/b"), 10);
      	//ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='s2id_program_epg_event_search-epg-bouquet-filter']/a/span[2]/b"), By.xpath(".//*[@id='select2-drop']/ul/li[3]/div"));
      	ReusableFunc.ClickByXpath(driver,".//*[@id='s2id_program_epg_event_search-epg-bouquet-filter']/a/span[2]/b", "bouquet-filter");
      	ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='select2-drop']/div/input"), "DStv Compact");
      	ReusableFunc.ClickByXpath(driver, ".//*[@id='select2-drop']/ul/li[2]/div", "Select Compact");
      	
      	Thread.sleep(5000);
      	ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='s2id_program_epg_event_search-epg-channel-filter']/a/span[2]/b"), 10);
        //ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='s2id_program_epg_event_search-epg-channel-filter']/a/span[2]/b"), By.xpath(".//*[@id='select2-drop']/ul/li[5]/div"));
      	ReusableFunc.ClickByXpath(driver, ".//*[@id='s2id_program_epg_event_search-epg-channel-filter']/a/span[2]/b", "channel filter");
      	ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='select2-drop']/div/input"), "Vu");
      	ReusableFunc.ClickByXpath(driver, ".//*[@id='select2-drop']/ul/li/div", "select Vuzu");
      	
      	
//      	ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='s2id_program_epg_event_search-epg-country-filter']/a/span[2]/b"), 10);
//      	//ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='s2id_program_epg_event_search-epg-bouquet-filter']/a/span[2]/b"), By.xpath(".//*[@id='select2-drop']/ul/li[3]/div"));
//      	ReusableFunc.ClickByXpath(driver,".//*[@id='s2id_program_epg_event_search-epg-bouquet-filter']/a/span[2]/b", "bouquet-filter");
//      	ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='select2-drop']/div/input"), "DStv Compact");
//      	ReusableFunc.ClickByXpath(driver, ".//*[@id='select2-drop']/ul/li[2]/div", "Select Compact");
      	
      	Thread.sleep(2000);
      	//ReusableFunc.ClickDropdownElement(driver, By.id("program_epg_event_search-epg-start-date-filter"), By.xpath(".//*[@id='select2-drop']/ul/li[3]/div"));
      	ReusableFunc.waitForElementToDisplay(driver, By.id("program_epg_event_search-epg-modal-table_wrapper"), 20);
      	ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='program_epg_event_search-epg-modal-table_wrapper']/div[1]"), "search-epg-modal");
      	ReusableFunc.ClickByXpath(driver, ".//*[@id='epg-modal-table-body']/tr[1]/td[7]/div/input", "EventRadioButton");
      	Thread.sleep(2000);
      	ReusableFunc.waitForElementToBeClickable(driver, By.id("program_epg_event_search-saveEPGEvent"), 10);
      	ReusableFunc.ClickByID(driver, "program_epg_event_search-saveEPGEvent");
      	Thread.sleep(3000);
        saveUsingBottomButton(programName);

	}

	@Test(priority = -7, dependsOnMethods = { "addProgram" })
	public void editProgram() throws Exception {
		navigateBackToProgramPage();
		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='program_List_form_filter']/label/input"), 5);
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='program_List_form_filter']/label/input")),programName);
		Thread.sleep(3000);
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='program_List_form']/tbody/tr/td[2]"), programName);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='program_List_form']/tbody/tr/td[4]/a[1]", "Edit button");
        
		//Edit Name
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='name']")),editedProgramName);
		Thread.sleep(2000);
	
		//Edit synopsis with max characters
        RandomString rs4 = new RandomString(320);
      	String lsynopsis="This is the maxed out synopsis "+rs4.nextString();
        ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("synopsis"),lsynopsis);
        
        //edit group name
        ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("groupName"), "Automationnnn");
        
   
		//Save with top save button
//		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[1]/button", "Save");
		Thread.sleep(2000);
		saveUsingTopRightButton(editedProgramName);
		
	}

	@Test(priority = -7, dependsOnMethods = { "editProgram" })
	public void deleteProgram() throws Exception {
		navigateBackToProgramPage();
		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='program_List_form_filter']/label/input"), 5);
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='program_List_form_filter']/label/input")),editedProgramName);
		Thread.sleep(3000);
		//ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='program_List_form']/tbody/tr/td[2]"), 5);
	    ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='program_List_form']/tbody/tr/td[2]"), editedProgramName);
		ReusableFunc.ClickByXpath(driver,".//*[@id='program_List_form']/tbody/tr[1]/td[4]/a[2]/i", "delete from listing page");
	
		//confirm delete
		confirmDelete(editedProgramName);
		System.out.println("......Program Test Completed");

	}

	private void  selectPosterImage() throws Exception{
		Thread.sleep(3000);
		ReusableFunc.waitForElementToDisplay(driver, By.id("program_images_poster_link_image_asset_search_"), 10);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='program_images_poster_link_image_asset_search_']"), "Jaguar F type");
		//Thread.sleep(2000);
		ReusableFunc.waitForElementToDisplay(driver, By.id("program_images_poster_link_image_asset_search"), 5);
        ReusableFunc.ClickByXpath(driver, ".//*[@id='program_images_poster_link_image_asset_search']/div[2]/a", "searchPosterImages");
        ReusableFunc.waitForElementToDisplay(driver, By.id("program_images_poster_link_image_asset_search__modal"), 10);
        ReusableFunc.ClickByXpath(driver, ".//*[@id='image_asset_search_form']/tbody/tr[1]/td[1]/a/i", "koala");
        //Thread.sleep(3000);
        ReusableFunc.waitForElementToBeClickable(driver, By.id("auto_crop_save_poster"), 10);
        ReusableFunc.ClickByXpath(driver, ".//*[@id='auto_crop_save_poster']", "auto_crop_save_poster");
        Thread.sleep(10000);
	}
	
	private void  selectBillboardImage() throws Exception{
		Thread.sleep(3000);
		ReusableFunc.waitForElementToDisplay(driver, By.id("program_images_billboard_link_image_asset_search_"), 10);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='program_images_billboard_link_image_asset_search_']"), "Jaguar F type");
		//Thread.sleep(2000);
		ReusableFunc.waitForElementToDisplay(driver, By.id("program_images_billboard_link_image_asset_search"), 5);
        ReusableFunc.ClickByXpath(driver, ".//*[@id='program_images_billboard_link_image_asset_search']/div[2]/a", "searchBillboardImages");
        ReusableFunc.waitForElementToDisplay(driver, By.id("program_images_billboard_link_image_asset_search__modal"), 10);
        ReusableFunc.ClickByXpath(driver, ".//*[@id='image_asset_search_form']/tbody/tr[1]/td[1]/a/i", "koaaala");
        //Thread.sleep(3000);
        ReusableFunc.waitForElementToBeClickable(driver, By.id("auto_crop_save_billboard"), 10);
        ReusableFunc.ClickByXpath(driver, ".//*[@id='auto_crop_save_billboard']", "auto_crop_save_billboard");
        Thread.sleep(3000);
	}
	
	private void  selectHeroImage() throws Exception{
		Thread.sleep(3000);
		ReusableFunc.waitForElementToDisplay(driver, By.id("program_images_hero-image_link_image_asset_search_"), 10);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='program_images_hero-image_link_image_asset_search_']"), "Jaguar F type");
		//Thread.sleep(2000);
		ReusableFunc.waitForElementToDisplay(driver, By.id("program_images_hero-image_link_image_asset_search"), 5);
        ReusableFunc.ClickByXpath(driver, ".//*[@id='program_images_hero-image_link_image_asset_search']/div[2]/a", "searchHero");
        ReusableFunc.waitForElementToDisplay(driver, By.id("program_images_hero-image_link_image_asset_search__modal"), 10);
        ReusableFunc.ClickByXpath(driver, ".//*[@id='image_asset_search_form']/tbody/tr[1]/td[1]/a/i", "koaaala");
        //Thread.sleep(3000);
        ReusableFunc.waitForElementToBeClickable(driver, By.id("auto_crop_save_hero_image"), 10);
        ReusableFunc.ClickByXpath(driver, ".//*[@id='auto_crop_save_hero_image']", "auto_crop_save_hero_image");
        Thread.sleep(3000);
	}
	
	private void  selectHeroImageAMGO() throws Exception{
		Thread.sleep(3000);
		ReusableFunc.waitForElementToDisplay(driver, By.id("program_images_hero-image-amgo_link_image_asset_search_"), 10);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='program_images_hero-image-amgo_link_image_asset_search_']"), "Jaguar F type");
		Thread.sleep(2000);
		ReusableFunc.waitForElementToDisplay(driver, By.id("program_images_hero-image-amgo_link_image_asset_search"), 5);
        ReusableFunc.ClickByXpath(driver, ".//*[@id='program_images_hero-image-amgo_link_image_asset_search']/div[2]/a", "searchHeroAMGO");
        ReusableFunc.waitForElementToDisplay(driver, By.id("program_images_hero-image-amgo_link_image_asset_search__modal"), 10);
        ReusableFunc.ClickByXpath(driver, ".//*[@id='image_asset_search_form']/tbody/tr[1]/td[1]/a/i", "koaaala");
        //Thread.sleep(3000);
        ReusableFunc.waitForElementToBeClickable(driver, By.id("auto_crop_save_hero_image_amgo"), 10);
        ReusableFunc.ClickByXpath(driver, ".//*[@id='auto_crop_save_hero_image_amgo']", "auto_crop_save_hero_image_amgo");
        Thread.sleep(3000);
	}

	private void navigateToProgramPage() throws Exception {
		  ReusableFunc.ClickByLinkText(driver, "Media");
		  Thread.sleep(1000);
		  //ReusableFunc.waitForElementToDisplay(driver, By.id("Program"), 3);
		  ReusableFunc.ClickByLinkText(driver, "Program");
		  Thread.sleep(1000);
		  
	}
	
	private void navigateBackToProgramPage() throws Exception {
		ReusableFunc.ClickByLinkText(driver, "Media");
		Thread.sleep(1000);
		navigateToProgramPage();
	}
	
	private void saveUsingBottomButton(String name) throws Exception {
		//ReusableFunc.waitForElementToBeClickable(driver, By.xpath(".//*[@id='form']/div[2]/div/div/button"), 3);
		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[2]/div/div/button", "save");
		Thread.sleep(1000);
		ConfirmSave(name);
	}
	
	private void saveUsingTopRightButton(String name) throws Exception {
		//ReusableFunc.waitForElementToBeClickable(driver, By.xpath(".//*[@id='form']/div[1]/div[2]/div[1]/button"), 3);
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[1]/div[2]/div[1]/button", "save");
		Thread.sleep(1000);
		ConfirmSave(name);
	}
	
	private void ConfirmSave(String name) throws Exception {
		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='program_List_form_filter']/label/input"), 20);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='program_List_form_filter']/label/input"), name);
		Thread.sleep(2000);
		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='program_List_form']/tbody/tr[1]/td[2]"), 5);
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='program_List_form']/tbody/tr[1]/td[2]"), name);
		Thread.sleep(1000);
	}
	
	private void confirmDelete(String name) throws Exception {
		 ReusableFunc.VerifyTextOnPage(driver,"Are you sure");
		 ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "delete");
		 ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='program_List_form_filter']/label/input"), 20);
		 ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='program_List_form_filter']/label/input"), name);
		 Thread.sleep(2000);
		 ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='program_List_form']/tbody/tr/td"), 5);
		 ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='program_List_form']/tbody/tr/td"), "No data available in table");
		 Thread.sleep(1000);
	}
		
//	public static void waitForElementToDisplay(RemoteWebDriver driver,By by,int timePeriodInSeconds){
//		WebDriverWait wait = new WebDriverWait(driver, timePeriodInSeconds);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
//	}
//	
//	public static void waitForElementToBeClickable(RemoteWebDriver driver,By by,int timePeriodInSeconds){
//		WebDriverWait wait = new WebDriverWait(driver, timePeriodInSeconds);
//		wait.until(ExpectedConditions.elementToBeClickable(by));
//	}

}
