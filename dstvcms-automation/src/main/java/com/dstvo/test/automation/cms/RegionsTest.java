package com.dstvo.test.automation.cms;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class RegionsTest extends TestBase {

	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}

	@Test
	public void regionsListing() throws Exception{
		ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
		System.out.println("Starting Regions test");
		Thread.sleep(1000);
		navigateToRegionsPage();
		//ReusableFunc.VerifyTextOnPage(driver, "Test Cat");
	}
	
	@Test(dependsOnMethods = { "regionsListing" })
	public void addRegion() throws Exception{
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='regions-list']/tbody/tr[8]/td[2]/a/i", "view assigned regions");
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div[2]/div[2]/div/a", "top +Add");
		Utilities.fluentWait(By.xpath(".//*[@id='description']"), driver);
		//Generate random 4 char string
	 	RandomString rs = new RandomString(3);
	 	String name="000NR"+rs.nextString();
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),name);
		  
		// input region name
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='description']")),"East AFrica");
		
        //Assign Countries to region
		ReusableFunc.CheckCheckBox(driver,driver.findElementById("countries2"));
		ReusableFunc.CheckCheckBox(driver,driver.findElementById("countries6"));
		ReusableFunc.CheckCheckBox(driver,driver.findElementById("countries7"));
		ReusableFunc.CheckCheckBox(driver,driver.findElementById("countries8"));
				
		//Save(top button)	
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/button", "save");
		
				
	}
	
	
	@Test(dependsOnMethods = { "addRegion" })
	public void addRegion2() throws Exception{
		//navigateBackToRegionsPage();
		Utilities.fluentWait(By.xpath(".//*[@id='page_content']/div[2]/div/div[3]/div/div[1]/a"), driver);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div[3]/div/div[1]/a", "Bottom Add button");
		Utilities.fluentWait(By.xpath(".//*[@id='description']"), driver);
		
		//Generate random 4 char string
	 	RandomString rs1 = new RandomString(4);
	 	String name2="West "+rs1.nextString();
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),name2);
		  
		// input region name
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='description']")),"The Western Region");
		
        //Assign Countries to region
		ReusableFunc.CheckCheckBox(driver,driver.findElementById("countries1"));
		ReusableFunc.CheckCheckBox(driver,driver.findElementById("countries3"));
		ReusableFunc.CheckCheckBox(driver,driver.findElementById("countries4"));
		ReusableFunc.CheckCheckBox(driver,driver.findElementById("countries11"));
				
		//Save(bottom button)	
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "save");
		
				
	}
	
	@Test(dependsOnMethods = { "addRegion2" })
	public void cancelAddRegion() throws Exception{
		//navigateBackToRegionsPage();
		Thread.sleep(2000);
		//click bottom +Add		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div[3]/div/div[1]/a", "Bottom +Add button");
		Thread.sleep(2000);
		
		//input region name
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='name']")),"The Region");
		
		// input region description
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='description']")),"The X Region");
		
        //Assign Countries to region
		ReusableFunc.CheckCheckBox(driver,driver.findElementById("countries1"));
		ReusableFunc.CheckCheckBox(driver,driver.findElementById("countries3"));
		ReusableFunc.CheckCheckBox(driver,driver.findElementById("countries4"));
		ReusableFunc.CheckCheckBox(driver,driver.findElementById("countries11"));
				
		//Save(bottom button)	
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/a", "cancel");
				
		/*String bodyText = driver.findElement(By.tagName("body")).getText();
		Assert.assertFalse(bodyText.contains("The Region"), "create region was not cancelled!");
		Thread.sleep(1000);*/
	
	
	}
	
	@Test(dependsOnMethods = { "addRegion2" })
	public void editRegion() throws Exception{
		//navigateBackToRegionsPage();
		Thread.sleep(2000);
		//ReusableFunc.ClickByXpath(driver, ".//*[@id='region_93ccc85c-5f2b-4208-b32b-118739c8ccc1']/div/div/a[1]/i", "edit");
		ReusableFunc.ClickByCss(driver, ".icon-pencil");
		
		Thread.sleep(2000);
		
		driver.findElementByXPath(".//*[@id='name']").clear();
		//Generate random 4 char string
	 	RandomString rs2 = new RandomString(2);
	 	String name3="01TestRegion"+rs2.nextString();
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),name3);
		  
		// input region description
		driver.findElementByXPath(".//*[@id='description']").clear();
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='description']")),"This region has been edited");
		
		//Assign Countries to region
		ReusableFunc.CheckCheckBox(driver,driver.findElementById("countries5"));
		ReusableFunc.CheckCheckBox(driver,driver.findElementById("countries12"));
		ReusableFunc.CheckCheckBox(driver,driver.findElementById("countries1"));
		
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/button", "save");
		
		
		
	}
	
	@Test(dependsOnMethods = { "editRegion" })
	public void deleteRegion() throws Exception{
		//navigateBackToRegionsPage();
		Thread.sleep(2000);
		//delete from regions listing
		ReusableFunc.ClickByCss(driver, ".icon-trash");
		ReusableFunc.VerifyTextOnPage(driver, "sure?");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/a/i", "Cancel");
		
		Thread.sleep(2000);
		ReusableFunc.ClickByCss(driver, ".icon-trash");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "Click on remvoe");
		System.out.println("Regions test completed");
	}
	
	/*@Test(dependsOnMethods = { "deleteRegion" })
	public void deleteRegion2() throws Exception{
		//delete from edit page
		ReusableFunc.ClickByXpath(driver, ".//*[@id='region_fc8628f1-fa89-4425-bd32-8cc0925adddc']/div/div/a[1]/i", "edit");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/a", "Remove");
		ReusableFunc.VerifyTextOnPage(driver, "sure?");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/a", "Cancel");
		
	}*/
	private void navigateToRegionsPage() throws Exception {
		ReusableFunc.ClickByLinkText(driver, "Global Settings");
		Thread.sleep(2000);
		ReusableFunc.ClickByLinkText(driver, "Regions");
		Thread.sleep(2000);
	}
	
	private void navigateBackToRegionsPage() throws Exception {
		ReusableFunc.ClickByLinkText(driver, "Global Settings");
		navigateToRegionsPage();
	}
}
