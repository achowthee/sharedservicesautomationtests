package com.dstvo.test.automation.cms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class RoleTypesTest extends TestBase{
		
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}
	
	
  
  @Test
  public void addRoleType1() throws Exception{
	  ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
	  System.out.println("Starting Roles test");
	  Thread.sleep(2000);
	  //Click Global settings first, then Role Types
	  ReusableFunc.ClickByLinkText(driver, "Global Settings");
	  ReusableFunc.ClickByLinkText(driver, "Role Types");
	  	    
	  Thread.sleep(2000);
	  
	  //1 character
	  RandomString rs = new RandomString(1);
 	  String name=""+rs.nextString();
	  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),name);
	  
	  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("description")),"Test");
	  
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div/div[2]/a/i", "cancel save");
	  Thread.sleep(1000);
	  
	  
	  
  }
	 
  
  @Test(dependsOnMethods = { "addRoleType1" })
  public void addRoleType() throws Exception{
	  navigateBackToRoleTypePage();
	  ReusableFunc.ClickByLinkText(driver, "Role Types");
	  Thread.sleep(2000);
	  //Generate random 4 string
 	  RandomString rs3 = new RandomString(1);
 	  String name2="AAARole"+rs3.nextString();
	  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),name2);
	  
	  //10 character
	  RandomString rs4 = new RandomString(10);
 	  String description ="ASAP Ferg, ASAP Rocky, That whole ASAP crew is.."+rs4.nextString();
	  
	  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("description")),description);
	  
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div/div[2]/button", "save");

  }  
  
  @Test(dependsOnMethods = { "addRoleType1" })
  public void editRoleType() throws Exception{
	  navigateBackToRoleTypePage();
	  ReusableFunc.ClickByXpath(driver, "//*[@id='roleType_a64473e4-55ba-437a-8b0e-c10a32190194_container']/div[3]/a[1]", "edit"); 
	  Thread.sleep(1000);
	  driver.findElementByXPath(".//*[@id='name']").clear();
	  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),"000Auto Role test Edited Do not Delete");
	  
	  driver.findElementByXPath(".//*[@id='description']").clear();
	  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("description")),"Automation Test Description Do not Delete");
	  
	  	  
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div/div[2]/button", "button");
	  Thread.sleep(1000);
	
	  	  
  }
  
  @Test(dependsOnMethods={"editRoleType"})
  public void deleteRoleType() throws Exception{
	  navigateBackToRoleTypePage();
	  //ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("//*[@id='roleType_a64473e4-55ba-437a-8b0e-c10a32190194_container']/div[3]/a[2]"), "delete button");
	  ReusableFunc.ClickByXpath(driver, "//*[@id='roleType_a64473e4-55ba-437a-8b0e-c10a32190194_container']/div[3]/a[2]", "delete button");
	  ReusableFunc.VerifyTextOnPage(driver, "recovered");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div/div[2]/a", "cancel button");
	  Thread.sleep(1000);
	  System.out.println("Roles test completed");
  }
  
  private void navigateToRoleTypePage() throws Exception {
	//Click Global settings first, then Role Types
	  ReusableFunc.ClickByLinkText(driver, "Global Settings");
	  Thread.sleep(1000);
	  ReusableFunc.ClickByLinkText(driver, "Role Types");
	  Thread.sleep(2000);
}
	
	private void navigateBackToRoleTypePage() throws Exception {
		ReusableFunc.ClickByLinkText(driver, "Global Settings");
		Thread.sleep(1000);
		navigateToRoleTypePage();
	}
  
  
}
