package com.dstvo.test.automation.cms;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class StatusArticleTest extends TestBase {

	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}

	@Test
	public void statusArticleListing() throws Exception{
		ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
		System.out.println("Starting Status-Article test");
		Thread.sleep(1000);
		ReusableFunc.ClickByLinkText(driver, "Global Settings");
		ReusableFunc.ClickByLinkText(driver, "Status");
		ReusableFunc.VerifyTextOnPage(driver, "Archived");
	}
	
	@Test(dependsOnMethods = { "statusArticleListing" })
	public void addArticleStatus() throws Exception{
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, "Article Status");
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='statustype_content_article']/div[2]/div/div/a", "+add button");
		Thread.sleep(1000);
		ReusableFunc.VerifyTextOnPage(driver, "Type the status name here");
				
		//Generate random char string
	 	RandomString rs = new RandomString(2);
	 	String name="Test Aricle : "+rs.nextString();
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),name);
		  
	
		//Save(top button)	
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/button", "save");
		
	}
	
	
	@Test(dependsOnMethods = { "addArticleStatus" })
	public void cancelAddArticleStatus() throws Exception{
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, "Article Status");
			
		ReusableFunc.ClickByXpath(driver, ".//*[@id='statustype_content_article']/div[2]/div/div/a", "+add button");
		Thread.sleep(1000);
		ReusableFunc.VerifyTextOnPage(driver, "Type the status name here");
					
		//Generate random char string
		RandomString rs1 = new RandomString(2);
		String name2="Automation2 test "+rs1.nextString();
	    ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),name2);
			  
		
		//Save(bottom button)	
	    ReusableFunc.ClickByLinkText(driver, "Cancel");
		//ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "save");
		
				
		String bodyText = driver.findElement(By.tagName("body")).getText();
		Assert.assertFalse(bodyText.contains("Automation2 "), "createarticle status was not cancelled!");
		Thread.sleep(1000);
	}
	
	@Test(dependsOnMethods = { "cancelAddArticleStatus" })
	public void editArticleStatus() throws Exception{
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='product_f975fad7-1062-4e61-8bed-4d2d891ed094']/div/div/a[1]/i", "edit");
		
		Thread.sleep(2000);
		
		driver.findElementByXPath(".//*[@id='name']").clear();
		ReusableFunc.VerifyTextOnPage(driver, "Type the status name here");
			
		//Generate random char string
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),"1st Automation Article Edit DONT DELETE");
			  
		
		//Save(bottom button)	
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "save");
	
	}
	
	@Test(dependsOnMethods = { "editArticleStatus" })
	public void deleteArticleStatus() throws Exception{
		Thread.sleep(2000);
		//delete from article status listing
		ReusableFunc.ClickByXpath(driver, ".//*[@id='product_f975fad7-1062-4e61-8bed-4d2d891ed094']/div/div/a[2]/i", "delete");
		ReusableFunc.VerifyTextOnPage(driver, "sure?");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/a/i", "Cancel");
		System.out.println("Status-Article test completed");
	}
}
