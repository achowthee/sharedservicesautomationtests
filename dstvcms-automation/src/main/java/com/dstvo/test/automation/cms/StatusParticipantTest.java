package com.dstvo.test.automation.cms;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class StatusParticipantTest extends TestBase {

	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}

	@Test
	public void statusParticipantListing() throws Exception{
		ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
		System.out.println("Starting Status-Participants test");
		Thread.sleep(1000);
		ReusableFunc.ClickByLinkText(driver, "Global Settings");
		ReusableFunc.ClickByLinkText(driver, "Status");
		ReusableFunc.VerifyTextOnPage(driver, "Archived");
	}
	
	@Test(dependsOnMethods = { "statusParticipantListing" })
	public void addParticipantStatus() throws Exception{
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, "Article Status");
		
		
		//Click Participant
		ReusableFunc.ClickByLinkText(driver,"Participant");
		
		//Click add
		ReusableFunc.ClickByXpath(driver, ".//*[@id='statustype_content_participant']/div[2]/div/div/a", "+add button");
		Thread.sleep(1000);
		ReusableFunc.VerifyTextOnPage(driver, "Type the status name here");
				
		//Generate random char string
	 	RandomString rs = new RandomString(3);
	 	String name="Test Part: "+rs.nextString();
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),name);
		  
	
		//Save(top button)	
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/button", "save");
		
		
				
	}
	
	
	@Test(dependsOnMethods = { "addParticipantStatus" })
	public void canceladdParticipantStatus() throws Exception{
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, "Article Status");
			
		ReusableFunc.ClickByXpath(driver, ".//*[@id='statustype_content_participant']/div[2]/div/div/a", "+add button");
		Thread.sleep(1000);
		ReusableFunc.VerifyTextOnPage(driver, "Type the status name here");
					
		//Generate random char string
		RandomString rs1 = new RandomString(1);
		String name2="Participanttt "+rs1.nextString();
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),name2);
			  
		
		//Save(bottom button)	
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/a", "save");
		
				
		String bodyText = driver.findElement(By.tagName("body")).getText();
		Assert.assertFalse(bodyText.contains("Participanttt"), "create Participant status was not cancelled!");
		Thread.sleep(1000);
	
	
	}
	
	@Test(dependsOnMethods = { "canceladdParticipantStatus" })
	public void editParticipantStatus() throws Exception{
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='product_9e44a9ce-292a-424a-b559-6c8dc6bbd8d8']/div/div/a[1]/i", "edit");
		
		Thread.sleep(2000);
		
		driver.findElementByXPath(".//*[@id='name']").clear();
		ReusableFunc.VerifyTextOnPage(driver, "Type the status name here");
			
		//Edit name
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),"1st Participant Automation QA Edit dont delete");
			  
		
		//Save(bottom button)	
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "save");
		
		
	}
	
	@Test(dependsOnMethods = { "editParticipantStatus" })
	public void deleteParticipantStatus() throws Exception{
		Thread.sleep(2000);
		//delete from participants status listing
		ReusableFunc.ClickByXpath(driver, ".//*[@id='product_9e44a9ce-292a-424a-b559-6c8dc6bbd8d8']/div/div/a[2]/i", "delete");
		ReusableFunc.VerifyTextOnPage(driver, "sure?");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/a", "Cancel");
		System.out.println("Status-Participants test completed");
	}
	
}
