package com.dstvo.test.automation.cms;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class StatusPhotoGalleryTest extends TestBase {

	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}

	@Test
	public void statusPhotoGalleryListing() throws Exception{
		ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
		System.out.println("Starting Status-Photo Gallery test");
	    Thread.sleep(1000);
		ReusableFunc.ClickByLinkText(driver, "Global Settings");
		ReusableFunc.ClickByLinkText(driver, "Status");
		ReusableFunc.VerifyTextOnPage(driver, "Archived");
	}
	
	@Test(dependsOnMethods = { "statusPhotoGalleryListing" })
	public void addStatusPhotoGallery() throws Exception{
		Thread.sleep(1000);
		ReusableFunc.VerifyTextOnPage(driver, "Article Status");
		
		
		//Click Photo Gallery
		ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div/div/div/ul/li[3]/a", "Photo gallery");
		
		//Click add
		ReusableFunc.ClickByXpath(driver, ".//*[@id='statustype_content_photogallery']/div[2]/div/div/a", "+add button");
		Thread.sleep(1000);
		ReusableFunc.VerifyTextOnPage(driver, "Type the status name here");
				
		//Generate random char string
	 	RandomString rs = new RandomString(3);
	 	String name="Test Photo: "+rs.nextString();
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),name);
		  
	
		//Save(top button)	
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/button", "save");
		
		
				
	}
	
	
		
	@Test(dependsOnMethods = { "addStatusPhotoGallery" })
	public void canceladdStatusPhotoGallery() throws Exception{
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, "Article Status");
		Thread.sleep(1000);	
						
		//Click add
		ReusableFunc.ClickByXpath(driver, ".//*[@id='statustype_content_photogallery']/div[2]/div/div/a", "+add button");
		Thread.sleep(1000);
		ReusableFunc.VerifyTextOnPage(driver, "Type the status name here");
					
			
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),"Photooooo");
		
		//Cancel
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/a", "cancel");
			  
				
		String bodyText = driver.findElement(By.tagName("body")).getText();
		Assert.assertFalse(bodyText.contains("Photooooo"), "create Photo gallery  status was not cancelled!");
		
	
	
	}
	
	@Test(dependsOnMethods = { "canceladdStatusPhotoGallery" })
	public void editPhotoGalleryStatus() throws Exception{
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='product_3c96162d-8553-4bdd-b1e2-7ec7aba38847']/div/div/a[1]/i", "edit");
		
		Thread.sleep(2000);
		
		driver.findElementByXPath(".//*[@id='name']").clear();
		ReusableFunc.VerifyTextOnPage(driver, "Type the status name here");
			
		//Edit name
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),"1st automation Photo gallery  Edit");
			  
		
		//Save(bottom button)	
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "save");
		
		
	}
	
	@Test(dependsOnMethods = { "editPhotoGalleryStatus" })
	public void deletePhotoGalleryStatus() throws Exception{
		Thread.sleep(2000);
		//delete from photo gallery status listing
		ReusableFunc.ClickByXpath(driver, ".//*[@id='product_3c96162d-8553-4bdd-b1e2-7ec7aba38847']/div/div/a[2]/i", "delete");
		ReusableFunc.VerifyTextOnPage(driver, "sure?");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/a", "Cancel");
		System.out.println("Status-Photo Gallery test completed");
	}
	
}
