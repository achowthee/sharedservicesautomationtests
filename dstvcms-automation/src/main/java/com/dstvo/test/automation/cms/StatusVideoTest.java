package com.dstvo.test.automation.cms;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class StatusVideoTest extends TestBase {

	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}

	@Test
	public void statusVideoListing() throws Exception{
		ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
		System.out.println("Starting Status-Video test");
		Thread.sleep(1000);
		ReusableFunc.ClickByLinkText(driver, "Global Settings");
		ReusableFunc.ClickByLinkText(driver, "Status");
		ReusableFunc.VerifyTextOnPage(driver, "Archived");
	}
	
	@Test(dependsOnMethods = { "statusVideoListing" })
	public void addStatusVideo() throws Exception{
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, "Article Status");
		
		
		//Click Video
		ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div/div/div/ul/li[4]/a", "video");
		
		//Click add
		ReusableFunc.ClickByXpath(driver, ".//*[@id='statustype_content_video']/div[2]/div/div/a/i", "+add button");
		Thread.sleep(1000);
		ReusableFunc.VerifyTextOnPage(driver, "Type the status name here");
				
		//Generate random char string
	 	RandomString rs = new RandomString(3);
	 	String name="Test vid"+rs.nextString();
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),name);
		  
	
		//Save(top button)	
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/button", "save");
		
		
				
	}
	
	
	@Test(dependsOnMethods = { "addStatusVideo" })
	public void cancelAddStatusVideo() throws Exception{
		Thread.sleep(2000);
		ReusableFunc.VerifyTextOnPage(driver, "Video Status");
		
		//Click add
		ReusableFunc.ClickByXpath(driver, ".//*[@id='statustype_content_video']/div[2]/div/div/a", "+add button");
		Thread.sleep(1000);
		ReusableFunc.VerifyTextOnPage(driver, "Type the status name here");
					
			
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),"Videooooo");
		
		//Cancel
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/a", "cancel");
			  
				
		String bodyText = driver.findElement(By.tagName("body")).getText();
		Assert.assertFalse(bodyText.contains("Videooooo"), "create Video status was not cancelled!");
		Thread.sleep(1000);
	
	
	}
	
	@Test(dependsOnMethods = { "cancelAddStatusVideo" })
	public void editStatusVideo() throws Exception{
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='product_0e55ddeb-7df8-44de-98c1-06ca75820946']/div/div/a[1]/i", "edit");
		
		Thread.sleep(2000);
		
		driver.findElementByXPath(".//*[@id='name']").clear();
		ReusableFunc.VerifyTextOnPage(driver, "Type the status name here");
			
		//Edit name
		ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),"1st Status don't delete");
			  
		
		//Save(bottom button)	
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "save");
			
	}
	
	@Test(dependsOnMethods = { "editStatusVideo" })
	public void deleteStatusVideo() throws Exception{
		Thread.sleep(2000);
		//delete from video status listing
		ReusableFunc.ClickByXpath(driver, ".//*[@id='product_0e55ddeb-7df8-44de-98c1-06ca75820946']/div/div/a[2]/i", "delete");
		ReusableFunc.VerifyTextOnPage(driver, "sure?");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/a", "Cancel");
		System.out.println("Status-Video test completed");
	}
	
}
