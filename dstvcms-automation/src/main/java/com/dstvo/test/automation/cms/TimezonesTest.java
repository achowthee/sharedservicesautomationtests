package com.dstvo.test.automation.cms;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class TimezonesTest extends TestBase {

	
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}

	@Test
	public void addTimezone() throws Exception {
		ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
		System.out.println("Starting Time Zone test");
		Thread.sleep(1000);
//		ReusableFunc.ClickByLinkText(driver, "Global Settings");
//		ReusableFunc.ClickByLinkText(driver, "Time Zones");
		navigateToTimezonesPage();
		ReusableFunc.ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[1]/div/div[1]/a","add Timezone");
		ReusableFunc.VerifyTextOnPage(driver, "Type the time zone name here");

		// input time zone name
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='name']"))," Australian Western Standard Time");

		// input Abbreviation
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='code']")),"AWST");

		// input UTC offset
		ReusableFunc.ClickDropdownElement(driver, By.id("utcOffsetSign_chosen"), By.xpath(".//*[@id='utcOffsetSign_chosen']/div/ul/li[2]"));
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElementById("utcOffsetHours"), "7");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[1]/div[3]/div[2]/div[2]/div/div/div/div/button[1]", "Should be 8");
        //minutes
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[1]/div[3]/div[2]/div[3]/div/div/div/div/button[1]", "Should be 15");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[1]/div[3]/div[2]/div[3]/div/div/div/div/button[2]", "Back to 0");

		//Save
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/button", "Save");
		
		
	
	}

	@Test(dependsOnMethods = { "addTimezone" })
	public void ediTimezone() throws Exception {
		navigateBackToTimezonesPage();
		ReusableFunc.ClickByXpath(driver,".//*[@id='timezones-list']/tbody/tr/td[2]/a[1]/i","Edit button");
		ReusableFunc.VerifyTextOnPage(driver, " Australian Western Standard Time");
        Thread.sleep(1000);
		// Edit Name
        driver.findElementByXPath(".//*[@id='name']").clear();
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='name']")),"1Australian Western Standard Time");
		
		Thread.sleep(2000);
		
		// input UTC offset
		ReusableFunc.ClickDropdownElement(driver, By.id("utcOffsetSign_chosen"), By.xpath(".//*[@id='utcOffsetSign_chosen']/div/ul/li[1]"));
		//ReusableFunc.EnterTextInTextBox(driver,	driver.findElementById("utcOffsetHours"), "7");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[1]/div[3]/div[2]/div[2]/div/div/div/div/button[2]", "Should be 7");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[1]/div[3]/div[2]/div[2]/div/div/div/div/button[2]", "Should be 6");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[1]/div[3]/div[2]/div[2]/div/div/div/div/button[2]", "Should be 5");


        //minutes
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[1]/div[3]/div[2]/div[3]/div/div/div/div/button[1]", "Should be 15");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[1]/div[3]/div[2]/div[3]/div/div/div/div/button[1]", "Should be 30");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[1]/div[3]/div[2]/div[3]/div/div/div/div/button[1]", "Should be 45");


		//Save
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "Save");

	}

	
	
	@Test(dependsOnMethods = { "ediTimezone"})
	public void addTimezone2() throws Exception {
		navigateBackToTimezonesPage();
		ReusableFunc.ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[2]/a","add Timezone bottom page");
		ReusableFunc.VerifyTextOnPage(driver, "Type the time zone name here");

		// input time zone name
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='name']")),"1Eastern Standard Time");

		// input Abbreviation
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='code']")),"AEST");

		// input UTC offset
		ReusableFunc.ClickDropdownElement(driver, By.id("utcOffsetSign_chosen"), By.xpath(".//*[@id='utcOffsetSign_chosen']/div/ul/li[1]"));
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElementById("utcOffsetHours"), "10");
        //minutes
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[1]/div[3]/div[2]/div[3]/div/div/div/div/button[1]", "Should be 15");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[1]/div[3]/div[2]/div[3]/div/div/div/div/button[2]", "Back to 0");

		//Save
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "Save");
		
	}
	
	@Test(dependsOnMethods = { "addTimezone2"})
	public void addTimezoneRandom() throws Exception {
		navigateBackToTimezonesPage();
		ReusableFunc.ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[2]/a","add Timezone bottom page");
		ReusableFunc.VerifyTextOnPage(driver, "Type the time zone name here");
		
		//Generate random 4 char string
	 	  RandomString rs = new RandomString(10);
	 	  String name="Random Timezone"+rs.nextString();
		  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("name")),name);
		  
		  RandomString rs2 = new RandomString(4);
		  String Abbreviation="5"+rs2.nextString();
		  ReusableFunc.EnterTextInTextBox(driver,driver.findElement(By.id("code")),Abbreviation);
		  
		  
		  //UTC Offset
		  ReusableFunc.ClickDropdownElement(driver, By.id("utcOffsetSign_chosen"), By.xpath(".//*[@id='utcOffsetSign_chosen']/div/ul/li[1]"));
		 //set hours to 13 when max is 12
		  driver.findElementByXPath(".//*[@id='utcOffsetHours']").clear();
		  ReusableFunc.EnterTextInTextBox(driver,driver.findElementByXPath(".//*[@id='utcOffsetHours']"), "13");
		  
	      //minutes
		  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[1]/div[3]/div[2]/div[3]/div/div/div/div/button[1]", "Should be 15");
		  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[1]/div[3]/div[2]/div[3]/div/div/div/div/button[1]", "Should be 30");
		  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[1]/div[3]/div[2]/div[3]/div/div/div/div/button[1]", "Should be 45");


		  //Save
		  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "Save");
		  Thread.sleep(1000);
		  
		  //Should warn that hours should be less than 12
		  ReusableFunc.VerifyTextOnPage(driver, "must be less than or equal to 12");
		  driver.findElementByXPath(".//*[@id='utcOffsetHours']").clear();
		  ReusableFunc.EnterTextInTextBox(driver,driver.findElementByXPath(".//*[@id='utcOffsetHours']"), "12");
		  
		  
		  //Save
		  ReusableFunc.ClickByXpath(driver, " .//*[@id='form']/div[1]/div[2]/div/button", "Save");
	}
		
	@Test(dependsOnMethods = { "addTimezoneRandom" })
	public void deleteTimezone() throws Exception {
		navigateBackToTimezonesPage();
		ReusableFunc.VerifyTextOnPage(driver, "1Australian Western Standard Time");
		// delete from Timezone edit page
		ReusableFunc.ClickByXpath(driver,".//*[@id='timezones-list']/tbody/tr[1]/td[2]/a[1]","Edit button");
		ReusableFunc.VerifyTextOnPage(driver, "AWST");
		ReusableFunc.ClickByLinkText(driver,"Remove");
		Thread.sleep(1000);
		ReusableFunc.VerifyTextOnPage(driver, "Are you sure?");
		
		
		//confirm delete
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "Remove");
		/*String bodyText = driver.findElement(By.tagName("body")).getText();
		Assert.assertFalse(bodyText.contains("1Australian Western Standard Time"), "TimeZone was not deleted!");*/
		Thread.sleep(1000);

	}
	
		
	@Test(dependsOnMethods = { "deleteTimezone" })
	public void deleteTimezone2() throws Exception {
		navigateBackToTimezonesPage();
		ReusableFunc.VerifyTextOnPage(driver, "1Eastern Standard Time");
		Thread.sleep(1000);
		// delete from Time zones listing page
		ReusableFunc.ClickByXpath(driver,".//*[@id='timezones-list']/tbody/tr[1]/td[2]/a[2]","First should be 1Eastern");
		Thread.sleep(1000);
		ReusableFunc.VerifyTextOnPage(driver, "Are you sure?");
		
		//cancel delete
		ReusableFunc.ClickByLinkText(driver, "Cancel");
		
		// delete from products listing page
		Thread.sleep(1000);
		ReusableFunc.ClickByXpath(driver,".//*[@id='timezones-list']/tbody/tr[1]/td[2]/a[2]","First should be 1Eastern");
		Thread.sleep(1000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "Remove");
		/*String bodyText1 = driver.findElement(By.tagName("body")).getText();
		Assert.assertFalse(bodyText1.contains("1Eastern Standard Time"), "Timezone was not deleted!");*/
		Thread.sleep(1000);
		System.out.println("Time Zone test completed");
	}
	
	
	
	@Test(dependsOnMethods = { "deleteTimezone2" })
	public void deleteRandomTimezone() throws Exception {
		navigateBackToTimezonesPage();
		// delete from Time zones listing page
		ReusableFunc.ClickByXpath(driver,".//*[@id='timezones-list']/tbody/tr[2]/td[2]/a[2]","should be random");
		Thread.sleep(1000);
		ReusableFunc.VerifyTextOnPage(driver, "Are you sure?");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "Remove");
		String bodyText1 = driver.findElement(By.tagName("body")).getText();
		Thread.sleep(1000);
	}
	
	private void navigateToTimezonesPage() throws Exception {
		ReusableFunc.ClickByLinkText(driver, "Global Settings");
		Thread.sleep(2000);
		ReusableFunc.ClickByLinkText(driver, "Time Zones");
		Thread.sleep(2000);
	}
	
	private void navigateBackToTimezonesPage() throws Exception {
		ReusableFunc.ClickByLinkText(driver, "Global Settings");
		navigateToTimezonesPage();
	}
	


	
}

	

