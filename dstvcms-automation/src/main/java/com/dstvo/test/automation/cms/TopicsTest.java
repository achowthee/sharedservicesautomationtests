package com.dstvo.test.automation.cms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class TopicsTest extends TestBase{
		
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}
	
	
  
  @Test
  public void CreateNewTopic() throws Exception {
	  ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
	  System.out.println("Starting Topics test");
	  Thread.sleep(2000);
	  //Click Content
	  ReusableFunc.ClickByLinkText(driver, "Content" );
	  //Click Topics
	  ReusableFunc.ClickByLinkText(driver, "Topics");
	  System.out.println("On topics list page");
	  
	  //Click on top Add Topic and cancel
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div[1]/a", "Top Add topic");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[1]/a/i", "Click on bottom cancel");
	  
	  //Now capturing an Topic using the bottom Add topic
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div[3]/div/a[1]", "Bottom Add topic");
	  System.out.println("You are on the Add topic page");
	  Utilities.fluentWait(By.xpath(".//*[@id='form']/div[1]/div[2]/div[1]/button"), driver);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[1]/button", "Top save");
	  System.out.println("You are trying to save a blank topic");
	  Utilities.fluentWait(By.xpath(".//*[@id='form']/div[1]/div[1]/div[1]/strong[2]"), driver);
	  //Thread.sleep(1000);
	  //ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='form']/div[1]/div[1]/div[1]"), "Error! There were 5 errors when trying to save your Topic. Please review the Topic below and make sure you have completed all mandatory fields.");
	  
	  //Enter HeadLine and short blurb
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("headline"), "FaatimasTopicTest");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("shortBlurb"), "TestingTestingShortBlurb");
	  System.out.println("HeadLine and short blurb entered");
      Thread.sleep(2000);
      
	  //Enter body
	  //ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath("html/body/p"), "Automation test Body being entered");
      ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='slug']"), "Slug");
      ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='permalink']"), "Permalink");
	  Thread.sleep(2000);
      
	  
	//Related Items linking
	  //Typing a URL
	  //ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='billboard_image_relateditem-body']/div[2]/div/a"), "check the related items button is there");
	  //ReusableFunc.verifyElementIsDisplayed(driver, By.id("url-link-clear-button"), "Verify related items button is gone and clear is displayed");
	  //ReusableFunc.ClickByID(driver, "url-link-clear-button67d368e2_a70a_4e52_9e11_9772dc9620dd_");
	  //ReusableFunc.verifyElementIsDisplayed(driver, By.xpath(".//*[@id='billboard_image_relateditem-body']/div[2]/div/a"), "check the related items button is there");
	  
	  //Adding a related item
	  //ReusableFunc.ClickByXpath(driver, ".//*[@id='related_item_198d9e1c_be2a_4275_9ddc_9324ec088445__relateditem-body']/div[2]/div/div[1]/a/i", "Boo");
	  //Thread.sleep(2000);
	  //System.out.println("Related item button clicked");
	  
	  //ReusableFunc.ClickByXpath(driver, ".//*[@id='s2id_related_item_335430d7_d6ec_4e4c_8c70_bcdf6de76173__relateditem-product-filter']/a/span[2]/b", "Product Dropdown");
	  //ReusableFunc.ClickByXpath(driver, ".//*[@id='select2-drop']/ul/li[8]/div","Select Product"); 
	  
	  /*ReusableFunc.ClickByXpath(driver, ".//*[@id='s2id_related_item_67d368e2_a70a_4e52_9e11_9772dc9620dd__relateditem-product-filter']/a/span[2]/b", "Select product dropdown");
	  Utilities.fluentWait(By.xpath(".//*[@id='select2-drop']/div/input"), driver);
	  ReusableFunc.clearTextBox(driver, By.xpath(".//*[@id='select2-drop']/div/input"));
	  Thread.sleep(10000);
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='select2-drop']/div/input"), "QA Automation");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='select2-drop']/ul/li[2]/div", "Selecting QA Automation");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='related_item_67d368e2_a70a_4e52_9e11_9772dc9620dd__related-items-search-results_filter']/label/input"), "Regression test article");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='related_item_67d368e2_a70a_4e52_9e11_9772dc9620dd__related-items-search-results']/tbody/tr[1]/td[9]/input", "Selecting Regression test article");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='-related_item_67d368e2_a70a_4e52_9e11_9772dc9620dd__link-relateditem']/div[3]/button[1]", "Click save");
	  */
	  
	  
	  
	  //Search for image (16x9) and cancel
      ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.id("toentitymages_topic-16x9_link_image_asset_search_")),"log");
	  System.out.println("Typed in an image to search for");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='toentitymages_topic-16x9_link_image_asset_search']/div[2]/a", "search");
	  System.out.println("Clicked search button to search for image");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='image_asset_search_form']/tbody/tr[1]/td[1]/a/i", "select A pic");
	  Thread.sleep(2000);
	  System.out.println("Selected image pop-up opens");
	  //Click cancel button
	  ReusableFunc.ClickByID(driver, "edit-image-cancel-button");
	  Thread.sleep(2000);
	  System.out.println("Back on ass topic page");
	  Thread.sleep(2000);
	  
	  
	  //Relink an image and save it
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.id("entity_images_topic-16x9_image_asset_search_")),"log");
	  System.out.println("Enter image name to search again");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='entity_images_topic-16x9_image_asset_search']/div[2]/a", "search");
	  System.out.println("Clicked search button");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='image_asset_search_form']/tbody/tr[1]/td[1]/a/i", "select A pic");
	  Thread.sleep(7000);
	  ReusableFunc.ClickByID(driver, "auto_crop_save_topic_16x9");
	  System.out.println("Image saved and shows on add topic page");
	  //Ensure the cropping options are available
	  //ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/div[2]/div/div/div[2]/div[1]/a", "Click on the Crop heading");
	  //ReusableFunc.verifyIdElementIsDisplayed(driver, driver.findElementById("crop_commands_article_16x9"));
	  Thread.sleep(5000);
	  
	  
	  //Add editorial comments
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("commentDescriptionValue"), "Editorial comment, entering text right now");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='comments']/div[2]/div[2]/a", "Add Comment");
	  System.out.println("Editorial comment1 done");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("commentDescriptionValue"), "Editorial comment, entering text again");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='comments']/div[2]/div[2]/a", "Add Comment");
	  System.out.println("Editorial comment2 done");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='commentClone']/div[3]/a/i", "Delete 2nd comment");
	  System.out.println("Deleted a selected editorial comment");
	  
	  
	  
	  //Enter Tags
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='form']/div[1]/div[1]/div[8]/div/div/div[2]/div/div/div/input[2]"), "Automation topic tag being entered");
	  System.out.println("Automation Tag entered");
	
	  //SEO
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("seoTitle"), "Automation topic SEO Title");
	  System.out.println("SEO Title entered");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("seoDescription"), "Automation topic SEO Description");
	  System.out.println("SEO description entered");
	  Thread.sleep(2000);
	  //ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='form']/div[1]/div[1]/div[8]/div/div/div[2]/div/div[3]/div/input[2]"), "KeywordTag");
	  //System.out.println("SEO Keywords entered");
	  
	  //Social Networks
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagsog_title"), "Automation topic SocialMedia Tags title");
	  System.out.println("Social networks title entered");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagsog_description"), "Automation topic SocialMedia Tags Description");
	  System.out.println("Social networks description entered");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagsog_url"), "Automation topic SocialMedia URL");
	  System.out.println("Social networks url entered");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagsog_image"), "Automation topic SocialMedia Image");
	  System.out.println("Social networks image entered");
	  Thread.sleep(1000);
	  
	  
	  //Meta Cards
	  //Summary Card Type
	  //ReusableFunc.ClickByXpath(driver, ".//*[@id='socialMediaTagstwitter_card_chosen']/a/div/b", "Card Type dropdown");
	  //ReusableFunc.ClickByXpath(driver, ".//*[@id='socialMediaTagstwitter_card_chosen']/div/ul/li[3]", "Card Type dropdown");
	  //or
	  //ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='socialMediaTagstwitter_card_chosen']/a/div/b"), By.xpath(".//*[@id='socialMediaTagstwitter_card_chosen']/div/ul/li[2]"));
	  //System.out.println("Card Type selected");
	  //Thread.sleep(3000);
	  
	  
	  
	  //Title and other text
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagstwitter_title"), "Automation topic Twitter title");
	  System.out.println("Twitter Title");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagstwitter_description"), "Automation topic Twitter desc");
	  System.out.println("Twitter Description");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagstwitter_url"), "Automation topic SocialMedia Twitter URL");
	  System.out.println("Twitter URL");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagstwitter_image"), "Automation topic SocialMedia Twitter Image");
	  System.out.println("Twitter image");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagstwitter_image_src"), "Automation topic SocialMedia Twitter large");
	  System.out.println("Twitter large image");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagstwitter_site"), "Automation topic SocialMedia twitter site");
	  System.out.println("Twitter site");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagstwitter_creator"), "Automation topic SocialMedia twitter creator");
	  System.out.println("Twitter Creator");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("socialMediaTagsfb_appid"), "Automation topic SocialMedia Fb AppID");
	  System.out.println("Network Id facebook app Id");
	  //ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='form']/div[1]/div[1]/div[9]/div/div/div[2]/div/div[15]/div/input[2]"), "One");
	  //System.out.println("Admin Tags entered");
	  
	  //Select a status
	  ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='status_chosen']/a/div/b"), By.xpath(".//*[@id='status_chosen']/div/ul/li[2]"));
	  System.out.println("Status selected");
	  Thread.sleep(3000);
	  
	  //Select a publisher
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='s2id_publisher']/a", "Click on Organization dropdown");
	  Utilities.fluentWait(By.id("select2-drop"), driver);
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='select2-drop']/div/input"), "Au");
	  System.out.println("Here now");
	  Utilities.fluentWait(By.xpath(".//*[@id='select2-drop']/ul/li/div"), driver);
	  System.out.println("Here now2");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='select2-drop']/ul/li/div", "Selecting the automation organization");
	  System.out.println("Publisher selected");
	  Thread.sleep(3000);
	  
	  //Select an author
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='s2id_author']/a/span[2]/b", "Click on Person dropdown");
	  Utilities.fluentWait(By.id("select2-drop"), driver);
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='select2-drop']/div/input"), "Au");
	  System.out.println("Here now 3");
	  Utilities.fluentWait(By.xpath(".//*[@id='select2-drop']/ul/li[1]/div"), driver);
	  System.out.println("Here now 4");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='select2-drop']/ul/li[1]/div", "Selecting the automation person");
	  System.out.println("Author selected");
	  Thread.sleep(3000);
	  
	 //Select Publish date
	  //ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("publishDate"), "2015-04-20");
	  //System.out.println("Publish Date entered");
	  //ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[2]/div[5]/div/div[1]/span", "Publish Date dropdown appears");
	  //ReusableFunc.ClickByXpath(driver, "html/body/div[16]/div[1]/table/tbody/tr[5]/td[5]", "Publish Date dropdown appears");
	 
	  //Select Publish Time
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("publishTime"), "10:00:00");
	  System.out.println("Publish Time entered");
	  Thread.sleep(1000);

	  //Select Expiry Date
	  //ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("expiryDate"), "2017-04-20");
	  //System.out.println("Expiry Date entered");
	  //Thread.sleep(2000);
	  
	  //Select Expiry Time
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("expiryTime"), "10:00:00");
	  System.out.println("Expiry Time entered");
	  Thread.sleep(4000);
	  
	  //ReusableFunc.ClickDropdownElement(driver, By.xpath("publishDate"), By.xpath("html/body/div[5]/div[1]/table/tbody/tr[5]/td[5]"));
	  //System.out.println("Publish Date selected");
	  //Thread.sleep(3000);
	  //ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[2]/div[5]/div/div[1]/span", "Click on Person dropdown");
	  //Utilities.fluentWait(By.id("select2-drop"), driver);
	  //ReusableFunc.ClickByXpath(driver, "html/body/div[5]/div[1]/table/tbody/tr[5]/td[5]", "Selecting the automation person");
	  
	  //Publish on Select and deselect
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[2]/div[7]/div[1]/label", "selecting the 1st Publish on");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[2]/div[7]/div[2]/label", "selecting the 2nd Publish on");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[2]/div[7]/div[3]/label", "selecting the 3rd Publish on");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[2]/div[7]/div[4]/label", "selecting the 4th Publish on");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[2]/div[7]/div[3]/label", "unselecting the 3rd Publish on");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[2]/div[7]/div[4]/label", "unselecting the 4th Publish on");
	  System.out.println("Published on platforms selected");
	  
	  //Selecting a Language
	  ReusableFunc.ClickByID(driver, "language_chosen");
	  Utilities.fluentWait(By.xpath(".//*[@id='language_chosen']/div/div/input"), driver);
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='language_chosen']/div/div/input"), "aut");
	  Utilities.fluentWait(By.xpath(".//*[@id='language_chosen']/div/ul/li[1]"), driver);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='language_chosen']/div/ul/li[1]", "Selecting AutoLang");
	  System.out.println("Language selected");
	  
	  //Enable/Disable comments
	  //ReusableFunc.ClickByID(driver, "enableComments1");
	  //ReusableFunc.ClickByXpath(driver, ".//*[@id='enableComments1']", "enable comments");
	  //ReusableFunc.ClickByXpath(driver, ".//*[@id='enableComments1']", "disable comments");
	  
	  //select a product
	  ReusableFunc.ClickByID(driver, "product_select_chosen");
	  Utilities.fluentWait(By.xpath(".//*[@id='product_select_chosen']/ul/li/input"), driver);
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='product_select_chosen']/ul/li/input"), "QA");
	  Thread.sleep(2000);
	  ReusableFunc.ClickDropdownElement(driver, By.id("product_select_chosen"), By.xpath(".//*[@id='product_select_chosen']/div/ul/li"));
	  System.out.println("Product1 has been selected");
	  
	  //select the second product
	  ReusableFunc.ClickByID(driver, "product_select_chosen");
	  Utilities.fluentWait(By.xpath(".//*[@id='product_select_chosen']/ul/li/input"), driver);
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='product_select_chosen']/ul/li/input"), "qa");
	  Thread.sleep(2000);
	  ReusableFunc.ClickDropdownElement(driver, By.id("product_select_chosen"), By.xpath(".//*[@id='product_select_chosen']/div/ul/li[2]"));
	  System.out.println("Product2 has been selected");
	  Thread.sleep(1000);
	  
	  //Select 3rd product 
	  ReusableFunc.ClickByID(driver, "product_select_chosen");
	  Utilities.fluentWait(By.xpath(".//*[@id='product_select_chosen']/ul/li/input"), driver);
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='product_select_chosen']/ul/li/input"), "bl");
	  Thread.sleep(2000);
	  ReusableFunc.ClickDropdownElement(driver, By.id("product_select_chosen"), By.xpath(".//*[@id='product_select_chosen']/div/ul/li[1]"));
	  System.out.println("Product3 has been selected");
	  Thread.sleep(1000);
	  
	  //Delete 3rd Product
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='product_select_chosen']/ul/li[3]/a", "Delete A selected product");
	  
	  
	  //Select a category
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='category_select']/div[1]/a", "Select Category");
	  Thread.sleep(2000);
	  //Select 1st Category
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='category_select-topic_category_category_9eb6a8fa-76b0-41f8-89e9-7b0eaf553676']/label[1]/label", "Enable");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='category_select-topic_category_category_9eb6a8fa-76b0-41f8-89e9-7b0eaf553676']/label[2]/label", "Disable");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='category_select-topic_category_category_9eb6a8fa-76b0-41f8-89e9-7b0eaf553676']/label[1]/label", "Enable");
	
	  //Select another Category and choose another sub category
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='category_select-topic_category_product_db7fe1e6-4fc3-4eef-87d1-aa4712afb3e3']/div/ul/li[2]/a", "Select Mobile cateogry");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='category_select-topic_category_category_2b2c8b6f-c188-4fc2-8878-8754ad9f305c']/label[2]/label", "Select Entertainment");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='category_select-topic_category']/div[3]/button", "Save");
	  System.out.println("Categories and sub categories selected");
	  Thread.sleep(3000);
	  
	  //Link countries
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='country_select']/div[1]/a", "Click Link country");
	  Thread.sleep(1000);
	  System.out.println("working so far....");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='topic_country']/div[2]/div/ul/li[1]/a", "Click Global");
	  System.out.println("working so far1.1....");
	  //Select 3 countries in the 1st region
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='topic_country_regions_product_']/div[5]/div[1]/a", "Some countries");
	  Thread.sleep(1000);
	  System.out.println("working so far2....");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='topic_country_global_check_group_5d69f827-6325-420f-9e9d-39d9484db398']/label[1]/label", "1st country");
	  System.out.println("working so far3....");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='topic_country_global_check_group_5d69f827-6325-420f-9e9d-39d9484db398']/label[2]/label", "4th country");
	  //Select another country from the a different region
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='topic_country_regions_product_']/div[4]/div[1]/a", "Click Southern African");
	  Thread.sleep(1000);
	  System.out.println("working so far4....");
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='topic_country_global_check_group_1c2adc79-36cc-4655-9c3a-961138307d18']/label[2]/label", "2nd country");
	  Thread.sleep(2000);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='topic_country_global_check_group_1c2adc79-36cc-4655-9c3a-961138307d18']/label[1]/label", "3rd country");
	  Thread.sleep(2000);
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='topic_country']/div[3]/button", "Click Save");
	  Thread.sleep(2000);
	  System.out.println("Countries have been selected");
	  Thread.sleep(2000);
	  
	  //Save topic
	  Utilities.fluentWait(By.xpath(".//*[@id='form']/div[1]/div[1]/div[1]/strong[2]"), driver);
	  ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[1]/div[2]/div[1]/button", "Save");
	  Thread.sleep(1000);
	  System.out.println("Your topic has been successfully saved");
	  //Thread.sleep(5000);
	  //ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("topicListTableId_filter"),"faatimastopictest" );
	  //Thread.sleep(2000);
	  //ReusableFunc.VerifyTextOnPage(driver, "faatimastopictest");
  }
  
  /*@Test
  public void EditArtcile() throws Exception {
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='articles-list_filter']/label/input"), "Automation");
  }*/
  
}
  
	  