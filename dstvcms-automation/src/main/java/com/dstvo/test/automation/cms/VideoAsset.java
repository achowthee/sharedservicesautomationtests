package com.dstvo.test.automation.cms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;
import com.thoughtworks.selenium.Wait;

public class VideoAsset extends TestBase{
	
	RandomString rs = new RandomString(5);
//	String name = "Test"+ rs.nextString();
	String name = "TestVideoSlugName";
    String editedSlug = name + "Edited";
	String filePath = rs.nextString();
	
	
	
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}
	@Test (priority = -10)
	public void videoAssetHomePage() throws Exception {
		System.out.println("Starting...Video Asset test"); 
		ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
		Thread.sleep(2000);
		 
		 //Click Global settings first, then Age Restrictions
		 navigateToVideoAssetPage();
		 ReusableFunc.VerifyTextOnPage(driver, "Video Assets");
		 Thread.sleep(1000);
	     ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("searchSlug"), name);
		 Thread.sleep(2000);
		 String actualText = driver.findElementByXPath(".//*[@id='video_Asset_List_form']/tbody/tr/td[1]").getText();
//		 System.out.println("Search Results: " + actualText);
		// If so, delete it
		 if (actualText.contains(name)) {
			ReusableFunc.ClickByXpath(driver,".//*[@id='video_Asset_List_form']/tbody/tr[1]/td[4]/a[2]/i", "Delete Button");
			confirmDelete(name);
		}
	}
  
	@Test (priority = -9, dependsOnMethods = { "videoAssetHomePage" })
	  public void CancelEntry() throws Exception{
		  navigateBackToVideoAssetPage();
		  Thread.sleep(1000);
		  ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div[1]/div/div[1]/div/a", "Click Add Video Asset");
		  String toBeCancedAssert = name + "TobeCanceled";
		  //Insert data into all the fields
		  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("name"), toBeCancedAssert); //slug
		  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("manItemId"), "Video Asset Automation MAN ID"); //MAN Item
		  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("filePath"), filePath); //File path
		  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("fileSizeInKiloBytes"), "50000000000000 "); //File size
		  ReusableFunc.ClickDropdownElement(driver, By.id("programType_chosen"), By.xpath(".//*[@id='programType_chosen']/div/ul/li[3]")); //Program type
		  ReusableFunc.ClickDropdownElement(driver, By.id("fileServer_chosen"), By.xpath(".//*[@id='fileServer_chosen']/div/ul/li[3]")); //File server
		  ReusableFunc.ClickDropdownElement(driver, By.id("fileType_chosen"), By.xpath(".//*[@id='fileType_chosen']/div/ul/li[3]")); //File type
		  ReusableFunc.ClickDropdownElement(driver, By.id("aspectRatio_chosen"), By.xpath(".//*[@id='aspectRatio_chosen']/div/ul/li[3]")); //Aspect ratio
		  
		  ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/a", "Cancel Save"); //cancel 
		  //Ensure that Video Asset was not created and is on the listing page
		  Thread.sleep(2000);
		  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("searchSlug"), toBeCancedAssert);
			
		  String bodyText = driver.findElement(By.tagName("body")).getText();
		  Assert.assertFalse(bodyText.contains(toBeCancedAssert), "Cancel entry failed");
		  Thread.sleep(2000);
		  
	  }
	@Test (priority = -8, dependsOnMethods = { "videoAssetHomePage" })
	public void NewVideoAssetBlankEntry() throws Exception {
		// Search If video assert already Exist
		navigateBackToVideoAssetPage();
		ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div[1]/div/div[1]/div/a", "Click Add Video Asset");
		//Save without any input to bring up all the errors
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/button", "Click on the top save");
		Thread.sleep(3000);
		//Verifying that all the error messages are displayed
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='form']/div[1]/div[1]/div[1]/div/label"), "Slug may not be empty");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='form']/div[1]/div[1]/div[3]/div/label"), "Program type may not be null");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='form']/div[1]/div[1]/div[4]/div/label"), "File server may not be empty");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='form']/div[1]/div[1]/div[5]/div/label"), "File path may not be empty");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='form']/div[1]/div[1]/div[6]/div/label"), "Platforms encoded for may not be null");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='form']/div[1]/div[3]/div/div/div[2]/div/div[3]/label"), "Aspect Ratio may not be null");
	}
	@Test (priority = -7, dependsOnMethods = { "videoAssetHomePage" })
	public void NewVideoAsset() throws Exception {

	  //Insert data into all the fields
	  navigateBackToVideoAssetPage();
	  ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div[1]/div/div[1]/div/a", "Click Add Video Asset");
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("name"), name); //slug
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("manItemId"), "KN_kwela_20141126_E750_ST_PVOD.mpg"); //MAN Item
	  ReusableFunc.ClickDropdownElement(driver, By.id("programType_chosen"), By.xpath(".//*[@id='programType_chosen']/div/ul/li[3]")); //Program type
	  ReusableFunc.ClickDropdownElement(driver, By.id("fileServer_chosen"), By.xpath(".//*[@id='fileServer_chosen']/div/ul/li[10]")); //File server
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("filePath"), "KN_kwela_20141126_E750_ST_PVOD.mpg"); //File path
	  ReusableFunc.ClickDropdownElement(driver, By.id("encodedForPlatforms_chosen"), By.xpath(".//*[@id='encodedForPlatforms_chosen']/div/ul/li[1]"));
	  
	  ReusableFunc.ClickDropdownElement(driver, By.id("fileType_chosen"), By.xpath(".//*[@id='fileType_chosen']/div/ul/li[3]")); //File type
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("fileSizeInKiloBytes"), "50000000000000 "); //File size
	  ReusableFunc.ClickDropdownElement(driver, By.id("aspectRatio_chosen"), By.xpath(".//*[@id='aspectRatio_chosen']/div/ul/li[3]")); //Aspect ratio
	  Thread.sleep(2000);
	  saveUsingBottomButton(name);
  }
 
	@Test (priority = -6, dependsOnMethods = { "NewVideoAsset" })
  public void EditVideoAsset() throws Exception{
	  navigateBackToVideoAssetPage();
	  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("searchSlug"), name);
	  Thread.sleep(2000);
	  ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='video_Asset_List_form']/tbody/tr[1]/td[1]"), name);
	  ReusableFunc.ClickByXpath(driver,".//*[@id='video_Asset_List_form']/tbody/tr/td[4]/a[1]/i", "Edit Button");
	  //ReusableFunc.ClickByCss(driver, ".icon-edit.bigger-120");
	  Thread.sleep(3000);
	  ReusableFunc.verifyTextInWebElement(driver, By.id("page_sub_title"), name);
	  //ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("name"), editedSlug); //update the slug 
	  saveUsingTopRightButton(editedSlug);
	}
  
    //Unable to Delete
	@Test (priority = -5, dependsOnMethods = { "EditVideoAsset" })
	//@Test (dependsOnMethods = { "videoAssetHomePage" })
	public void DeleteVideoAsset() throws Exception{
		 navigateBackToVideoAssetPage();
		 Thread.sleep(2000);
		 ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("searchSlug"), editedSlug);
		 Thread.sleep(2000);
		 String actualText = driver.findElementByXPath(".//*[@id='video_Asset_List_form']/tbody/tr/td[1]").getText();
		 //System.out.println("Search Results: " + actualText);
		// If so, delete it
		 if (actualText.contains(editedSlug)) {
			ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='video_Asset_List_form']/tbody/tr[1]/td[1]"), editedSlug);
			ReusableFunc.ClickByXpath(driver,".//*[@id='video_Asset_List_form']/tbody/tr[1]/td[4]/a[2]/i", "Delete Button");
			confirmDelete(editedSlug);
		}
		 
	    System.out.println("......Video asset tests Completed ");	
	 }

	  //componet error
	 /* @Test (dependsOnMethods = { "CancelEntry" })
	  public void FilterBy() throws Exception{
		  WebDriverWait wait = new WebDriverWait(driver, 18);
		  WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("programTypeFilter")));

		  //Filter By program Type
		  ReusableFunc.ClickDropdownElement(driver, By.id("programTypeFilter"), By.cssSelector("html body div.main-container div#main_content.main-content div#page_content.page-content div.row-fluid div.span12 div.rowfluid div.span12 div.row-fluid div#video_Asset_List_form_wrapper.dataTables_wrapper div.row-fluid div.span6 div.span3 select#programTypeFilter.chzn-select option"));
		  Thread.sleep(1000);
		  
		  //Filter By File Type
		  ReusableFunc.ClickDropdownElement(driver, By.id("assetTypeFilter"), By.cssSelector("html body div.main-container div#main_content.main-content div#page_content.page-content div.row-fluid div.span12 div.rowfluid div.span12 div.row-fluid div#video_Asset_List_form_wrapper.dataTables_wrapper div.row-fluid div.span6 div.span3 select#assetTypeFilter.chzn-select option"));
		  Thread.sleep(1000);
		  
	}*/
	private void navigateToVideoAssetPage() throws Exception {
		  ReusableFunc.ClickByLinkText(driver, "Media");
		  Thread.sleep(1000);
		  ReusableFunc.ClickByLinkText(driver, "Video Asset");	
		  Thread.sleep(2000);
	}
	
	private void navigateBackToVideoAssetPage() throws Exception {
		ReusableFunc.ClickByLinkText(driver, "Media");
		navigateToVideoAssetPage();
	}
	
	private void saveUsingBottomButton(String slug) throws Exception {
		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[2]/div/div/button", "save");
		Thread.sleep(2000);
		ConfirmSave(slug);
	}
	
	private void saveUsingTopRightButton(String slug) throws Exception {
		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[1]/div[2]/div/button", "save");
		Thread.sleep(2000);
		ConfirmSave(slug);
	}
	
	private void ConfirmSave(String slug) throws Exception {
		  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("searchSlug"), name);
		  Thread.sleep(2000);
		  ReusableFunc.VerifyTextOnPage(driver, name);
		
	}
	
	private void confirmDelete(String slug) throws Exception {
		 ReusableFunc.VerifyTextOnPage(driver,"Are you sure");
		 ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "delete");
		 Thread.sleep(2000);
		 ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("searchSlug"), slug);
		 Thread.sleep(2000);
		 ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='video_Asset_List_form']/tbody/tr[1]/td[1]"), "No data available in table");
		 Thread.sleep(1000);
	}
  
  
}
  
	  