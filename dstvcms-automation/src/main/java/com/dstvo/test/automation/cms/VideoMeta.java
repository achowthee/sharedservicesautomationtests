package com.dstvo.test.automation.cms;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.RandomString;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class VideoMeta extends TestBase{
	RandomString rs = new RandomString(50);
	String shortSynopsis = rs.nextString();
	String synopsis = rs.nextString();
	String videoMetaTitle = "VidoeMetaUATtest";
	String videoMetaTitleEdited = videoMetaTitle+ " Edited";
	
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}
	
	@Test(priority = -20)
	public void vidoeMataHomePage() throws Exception{
		System.out.println("Starting...Video Meta");
		ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
		Thread.sleep(2000);
		navigateToVidoeMataPage();
		Thread.sleep(2000);
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='page_content']/div[1]/h1"), "Video Meta List");
		Thread.sleep(1000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='searchTitle']"),videoMetaTitle);
		Thread.sleep(2000);
		String actualText;
		try {
			actualText = driver.findElementByXPath(".//*[@id='video_Meta_List_form']/tbody/tr/td[2]").getText();
			//System.out.println("TRY " + actualText);
		} catch (Exception e) {
			actualText = driver.findElementByXPath(".//*[@id='video_Meta_List_form']/tbody/tr/td").getText();
			//System.out.println("Catch " + actualText);
		}
		 
		 //System.out.println("Search Results: " + actualText);
		// If so, delete it
		 if (actualText.contains(videoMetaTitle)) {
			ReusableFunc.ClickByXpath(driver,".//*[@id='video_Meta_List_form']/tbody/tr/td[6]/a[2]", "Delete Button");
			confirmDelete(videoMetaTitle);
		}
	}
	
	@Test(priority = -19, dependsOnMethods={"vidoeMataHomePage"})
	public void cancelVidoeAddVideoMeta() throws Exception{
		navigateBackToVidoeMataPage();
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[1]/div/a","add program (bottom +add)");
		ReusableFunc.waitForElementToDisplay(driver, By.id("title"), 5);
		ReusableFunc.VerifyTextOnPage(driver, "Crew");
		String toBeCanceled = videoMetaTitle + " 2bCanceled";
		// input program name
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='title']")),toBeCanceled);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='video-meta-form']/div[1]/div[2]/div[1]/a", "Cancel");
		Thread.sleep(2000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='searchTitle']"), toBeCanceled);
		Thread.sleep(2000);
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='video_Meta_List_form']/tbody/tr/td"), "No data available in table");
	}

	@Test(priority = -18, dependsOnMethods={"vidoeMataHomePage"})
	public void addNewVideoMetaBlankEntry() throws Exception {
		navigateBackToVidoeMataPage();
		ReusableFunc.ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[1]/div/div[2]/div/a","add program (top +add)");
		ReusableFunc.waitForElementToDisplay(driver, By.id("title"), 5);
		ReusableFunc.VerifyTextOnPage(driver, "Crew");
		//attempt to save blank entry
		ReusableFunc.ClickByXpath(driver, ".//*[@id='video-meta-form']/div[1]/div[2]/div[1]/button", "save blank");
		Thread.sleep(3000);
		ReusableFunc.VerifyTextOnPage(driver, "Error!");
		ReusableFunc.VerifyTextOnPage(driver, "may not be empty");
		
	}
	
	@Test(priority = -17, dependsOnMethods={"vidoeMataHomePage"})
	public void addNewVideoMeta() throws Exception {
		navigateBackToVidoeMataPage();
		ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div[1]/div/div[2]/div/a", "Click Add Video Meta");
		ReusableFunc.waitForElementToDisplay(driver, By.id("title"), 5);
		ReusableFunc.VerifyTextOnPage(driver, "Crew");
		//Insert data into all the fields
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("title"), videoMetaTitle); //Title
		
		
		//Add Crew
	    addCrew();
		//Create Crew
		createCrew();
		
		//Create New Program
		createNewProgram();
		//Select Program
		selectProgram();
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("episode"), "5");
		ReusableFunc.ClickDropdownElement(driver, By.id("seasonNumber"), By.xpath(".//*[@id='seasonNumber']/option[2]"));//Season
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("shortSynopsis"), shortSynopsis);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("synopsis"), synopsis);
		Thread.sleep(5000);
		
		//Year Of Release
		/** Not Clickable
		ReusableFunc.ClickByXpath(driver, ".//*[@id='yearOfRelease_chosen']/a","Year Of Release");
		Thread.sleep(1000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='yearOfRelease_chosen']/div/div/input"), "201");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='yearOfRelease_chosen']/div/ul/li[2]", "2014");
		*/
		//Duration
		duration();
		//Tags
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='video-meta-form']/div[1]/div[1]/div[12]/div/div/input[2]"), "AutoTag");
		//Studio
		ReusableFunc.ClickDropdownElement(driver, By.id("studio_chosen"), By.xpath(".//*[@id='studio_chosen']/div/ul/li[1]"));
		//Channels
		ReusableFunc.ClickByXpath(driver, ".//*[@id='s2id_channels-video-meta']/ul", "Channels");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("s2id_autogen2"), "Universal");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='select2-drop']/ul/li[1]/div", "Universal Chanel");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("s2id_autogen2"), "Vuz");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='select2-drop']/ul/li[1]/div", "VUZU Chanel");
		//Air Date & Time
		airDateAndTime();
		//Selecting the age restriction	
		ReusableFunc.ClickDropdownElement(driver, By.id("ageRestriction_chosen"), By.xpath(".//*[@id='ageRestriction_chosen']/div/ul/li[3]"));
		//Check all Advisory are selectable
		ReusableFunc.ClickByID(driver, "ratingAdvisoryList1");
		ReusableFunc.ClickByID(driver, "ratingAdvisoryList2");
		ReusableFunc.ClickByID(driver, "ratingAdvisoryList3");
		ReusableFunc.ClickByID(driver, "ratingAdvisoryList4");
		ReusableFunc.ClickByID(driver, "ratingAdvisoryList5");
		ReusableFunc.ClickByID(driver, "ratingAdvisoryList6");
		//Disable some
		ReusableFunc.ClickByID(driver, "ratingAdvisoryList1");
		ReusableFunc.ClickByID(driver, "ratingAdvisoryList4");
		ReusableFunc.ClickByID(driver, "ratingAdvisoryList3");
		//Audio language
		Thread.sleep(2000);
//		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='audioSelect_chosen']/a/span"), By.xpath(".//*[@id='audioSelect_chosen']/div/ul/li[1]"));
		//Subtitle language
		Thread.sleep(1000);
//		ReusableFunc.ClickDropdownElement(driver, By.id("subtitleLanguages_chosen"), By.xpath(".//*[@id='subtitleLanguages_chosen']/div/ul/li[1]"));
		//Country off origin
		Thread.sleep(1000);
		ReusableFunc.ClickDropdownElement(driver, By.id("countryOfOrigin_chosen"), By.xpath(".//*[@id='countryOfOrigin_chosen']/div/ul/li[75]"));
		//Regions off appeal
		Thread.sleep(1000);
		ReusableFunc.ClickDropdownElement(driver, By.id("regionsOfAppeal_chosen"), By.xpath(".//*[@id='regionsOfAppeal_chosen']/div/ul/li[3]"));
		//Select product
		Thread.sleep(1000);
		ReusableFunc.ClickDropdownElement(driver, By.id("videoMeta_product_select_chosen"), By.xpath(".//*[@id='videoMeta_product_select_chosen']/div/ul/li[33]"));
		//Categories
		ReusableFunc.ClickByXpath(driver, ".//*[@id='videoMeta_category_select']/div[1]/a", "Select Category");
		ReusableFunc.VerifyTextOnPage(driver, "Select Categories");
		Thread.sleep(2000);
		//ReusableFunc.ClickByID(driver, "categorisations224", "sub category one.2");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='videoMeta_category_select-article_category']/div[3]/button", "save");
		//Image upload
		String searchItem = "Q";
		Thread.sleep(1000);
		imageUpload(searchItem);
		//Create Video Asset
		createVideoAssert();
		//Link video asset
		linkVideoAssert();
		//slug
		RandomString rs2 = new RandomString(4);
		RandomString rs5 = new RandomString(12);
	 	String slug="slugTitl-"+rs2.nextString()+"-4820-812f-" +rs5.nextString();
	 	ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("slug"), slug);
		//Scheduling
	 	scheduling();
		//SEO
	 	Thread.sleep(1000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("seoTitle"), videoMetaTitle);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("seoDescription"), "Discover a range of Microsoft books on Windows Server, Microsoft Azure");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("seoDescription"),videoMetaTitle);
		Thread.sleep(2000);
		//link Guide Event
		linkGuideEvent();
		//save
		Thread.sleep(3000);
		saveUsingTopRightButton(videoMetaTitle);
	}
	
	@Test (priority = -16, dependsOnMethods = { "addNewVideoMeta" })
	public void editVideoMeta() throws Exception{
		navigateBackToVidoeMataPage();
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("searchTitle"), videoMetaTitle);
		  Thread.sleep(3000);
		  ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='video_Meta_List_form']/tbody/tr/td[2]"), videoMetaTitle);
		  ReusableFunc.ClickByXpath(driver,".//*[@id='video_Meta_List_form']/tbody/tr/td[6]/a[1]", "Edit Button");
		  //ReusableFunc.ClickByCss(driver, ".icon-edit.bigger-120");
		  Thread.sleep(3000);
		  ReusableFunc.verifyTextInWebElement(driver, By.id("page_sub_title"), videoMetaTitle);
		  ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("title"), videoMetaTitleEdited); //update the slug 
		  saveUsingBottomButton(videoMetaTitleEdited);
	}
	
	@Test (priority = -15, dependsOnMethods = { "editVideoMeta" })
	public void deleteVideoMeta() throws Exception{
		navigateBackToVidoeMataPage();
		Thread.sleep(2000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='searchTitle']"),videoMetaTitle);
		Thread.sleep(2000);
		String actualText = driver.findElementByXPath(".//*[@id='video_Meta_List_form']/tbody/tr/td[2]").getText();
		System.out.println("Search Results: " + actualText);
		// If so, delete it
		if (actualText.contains(videoMetaTitleEdited)) {
			ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='video_Meta_List_form']/tbody/tr/td[2]"), videoMetaTitleEdited);
			ReusableFunc.ClickByXpath(driver,".//*[@id='video_Meta_List_form']/tbody/tr/td[6]/a[2]", "Delete Button");
			confirmDelete(videoMetaTitleEdited);
		}
		System.out.println("......Video Meta tests Completed");	
	}
	
	private void linkGuideEvent() throws Exception {
		// TODO Auto-generated method stub
		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='video_meta_epg_event_search-openEPGEvent']"), 15);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='video_meta_epg_event_search-openEPGEvent']", "link Guide Event button");
		//ReusableFunc.ClickByID(driver, "video_meta_epg_event_search-openEPGEvent", "link Guide Event button");
		ReusableFunc.verifyTextInWebElement(driver, By.id("video_meta_epg_event_search-openEPGEvent"), "Link Guide Event");
		Thread.sleep(1000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='video_meta_epg_event_search-link-guide-events']/div[3]/button[2]", "Cancel Link Guide Event");
		
	}

	private void scheduling() throws Exception {
		// TODO Auto-generated method stub
		ReusableFunc.ClickByXpath(driver, ".//*[@id='video-meta-form']/div[1]/div[2]/div[3]/div/div/div[2]/div/a", "add Schedule");
		ReusableFunc.ClickByID(driver, "videoPackage_chosen");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='videoPackage_chosen']/div/div/input"), "test of");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='videoPackage_chosen']/div/ul/li[1]", "Test Of VideoPackage");
		//Start Date & Time
		/*******************************************************************
		ReusableFunc.ClickByID(driver, "modalStartDate");
		for (int i = 0; i < 3; i++) {
			//ReusableFunc.ClickByXpath(driver,"html/body/div[9]/div[1]/table/thead/tr[1]/th[3]","Move Month Up");
			ReusableFunc.waitForElementToDisplay(driver, By.cssSelector(".icon-arrow-right"), 5);
			ReusableFunc.ClickByCss(driver, ".icon-arrow-right", "Move Month Up");
			ReusableFunc.ClickByCss(driver, ".next");
			Thread.sleep(2000);
		}
		ReusableFunc.ClickByXpath(driver, "html/body/div[9]/div[1]/table/tbody/tr[2]/td[2]", "Select Day");
		*/
		ReusableFunc.waitForElementToDisplay(driver, By.id("modalStartDate"), 10);
		ReusableFunc.ClickByID(driver, "modalStartDate", "modalStartDate");
		//current date format YYYY-MM-DD
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("modalStartDate"), "2015-06-16" );
		
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='schedule-create-form']/div[1]/div[2]/div/div[2]/span/i","schedule Start Time");
		for (int i = 0; i < 3; i++) {
			                             
			ReusableFunc.ClickByXpath(driver,".//*[@id='schedule-create-form']/div[1]/div[2]/div/div[2]/div/table/tbody/tr[1]/td[1]/a","Changing the hour Up");
		}
		ReusableFunc.ClickByXpath(driver, ".//*[@id='schedule-create-form']/div[1]/div[2]/div/div[2]/div/table/tbody/tr[3]/td[1]/a", "Changing the hour Down");
		
		for (int i = 0; i < 12; i++) {
			ReusableFunc.ClickByXpath(driver, ".//*[@id='schedule-create-form']/div[1]/div[2]/div/div[2]/div/table/tbody/tr[1]/td[3]/a", "Changing the minutes Up");
		}
		ReusableFunc.ClickByXpath(driver, ".//*[@id='schedule-create-form']/div[1]/div[2]/div/div[2]/div/table/tbody/tr[3]/td[3]/a", "Changing the minutes Down");
		
		for (int i = 0; i < 11; i++) {
			ReusableFunc.ClickByXpath(driver, ".//*[@id='schedule-create-form']/div[1]/div[2]/div/div[2]/div/table/tbody/tr[3]/td[5]/a", "Changing the seconds down");
		}
		ReusableFunc.ClickByXpath(driver, ".//*[@id='schedule-create-form']/div[1]/div[2]/div/div[2]/div/table/tbody/tr[1]/td[5]/a", "Changing the seconds up");
		/******************************************************************
		//Expiry Date & Time
		ReusableFunc.ClickByID(driver, "modalExpiryDate");
		for (int i = 0; i < 9; i++) {
			ReusableFunc.ClickByXpath(driver,"html/body/div[10]/div[1]/table/thead/tr[1]/th[3]","Move Month Up");
			Thread.sleep(2000);
		}
		ReusableFunc.ClickByXpath(driver, "html/body/div[10]/div[1]/table/tbody/tr[2]/td[2]", "Select Day");
		*/
		ReusableFunc.waitForElementToDisplay(driver, By.id("modalExpiryDate"), 10);
		ReusableFunc.ClickByID(driver, "modalExpiryDate", "modalExpiryDate");
		//current date format YYYY-MM-DD
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("modalExpiryDate"), "2016-06-16" );
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='schedule-create-form']/div[1]/div[3]/div/div[2]/span/i","schedule Expiry Time");
		for (int i = 0; i < 3; i++) {
			                             
			ReusableFunc.ClickByXpath(driver,".//*[@id='schedule-create-form']/div[1]/div[3]/div/div[2]/div/table/tbody/tr[1]/td[1]/a","Changing the hour Up");
		}
		ReusableFunc.ClickByXpath(driver, ".//*[@id='schedule-create-form']/div[1]/div[3]/div/div[2]/div/table/tbody/tr[3]/td[1]/a", "Changing the hour Down");
		
		for (int i = 0; i < 12; i++) {
			ReusableFunc.ClickByXpath(driver, ".//*[@id='schedule-create-form']/div[1]/div[3]/div/div[2]/div/table/tbody/tr[1]/td[3]/a", "Changing the minutes Up");
		}
		ReusableFunc.ClickByXpath(driver, ".//*[@id='schedule-create-form']/div[1]/div[3]/div/div[2]/div/table/tbody/tr[3]/td[3]/a", "Changing the minutes Down");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='schedule-create-form']/div[1]/div[3]/div/div[2]/div/table/tbody/tr[2]/td[5]/input"), "60");
		/*******************************************************************/
		
//		ReusableFunc.ClickByXpath(driver,".//*[@id='formIsLive1']" , "Video is live");
//		ReusableFunc.ClickByID(driver,"formIsSpecialHighlight1" , "Special Highlights");
		
		Thread.sleep(2000);
		ReusableFunc.ClickByID(driver, "regionAvailability_chosen", "Region Availability");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='regionAvailability_chosen']/div/div/input"), "Automation test");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='regionAvailability_chosen']/div/ul/li[1]", "Choose Automation test");
		

		ReusableFunc.ClickByID(driver, "countryBlackList_chosen", "Country BlackList");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='countryBlackList_chosen']/ul/li/input"), "Test C");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='countryBlackList_chosen']/div/ul/li[1]", "Test Country");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='countryBlackList_chosen']/ul/li/input"), "test M");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='countryBlackList_chosen']/div/ul/li[1]", "Test Multiple Languages");
		//Platforms
		for (int i = 0; i < 3; i++) {
			int row = i + 2;
			int col = row + 1;
			ReusableFunc.CheckCheckBox(driver,driver.findElementByXPath(".//*[@id='billboardListTableId']/tbody/tr["+row+"]/td[1]/input[1]"));
			ReusableFunc.CheckCheckBox(driver,driver.findElementByXPath(".//*[@id='billboardListTableId']/thead/tr/th["+col+"]/input"));
		}
		ReusableFunc.ClickByID(driver, "saveNewScheduleButtonId", "save New Schedule");
		
	}

	private void imageUpload(String searchItem) throws Exception {
		// TODO Auto-generated method stub
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("videometa_images_play-image_link_image_asset_search_"), searchItem);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='videometa_images_play-image_link_image_asset_search']/div[2]/a", "Clicking the search button");
		ReusableFunc.waitForElementToDisplay(driver, By.id("videometa_images_play-image_link_image_asset_search__modal"), 10); 
		ReusableFunc.ClickByXpath(driver, ".//*[@id='image_asset_search_form']/tbody/tr[1]/td[1]/a/i", "clicking to the link image button");
		Thread.sleep(5000);	
	}

	private void airDateAndTime() throws Exception{
		// TODO Auto-generated method stub
		ReusableFunc.ClickByID(driver, "formAirDate");
		for (int i = 0; i < 3; i++) {
			ReusableFunc.ClickByXpath(driver,"html/body/div[6]/div[1]/table/thead/tr[1]/th[3]","increment Month");
		}
		ReusableFunc.ClickByXpath(driver,"html/body/div[6]/div[1]/table/thead/tr[1]/th[1]","decrement Month");
		ReusableFunc.ClickByXpath(driver, "html/body/div[6]/div[1]/table/tbody/tr[4]/td[4]", "Select Day");
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='video-meta-form']/div[1]/div[1]/div[15]/div/div/div[2]/span","Air Time");
		for (int i = 0; i < 3; i++) {
			ReusableFunc.ClickByXpath(driver,".//*[@id='video-meta-form']/div[1]/div[1]/div[15]/div/div/div[2]/div/table/tbody/tr[1]/td[1]/a","Changing the hour Up");
		}
		ReusableFunc.ClickByXpath(driver, ".//*[@id='video-meta-form']/div[1]/div[1]/div[15]/div/div/div[2]/div/table/tbody/tr[3]/td[1]/a", "Changing the hour Down");
		
		for (int i = 0; i < 12; i++) {
			ReusableFunc.ClickByXpath(driver, ".//*[@id='video-meta-form']/div[1]/div[1]/div[15]/div/div/div[2]/div/table/tbody/tr[1]/td[3]/a", "Changing the minutes Up");
		}
		ReusableFunc.ClickByXpath(driver, ".//*[@id='video-meta-form']/div[1]/div[1]/div[15]/div/div/div[2]/div/table/tbody/tr[3]/td[3]/a", "Changing the minutes Down");
		
		for (int i = 0; i < 11; i++) {
			ReusableFunc.ClickByXpath(driver, ".//*[@id='video-meta-form']/div[1]/div[1]/div[15]/div/div/div[2]/div/table/tbody/tr[1]/td[5]/a", "Changing the seconds down");
		}
		ReusableFunc.ClickByXpath(driver, ".//*[@id='video-meta-form']/div[1]/div[1]/div[15]/div/div/div[2]/div/table/tbody/tr[3]/td[5]/a", "Changing the seconds up");
				
		
	}

	private void duration() throws Exception {
		// TODO Auto-generated method stub
		ReusableFunc.ClickByXpath(driver, ".//*[@id='video-meta-form']/div[1]/div[1]/div[11]/div/div/span", "Clicking on the duration clock");
		Thread.sleep(2000);
		for (int i = 0; i < 3; i++) {
			ReusableFunc.ClickByXpath(driver,".//*[@id='video-meta-form']/div[1]/div[1]/div[11]/div/div/div/table/tbody/tr[1]/td[1]/a","Changing the hour Up");
		}
		ReusableFunc.ClickByXpath(driver, ".//*[@id='video-meta-form']/div[1]/div[1]/div[11]/div/div/div/table/tbody/tr[3]/td[1]/a", "Changing the hour Down");
		for (int i = 0; i < 12; i++) {
			ReusableFunc.ClickByXpath(driver, ".//*[@id='video-meta-form']/div[1]/div[1]/div[11]/div/div/div/table/tbody/tr[1]/td[3]/a", "Changing the minutes Up");
		}
		ReusableFunc.ClickByXpath(driver, ".//*[@id='video-meta-form']/div[1]/div[1]/div[11]/div/div/div/table/tbody/tr[3]/td[3]/a", "Changing the minutes Down");
		for (int i = 0; i < 11; i++) {
			ReusableFunc.ClickByXpath(driver, ".//*[@id='video-meta-form']/div[1]/div[1]/div[11]/div/div/div/table/tbody/tr[3]/td[5]/a", "Changing the seconds down");
		}
		ReusableFunc.ClickByXpath(driver, ".//*[@id='video-meta-form']/div[1]/div[1]/div[11]/div/div/div/table/tbody/tr[1]/td[5]/a", "Changing the seconds up");
			
		
	}

	private void addCrew() throws Exception {
		// TODO Auto-generated method stub
		ReusableFunc.waitForElementToDisplay(driver, By.id("addCrew"), 5);
		ReusableFunc.ClickByID(driver, "addCrew");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='s2id_participants2.role']/a", "Role dropdown Field");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='select2-drop']/div/input"), "Wri");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='select2-drop']/ul/li/div", "Role Search Results");
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='s2id_participants2.person']/a", "Person dropdown Field");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='select2-drop']/div/input"), "Automation");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='select2-drop']/ul/li/div", "Role Search Results");
		Thread.sleep(2000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("participants2.propertiesNote"), "I hate this part of the test :(");
	}

	private void createCrew() throws Exception {
		// TODO Auto-generated method stub
		Thread.sleep(5000);
		ReusableFunc.waitForElementToBeClickable(driver, By.id("create-new-crew-member"), 10);
		ReusableFunc.ClickByID(driver, "create-new-crew-member");
		ReusableFunc.waitForElementToDisplay(driver, By.id("create-new-crew-member-modal"), 5);
		ReusableFunc.verifyElementIsDisplayed(driver, By.id("create-new-crew-member-modal"), "create-new-crew-member-modal");
		Thread.sleep(3000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/a", "Cancel Button");
		Thread.sleep(2000);
		
	}

	private void selectProgram() throws Exception {
		// TODO Auto-generated method stub
		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='s2id_program']/a/span[1]"), 5);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='s2id_program']/a/span[1]", "clicking on prgram");
		Thread.sleep(2000);
		Utilities.fluentWait(By.xpath(".//*[@id='select2-drop']/div/input"), driver);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='select2-drop']/div/input"), "UAT Pro");
		Thread.sleep(2000);
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[@id='select2-drop']/div/input"), By.xpath(".//*[@id='select2-drop']/ul/li/div"));
		
		
		
		
	}

	private void createNewProgram() throws Exception{
		// TODO Auto-generated method stub
		ReusableFunc.ClickByID(driver, "create-new-program");
		ReusableFunc.waitForElementToDisplay(driver, By.id("create-new-program-modal"), 5);
		ReusableFunc.verifyElementIsDisplayed(driver, By.id("create-new-program-modal"), "create-new-program-moda");
		Thread.sleep(3000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div[1]/a", "Cancel Button");
		Thread.sleep(2000);
		
	}

	private void createVideoAssert() throws Exception {
		// TODO Auto-generated method stub
		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='video-meta-form']/div[1]/div[1]/div[25]/div/div/div[2]/div/div[2]/div/a[1]"), 5);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='video-meta-form']/div[1]/div[1]/div[25]/div/div/div[2]/div/div[2]/div/a[1]", "Create New Vodeo Asset Button");;
		ReusableFunc.waitForElementToDisplay(driver, By.id("create-new-video-asset-modal"), 5);
		ReusableFunc.VerifyTextOnPage(driver, "Video Asset");
		ReusableFunc.VerifyTextOnPage(driver, "File server");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='video-asset-form']/div[1]/div[2]/div/a", "Cancel Button");
		Thread.sleep(5000);
		
	}

	private void linkVideoAssert() throws Exception {
		// TODO Auto-generated method stub
		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='video-meta-form']/div[1]/div[1]/div[25]/div/div/div[2]/div/div[2]/div/a[2]"), 5);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='video-meta-form']/div[1]/div[1]/div[25]/div/div/div[2]/div/div[2]/div/a[2]", "Link Video Assert");
		ReusableFunc.waitForElementToDisplay(driver, By.id("add-existing-video-asset-modal"), 5);
		Thread.sleep(2000);
		//ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='existing-video-assets-modal-table_filter']/label/input"), "zabalaza");
		Thread.sleep(3000);
		//Clinking Radio Buttons Fails
		/**
		for (int i = 1; i <= 5; i++) {
			//Methos 1 Failed
			WebElement id1Location = driver.findElement(By.xpath(".//*[@id='existing-video-assets-modal-table']/tbody/tr["+i+"]/td[4]/div/label/input"));
			String id = id1Location.getAttribute("id");
			ReusableFunc.ClickByID(driver, id);
		}
		*/
		//Method 2 Direct XPath Failed
		//ReusableFunc.ClickByXpath(driver, ".//*[@id='existing-video-assets-modal-table']/tbody/tr[1]/td[4]/div/label/input", "Selecting the 1st zabalaza");
		//ReusableFunc.ClickByXpath(driver, ".//*[@id='existing-video-assets-modal-table']/tbody/tr[2]/td[4]/div/label/input", "Selecting the 2st zabalaza");
		//ReusableFunc.ClickByXpath(driver, ".//*[@id='existing-video-assets-modal-table']/tbody/tr[3]/td[4]/div/label/input", "Selecting the 3st zabalaza");
		
		ReusableFunc.ClickByID(driver, "addVideoAssets");
		Thread.sleep(2000);
		
	}

	private void navigateToVidoeMataPage() throws Exception {
		ReusableFunc.ClickByLinkText(driver, "Media");
		Thread.sleep(1000);
		//ReusableFunc.waitForElementToDisplay(driver, By.id("VidoeMata"), 3);
		ReusableFunc.ClickByLinkText(driver, "Video Meta");
		Thread.sleep(1000);
	}
	private void navigateBackToVidoeMataPage() throws Exception {
		ReusableFunc.ClickByLinkText(driver, "Media");
		Thread.sleep(1000);
		navigateToVidoeMataPage();
	}
	private void saveUsingBottomButton(String name) throws Exception {
		//ReusableFunc.waitForElementToBeClickable(driver, By.xpath(".//*[@id='form']/div[2]/div/div/button"), 3);
		ReusableFunc.ClickByXpath(driver,".//*[@id='video-meta-form']/div[2]/div/div/button", "save");
		Thread.sleep(1000);
		ConfirmSave(name);
	}
	private void saveUsingTopRightButton(String name) throws Exception {
		//ReusableFunc.waitForElementToBeClickable(driver, By.xpath(".//*[@id='form']/div[1]/div[2]/div[1]/button"), 3);
		ReusableFunc.waitForElementToBeClickable(driver, By.xpath(".//*[@id='video-meta-form']/div[1]/div[2]/div[1]/button"), 3);
		
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver,".//*[@id='video-meta-form']/div[1]/div[2]/div[1]/button", "save");
		Thread.sleep(1000);
		ConfirmSave(name);
	}
	private void ConfirmSave(String name) throws Exception {
		ReusableFunc.waitForElementToDisplay(driver, By.id("searchTitle"), 11);
		Thread.sleep(2000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("searchTitle"), name);
		Thread.sleep(3000);
		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='video_Meta_List_form']/tbody/tr/td[2]"), 20);
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='video_Meta_List_form']/tbody/tr/td[2]"), name);
		Thread.sleep(1000);
	}
	private void confirmDelete(String name) throws Exception {
		ReusableFunc.VerifyTextOnPage(driver,"Are you sure");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "delete");
		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='searchTitle']"), 5);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='searchTitle']"), name);
		Thread.sleep(2000);
		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='video_Meta_List_form']/tbody/tr/td"), 5);
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='video_Meta_List_form']/tbody/tr/td"), "No data available in table");
		Thread.sleep(1000);
	}
}
