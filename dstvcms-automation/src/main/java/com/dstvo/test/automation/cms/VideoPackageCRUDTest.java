package com.dstvo.test.automation.cms;

import java.util.UUID;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class VideoPackageCRUDTest extends TestBase {
	private String videoPackageName = "001TestingVideoPackage123";
	private String videoPackageNameEdited = videoPackageName + " Edited";

	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}

	@Test(priority = -20)
	public void videoPackagesHomePage() throws Exception{
		System.out.println("Starting...Video Package ");
		ReusableFunc.NewCMSLogin(driver, By.id("username"), By.id("password"), "loginButton");
		Thread.sleep(2000);
		
		navigateToVideoPackages();
		Thread.sleep(2000);
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='page_content']/div[1]/h1"), "Video Packages List");
		Thread.sleep(1000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='video_Package_List_form_filter']/label/input"),videoPackageName);
		Thread.sleep(2000);
		String actualText;
		try {
			actualText = driver.findElementByXPath(".//*[@id='video_Package_List_form']/tbody/tr/td[1]").getText();
			//System.out.println("TRY " + actualText);
		} catch (Exception e) {
			actualText = driver.findElementByXPath(".//*[@id='video_Package_List_form']/tbody/tr/td").getText();
			//System.out.println("Catch " + actualText);
		}
		 
		 //System.out.println("Search Results: " + actualText);
		// If so, delete it
		 if (actualText.contains(videoPackageName)) {
			ReusableFunc.ClickByXpath(driver,".//*[@id='video_Package_List_form']/tbody/tr/td[3]/a[2]", "Delete Button");
			confirmDelete(videoPackageName);
		}
	}
	
	@Test(priority = -19, dependsOnMethods={"videoPackagesHomePage"})
	public void cancelAddVideoPackages() throws Exception{
		navigateBackToVideoPackages();
		Thread.sleep(2000);
		ReusableFunc.ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[2]/a","add VideoPackages (bottom +add)");
		ReusableFunc.waitForElementToDisplay(driver, By.id("name"), 5);
		ReusableFunc.VerifyTextOnPage(driver, "Package Name");
		String toBeCanceled = videoPackageName + " 2bCanceled";
		// input Editorial name
		ReusableFunc.EnterTextInTextBox(driver,	driver.findElement(By.xpath(".//*[@id='name']")),toBeCanceled);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/a", "Cancel");
		Thread.sleep(2000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='video_Package_List_form_filter']/label/input"), toBeCanceled);
		Thread.sleep(2000);
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='video_Package_List_form']/tbody/tr/td"), "No data available in table");
	}

	@Test(priority = -18, dependsOnMethods={"videoPackagesHomePage"})
	public void addNewVideoPackagesBlankEntry() throws Exception {
		navigateBackToVideoPackages();
		ReusableFunc.ClickByXpath(driver,".//*[@id='page_content']/div[2]/div/div[1]/div/div[1]/a","add Vide Packages (top +add)");
		ReusableFunc.waitForElementToDisplay(driver, By.id("name"), 5);
		ReusableFunc.VerifyTextOnPage(driver, "Package Name");
		//attempt to save blank entry
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[1]/div[2]/div/button", "save blank");
		Thread.sleep(3000);
		ReusableFunc.VerifyTextOnPage(driver, "may not be empty");
		
	}
	
	@Test(priority = -17, dependsOnMethods={"videoPackagesHomePage"})
	public void addNewVideoPackages() throws Exception {
		navigateBackToVideoPackages();
		ReusableFunc.ClickByXpath(driver, ".//*[@id='page_content']/div[2]/div/div[1]/div/div[1]/a","add Vide Packages (top +add)");
		ReusableFunc.waitForElementToDisplay(driver, By.id("name"), 5);
		ReusableFunc.VerifyTextOnPage(driver, "Package Name");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("name"),videoPackageName);
		UUID irdetoPackageIds =UUID.randomUUID();
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("irdetoPackageIds"),irdetoPackageIds.toString() );
		String destription = "Amend my facility\nPlease complete your details in the fields below and one of our friendly client care consultants will call you back shortly. \n Alternatively, you can also contact our client care department on 08 600 70 000. Kindly note that all our advisors work under supervision.";
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("description_wysiwyg"), destription );
		//Do more test on the wysiwyg
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("lastChancePeriod"), "72" );
		ReusableFunc.ClickByID(driver, "typeOfTVOD_chosen", "Type of VOD");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='typeOfTVOD_chosen']/div/div/input"),"Tra" );
		ReusableFunc.ClickByXpath(driver, ".//*[@id='typeOfTVOD_chosen']/div/ul/li", "Transactional VOD");
		//ReusableFunc.ClickDropdownElement(driver, By.id(".//*[@id='typeOfTVOD_chosen']/a"), By.xpath(".//*[@id='typeOfTVOD_chosen']/div/ul/li[2]"));
		//addProductToPackage();
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("videoPackage_images_billboard_link_image_asset_search_"), "big bro" );
		ReusableFunc.ClickByXpath(driver, ".//*[@id='videoPackage_images_billboard_link_image_asset_search']/div[2]/a", "search Button");
		ReusableFunc.VerifyTextOnPage(driver, "Image Assets");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='image_asset_search_form']/tbody/tr/td[1]/a","Big Brother Africa 2014");
		ReusableFunc.ClickByID(driver, "auto_crop_save_billboard", "auto_crop");
		Thread.sleep(5000);
		//save
		//saveUsingTopRightButton(videoPackageName);
		Thread.sleep(5000);
		saveUsingBottomButton(videoPackageName);
	}
	
	private void addProductToPackage() throws Exception {
		// TODO Auto-generated method stub
		ReusableFunc.ClickByID(driver, "btnAssignProduct", "addProduct");
		ReusableFunc.VerifyTextOnPage(driver, "Make Package available on Product");
		Thread.sleep(1000);
		ReusableFunc.ClickDropdownElement(driver, By.id("packageProductInstances1_product_chosen"), By.xpath(".//*[@id='packageProductInstances1_product_chosen']/div/ul/li[7]"));
		ReusableFunc.ClickByID(driver,"packageProductInstances1_currency_chosen", "south Africa");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='packageProductInstances1_currency_chosen']/div/div/input"), "South");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='packageProductInstances1_currency_chosen']/div/ul/li", "South Africa");
		Thread.sleep(3000);
		ReusableFunc.ClickByID(driver, "packageProductInstances1.price", "price");
		Thread.sleep(3000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("packageProductInstances1.price"), "27");
		//ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("packageProductInstances1.promoURL"), "http://10.10.34.100:8080/");
		/** Buggy */
		StartDateAndTime();
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='package-product-select-2']/div[3]/button[1]", "Save");
		
	}

	@Test (priority = -16, dependsOnMethods = { "addNewVideoPackages" })
	public void editVideoPackages() throws Exception{
		navigateBackToVideoPackages();
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='video_Package_List_form_filter']/label/input"), videoPackageName);
		Thread.sleep(2000);
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='video_Package_List_form']/tbody/tr[1]/td[1]"), videoPackageName);
		ReusableFunc.ClickByXpath(driver,".//*[@id='video_Package_List_form']/tbody/tr[1]/td[3]/a[1]", "Edit Button");
		Thread.sleep(3000);
		//ReusableFunc.verifyTextInWebElement(driver, By.id("page_sub_title"), videoPackageName);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("name"), videoPackageNameEdited); //update the Editorial Name
		saveUsingBottomButton(videoPackageNameEdited);
	}
	
	@Test (priority = -15, dependsOnMethods = { "editVideoPackages" })
	public void deleteVideoPackages() throws Exception{
		navigateBackToVideoPackages();
		Thread.sleep(2000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='video_Package_List_form_filter']/label/input"),videoPackageName);
		Thread.sleep(2000);
		String actualText = driver.findElementByXPath(".//*[@id='video_Package_List_form']/tbody/tr[1]/td[1]").getText();
		System.out.println("Search Results: " + actualText);
		// If so, delete it
		if (actualText.contains(videoPackageNameEdited)) {
			ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='video_Package_List_form']/tbody/tr[1]/td[1]"), videoPackageNameEdited);
			ReusableFunc.ClickByXpath(driver,".//*[@id='video_Package_List_form']/tbody/tr[1]/td[3]/a[2]", "Delete Button");
			confirmDelete(videoPackageNameEdited);
		}
		System.out.println("completed...Video Package tests ");	
	}
	
	
	private void StartDateAndTime() throws Exception{
		// TODO Auto-generated method stub
		ReusableFunc.ClickByID(driver, "packageProductInstances1.startDate");
		//current date format YYYY-MM-DD
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("packageProductInstances1.startDate"), "2015-06-16" );
		//Current Time Format HH:MM:SS.mmm
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("packageProductInstances1.startTime"), "01:30:00.000" );
		
		
		ReusableFunc.ClickByID(driver, "packageProductInstances1.expiryDate");
		//current date formart YYYY-MM-DD
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("packageProductInstances1.expiryDate"), "2015-06-16" );
		//Current Time Format HH:MM:SS.mmm
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("packageProductInstances1.expiryTime"), "01:30:00.000" );
		
		
		/** Preferred Way Of doing it 
		for (int i = 0; i < 3; i++) {
			ReusableFunc.ClickByXpath(driver,"html/body/div[31]/div[1]/table/thead/tr[1]/th[3]","increment Month");
		}
		ReusableFunc.ClickByXpath(driver,"html/body/div[31]/div[1]/table/thead/tr[1]/th[1]","decrement Month");
		ReusableFunc.ClickByXpath(driver, "html/body/div[31]/div[1]/table/tbody/tr[2]/td[2]", "Select Day");
		
		ReusableFunc.ClickByXpath(driver, ".//*[@id='package-product-select-2']/div[2]/div[5]/div/div[2]/span","Start Time");
		for (int i = 0; i < 3; i++) {
			ReusableFunc.ClickByXpath(driver,".//*[@id='package-product-select-2']/div[1]/div[1]/div[15]/div/div/div[2]/div/table/tbody/tr[1]/td[1]/a","Changing the hour Up");
		}
		ReusableFunc.ClickByXpath(driver, ".//*[@id='package-product-select-2']/div[1]/div[1]/div[15]/div/div/div[2]/div/table/tbody/tr[3]/td[1]/a", "Changing the hour Down");
		
		for (int i = 0; i < 12; i++) {
			ReusableFunc.ClickByXpath(driver, ".//*[@id='package-product-select-2']/div[1]/div[1]/div[15]/div/div/div[2]/div/table/tbody/tr[1]/td[3]/a", "Changing the minutes Up");
		}
		ReusableFunc.ClickByXpath(driver, ".//*[@id='package-product-select-2']/div[1]/div[1]/div[15]/div/div/div[2]/div/table/tbody/tr[3]/td[3]/a", "Changing the minutes Down");
		
		for (int i = 0; i < 11; i++) {
			ReusableFunc.ClickByXpath(driver, ".//*[@id='package-product-select-2']/div[1]/div[1]/div[15]/div/div/div[2]/div/table/tbody/tr[1]/td[5]/a", "Changing the seconds down");
		}
		*/
		ReusableFunc.ClickByXpath(driver, ".//*[@id='package-product-select-2']/div[1]/div[1]/div[15]/div/div/div[2]/div/table/tbody/tr[3]/td[5]/a", "Changing the seconds up");
				
		
	}
	
	private void navigateToVideoPackages() throws Exception {
		ReusableFunc.ClickByLinkText(driver, "Media");
		Thread.sleep(1000);
		//ReusableFunc.waitForElementToDisplay(driver, By.id("Video Package"), 3);
		ReusableFunc.ClickByLinkText(driver, "Video Package");
		Thread.sleep(1000);
	}
	private void navigateBackToVideoPackages() throws Exception {
		ReusableFunc.ClickByLinkText(driver, "Media");
		Thread.sleep(1000);
		navigateToVideoPackages();
	}
	private void saveUsingBottomButton(String name) throws Exception {
		ReusableFunc.waitForElementToBeClickable(driver, By.xpath(".//*[@id='form']/div[2]/div/div/button"), 5);
		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[2]/div/div/button", "save");
		Thread.sleep(1000);
		ConfirmSave(name);
	}
	private void saveUsingTopRightButton(String name) throws Exception {
		//ReusableFunc.waitForElementToBeClickable(driver, By.xpath(".//*[@id='form']/div[1]/div[2]/div[1]/button"), 3);
		ReusableFunc.waitForElementToBeClickable(driver, By.xpath(".//*[@id='form']/div[1]/div[2]/div[1]/button"), 5);
		
		Thread.sleep(5000);
		ReusableFunc.ClickByXpath(driver,".//*[@id='form']/div[1]/div[2]/div[1]/button", "save");
		Thread.sleep(1000);
		ConfirmSave(name);
	}
	private void ConfirmSave(String name) throws Exception {
		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='video_Package_List_form_filter']/label/input"), 11);
		Thread.sleep(2000);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='video_Package_List_form_filter']/label/input"), name);
		Thread.sleep(2000);
		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='video_Package_List_form']/tbody/tr[1]/td[1]"), 11);
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='video_Package_List_form']/tbody/tr[1]/td[1]"), name);
		Thread.sleep(1000);
	}
	private void confirmDelete(String name) throws Exception {
		ReusableFunc.VerifyTextOnPage(driver,"Are you sure");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='form']/div[2]/div/div/button", "delete");
		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='video_Package_List_form_filter']/label/input"), 5);
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath(".//*[@id='video_Package_List_form_filter']/label/input"), name);
		Thread.sleep(2000);
		ReusableFunc.waitForElementToDisplay(driver, By.xpath(".//*[@id='video_Package_List_form']/tbody/tr/td"), 5);
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='video_Package_List_form']/tbody/tr/td"), "No data available in table");
		Thread.sleep(1000);
	}
}
