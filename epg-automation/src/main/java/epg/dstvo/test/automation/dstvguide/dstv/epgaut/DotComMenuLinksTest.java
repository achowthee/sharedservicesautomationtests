/**
 * 
 */
/**
 * @author Belinda.Ndlovu
 *
 */
package com.dstvo.test.automation.responsivesite.dstv.responsivedotcomaut;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;


public class DotComMenuLinksTest extends TestBase{
	

    @BeforeMethod
	@Override
    public  void before() throws Exception {
        super.before();        
        getWebDriver().get(getSiteURL());
       
    } 
    
        
    @Test
    public void canAccessHomeViaMenuLink() throws Exception {
     	ReusableFunc.ContainsPageTitle(driver, "DStv");
     	ReusableFunc.VerifyTextOnPage(driver, "Latest Entertainment");
    	ReusableFunc.VerifyTextOnPage(driver, "Latest Sport");
    	
    }
    
        
}
