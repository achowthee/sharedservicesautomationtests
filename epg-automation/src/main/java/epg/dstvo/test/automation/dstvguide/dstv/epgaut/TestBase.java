package com.dstvo.test.automation.responsivesite.dstv.responsivedotcomaut;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.remote.*;
import java.net.URL;


public class TestBase {
	  //  private static ChromeDriverService service;
	    public RemoteWebDriver driver;  //was private, changed to use in other tests       

 
	    @BeforeMethod
		public void before() throws Exception {
	        driver = new RemoteWebDriver(new URL("http://197.80.203.161:4457/wd/hub"), DesiredCapabilities.firefox());
	        
	    }

	    @AfterMethod
		public void after() {
	        if (driver != null) {
	        driver.quit();
	        }
	    }
	    
	    protected String getSiteURL (){
	        String URL = System.getProperty("testUrl");
	        if (URL == null){
	                return "http://aut.responsive.dstv.com";
	        }
	        else{
	                return URL;
	        }
	        
	    }
	        
	
                
	    	    
	    protected RemoteWebDriver getWebDriver() {
	        return driver;
	    }
	    
	    
	}
