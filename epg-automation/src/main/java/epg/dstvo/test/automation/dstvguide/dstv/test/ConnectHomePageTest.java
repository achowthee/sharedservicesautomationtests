package com.dstvo.test.automation.responsivesite.dstv.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ConnectHomePageTest extends TestBase {
	
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}
	
	
	@Test
	public void connectLoginUsername() throws Exception{
		Thread.sleep(10000);
		ReusableFunc.ClickByXpath(driver, "html/body/div[2]/div/div[2]/div[3]/div/div/div[1]/a", "Login");
		ConnectControl.connectLiteLoginUsernameEmail(driver, "printest", "123456", "");
	}
	
	@Test
	public void connectLoginEmail() throws Exception{
		Thread.sleep(10000);
		ReusableFunc.ClickByXpath(driver, "html/body/div[2]/div/div[2]/div[3]/div/div/div[1]/a", "Login");
		ConnectControl.connectLiteLoginUsernameEmail(driver, "prinita.pillay@dstvo.com", "123456", "");
	}
	
	@Test
	public void connectLoginFacebook() throws Exception{
		Thread.sleep(10000);
		ReusableFunc.ClickByXpath(driver, "html/body/div[2]/div/div[2]/div[3]/div/div/div[1]/a", "Login");
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/form/div[9]/div/div/a[3]/img", "F");
		ConnectControl.connectLiteLoginFacebook(driver, "ashenychowtheey@gmail.com", "ashen123", "");
	}
	
	@Test
	public void connectLoginGoogle() throws Exception{
		Thread.sleep(10000);
		ReusableFunc.ClickByXpath(driver, "html/body/div[2]/div/div[2]/div[3]/div/div/div[1]/a", "Login");
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/form/div[9]/div/div/a[2]/img", "G+");
		ConnectControl.connectLiteLoginGoogle(driver, "ashenychowtheey@gmail.com", "ashen123", "");
	}
	
	
	/*@Test
	public void connectUpdateMe() throws Exception{
		Thread.sleep(4000);
		ReusableFunc.ClickByXpath(driver, "html/body/div[2]/div/div[2]/div[3]/div/div/div[1]/a", "Login");
		ConnectControl.connectLiteLoginUsernameEmail(driver, "printest", "123456", "");
		ConnectControl.connectLiteLoginMobile(driver, strUserName, strPassword, text);
	}*/
	
	
	
	
}
