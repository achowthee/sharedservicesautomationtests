package com.dstvo.test.automation.responsivesite.dstv.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class Redirects extends TestBase {

	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}
	
	
	@Test
	public void canAccessRedirectsStagingCompaigns() throws Exception {
		//premium
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/premium");
		ReusableFunc.CompareUrls(driver, "http://campaign.dstv.com/knowbest/");
		//explora
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/explora");
		ReusableFunc.CompareUrls(driver, "http://campaigns.dstv.com/explora/");
		//deliciousfestival
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/deliciousfestival");
		ReusableFunc.CompareUrls(driver, "http://campaigns.dstv.com/deliciousfestival/");		
		//getconnected
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/getconnected");
		ReusableFunc.CompareUrls(driver, "http://campaigns.dstv.com/getconnected/");		
		//unlock-premium
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/unlock-premium");
		ReusableFunc.CompareUrls(driver, "http://campaign.dstv.com/knowbest/");
		//tia
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/tia");
		ReusableFunc.CompareUrls(driver, "http://campaigns.dstv.com/thisisafrica");		
		//thisisafrica
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/thisisafrica");
		ReusableFunc.CompareUrls(driver, "http://campaigns.dstv.com/thisisafrica");		
		//nicelifeproblems
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/nicelifeproblems");
		ReusableFunc.CompareUrls(driver, "http://campaign.dstv.com/nicelifeproblems/");		
		//compact
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/nicelifeproblems");
		ReusableFunc.CompareUrls(driver, "http://campaign.dstv.com/nicelifeproblems/");		
		//now
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/now");
		ReusableFunc.CompareUrls(driver, "http://now.dstv.com/");		
		//boxoffice
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/boxoffice");
		ReusableFunc.CompareUrls(driver, "http://boxoffice.dstv.com/");		
		//win
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/win");
		ReusableFunc.CompareUrls(driver, "http://www.dstv.com/competitions");		
		//valentines
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/valentines");
		ReusableFunc.CompareUrls(driver, "http://campaigns.dstv.com/valentines/");		
		//foodies
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/foodies");
		ReusableFunc.CompareUrls(driver, "http://www.dstv.com/topic/foodies-community-20150313");		
		//homegrown
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/homegrown");
		ReusableFunc.CompareUrls(driver, "http://www.dstv.com/topic/dstv-homegrown-20150313");		
		//glam
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/glam");
		ReusableFunc.CompareUrls(driver, "http://www.dstv.com/topic/glam-community-20150313");		
		//kids
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/kids");
		ReusableFunc.CompareUrls(driver, "http://www.dstv.com/topic/kids-community-20150313");		
		//knowbest
		//ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/knowbest");
		//ReusableFunc.CompareUrls(driver, "http://campaign.dstv.com/knowbest/");
	}
	
	@Test
	public void canAccessRedirectsStagingSites() throws Exception {
		//catchup
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/catchup");
		ReusableFunc.CompareUrls(driver, "http://now.dstv.com/");
		//channels
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/channels");
		ReusableFunc.CompareUrls(driver, "http://guide.dstv.com/channels");
		//default.aspx
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/default.aspx");
		ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/");
		//emailscam
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/emailscam");
		ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/");
		//guide
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/guide");
		ReusableFunc.CompareUrls(driver, "http://guide.dstv.com/");
		//mobile
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/mobile");
		ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/");
		//pages
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/pages");
		ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/news");
		//subpages
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/subpages");
		ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/");
		//video
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/video");
		ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/");
		//mobile
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/mobile");
		ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/");
		//mobile/streaming
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/mobile/streaming");
		ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/");
		//mobile/broadcast
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/mobile/broadcast");
		ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/");
		//pages/termsconditions
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/pages/termsconditions");
		ReusableFunc.CompareUrls(driver, "http://www.dstv.com/topic/multichoice-ts-and-cs-20150313");
		//pages/contactus
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/pages/contactus");
		ReusableFunc.CompareUrls(driver, "http://selfservice.dstv.com/self-service/contact-us");
		//content/Now-Showing-in-the-Dark/1175
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/content/Now-Showing-in-the-Dark/1175");
		ReusableFunc.CompareUrls(driver, "http://www.dstv.com/topic/now-showing-in-the-dark-20150317");
	}
	
	@Test
	public void canAccessRedirectsStagingConnectNotLoggedIn() throws Exception {
		//connect
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/connect");
		ReusableFunc.CompareUrls(driver, "https://connect.dstv.com/Login?redirect=%2f");
		//connect/ForgotPasword
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/connect/ForgotPasword");
		ReusableFunc.CompareUrls(driver, "https://connect.dstv.com/Login?redirect=%2f");
		//Connect/Login
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/connect/Login");
		ReusableFunc.CompareUrls(driver, "https://connect.dstv.com/Login?redirect=%2f");		
		//Connect/Newsletters
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/connect/Newsletters");
		ReusableFunc.CompareUrls(driver, "https://connect.dstv.com/Login?redirect=%2fnewsletters");
		//Connect/Profile/Default.aspx
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/connect/Profile/Default.aspx");
		ReusableFunc.CompareUrls(driver, "https://connect.dstv.com/Login?redirect=%2fprofile");
		//Connect/Register
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/connect/Register");
		ReusableFunc.CompareUrls(driver, "https://connect.dstv.com/Login?redirect=%2f");
		//Connect/Smartcards
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/connect/Smartcards");
		ReusableFunc.CompareUrls(driver, "https://connect.dstv.com/Login?redirect=%2fsmartcards");
	}
	
	@Test
	public void canAccessRedirectsStagingConnectLoggedIn() throws Exception {
		//connect/Login
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/connect/Login");
		ConnectControl.connectLiteLoginUsernameEmail(driver, "prinita.pillay@dstvo.com", "123456", "");
		ReusableFunc.CompareUrls(driver, "https://connect.dstv.com/");
		//connect/ForgotPasword
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/connect/ForgotPasword");
		ReusableFunc.CompareUrls(driver, "https://connect.dstv.com/");
		//Connect/Newsletters
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/connect/Newsletters");
		ReusableFunc.CompareUrls(driver, "https://connect.dstv.com/newsletters");
		//Connect/Profile/Default.aspx
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/connect/Profile/Default.aspx");
		ReusableFunc.CompareUrls(driver, "https://connect.dstv.com/profile");
		//Connect/Register
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/connect/Register");
		ReusableFunc.CompareUrls(driver, "https://connect.dstv.com/");
		//Connect/Smartcards
		ReusableFunc.navigateToURL(driver, "http://staging-dstv.dstv.com/connect/Smartcards");
		ReusableFunc.CompareUrls(driver, "https://connect.dstv.com/smartcards");
	}
}
