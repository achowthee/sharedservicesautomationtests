package com.dstvo.test.automation.responsivesite.dstv.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ResponsiveCompetitions extends TestBase {

	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}
	
	@Test
	public void canAccessListingsPage() throws Exception {
		driver.get("http://staging-dstv.dstv.com/competitions");
		ReusableFunc.ContainsPageTitle(driver, "Rewards");
	}
	
	@Test
	public void clickFirstItemImage() throws Exception {
		driver.get("http://staging-dstv.dstv.com/competitions");
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/div[2]/div/div/div/div/div/a/img", "First Competition Image");
		ReusableFunc.VerifyPageTitle(driver, "Win Great Prizes with Our Subscriber Competitions | DStv");
	}
	
	@Test
	public void clickFirstItemBlurb() throws Exception {
		driver.get("http://staging-dstv.dstv.com/competitions");
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/div[2]/div/div/div/div/div/a/div[1]", "First Competition Blurb");
		ReusableFunc.VerifyPageTitle(driver, "Win Great Prizes with Our Subscriber Competitions | DStv");
	}

}
