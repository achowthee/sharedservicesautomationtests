package com.dstvo.test.automation.responsivesite.dstv.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ResponsiveDotComHomePageTest extends TestBase {
	
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}
	
	@Test
	public void isContainingPageTitle() throws Exception {
		ReusableFunc.ContainsPageTitle(driver, "DStv | Movies | Sport | Series | Music | TV Guide | Entertainment");
		ReusableFunc.VerifyTextOnPage(driver, "Get DStv");
	}
	
	
	@Test
	public void canAccessSpotLight() throws Exception {
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/div[2]/div/h3/a", "Spotlight");
		ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/en-za/news");
		ReusableFunc.ContainsPageTitle(driver, "News");
	}
	
	@Test
	public void canAccessHotOnTV() throws Exception {
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/div[5]/div/h3/a", "Hot on TV");
	//	ReusableFunc.CompareUrls(driver, "http://staging-dstv.dstv.com/en-za/highlights");
		ReusableFunc.ContainsPageTitle(driver, "Hot on TV");	
	}
	
	@Test
	public void canAccessCatchUp() throws Exception {
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/div[8]/div/h3/a", "Catch Up");
		ReusableFunc.CompareUrls(driver, "http://catchup.dstv.com/");
	}
	
	@Test
	public void canAccessBoxOffice() throws Exception {
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/div[11]/div/h3/a", "BoxOffice");
		ReusableFunc.CompareUrls(driver, "http://boxoffice.dstv.com/");
		ReusableFunc.ContainsPageTitle(driver, "BoxOffice");
	}
	
}
