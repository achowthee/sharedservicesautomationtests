package com.dstvo.test.automation.responsivesite.dstv.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ResponsiveMegaNavSearchTest extends TestBase {
	
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}
	
	
	@Test
	public void canSearchbySearchKey() throws Exception {
		ReusableFunc.ClickByXpath(driver, "html/body/div[2]/div/div[2]/ul/li[7]/button", "Search");
		ReusableFunc.searchBySearchKey(driver, By.xpath("html/body/div[2]/div/div[1]/div/form/div/input"), By.xpath("html/body/div[2]/div/div[1]/div/form/div/a"), "test");
		Thread.sleep(5000);
	    ReusableFunc.VerifyTextOnPage(driver, "Search results for");
	}
	
	@Test
	public void canSearchbyEnterKey() throws Exception {
		ReusableFunc.ClickByXpath(driver, "html/body/div[2]/div/div[2]/ul/li[7]/button", "Search");
		ReusableFunc.searchByEnterKey(driver, By.xpath("html/body/div[2]/div/div[1]/div/form/div/input"), "win");
		Thread.sleep(5000);
	    ReusableFunc.VerifyTextOnPage(driver, "Search results for");
	}
}
