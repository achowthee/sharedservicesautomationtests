package com.dstvo.test.automation.responsivesite.dstv.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ResponsiveMegaNavTestTVGuide extends TestBase {
	
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}


	//What's On
	@Test
	public void canAccess14DayGuide() throws Exception {
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/ul/li[1]/div/div/ul[1]/li[2]/a"), "http://www.dstv.com/guide/");
		Thread.sleep(1000);
		getWebDriver().get(getSiteURL());
	}
	
	@Test
	public void canAccessHighlights() throws Exception {
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/ul/li[1]/div/div/ul[1]/li[3]/a"), "http://www.dstv.com/highlights/");
		Thread.sleep(1000);
		getWebDriver().get(getSiteURL());
	}
	
		
	//Recommended
	@Test
	public void canAccessFoodies() throws Exception {
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/ul/li[1]/div/div/ul[3]/li[2]/a"), "http://www.dstv.com/content/DStv-Foodies/475");
		Thread.sleep(1000);
		getWebDriver().get(getSiteURL());
	}
	
	@Test
	public void canAccessHomegrown() throws Exception {
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/ul/li[1]/div/div/ul[3]/li[3]/a"), "http://www.dstv.com/content/DStvHomegrown/695");
		Thread.sleep(1000);
		getWebDriver().get(getSiteURL());
	}
	
	@Test
	public void canAccessGlam() throws Exception {
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/ul/li[1]/div/div/ul[3]/li[4]/a"), "http://www.dstv.com/content/DStvGlam/925/");
		Thread.sleep(1000);
		getWebDriver().get(getSiteURL());
	}
	
	@Test
	public void canAccessThisIsAfrica() throws Exception {
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/ul/li[1]/div/div/ul[3]/li[5]/a"), "http://www.dstv.com/thisisafrica");
		Thread.sleep(1000);
		getWebDriver().get(getSiteURL());
	}
	
	@Test
	public void canAccessLatestNews() throws Exception {
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/ul/li[1]/div/div/ul[3]/li[6]/a"), "http://www.dstv.com/news/");
		Thread.sleep(1000);
		getWebDriver().get(getSiteURL());
	}
	
	@Test
	public void canAccessKids() throws Exception {
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/ul/li[1]/div/div/ul[3]/li[7]/a"), "http://www.dstv.com/content/DStv_Kids/565");
		Thread.sleep(1000);
		getWebDriver().get(getSiteURL());
	}
	
	@Test
	public void canAccessMusic() throws Exception {
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/ul/li[1]/div/div/ul[3]/li[8]/a"), "http://www.dstv.com/content/DStv_Music/515");
		Thread.sleep(1000);
		getWebDriver().get(getSiteURL());
	}
	
	
	//M-Net
	@Test
	public void canAccessMzansiMagic() throws Exception {
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/ul/li[1]/div/div/ul[4]/li[2]/a"), "http://mzansimagic.dstv.com/");
		Thread.sleep(1000);
		getWebDriver().get(getSiteURL());
	}
	
	@Test
	public void canAccessMnet() throws Exception {
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/ul/li[1]/div/div/ul[4]/li[3]/a"), "https://mnet.dstv.com/country");
		Thread.sleep(1000);
		getWebDriver().get(getSiteURL());
	}
	
	@Test
	public void canAccesskykNet() throws Exception {
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/ul/li[1]/div/div/ul[4]/li[4]/a"), "http://kyknet.dstv.com/");
		Thread.sleep(1000);
		getWebDriver().get(getSiteURL());
	}
	
	@Test
	public void canAccessVuzu() throws Exception {
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/ul/li[1]/div/div/ul[4]/li[5]/a"), "http://vuzu.dstv.com/");
		Thread.sleep(1000);
		getWebDriver().get(getSiteURL());
	}
	
	@Test
	public void canAccessAfricaMagic() throws Exception {
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/ul/li[1]/div/div/ul[4]/li[6]/a"), "http://africamagic.dstv.com/");
		Thread.sleep(1000);
		getWebDriver().get(getSiteURL());
	}
	
	@Test
	public void canAccessChannelO() throws Exception {
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/ul/li[1]/div/div/ul[4]/li[7]/a"), "http://channelo.dstv.com/home/");
		Thread.sleep(1000);
		getWebDriver().get(getSiteURL());
	}
	
	@Test
	public void canAccessMaishaMagic() throws Exception {
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/ul/li[1]/div/div/ul[4]/li[8]/a"), "http://maishamagic.dstv.com/");
		Thread.sleep(1000);
		getWebDriver().get(getSiteURL());
	}
		
	
	//Must See on DStv
	@Test
	public void canAccessCompetitionOne() throws Exception {
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[1]/ul/li[2]/div/div/div[1]/a/div"), "http://www.dstv.com/content/DStvGlam/925/");
		Thread.sleep(1000);
		getWebDriver().get(getSiteURL());
	}
}
