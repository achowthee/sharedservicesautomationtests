package com.dstvo.test.automation.responsivesite.dstv.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ResponsiveMegaNavWatchTest extends TestBase {
	
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}
	
	
	//Catch Up
	@Test
	public void canAccessCatchUp() throws Exception {
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/ul/li[1]/div/div/ul[1]/li[2]/a"), "http://catchup.dstv.com/browse/movies/2/?title=Movies");
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/ul/li[1]/div/div/ul[1]/li[3]/a"), "http://catchup.dstv.com/browse/series/3/?title=Series");
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/ul/li[1]/div/div/ul[1]/li[4]/a"), "http://catchup.dstv.com/browse/actuality-information/6/?title=Actuality/Information");
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/ul/li[1]/div/div/ul[1]/li[5]/a"), "http://catchup.dstv.com/browse/music/9/?title=Music");
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/ul/li[1]/div/div/ul[1]/li[6]/a"), "http://catchup.dstv.com/browse/sport/5/?title=Sport");
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/ul/li[1]/div/div/ul[1]/li[7]/a"), "http://catchup.dstv.com/browse/kids/8/?title=Kids");
		getWebDriver().get(getSiteURL());
	}
	
	
	//BoxOffice
	@Test
	public void canAccessBoxOffice() throws Exception {
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/ul/li[1]/div/div/ul[2]/li[2]/a"), "http://boxoffice.dstv.com/browse-movies/now");
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/ul/li[1]/div/div/ul[2]/li[3]/a"), "http://boxoffice.dstv.com/browse-movies/pvr");
		getWebDriver().get(getSiteURL());
	}
	
	
	//Live TV
	@Test
	public void canAccessLiveTV() throws Exception {
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/ul/li[1]/div/div/ul[3]/li[2]/a"), "http://campaigns.dstv.com/now/");
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/ul/li[1]/div/div/ul[3]/li[3]/a"), "http://www.supersport.com/live-video");
		getWebDriver().get(getSiteURL());
	}
	
	
	//Video
	@Test
	public void canAccessVideo() throws Exception {
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/ul/li[1]/div/div/ul[4]/li[2]/a"), "http://www.dstv.com/video/");
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/ul/li[1]/div/div/ul[4]/li[3]/a"), "http://www.supersport.com/video");
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/ul/li[1]/div/div/ul[4]/li[4]/a"), "http://idolssa.dstv.com/");
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/ul/li[1]/div/div/ul[4]/li[5]/a"), "http://bigbrotherafrica.dstv.com/");
		ReusableFunc.ClickMegaNavSwitchWindow(driver, By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/a"), By.xpath("html/body/div[2]/div/div[2]/ul/li[2]/ul/li[1]/div/div/ul[4]/li[6]/a"), "http://amvca2015-awards.dstv.com/");
		getWebDriver().get(getSiteURL());
	}
	
}
