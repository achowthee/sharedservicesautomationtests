package com.dstvo.test.automation.Billing;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class BillingTest extends TestBase {

	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
		

	}


	@Test (invocationCount=3600)
	public void BillingQ() throws Exception {
		 ReusableFunc.verifyElementIsDisplayed(driver, By.id("activemq_logo"), "mqlogo");
		 ReusableFunc.CompareUrls(driver, "http://10.10.14.203:8161/admin/queues.jsp");
		 WebElement sCellValue = driver.findElement(By.xpath("//*[@id='queues']/tbody/tr[1]/td[1]/a"));
		 String confirm = sCellValue.getText();
		 if(confirm.equalsIgnoreCase("Billing")){
			  
			 org.testng.Assert.assertEquals(confirm, "Billing");
			 //System.out.println(sCellValue.getText());
		 }
		else{
			ReusableFunc.ClickByXpath(driver, "//*[@id='queues']/thead/tr/th[1]/a", "Click to sort By name");
			System.out.println("Sorted by Name");
		 }
		 WebElement noOfConsumers = driver.findElement(By.xpath(".//*[@id='queues']/tbody/tr[1]/td[3]"));
		 noOfConsumers.getText();
		 org.testng.Assert.assertNotEquals(noOfConsumers.getText(), "0", "Billing has 0 consumers");
	     Thread.sleep(1000);

	    }
		

}
