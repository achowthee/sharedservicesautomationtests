package com.dstvo.test.automation.BillingReconJob;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

import hello.*;

public class BillingReconJob extends TestBase {
	Message msg = new Message("40030739", "Love Is Strange", "South Africa", 100.00f, 123l , 128l, "IBS.DoBilling seems to be down_Tests", false, "CreditCard", "35033760", "27831234567");
	
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
		

	}
	
	@Test(priority = -20)
	public void activeMqHome() throws Exception{
		ReusableFunc.waitForElementToDisplay(driver, By.id("activemq_logo"), 60);
		ReusableFunc.verifyElementIsDisplayed(driver, By.id("queues"), "queue Table");
		
	}
	
	@Test(priority = -19, dependsOnMethods = {"activeMqHome"})
	public void verfyConsumerExist() throws Exception{
		backToQueueMainPage();
		int rowRecon= 1;
		String queueName = driver.findElementByXPath(".//*[@id='queues']/tbody/tr["+rowRecon+"]/td[1]").getText();
		System.out.println(queueName);
		while(!(queueName).equals("BillingRecon")){
			rowRecon++;
			queueName = driver.findElementByXPath(".//*[@id='queues']/tbody/tr["+rowRecon+"]/td[1]/a").getText();
			System.out.println(queueName);
		}
		String consumers = driver.findElementByXPath(".//*[@id='queues']/tbody/tr["+rowRecon+"]/td[3]").getText();
		int numberOfConsumers = Integer.parseInt(consumers);
		org.testng.Assert.assertTrue((numberOfConsumers > 0) ? true: false );
	}
	
	@Test(priority = -18, dependsOnMethods = {"verfyConsumerExist"})
	public void billingErrors_SouthAfrica_BTD_Test() throws Exception{
		backToQueueMainPage();
		int numOfPendingBillingErrors = getNumberOfPendingMessages("BillingErrors");

		msg.setTitle("billingErrors_SouthAfrica_BTD_Test");
		msg.setCountry("South Africa_BTD");
		msg.setAmount(18.00f);
		sendToMessageToQ(CurrentEnviromentIP,msg.getAccountNumber(), msg.getTitle(), msg.getCountry(), msg.getAmount(), msg.getTransactionLogID(), msg.getRentalLogID(),msg.getErrorDescription(), msg.getIsFreeCredit(), msg.getRentalType(), msg.getCustomerNumber(), msg.getMSISDN());
		
		String actualMessage = msg.toString();
		Thread.sleep(10000);
		driver.navigate().refresh();
		Thread.sleep(50000);
		driver.navigate().refresh();
		Thread.sleep(2000);
		int numOfPendingBillingErrorsAfter = getNumberOfPendingMessages("BillingErrors");
	
		org.testng.Assert.assertTrue(((numOfPendingBillingErrorsAfter > numOfPendingBillingErrors ) ? true: false), numOfPendingBillingErrorsAfter +" > "+ numOfPendingBillingErrors );
		Thread.sleep(5000);
		//ReusableFunc.ClickByLinkText(driver, "BillingErrors");
		viewMessages("BillingErrors");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='messages']/tbody/tr["+numOfPendingBillingErrorsAfter+"]/td[1]/a", "newly Added Message");
		String expectedMessage = driver.findElement(By.xpath(".//*[@id='body']/tbody/tr/td/div/pre")).getText();
		org.testng.Assert.assertEquals(expectedMessage,actualMessage);
	}
	
	@Test(priority = -17, dependsOnMethods = {"verfyConsumerExist"})
	public void billingErrors_ZeroBalance() throws Exception{
		backToQueueMainPage();
		int numOfPendingBillingErrors = getNumberOfPendingMessages("BillingErrors");
		System.out.println("numOfPendingBillingErrors = "+ numOfPendingBillingErrors);

		msg.setTitle("billingErrors_ZeroBalance_Test");
		msg.setCountry("South Africa");
		msg.setAmount(0.00f);
		sendToMessageToQ(CurrentEnviromentIP,msg.getAccountNumber(), msg.getTitle(), msg.getCountry(), msg.getAmount(), msg.getTransactionLogID(), msg.getRentalLogID(),msg.getErrorDescription(), msg.getIsFreeCredit(), msg.getRentalType(), msg.getCustomerNumber(), msg.getMSISDN());
		
		String actualMessage = msg.toString();
		Thread.sleep(10000);
		driver.navigate().refresh();
		Thread.sleep(50000);
		driver.navigate().refresh();
		Thread.sleep(2000);
		int numOfPendingBillingErrorsAfter = getNumberOfPendingMessages("BillingErrors");
		System.out.println("numOfPendingBillingErrorsAfter = "+ numOfPendingBillingErrorsAfter);
	
		org.testng.Assert.assertTrue(((numOfPendingBillingErrorsAfter > numOfPendingBillingErrors ) ? true: false), numOfPendingBillingErrorsAfter +" > "+ numOfPendingBillingErrors );
		Thread.sleep(5000);
		//ReusableFunc.ClickByLinkText(driver, "BillingErrors");
		viewMessages("BillingErrors");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='messages']/tbody/tr["+numOfPendingBillingErrorsAfter+"]/td[1]/a", "newly Added Message");
		String expectedMessage = driver.findElement(By.xpath(".//*[@id='body']/tbody/tr/td/div/pre")).getText();
		org.testng.Assert.assertEquals(expectedMessage,actualMessage);
	}
	
	@Test(priority = -16, dependsOnMethods = {"verfyConsumerExist"})
	public void billing_EveryThingOkay() throws Exception{
		backToQueueMainPage();
		int numOfPendingBillingErrors = getNumberOfPendingMessages("Billing");

		msg.setTitle("billing_EveryThingOkay");
		msg.setCountry("South Africa");
		msg.setAmount(16.00f);
		sendToMessageToQ(CurrentEnviromentIP,msg.getAccountNumber(), msg.getTitle(), msg.getCountry(), msg.getAmount(), msg.getTransactionLogID(), msg.getRentalLogID(),msg.getErrorDescription(), msg.getIsFreeCredit(), msg.getRentalType(), msg.getCustomerNumber(), msg.getMSISDN());
		String actualMessage = msg.toString();
		Thread.sleep(10000);
		driver.navigate().refresh();
		Thread.sleep(50000);
		driver.navigate().refresh();
		Thread.sleep(2000);
		int numOfPendingBillingErrorsAfter = getNumberOfPendingMessages("Billing");
	
		org.testng.Assert.assertTrue(((numOfPendingBillingErrorsAfter > numOfPendingBillingErrors ) ? true: false), numOfPendingBillingErrorsAfter +" > "+ numOfPendingBillingErrors );
		System.out.println(numOfPendingBillingErrorsAfter + ">" + numOfPendingBillingErrors ); 
		Thread.sleep(5000);
		//ReusableFunc.ClickByLinkText(driver, "BillingErrors");
		viewMessages("Billing");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='messages']/tbody/tr["+numOfPendingBillingErrorsAfter+"]/td[1]/a", "newly Added Message");
		String expectedMessage = driver.findElement(By.xpath(".//*[@id='body']/tbody/tr/td/div/pre")).getText();
		org.testng.Assert.assertEquals(expectedMessage,actualMessage);
	}

	public int getNumberOfPendingMessages(String queue) throws InterruptedException{
		int row = 1;
		String pendingMsg="";
		String queueName = driver.findElementByXPath(".//*[@id='queues']/tbody/tr["+row+"]/td[1]").getText();
		if (queueName.equals(queue)) {
			System.out.println("getNumberOfPendingMessages....//*[@id='queues']/tbody/tr[" + row + "]/td[1]************** " + queueName);
			pendingMsg = driver.findElementByXPath(".//*[@id='queues']/tbody/tr["+row+"]/td[2]").getText();
			
			
		}else{
			while (!queueName.equals(queue)) {
				row++;
				queueName = driver.findElementByXPath(".//*[@id='queues']/tbody/tr[" + row + "]/td[1]/a").getText();
				System.out.println("getNumberOfPendingMessages....//*[@id='queues']/tbody/tr[" + row+ "]/td[1]************** " + queueName);
				pendingMsg = driver.findElementByXPath(".//*[@id='queues']/tbody/tr["+row+"]/td[2]").getText();
			}
		}
		return (Integer.parseInt(pendingMsg));
		
		
	}
	
	public void viewMessages(String queue) throws Exception{
		int row = 1;
		String queueName = driver.findElementByXPath(".//*[@id='queues']/tbody/tr["+row+"]/td[1]").getText();
		
		if (queueName.equals(queue)) {
			queueName = driver.findElementByXPath(".//*[@id='queues']/tbody/tr["+row+"]/td[1]").getText();
			System.out.println("if....//*[@id='queues']/tbody/tr["+row+"]/td[1]/a -----" +queueName);	
			
		}else{
			while (!queueName.equals(queue)) {
				row++;
				queueName = driver.findElementByXPath(".//*[@id='queues']/tbody/tr[" + row + "]/td[1]/a").getText();
				System.out.println("else....//*[@id='queues']/tbody/tr["+row+"]/td[1]/a -----" +queueName);
			}
		}
		ReusableFunc.ClickByXpath(driver,".//*[@id='queues']/tbody/tr["+row+"]/td[1]/a",queue );
		
	
	}
	
	public void backToQueueMainPage() throws Exception{
		ReusableFunc.ClickByXpath(driver, ".//*[@id='site-breadcrumbs']/a[2]", "QueueMain");
		ReusableFunc.VerifyTextOnPage(driver, "Queues");
		ReusableFunc.verifyTextInWebElement(driver, By.xpath(".//*[@id='queues']/thead/tr/th[2]/a"), "Number Of Pending Messages");
	}
	
	private void sendToMessageToQ(String enviroment,String accountNumber,String title,String Country,float amount,long transactionLogID,long rentalLogID,String errorDescription,Boolean isFreeCredit,String rentalType,String customerNumber,String MSISDN){
		String[] args = {};
		//Application.main(args);
		Application.main(args,enviroment,accountNumber, title, Country, amount, transactionLogID, rentalLogID,errorDescription, isFreeCredit, rentalType, customerNumber, MSISDN);

	}
	
}
