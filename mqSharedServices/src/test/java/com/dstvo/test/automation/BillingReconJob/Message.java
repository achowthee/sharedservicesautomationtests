package com.dstvo.test.automation.BillingReconJob;

public class Message {
	private float amount;
	private String customerNumber;
	private long rentalLogID;
	private String accountNumber;
	private String errorDescription; 
	private long transactionLogID; 
	private String rentalType; 
	private Boolean isFreeCredit;
	private String country;
	private String MSISDN; 
	private String title;
	
	public Message( String accountNumber,  String title, String country, float amount, long transactionLogID, long rentalLogID,
			 String errorDescription, Boolean isFreeCredit,String rentalType,  String customerNumber,String mSISDN) {
		super();
		this.amount = amount;
		this.customerNumber = customerNumber;
		this.rentalLogID = rentalLogID;
		this.accountNumber = accountNumber;
		this.errorDescription = errorDescription;
		this.transactionLogID = transactionLogID;
		this.rentalType = rentalType;
		this.isFreeCredit = isFreeCredit;
		this.country = country;
		MSISDN = mSISDN;
		this.title = title;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public String getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	public long getRentalLogID() {
		return rentalLogID;
	}
	public void setRentalLogID(long rentalLogID) {
		this.rentalLogID = rentalLogID;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getErrorDescription() {
		return errorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	public long getTransactionLogID() {
		return transactionLogID;
	}
	public void setTransactionLogID(long transactionLogID) {
		this.transactionLogID = transactionLogID;
	}
	public String getRentalType() {
		return rentalType;
	}
	public void setRentalType(String rentalType) {
		this.rentalType = rentalType;
	}
	public Boolean getIsFreeCredit() {
		return isFreeCredit;
	}
	public void setIsFreeCredit(Boolean isFreeCredit) {
		this.isFreeCredit = isFreeCredit;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getMSISDN() {
		return MSISDN;
	}
	public void setMSISDN(String mSISDN) {
		MSISDN = mSISDN;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@Override
	public String toString() {
		return "{Amount=" + amount + ", CustomerNumber="
				+ customerNumber + ", RentalLogID=" + rentalLogID
				+ ", AccountNumber=" + accountNumber + ", ErrorDescription="
				+ errorDescription + ", TransactionLogID=" + transactionLogID
				+ ", RentalType=" + rentalType + ", IsFreeCredit="
				+ isFreeCredit + ", Country=" + country + ", MSISDN=" + MSISDN
				+ ", Title=" + title + "}";
	}
	
	
	
}
