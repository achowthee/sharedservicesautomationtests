package com.dstvo.test.automation.BillingReconJob;

import java.net.URL;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestBase {

	  //  private static ChromeDriverService service;
	    public RemoteWebDriver driver;  //was private, changed to use in other tests   
	    final private String URLEnvDevServer = "10.40.4.12";
	    final private String URLEnvQAServer = "10.10.34.199";
	    final private String URLEnvUATServer = "10.10.14.75";
	    final private String URLEnvProdServer = "rnd-tal-boa-p1.dstvo.local";
	    public String CurrentEnviromentIP = URLEnvUATServer;
	    
	    @BeforeClass
		public void before() throws Exception {
	 	   // driver = new RemoteWebDriver(new URL("http://197.80.203.161:4457/wd/hub"), DesiredCapabilities.firefox());

		    /**To run tests on autBox using chrome */ 
//		    driver = new RemoteWebDriver(new URL("http://197.80.203.161:4466/wd/hub"), DesiredCapabilities.chrome());
		 	//System.setProperty("webdriver.chrome.driver", "C://ProgramFiles(x86)//chromedriver_win32/chromedriver.exe");

		    
//		    /**To run tests on autBox using chrome */ 
//		    driver = new RemoteWebDriver(new URL("http://197.80.203.161:4466/wd/hub"), DesiredCapabilities.chrome());
		    
		 	/**To run tests locally using chrome*/
//		 	/* WINDOWS */
	        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe"); 
//		    /* MAC */
//		 	System.setProperty("webdriver.chrome.driver", "///Applications/chromedriver");
//		    
			driver = new ChromeDriver();
			/**END To run tests locally using chrome*/
		 	 
		 	driver.manage().window().maximize();


		       /*FirefoxProfile profile = new FirefoxProfile();
		       profile.setPreference("network.proxy.type", 0);
	    	    profile.setPreference("network.proxy.type", 1);
	    	    profile.setPreference("network.proxy.http", "197.80.203.9");
	    	    profile.setPreference("network.proxy.http_port", 8080);
	    	    profile.setPreference("network.proxy.ssl", "197.80.203.9");
	    	    profile.setPreference("network.proxy.ssl_port", 8080);
	    	    profile.setPreference("network.proxy.ftp", "197.80.203.9");
	    	    profile.setPreference("network.proxy.ftp_port", 8080);
	    	    profile.setPreference("network.proxy.Socks", "197.80.203.9");
	    	    profile.setPreference("network.proxy.Socks_port", 8080);
	    	    profile.setPreference("network.proxy.no_proxies_on","ssl.dstv.com");*/
	      	   //driver = new FirefoxDriver(profile);
		
	        
	 
	    		
	        
	    }

	    @AfterClass
		public void after() {
	        if (driver != null) {
	        ///driver.quit();
	        }
	    }
	    
	    protected String getSiteURL (){
	        String URL = System.getProperty("testUrl");
	        if (URL == null){
	                return "http://"+CurrentEnviromentIP +":8161/admin/queues.jsp";
	        
	        }
	        else{
	                return URL;
	        }
	        
	    }
	        
	
                
	    	    
	    protected RemoteWebDriver getWebDriver() {
	        return driver;
	    }
	    
	    
	}
