package com.dstvo.test.automation.boRegistration;

import java.net.URL;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestBase {

	  //  private static ChromeDriverService service;
	    public RemoteWebDriver driver;  //was private, changed to use in other tests       

 
	    @BeforeClass
		public void before() throws Exception {
	    driver = new RemoteWebDriver(new URL("http://197.80.203.247:4443/wd/hub"), DesiredCapabilities.firefox());
	        
	        
	    	//FirefoxProfile profile = new FirefoxProfile();
	    	/*profile.setPreference("network.proxy.type", 0);
    	    profile.setPreference("network.proxy.type", 1);
    	    profile.setPreference("network.proxy.http", "197.80.203.9");
    	    profile.setPreference("network.proxy.http_port", 8080);
    	    profile.setPreference("network.proxy.ssl", "197.80.203.9");
    	    profile.setPreference("network.proxy.ssl_port", 8080);
    	    profile.setPreference("network.proxy.ftp", "197.80.203.9");
    	    profile.setPreference("network.proxy.ftp_port", 8080);
    	    profile.setPreference("network.proxy.Socks", "197.80.203.9");
    	    profile.setPreference("network.proxy.Socks_port", 8080);
    	    profile.setPreference("network.proxy.no_proxies_on","http://rnd-tal-tvod-q1.dstvo.local:8161/admin/queues.jsp");*/
      	   // driver = new FirefoxDriver(profile);
	
	        
	 
	    		
	        
	    }

	    @AfterClass
		public void after() {
	        if (driver != null) {
	        driver.quit();
	        }
	    }
	    
	    protected String getSiteURL (){
	        String URL = System.getProperty("testUrl");
	        if (URL == null){
	                return "http://10.10.14.203:8161/admin/queues.jsp";
	        
	        }
	        else{
	                return URL;
	        }
	        
	    }
	        
	
                
	    	    
	    protected RemoteWebDriver getWebDriver() {
	        return driver;
	    }
	    
	    
	}
