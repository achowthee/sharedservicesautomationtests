package com.dstvo.automatedtest.site.common;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.jbehave.core.model.ExamplesTable;
import org.jbehave.core.steps.Parameters;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;
import com.google.common.base.Function;

public class OriginalProductionsFunctionLibrary {
	
    private static final String characterList = 
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public static String firstName = nameGenerator("First"); 
	public static String lastName = nameGenerator("Last");
	private static String titleText = "";
	private static String contestantName = "";
    
    
	public static boolean verifyTextOnPage(WebDriver driver, String textToFind)
			throws Exception {
		return driver.getPageSource().indexOf(textToFind) >= 0; 
	}

	public static void logIntoConnectLiteControls(WebDriver driver,
			String userName, String password) throws Exception {
		
		fluentWait(By.linkText("Login"), (RemoteWebDriver) driver);
		driver.findElement(By.linkText("Login")).click();
		
		enterTextIntoTextBox(driver.findElement(By.id("username")), userName);
		enterTextIntoTextBox(driver.findElement(By.id("password")), password);
		
		if (driver.findElement(By.className("nprogress-busy")).isDisplayed())
			Thread.sleep(2000);		
		
		ReusableFunc.ClickByID((RemoteWebDriver) driver, "login");
		
		fluentWait(By.className("navlogo"), (RemoteWebDriver) driver);
		fluentWait(By.className("connect-user"), (RemoteWebDriver) driver);
	}

	public static void logoutOfConnect(WebDriver driver) throws Exception {
		if(verifyTextOnPage(driver, "KiethPatel")){
			
			//open-menu
			fluentWait2(driver,By.className("open-menu"));
			driver.findElement(By.className("open-menu")).click();
			
			fluentWait2(driver, By.linkText("Logout"));
			driver.findElement(By.linkText("Logout")).click();
			fluentWait2(driver, By.linkText("Login"));
		}
		
	}

	public static WebElement fluentWait2(WebDriver driver, final By locator) {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(30, TimeUnit.SECONDS)
				.pollingEvery(2, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class);

		WebElement element = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				return driver.findElement(locator);
			}
		});
		return element;
	}

	public static void waitForElement(WebDriver driver, final By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	}

	public static void ClickByLinkText(WebDriver driver, String linkText)
			throws Exception {
		fluentWait2(driver, By.linkText(linkText));

		WebElement element = driver.findElement(By.linkText(linkText));

		if (element.isDisplayed())
			element.click();
		else
			throw new Exception("Link can not be found " + linkText);
	}
	
	public static void clickLink(WebDriver driver, String linkTextOrClassName) {
		By by = (linkTextOrClassName.startsWith(".")) ? By.className(linkTextOrClassName.replace(".",  "")) : By.linkText(linkTextOrClassName);
		waitForElement(driver, by);
		driver.findElement(by).click();
	}

	public static void clickOptionFromList(WebDriver driver,
			String menuClassName, int navigationOption) {
		List<WebElement> menuElements = driver.findElements(By
				.className("menuClassName"));
		menuElements.get(navigationOption).click();
	}

	public static void enterTextIntoTextBox(WebElement TextBox, String Text) {
		TextBox.sendKeys(Text);
	}

	public static void switchWindowsAndVerifyTitle(WebDriver driver, String popupUrl) throws Exception {
		// before any pop ups are open		
		String parentHandle = driver.getWindowHandle();
		// after you have pop ups
		for (String popUpHandle : driver.getWindowHandles()) {
			if (!popUpHandle.equals(parentHandle)) {
				driver.switchTo().window(popUpHandle);
				if (driver.getCurrentUrl().contains(popupUrl)) {	
				}
				else {					
					Assert.assertFalse(((driver.getCurrentUrl()).toString()+" does not contain "+popupUrl), true);
				}				
			}
		}
		driver.close();
		driver.switchTo().window(parentHandle);		
	}

	public static String randomstringGenerator(int stringLength) {
		StringBuffer randomString = new StringBuffer();
        for(int i=0; i<stringLength; i++){
            int number = Utilities.generateRandomInt(1, 26);
            char randomCharacter = characterList.charAt(number);
            randomString.append(randomCharacter);
        }
        return randomString.toString();
	}
	public static String nameGenerator(String stringName) {
		String randomstringName = stringName + OriginalProductionsFunctionLibrary.randomstringGenerator(5);
		return randomstringName;
		
	}
	
	public static boolean updateMyConnectDetails(RemoteWebDriver driver) throws Exception {
		driver.findElementByClassName("connect-menu-arrow").click();
		ReusableFunc.ClickByLinkText(driver, "Overview");
		
		driver.findElementByClassName("editimg").click();
		
		ReusableFunc.clearTextBox(driver, By.id("FirstName"));
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("FirstName"), firstName);
		ReusableFunc.clearTextBox(driver, By.id("Surname"));
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("Surname"), lastName);		
		driver.findElementByClassName("btn-submit").click();
		
		return true;		
	}
	
	public static boolean viewUpdatedConnectDetails(RemoteWebDriver driver) throws Exception {
		if (verifyTextOnPage(driver, firstName) && verifyTextOnPage(driver, lastName)){
			return true;
		}
		return false;
	}
	
	public static boolean viewProfilePageTitle(RemoteWebDriver driver) {
		ReusableFunc.verifyElementIsDisplayed(driver, By.className("widgetTitle"), "widgetTitle");
		return true;
	}
	public static boolean viewJudgesAndPresentersPage(RemoteWebDriver driver) {
		ReusableFunc.verifyElementIsDisplayed(driver, By.className("tweetlink"), "tweetlink");
		return true;
	}
	
	public static boolean getTheFirstVideoTitle(RemoteWebDriver driver) {
		driver.findElementByClassName("title").getText();
		return true;
	}
	
	public static boolean viewActiveCategory(RemoteWebDriver driver) {
		ReusableFunc.verifyElementIsDisplayed(driver, By.className("activeCat"), "activeCat");
		return true;
	}
	
	public static boolean checkIfNavigationIsSelected(RemoteWebDriver driver, String navigationItem) {
		driver.findElementByClassName("navItem").getText().contains(navigationItem);
		return true;
	}
	 
	public static boolean clickOnShare(RemoteWebDriver driver) throws Exception {
		WebElement hover;

		hover = driver.findElement(By.className("logo-share"));
		Actions builder = new Actions(driver);
		builder.moveToElement(hover).moveToElement(driver.findElement(By.xpath("//*[@id='videoWidget']/div[1]/div[1]/div[2]/a[1]"))).click().build().perform();
		return true;
	}
	
	public static void logIntoFacebook(RemoteWebDriver driver, String strUserName, String strPassword , String Text) throws Exception {
		   	
	 // before any pop ups are open
		String parentHandle = driver.getWindowHandle();

		// after you have pop ups
		for (String popUpHandle : driver.getWindowHandles()) {
			if (!popUpHandle.equals(parentHandle)) {
				driver.switchTo().window(popUpHandle);
				if(driver.getTitle().equalsIgnoreCase("Facebook"))	
					//Enter username 
					ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.xpath(".//*[contains(@id, 'email')]")), strUserName);
					//Enter password as 123456
					ReusableFunc.EnterTextInTextBox(driver, driver.findElement(By.xpath(".//*[contains(@id, 'pass')]")), strPassword);
					//Click Facebook login
					ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'u_0_1')]", "Facebook Login");						
				}
			}
	    ReusableFunc.VerifyTextOnPage(driver, Text);					
	}
          
	
	public static boolean viewSocialPopupPageTitle(RemoteWebDriver driver, String pageTitle) throws Exception {
		//ReusableFunc.verifyTextInWebElement(driver, By.tagName("title"), pageTitle);	
		
		String parentHandle = driver.getWindowHandle();
		for (String popUpHandle : driver.getWindowHandles()) {
			if (!popUpHandle.equals(parentHandle)) {
				driver.switchTo().window(popUpHandle);
					if (verifyTextOnPage(driver, pageTitle)) {						
					} else {
						Assert.assertFalse((pageTitle+" could not be found"), true);
					}
			}
		}
		return true;
	}

	public static boolean clickOnFirstInstagramImage(RemoteWebDriver driver) throws Exception {
		if (driver.getCurrentUrl().contains("Gallery")){
			Utilities.fluentWait(By.className("gridItemWrapper"), driver);
			driver.findElementByClassName("gridItemWrapper").click();
			return true;
		}
		else {
			fluentWait(By.className("galleryItem"), driver);
			driver.findElementByClassName("galleryItem").click();
			//ReusableFunc.ClickByXpath(driver, "//*[@id='homePageGalleryWidget']/div[1]/img", "first Instagram image.");
			return true;
		}		
	}

	public static boolean viewInstagramImageInModal(RemoteWebDriver driver) throws Exception {
		Utilities.waitForElement(driver, By.className("modal-image"), 120);
		ReusableFunc.verifyElementIsDisplayed(driver, By.className("modal-image"), "modal-image");
		driver.findElementByClassName("modal-close").click();
		return true;		
	}
	
	public static boolean viewTheFirstNewsArtcleTitle(RemoteWebDriver driver) {	
		Utilities.waitForElement(driver, By.className("standardTitle"), 30);
		titleText = driver.findElementByClassName("standardTitle").getText();
		return true;			
	}
	
	public static boolean verifyFirstNewsItemTitleIsSameAsHomePageTitle(RemoteWebDriver driver) 
	{
		fluentWait(By.className("standardTitle2"), driver);		
		String newsPageFirstArticleTitleText = driver.findElementByClassName("newsItem").findElement(By.className("standardTitle2")).getText();
		if (titleText.contains(newsPageFirstArticleTitleText))
		{
			return true;
		}
		return false;
	}

	public static boolean viewTheFirstVideoTitle(RemoteWebDriver driver) {
		fluentWait(By.className("vidoeTitle"), driver);		
		titleText = driver.findElementByClassName("vidoeTitle").getText();
		return true;
	}

	public static boolean verifyFirstVideoItemTitleIsSameAsHomePageTitle(
			RemoteWebDriver driver) {
		Utilities.fluentWait(By.className("standardTitle"), driver);
		String newsPageFirstArticleTitleText =driver.findElementByClassName("standardTitle").getText();
		if (titleText.contains(newsPageFirstArticleTitleText))
		{
			return true;
		}
		return false;
	}

	public static boolean searchOnHomePage(RemoteWebDriver driver, String searchWord) throws Exception {
		Utilities.isTextPresentAfterWait("More news", driver);
		
		driver.findElementByClassName("search")
			.findElement(By.id("searchIcon"))
				.click();		
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("searchBox"), searchWord);		
		ReusableFunc.ClickByID(driver, "searchButton");
		
		return true;
	}

	
	public static boolean viewSearchResults(RemoteWebDriver driver) {
		if (driver.getCurrentUrl().contains("Search")){
			Utilities.fluentWait(By.className("standardTitle2"), driver);
			ReusableFunc.verifyElementIsDisplayed(driver, By.className("standardTitle2"), "standardTitle2");
			return true;
		} else return false;
	}

	public static boolean hoverAndClickOnContestantMegaNav(RemoteWebDriver driver) throws Exception {
		hoverOnContestantMeganav(driver, "Contestant");
		
		fluentWait(By.className("Contestant"), driver);
		driver.findElementByClassName("Contestant").click();			
		return true;
	}

	public static void hoverOnContestantMeganav(RemoteWebDriver driver, String meganavItem) {
		WebElement hover;
				
		Utilities.fluentWait(By.className("profileNavItemHideMobile"), driver);		
		
		hover = driver.findElementByClassName("profileNavItemHideMobile");						
		Actions builder = new Actions(driver);		
		builder.moveToElement(hover).perform();
		
		fluentWait(By.className(meganavItem), driver);		
		builder.moveToElement(driver.
			findElement(By.className(meganavItem)))
				.clickAndHold()
					.build()
						.perform();
		
	}

	public static String getContestantName(RemoteWebDriver driver) {
		if (driver.getCurrentUrl().contains("Profile")){
			contestantName = driver.findElementById("profileNavTitle").getText();
		}
		else {
			Utilities.waitForElement(driver, By.className("Contestant"), 30);
			contestantName = driver.findElementByClassName("ContestantName").getText();
		}
		return contestantName;
	}

	
	public static boolean viewInstagramGalleryPage(RemoteWebDriver driver) {
		Utilities.waitForElement(driver, By.id("galleryHolder"), 120);
		ReusableFunc.verifyElementIsDisplayed(driver, By.id("galleryHolder"), "Instagram gallery holder");
		return true;
	}

	public static void viewContestantProfilePage(RemoteWebDriver driver) throws Exception {
		fluentWait(By.className("profileItemcopy"), driver);
		//ReusableFunc.verifyElementIsDisplayed(driver, By.className("profileItemcopy"), "profileItemcopy");
		driver.findElementByClassName("profileItemcopy").isDisplayed();
		Utilities.isTextOnPage(driver, contestantName);		
	}

	public static boolean navigateToNextContestant(RemoteWebDriver driver) throws Exception {
		Utilities.fluentWait(By.className("nav"), driver);
		driver.findElementByXPath("//*[@id='profileNavNext']/a").click();
		return true;
	}

	public static boolean viewFeed(RemoteWebDriver driver, String feedName, String textToVerify) throws Exception {		
	    if (driver.getCurrentUrl().contains(feedName))
			ReusableFunc.VerifyTextOnPage(driver, textToVerify);
		return true;			
	}

	public static boolean registerUsingConnectLiteControls(RemoteWebDriver driver) throws Exception {
		String userName =  nameGenerator("guest")+"@gmail.com";
		
		Utilities.fluentWait(By.className("connect-menu-arrow"),driver);
		
		driver.findElementByClassName("connect-menu-arrow").click();
		ReusableFunc.ClickByLinkText(driver, "New to Connect? Register");
		
		enterTextIntoTextBox(driver.findElementById("email"), userName);
		
		ReusableFunc.ClickByID(driver, "TermsAndConditions");

		enterTextIntoTextBox(driver.findElementById("password"), "123456");
		
		driver.findElementByClassName("submit-register").click();
		
		ReusableFunc.VerifyTextOnPage(driver, "Thanks for registering.");
		ReusableFunc.VerifyTextOnPage(driver, userName);
		
		Utilities.fluentWait(By.className("btn-submit"),driver);
		driver.findElementByClassName("btn-submit").click();
				
		return true;
	}

	public static boolean viewPage(RemoteWebDriver driver, String pageID) {
		Utilities.fluentWait(By.id(pageID), driver);
		driver.findElementById(pageID).isDisplayed();
		return true;
	}

	public static boolean voteOnFirstContestant(RemoteWebDriver driver) throws Exception {		
		if (driver.getCurrentUrl().contains(".com/?"))
		{
			hoverOnContestantMeganav(driver, "Contestant");
		}
		Utilities.waitForElement(driver, By.className("connect-menu-arrow"), 60);
		Thread.sleep(2000);
		driver.findElementByClassName("ContestanVoteButton").click();
		if (driver.getCurrentUrl().contains(".com/?"))
		{
			fluentWait(By.className("ContestanVoteButton"), driver);
			driver.findElementById("housemateContent")
			.findElement(By.className("confirmInfo"))
				.getText().contains("Are you sure you want to vote for");
		}
		else
		{
			fluentWait(By.className("confirmInfo"), driver);
			driver.findElementById("voteListContent")
			.findElement(By.className("confirmInfo"))
				.getText().contains("Are you sure you want to vote for");
		}		
		
		driver.findElementByClassName("ContestanVoteButton").click();
		return true;
	}

	public static boolean clickOnFirstMzansiMagicVideo(RemoteWebDriver driver) {
		Utilities.waitForElement(driver, By.id("homePageMzansiVideoWidget"), 60);
		driver.findElementById("homePageMzansiVideoWidget")
			.findElement(By.className("newsItem"))
				.findElement(By.className("standardTitle"))
					.click();
		return true;
	}

	public static boolean viewMzansiMagicVideoPlayPage(RemoteWebDriver driver) throws Exception {
		switchWindowsAndVerifyTitle(driver, "mzansi");
		return true;
	}

	public static boolean logIntoPageUsingFacebookOnConnect(
			RemoteWebDriver driver) throws Exception {
		Utilities.isTextPresentAfterWait("Login", driver);
		driver.findElementByClassName("connect-menu-arrow").click();
		Thread.sleep(2000);
		Utilities.isTextPresentAfterWait("New to Connect? Register", driver);
		ReusableFunc.ClickByXpath(driver, "//*[@id='connect']/div/div[2]/div/div[2]/div/div/img[3]", "Facebook image");
		Utilities.fluentWait(By.className("fb_logo"), driver);
		ConnectControl.connectLiteLoginFacebook(driver,
				"firstylaster@gmail.com", "password1*", "KiethPatel");	
		return true;
	}

	public static boolean logIntoPageUsingGoogleOnConnect(RemoteWebDriver driver) throws Exception {
		Utilities.isTextPresentAfterWait("Login", driver);
		driver.findElementByClassName("connect-menu-arrow").click();
		Thread.sleep(2000);
		Utilities.isTextPresentAfterWait("New to Connect? Register", driver);
		ReusableFunc.ClickByXpath(driver, "//*[@id='connect']/div/div[2]/div/div[2]/div/div/img[2]", "Google+ image");		
		
		Utilities.fluentWait(By.className("logo"), driver);
		ConnectControl.connectLiteLoginGoogle(driver,
				"firstylaster@gmail.com", "password1*", "KiethPatel");	
		return true;
	}

	public static boolean logIntoPageUsingYahooOnConnect(RemoteWebDriver driver) throws Exception {
		Utilities.isTextPresentAfterWait("Login", driver);
		driver.findElementByClassName("connect-menu-arrow").click();
		Thread.sleep(2000);
		Utilities.isTextPresentAfterWait("New to Connect? Register", driver);
		ReusableFunc.ClickByXpath(driver, "//*[@id='connect']/div/div[2]/div/div[2]/div/div/img[3]", "Yahoo image");		
		
		Utilities.fluentWait(By.className("logo"), driver);
		ConnectControl.connectLiteLoginGoogle(driver,
				"firstylaster@yahoo.com", "password1*", "KiethPatel");	
		return true;
	}

	public static WebElement fluentWait(final By locator, RemoteWebDriver rwd){
	      Wait<WebDriver> wait = new FluentWait<WebDriver>(rwd)
	              .withTimeout(300, TimeUnit.SECONDS)
	              .pollingEvery(5, TimeUnit.SECONDS)
	              .ignoring(NoSuchElementException.class);

	      WebElement webElement = wait.until(
	    		  new Function<WebDriver, WebElement>() {
	    			  public WebElement apply(WebDriver driver) {
	                      return driver.findElement(locator);
	    			  }
	              }
	    	);
	        return  webElement;              
	        };
}