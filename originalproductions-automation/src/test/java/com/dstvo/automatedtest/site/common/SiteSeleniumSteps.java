package com.dstvo.automatedtest.site.common;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.BeforeStories;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.beans.factory.annotation.Value;

public class SiteSeleniumSteps {
	private WebDriver driver;

	@Value("${test.baseUrl}")
	private String baseUrl;

	@Value("${test.seleniumServerUrl}")
	private String seleniumServerUrlString;

	private StringBuffer verificationErrors = new StringBuffer();
	private DesiredCapabilities capabilities;
	private URL seleniumServerUrl;

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	@BeforeStories
	public void setUp() throws IOException, InterruptedException,
			URISyntaxException {
		capabilities = DesiredCapabilities.chrome();
		capabilities.setJavascriptEnabled(true);
		seleniumServerUrl = new URL(seleniumServerUrlString);
	}

	@BeforeScenario
	public void setUpScenario() throws MalformedURLException {
		System.setProperty("webdriver.chrome.driver", "C:\\selenium\\chromedriver.exe");
		driver = new ChromeDriver(capabilities);
	}

	@AfterScenario
	public void tearDown() {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}
}
