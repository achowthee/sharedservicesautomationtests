package com.dstvo.automatedtest.site.opweb.story;

import java.util.List;

import org.jbehave.core.annotations.Named;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.dstvo.automatedtest.site.common.OriginalProductionsFunctionLibrary;
import com.dstvo.automatedtest.site.opweb.story.helpers.WebsiteLink;
import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.dstvo.test.automation.dstvfunctionlib.Utilities;

public class OPWebHomePage {
	
	public static boolean isBillboardDisplayed(WebDriver driver) {
		OriginalProductionsFunctionLibrary.waitForElement(driver, By.id("billboard"));
		return driver.findElement(By.className("billboardBack")).isDisplayed();
	}

	public static String getBillboardTitleDisplayed(WebDriver driver, int index) {
		List <WebElement> billboardSlides = driver.findElements(By.xpath("//div[@id='billboardSlides']/div[*]")); 
		for (WebElement billboard : billboardSlides)
		{
			if (!billboard.getAttribute("style").equals("display: none;")) {
				return billboard.findElement(By.className("billboardText")).findElement(By.className("billboardTextHeading")).getText();
			}
		}
		return "No active billboard found !";//driver.findElement(By.id(String.format("billboardTitle%1$d",index))).getText();	
	}

	public static boolean isBillboardLogoDisplayed(WebDriver driver) {
		return driver.findElement(By.id("productLogo0")).isDisplayed();
	}

	public static boolean verifySiteLinks(WebDriver driver, WebsiteLink[] links) throws Exception {
		for (WebsiteLink link : links) {			
			OriginalProductionsFunctionLibrary.clickLink(driver, link.getTitle());
			OriginalProductionsFunctionLibrary.switchWindowsAndVerifyTitle(driver, link.getUrl());
		}
		return true;
	}

	public static void clickOnTheSecondBillboardThumbnail(WebDriver driver) throws Exception {
		driver.findElement(By.id("thumbnail1")).click();				
	}

	public static void verifyThatBillboardIsDisplayed(WebDriver driver, String billboardTitle) throws Exception {
		OriginalProductionsFunctionLibrary.verifyTextOnPage(driver, billboardTitle);
	}

	public static void clickOnTheBillboardLink(WebDriver driver) throws Exception {
		driver.findElement(By.id("thumbnail0")).click(); 
		OriginalProductionsFunctionLibrary.verifyTextOnPage(driver, "Joburg Week 1");
		OriginalProductionsFunctionLibrary.clickLink(driver,".billboardText");
	}

	public static void clickOnTheFirstNewsArtcle(WebDriver driver) {			
		if (driver.getCurrentUrl().contains("mobile")) {
			driver.findElement(By.className("Heading")).click();
		}
		else {					
			driver.findElement(By.className("newsItem")).click();
		}	
	}

	public static boolean  verifyLinks(RemoteWebDriver driver, String link,
			String url) throws Exception {
		WebsiteLink[] channelLogoLinks = {
				new WebsiteLink(link, url)			
		};
		if (OPWebHomePage.verifySiteLinks(driver, channelLogoLinks)){
			return true;
		}
		return false;
	}
	
	public static void verifyVideosListedOnHomepageVideoWidget(WebDriver driver) {	
		OriginalProductionsFunctionLibrary.fluentWait(By.className("vidoeTitle"), (RemoteWebDriver)driver);
		ReusableFunc.verifyElementIsDisplayed((RemoteWebDriver) driver, By.className("vidoeItem"), "The first video");
	}

	public static void clickOnVideoListed(
			WebDriver driver) throws Exception {
		if (driver.getCurrentUrl().contains("video")){
			OriginalProductionsFunctionLibrary.fluentWait(By.className("standardTitle"), (RemoteWebDriver)driver);
			List<WebElement> videoList = driver.findElements(By.className("standardTitle"));
			videoList.get(1).click();
		}
		else {
			OriginalProductionsFunctionLibrary.clickLink(driver, ".vidoeTitle");
		}		
	}
	
	public static boolean clickOnMoreNews(RemoteWebDriver driver) throws Exception {
		if (driver.getCurrentUrl().contains("mobile")){
			ReusableFunc.ClickByID(driver, "ContentPlaceHolderBody_LatestNews_hlMore");
			return true;
		}
		else {
			ReusableFunc.ClickByID(driver, "articleWidgetMore");
			return true;
		}		
	}

	public static void viewFirstArticleOnArticlePage(RemoteWebDriver driver) {
		if (driver.getCurrentUrl().contains("mobile")) {
			ReusableFunc.verifyElementIsDisplayed(driver, By.className("Synopsis"), "Synopsis");
		}
		else {
			ReusableFunc.verifyElementIsDisplayed(driver, By.className("newsItem"), "newsItem");
		}			
	}

	public static boolean viewVideoOnVideoPage(RemoteWebDriver driver) {
		ReusableFunc.verifyElementIsDisplayed(driver, By.id("flashcontent"), "flashcontent");
		ReusableFunc.verifyElementIsDisplayed(driver, By.className("videoContent"), "videoContent");
		return true;
	}

	public static boolean viewPollQuestion(RemoteWebDriver driver) {
		ReusableFunc.verifyElementIsDisplayed(driver, By.className("pollQuestion"), "pollQuestion");
		return true;
	}

	public static boolean clickOnFirstPollOption(RemoteWebDriver driver) throws Exception {
		driver.findElementByClassName("pollAnswer").click();
		return true;
	}

	public static boolean viewPollResults(RemoteWebDriver driver) {
		ReusableFunc.verifyElementIsDisplayed(driver, By.id("pollResults"), "pollResults");
		return true;	
	}

	public static boolean viewArticleListOnMobile(RemoteWebDriver driver) {
		List<WebElement> articleList = driver.findElements(By.className("itemWrapper"));
		for (int count = 0; count<5; count ++){
			articleList.get(count).findElement(By.className("Synopsis"));			
		}
		return true;
	}

	public static boolean verifyWebsiteLogoExists(WebDriver driver) {
		OriginalProductionsFunctionLibrary.fluentWait(By.className("navlogo"), (RemoteWebDriver)driver);
		driver.findElement(By.className("navlogo")).isDisplayed();
		return true;
	}

	public static boolean logOutOfConnect(RemoteWebDriver driver) throws Exception {
		Utilities.fluentWait(By.className("connect-menu-arrow"), driver);
		ConnectControl.connectLogout(driver);
		return true;		
	}

	public static boolean verifySuccessfulVoteMessage(RemoteWebDriver driver) {
		OriginalProductionsFunctionLibrary.fluentWait(By.className("ThankYOuInfo"), driver);
		driver.findElementByClassName("ThankYOuInfo").isDisplayed();
		return true;
	}

}