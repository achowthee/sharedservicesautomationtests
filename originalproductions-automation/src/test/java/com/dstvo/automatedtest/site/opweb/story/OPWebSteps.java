package com.dstvo.automatedtest.site.opweb.story;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.dstvo.automatedtest.site.common.OriginalProductionsFunctionLibrary;
import com.dstvo.automatedtest.site.common.SiteSeleniumSteps;
import com.dstvo.test.automation.dstvfunctionlib.*;
import com.thoughtworks.selenium.Selenium;

@Component
public class OPWebSteps extends SiteSeleniumSteps {
	
	@Value("${test.baseUrl}")
	public String baseUrl;

		
	//Given statements
	@Given("I go to url $url")
	public void viewPage(String url) {
		getDriver().get(baseUrl + url);;
	}
	@Given("I resize my browser to $x, $y")
	public void resizeBrowser(int x, int y) {
		getDriver().manage().window().setSize(new Dimension(x, y));
		getDriver().navigate().refresh();
	}
	@Given("I maximize my browser")
	public void maximizeBrowser() {
		getDriver().manage().window().maximize();
		getDriver().navigate().refresh();
	}
	@Given("I am not logged into Connect")
	public void clickOnConnectLoginButton() throws Exception{
		OriginalProductionsFunctionLibrary.logoutOfConnect(getDriver());
	}
	
	
	//When statements
	@When("I can see the website logo")
	public void verifyWebsiteLogoExists(){		
		Assert.assertTrue(OPWebHomePage.verifyWebsiteLogoExists(getDriver()));
	}
	@When("the billboard is visible")
	public void checkThatBillboardIsDisplayed(){		
		Assert.assertTrue(OPWebHomePage.isBillboardDisplayed(getDriver()));
	}		
	@When("I click on the second billboard thumbnail")
	public void clickOnBillboardThumbnail() throws Exception{
		OPWebHomePage.clickOnTheSecondBillboardThumbnail(getDriver());
	}
	@When("I click on the billboard link")
	public void clickOnBillboardLink() throws Exception{
		OPWebHomePage.clickOnTheBillboardLink(getDriver());
	}
	@When("I search on the home page for the text $searchWord")
	public void searchOnHomePage(String searchWord) throws Exception{
		Assert.assertTrue(OriginalProductionsFunctionLibrary.searchOnHomePage((RemoteWebDriver) getDriver(), searchWord));
	}
	@When("I click on the first news article")
	public void clickOnTheFirstNewsArtcle(){
		OPWebHomePage.clickOnTheFirstNewsArtcle(getDriver());
	}
	@When("I can view the first article title")
	public void viewTheFirstNewsArtcleTitle(){
		Assert.assertTrue(OriginalProductionsFunctionLibrary.viewTheFirstNewsArtcleTitle((RemoteWebDriver) getDriver()));
	}
	@When("I click on more news...")
	public void clickOnMoreNews() throws Exception{
		Assert.assertTrue(OPWebHomePage.clickOnMoreNews((RemoteWebDriver) getDriver()));
	}	
	@When("I click on More...")
	public void clickOnMoreNewsForMobile() throws Exception{
		Assert.assertTrue(OPWebHomePage.clickOnMoreNews((RemoteWebDriver) getDriver()));
	}
	@When("I update my Connect  details")
	public void updateMyConnectDetails() throws Exception{
		Assert.assertTrue(OriginalProductionsFunctionLibrary.updateMyConnectDetails((RemoteWebDriver) getDriver()));
	}
	@When("I log into Connect using the Connect Login button with $connectEmailAddress and $connectPassword")
	public void logInUsingConnect(String connectEmailAddress, String connectPassword) throws Exception {		
		OriginalProductionsFunctionLibrary.logIntoConnectLiteControls(getDriver(),connectEmailAddress, connectPassword);
	}
	@When("I can see the first listed video on the homepage video widget")
	public void verifyVideosListedOnHomepageVideoWidget() throws Exception {
		OPWebHomePage.verifyVideosListedOnHomepageVideoWidget(getDriver());
	}
	@When("I click on a listed video")
	public void clickOnFirstVideoListedOnHomepageVideoWidget() throws Exception {
		OPWebHomePage.clickOnVideoListed(getDriver());
	}
	@When("I get the title of the first video")
	public void getTheFirstVideoTitleOnVideoPage() throws Exception {
		OriginalProductionsFunctionLibrary.getTheFirstVideoTitle((RemoteWebDriver) getDriver());
	}
	@When("I click on more videos...")
	public void clickOnMoreVideos() throws Exception{
		ReusableFunc.ClickByID((RemoteWebDriver) getDriver(), "videoWidgetMore");
	}
	@When("I can view the first video title")
	public void viewTheFirstVideoTitle(){
		Assert.assertTrue(OriginalProductionsFunctionLibrary.viewTheFirstVideoTitle((RemoteWebDriver) getDriver()));
	}
	@When("I click on the first Mzansi Magic video on the homepage")
	public void clickOnFirstMzansiMagicVideo(){
		Assert.assertTrue(OriginalProductionsFunctionLibrary.clickOnFirstMzansiMagicVideo((RemoteWebDriver) getDriver()));
	}
	@When("I can click on the Recommend button")
	public void clickOnRecommend() throws Exception{		
		Assert.assertTrue(OriginalProductionsFunctionLibrary.clickOnShare((RemoteWebDriver) getDriver()));
	}
	@When("I click on the first contestant in the Meganav")
	public void hoverAndClickOnNewsMegaNav() throws Exception{		
		Assert.assertTrue(OriginalProductionsFunctionLibrary.hoverAndClickOnContestantMegaNav((RemoteWebDriver) getDriver()));
	}
	@When("I navigate to the next contestant")
	public void navigateToNextContestant() throws Exception{		
		Assert.assertTrue(OriginalProductionsFunctionLibrary.navigateToNextContestant((RemoteWebDriver) getDriver()));
	}
	@When("I vote on the first contestant in the Meganav")
	public void voteOnFirstContestantInMegaNav() throws Exception{		
		Assert.assertTrue(OriginalProductionsFunctionLibrary.voteOnFirstContestant((RemoteWebDriver) getDriver()));
	}	
	@When("I vote on the first contestant on the vote page")
	public void voteOnFirstContestantOnVotePage() throws Exception{		
		Assert.assertTrue(OriginalProductionsFunctionLibrary.voteOnFirstContestant((RemoteWebDriver) getDriver()));
	}
	@When("I can log into Facebook")
	public void logIntoFacebook() throws Exception{
		OriginalProductionsFunctionLibrary.logIntoFacebook((RemoteWebDriver) getDriver(), "firstylaster@gmail.com", "password1*", "You are logged in as");
	}
	@When("I can see the Judges & Presenters title")
	public void viewProfilePageTitle() throws Exception{
		Assert.assertTrue(OriginalProductionsFunctionLibrary.viewProfilePageTitle((RemoteWebDriver) getDriver()));
	}
	@When("I click on the first judge")
	public void clickOnTheFirstjudge() throws Exception{
		ReusableFunc.ClickByID((RemoteWebDriver) getDriver(), "mainContent_mnuGareth");
	}
	@When("I click on the Facebook share icon")
	public void clickOnTheFacebookShareIcon() throws Exception{
		ReusableFunc.ClickByXpath((RemoteWebDriver) getDriver(), ".//*[contains(@id, 'articleWidget')]", "Facebook Login");
	}

	@When("I click on the first image")
	public void clickOnFirstInstagramImage() throws Exception{
		Assert.assertTrue(OriginalProductionsFunctionLibrary.clickOnFirstInstagramImage((RemoteWebDriver) getDriver()));
	}	
	@When("I click on more images...")
	public void clickOnMoreImages() throws Exception{
		ReusableFunc.ClickByID((RemoteWebDriver) getDriver(), "galleryWidgetMore");
	}
	@When("I can see the poll question")
	public void viewPollQuestion() throws Exception{
		Assert.assertTrue(OPWebHomePage.viewPollQuestion((RemoteWebDriver) getDriver()));
	}
	@When("I can click on the first option in the poll")
	public void clickOnFirstPollOption() throws Exception{
		Assert.assertTrue(OPWebHomePage.clickOnFirstPollOption((RemoteWebDriver) getDriver()));
	}
	
	
	//Then statements
	@Then("the title of the page must be $title")
	public void checkPageTitle(String title) {
		Assert.assertEquals("The page titles do not match.", title, getDriver().getTitle().subSequence(0, 35).toString());
	}	
	@Then("I can see the billboard heading $heading")
	public void checkBillboardTextIsDisplayed(String billboardTitle) {
		Assert.assertEquals(billboardTitle,OPWebHomePage.getBillboardTitleDisplayed(getDriver(), 0));
	}	
	@Then("the Idols logo is displayed")
	public void checkThatBillboardLogoIsDisplayed() {
		Assert.assertTrue(OPWebHomePage.isBillboardLogoDisplayed(getDriver()));
	}		
	@Then("I can view the search results")
	public void viewSearchResults(){
		Assert.assertTrue(OriginalProductionsFunctionLibrary.viewSearchResults((RemoteWebDriver) getDriver()));
	}
	@Then("I can see the $feedName feed and the text of $textToVerify")
	public void viewFeed(String feedName, String textToVerify) throws Exception{
		Assert.assertTrue(OriginalProductionsFunctionLibrary.viewFeed((RemoteWebDriver) getDriver(), feedName, textToVerify));
	}
		
	@Then("I can register using the connect controls")
	public void registerOnConnect() throws Exception{
		Assert.assertTrue(OriginalProductionsFunctionLibrary.registerUsingConnectLiteControls((RemoteWebDriver) getDriver()));
	}
	@Then("I am logged into Connect")
	public void verifyThatIamLoggedIntoConnect() throws Exception{
		Assert.assertTrue(OriginalProductionsFunctionLibrary.verifyTextOnPage(getDriver(), "Logged in as"));		
	}
	@Then("I can log into Connect using Facebook")
	public void logIntoPageUsingFacebookOnConnect() throws Exception{	
		Assert.assertTrue(OriginalProductionsFunctionLibrary.logIntoPageUsingFacebookOnConnect((RemoteWebDriver) getDriver()));		
	}
	@Then("I can log into Connect using Google")
	public void logIntoPageUsingGoogleOnConnect() throws Exception{
		Assert.assertTrue(OriginalProductionsFunctionLibrary.logIntoPageUsingGoogleOnConnect((RemoteWebDriver) getDriver()));		
	}
	@Then("I can log into Connect using Yahoo")
	public void logIntoIdolsPageUsingYahooOnConnect() throws Exception{
		Assert.assertTrue(OriginalProductionsFunctionLibrary.logIntoPageUsingYahooOnConnect((RemoteWebDriver) getDriver()));		
	}
	@Then("I can view my updated Connect details")
	public void viewUpdatedConnectDetails() throws Exception{		
		Assert.assertTrue(OriginalProductionsFunctionLibrary.viewUpdatedConnectDetails((RemoteWebDriver) getDriver()));
	}
	@Then("I log out of Connect")
	public void logOutOfConnect() throws Exception{
		Assert.assertTrue(OPWebHomePage.logOutOfConnect((RemoteWebDriver) getDriver()));
	}
	@Then("I receive a vote success message")
	public void verifySuccessfulVoteMessage() throws Exception{
		Assert.assertTrue(OPWebHomePage.verifySuccessfulVoteMessage((RemoteWebDriver) getDriver()));
	}
	
	@Then("I can view the ID of $pageId")
	public void viewVIPVideoPage(String pageId) throws Exception{		
		Assert.assertTrue(OriginalProductionsFunctionLibrary.viewPage((RemoteWebDriver) getDriver(), pageId));
	}
	@Then("I can view the video on the video page")
	public void viewFirstVideoOnVideoPage() throws Exception{		
		Assert.assertTrue(OPWebHomePage.viewVideoOnVideoPage((RemoteWebDriver) getDriver()));
	}
	@Then("I can view the active category")
	public void viewActiveCategory() throws Exception{		
		Assert.assertTrue(OriginalProductionsFunctionLibrary.viewActiveCategory((RemoteWebDriver) getDriver()));
	}
	@Then("I can compare the first video title on the homepage")
	public void verifyFirstVideoItemTitleIsSameAsHomePageTitle(){
		Assert.assertTrue(OriginalProductionsFunctionLibrary.verifyFirstVideoItemTitleIsSameAsHomePageTitle((RemoteWebDriver) getDriver()));
	}
	@Then("I can view the first news article")
	public void viewFirstArticleOnArticlePage() throws Exception{		
		OPWebHomePage.viewFirstArticleOnArticlePage((RemoteWebDriver) getDriver());
	}
	@Then("I can compare the first article title on the homepage")
	public void verifyFirstNewsItemTitleIsSameAsHomePageTitle(){
		Assert.assertTrue(OriginalProductionsFunctionLibrary.verifyFirstNewsItemTitleIsSameAsHomePageTitle((RemoteWebDriver) getDriver()));
	}
	@Then("I can view a list of news articles")
	public void viewArticleListOnMobile() throws Exception{		
		Assert.assertTrue(OPWebHomePage.viewArticleListOnMobile((RemoteWebDriver) getDriver()));
	}
	@Then("I can share the news article")
	public void shareArticleOnArticlePage() throws Exception{		
		OriginalProductionsFunctionLibrary.ClickByLinkText(getDriver(), "Post to Facebook");
	}
	@Then("I can see the navigation item selected is $navigationItem")
    public void checkIfNavigationIsSelected(String navigationItem) {
		Assert.assertTrue(OriginalProductionsFunctionLibrary.checkIfNavigationIsSelected((RemoteWebDriver) getDriver(), navigationItem));
    }	
	@Then("I can view the social popup with the title $PageTitle")
    public void viewSocialPopupPageTitle(String pageTitle) throws Exception {
		Assert.assertTrue(OriginalProductionsFunctionLibrary.viewSocialPopupPageTitle((RemoteWebDriver) getDriver(), pageTitle));
    }	
	@Then("I can view the poll results")
	public void viewPollResults() throws Exception{		
		Assert.assertTrue(OPWebHomePage.viewPollResults((RemoteWebDriver) getDriver()));
	}	
	@Then("I can view the text $text")
	public void viewTextOnPage(String textOnPage) throws Exception{		
		ReusableFunc.VerifyTextOnPage((RemoteWebDriver) getDriver(), textOnPage);
	}	
	@Then("I can view the profile")
	public void viewJudgesAndPresentersPage() throws Exception{		
		OriginalProductionsFunctionLibrary.viewJudgesAndPresentersPage((RemoteWebDriver) getDriver());
	}
	@Then("I can view the contestant profile details")
	public void viewContestantProfilePage() throws Exception{		
		OriginalProductionsFunctionLibrary.viewContestantProfilePage((RemoteWebDriver) getDriver());
	}
	@Then("I can view the image in a modal")
	public void viewInstagramImageInModal() throws Exception{		
		Assert.assertTrue(OriginalProductionsFunctionLibrary.viewInstagramImageInModal((RemoteWebDriver) getDriver()));
	}
	@Then("I can view the Instagram gallery page")
	public void viewInstagramGalleryPage() throws Exception{		
		Assert.assertTrue(OriginalProductionsFunctionLibrary.viewInstagramGalleryPage((RemoteWebDriver) getDriver()));
	}
	@Then("I can see the Mzansi Magic video play page")
	public void viewMzansiMagicVideoPlayPage() throws Exception{		
		Assert.assertTrue(OriginalProductionsFunctionLibrary.viewMzansiMagicVideoPlayPage((RemoteWebDriver) getDriver()));
	}
	@Then("I can verify the links")
	public void verifyLinks(@Named("link") String link,
			@Named("url") String url) throws Exception {
		Assert.assertTrue(OPWebHomePage.verifyLinks((RemoteWebDriver) getDriver(), link, url));
	}
}