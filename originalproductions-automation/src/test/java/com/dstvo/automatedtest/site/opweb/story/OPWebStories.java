package com.dstvo.automatedtest.site.opweb.story;

import java.net.URL;
import java.util.Arrays;
import java.util.List;

import org.jbehave.core.Embeddable;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.io.StoryPathResolver;
import org.jbehave.core.io.UnderscoredCamelCaseResolver;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.spring.SpringApplicationContextFactory;
import org.jbehave.core.steps.spring.SpringStepsFactory;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class OPWebStories extends JUnitStories {
	
	public OPWebStories() {
		// this will be overridden by maven plugin
		configuredEmbedder().embedderControls()
				.doGenerateViewAfterStories(false)
				.doIgnoreFailureInStories(false).doIgnoreFailureInView(false)
				.doVerboseFailures(true).useThreads(2)
				.useStoryTimeoutInSecs(3600);
	}

	@Override
	public Configuration configuration() {
		Class<? extends Embeddable> embeddableClass = this.getClass();
		
		//Create Story Reporter Builder
		 StoryPathResolver resolver = new UnderscoredCamelCaseResolver();
		 String storyPath = resolver.resolve(embeddableClass);
		 URL url = CodeLocations.codeLocationFromClass(embeddableClass);
		 System.out.println("***Stories path " + url.getPath());
		StoryReporterBuilder reporter = new StoryReporterBuilder().withCodeLocation(url)
		         .withDefaultFormats().withFormats(Format.HTML, Format.XML, Format.CONSOLE );
		
		return new MostUsefulConfiguration().useStoryLoader(
				new LoadFromClasspath(embeddableClass))
				.useStoryReporterBuilder(reporter);
	}
	
	@Override
	public InjectableStepsFactory stepsFactory() {
		return new SpringStepsFactory(configuration(), createContext());
	}

	protected ApplicationContext createContext() {
		return new SpringApplicationContextFactory("stepsContext.xml")
				.createApplicationContext();
	}

	@Override
	protected List<String> storyPaths() {
		StoryFinder sf = new StoryFinder();
		String[] includes = { "*.story" };
		String directory = CodeLocations.codeLocationFromClass(this.getClass()).getFile()+"stories";
		directory = directory.replace("%20", " ");
		
		System.out.println("CODE LOCATION IS :" + directory);
		
		List<String> listStrings = sf.findPaths(directory,
					(List<String>) Arrays.asList(includes), null, "stories/");
		System.out.println("STORY IS :"+listStrings.toString());
		return listStrings;
	}

}
