package com.dstvo.automatedtest.site.opweb.story.helpers;

public class WebsiteLink {

	private String title;
	private String url;
	private String expectedPageTitle;
	private String expectedTextOnpage;
	
	public WebsiteLink() {		
	}
	
	public WebsiteLink(String title, String url) {
		this.title = title;
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String textOnPage) {
		this.title = textOnPage;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getExpectedPageTitle() {
		return expectedPageTitle;
	}

	public void setExpectedPageTitle(String expectedPageTitle) {
		this.expectedPageTitle = expectedPageTitle;
	}

	public String getExpectedTextOnpage() {
		return expectedTextOnpage;
	}

	public void setExpectedTextOnpage(String expectedTextOnpage) {
		this.expectedTextOnpage = expectedTextOnpage;
	}

}
