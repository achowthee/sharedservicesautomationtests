Narrative:
In order to see Big Brother Mzansi website
As a user
I want to be able to use the Big Brother Mzansi website

Scenario: Search the website for the keyword "BBM"
Given I go to url bigbrothermzansi.dstv.com
When I search on the home page for the text BBM
Then I can view the search results

Scenario: Click the first option from the poll
Given I go to url bigbrothermzansi.dstv.com
When I can see the poll question
And I can click on the first option in the poll
Then I can view the poll results

Scenario: Viewing the billboard
Given I go to url bigbrothermzansi.dstv.com
When I can see the website logo
Then I can see the billboard heading Billboard1
When I click on the second billboard thumbnail
Then I can see the billboard heading Billboard2
When I click on the billboard link
Then the title of the page must be Speedhunters - Car Culture at Large

Scenario: View the Contestant details from the navigation in the MegaNav
Given I go to url bigbrothermzansi.dstv.com
And I maximize my browser
When I click on the first contestant in the Meganav
Then I can view the contestant profile details
When I navigate to the next contestant
Then I can view the contestant profile details

Scenario: Vote for the first contestant on the meganav
Given I go to url bigbrothermzansi.dstv.com
And I maximize my browser
And I am not logged into Connect
When I log into Connect using the Connect Login button with firstylaster@gmail.com and password1*
And I vote on the first contestant in the Meganav
Then I receive a vote success message
And I log out of Connect

Scenario: Vote for the first contestant on the vote page
Given I go to url bigbrothermzansi.dstv.com/vote
And I am not logged into Connect
When I log into Connect using the Connect Login button with firstylaster@gmail.com and password1*
And I vote on the first contestant on the vote page
Then I receive a vote success message
And I log out of Connect

Scenario: Register on Connect
Given I go to url bigbrothermzansi.dstv.com
And I am not logged into Connect
Then I can register using the connect controls

Scenario: login using Facebook
Given I go to url bigbrothermzansi.dstv.com
And I am not logged into Connect
Then I can log into Connect using Facebook

Scenario: login using Google
Given I go to url bigbrothermzansi.dstv.com
And I am not logged into Connect
Then I can log into Connect using Google

Scenario: Log into Connect and update details
Given I go to url bigbrothermzansi.dstv.com
And I am not logged into Connect
When I log into Connect using the Connect Login button with afrifocus1@gmail.com and 123456
And I update my Connect  details
Then I can view my updated Connect details
And I log out of Connect

Scenario: Log into BBA VIP with Kieth Patel
Given I go to url bigbrothermzansi.dstv.com/VIP
When I log into Connect using the Connect Login button with firstylaster@gmail.com and password1*
Then I can view the ID of vipVideoCategories
And I log out of Connect

Scenario: View videos from homepage video widget
Given I go to url bigbrothermzansi.dstv.com
When I can see the first listed video on the homepage video widget
And I click on a listed video
Then I can view the video on the video page
And I can view the active category
When I click on a listed video
Then I can view the video on the video page
And I can see the navigation item selected is Video

Scenario: Recommend the first video on the Video page
Given I go to url bigbrothermzansi.dstv.com
When I can see the first listed video on the homepage video widget
And I click on a listed video
Then I can view the video on the video page
When I can click on the Recommend button
Then I can view the social popup with the title Facebook

Scenario: Ensure that the first video on the homepage is the first video displayed on the video page
Given I go to url bigbrothermzansi.dstv.com
When I can view the first video title
And I click on more videos...
Then I can compare the first video title on the homepage

Scenario: View the first news article on article page
Given I go to url bigbrothermzansi.dstv.com
When I can see the website logo
And I click on the first news article
Then I can view the first news article

Scenario: Ensure that the first article on the homepage is the first article displayed on the news page
Given I go to url bigbrothermzansi.dstv.com
When I can view the first article title
And I click on more news...
Then I can compare the first article title on the homepage

Scenario: I want to view the first Mzansi Magic video page
Given I go to url bigbrothermzansi.dstv.com
When I click on the first Mzansi Magic video on the homepage
Then I can see the Mzansi Magic video play page

Scenario: Verify that search engines can access our site
Given I go to url bigbrothermzansi.dstv.com/?_escaped_fragment_
When I can see the website logo

Scenario: I want to access the profile feed
Given I go to url bigbrothermzansi.dstv.com/feeds/profile
Then I can see the profile feed and the text of Alusa

Scenario: I want to access the article feed
Given I go to url bigbrothermzansi.dstv.com/feeds/article
Then I can see the article feed and the text of BBM

Scenario: I want to access the Video feed
Given I go to url bigbrothermzansi.dstv.com/feeds/video?pageIndex=0&pageSize=200
Then I can see the video feed and the text of Kay

Scenario: Viewing the first image on Instagram gallery
Given I go to url bigbrothermzansi.dstv.com
When I click on the first image
Then I can view the image in a modal
When I click on more images...
Then I can view the Instagram gallery page
When I click on the first image
Then I can view the image in a modal

Scenario: Go to channel pages
Given I go to url bigbrothermzansi.dstv.com
When I can see the website logo
Then I can verify the links
Examples:     
|link			|url            |
|.logo-mzansi   |mzansimagic    |

Scenario: Go to social media links
Given I go to url bigbrothermzansi.dstv.com
When I can see the website logo
Then I can verify the links
Examples:     
|link           |url                        					|
|.logo-facebook |https://www.facebook.com/BBmzansi				|
|.logo-youtube  |https://www.youtube.com/user/bigbrothermzansi	|
|.logo-twitter  |https://twitter.com/BBMzansi					|
|.logo-instagram|https://instagram.com/BigBroMzansi				|

