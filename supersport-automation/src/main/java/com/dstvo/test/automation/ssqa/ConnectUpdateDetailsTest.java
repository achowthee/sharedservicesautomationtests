package com.dstvo.test.automation.ssqa;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;
import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ConnectUpdateDetailsTest extends TestBase {

	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}
	
	/*@Test
	public void updateDetailsAmernia() throws Exception {
		Thread.sleep(3000);
		ConnectControl.responsiveConnectLogin(driver, "bnptonylou@yahoo.com", "123456");
		Thread.sleep(1000);
		ConnectControl.UpdateMyDetails(driver, "queenbee", "beehive");
		//Select Country
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[contains(@id, 'Details1_ddlCountry')]"), By.xpath("/html/body/form/div[8]/div/div/div[2]/div[2]/div[2]/div[8]/div/div[2]/select/option[13]"));
		Thread.sleep(1000);
		if(!driver.findElementById("ContentPlaceHolder1_Details1_ddlProvince").isDisplayed());
   		System.out.println("Your country has been set with no provinces");
		ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'Details1_btnSave')]", "Unable to locate Save Details button");
		ReusableFunc.ClickByLinkText(driver, "Logout");*/
		
		
		
	
	
	@Test
	public void UpdateMyDetailsSouthAfrica() throws Exception {
		Thread.sleep(3000);
		ConnectControl.responsiveConnectLogin(driver, "bnplou@gmail.com", "123456");
		Thread.sleep(1000);
		ConnectControl.UpdateMyDetails(driver, "belzzzzz", "Ndlovu");
		//Select Country
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[contains(@id, 'Details1_ddlCountry')]"), By.xpath("/html/body/form/div[8]/div/div/div[2]/div[2]/div[2]/div[8]/div/div[2]/select/option[205]"));
		//Input a Province
		ReusableFunc.ClickDropdownElement(driver, By.xpath(".//*[contains(@id, 'Details1_ddlProvince')]"), By.xpath("/html/body/form/div[8]/div/div/div[2]/div[2]/div[2]/div[9]/div/div/select/option[3]"));
		ReusableFunc.ClickByXpath(driver, ".//*[contains(@id, 'Details1_btnSave')]", "Unable to locate Save Details button");
		Thread.sleep(1000);
		ReusableFunc.ClickByLinkText(driver, "Logout");
		
		
	}

}
