package com.dstvo.test.automation.ssqa;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class HomePageTest extends TestBase {
 
  @BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

	}

  
  @Test
  public void homepageIsdisplayed() throws Exception{
	  	  ReusableFunc.VerifyTextOnPage(driver, "Breaking News");
	  
  }
  
  @Test
  public void clickOnFootballTab() throws Exception{
  ReusableFunc.ClickByXpath(driver, ".//*[@id='mainnav']/li[3]/a", "Football");
  Thread.sleep(3000);
  ReusableFunc.CompareUrls(driver, "http://qa.supersport.com/football");
  ReusableFunc.ContainsPageTitle(driver, "Football");
  
  }
  
}