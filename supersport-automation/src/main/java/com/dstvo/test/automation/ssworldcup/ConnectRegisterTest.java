package com.dstvo.test.automation.ssworldcup;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ConnectControl;

public class ConnectRegisterTest extends TestBase{
	
	 @BeforeClass
	 @Override
		public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());

		}

	
	 @Test
	 public void Register() throws Exception {
		
		 ConnectControl.responsiveConnectRegister(driver);
		 Thread.sleep(1000);
	 
	 }
	 
	 @Test
	 public void RegisterIgnoreTerms()  throws Exception {
		 ConnectControl.ConnectRegisterIgnoreTerms(driver);
		 Thread.sleep(1000);
		 
		 
	 }
	 
	
}
