package com.dstvo.test.automation.tvodSupport;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class Rentals extends TestBase {
	
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}
	
	@Test
	public void signIn() throws Exception {
		ReusableFunc.VerifyTextOnPage(driver, "sign in");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath("html/body/div[1]/div/div[1]/div/form/input[1]"), "belindaAdmin@dstvo.com");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath("html/body/div[1]/div/div[1]/div/form/input[2]"), "123456");
		ReusableFunc.CheckCheckBox(driver, driver.findElementByXPath("html/body/div[1]/div/div[1]/div/form/label/input"));
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div/div[1]/div/form/button", "sign in button");
		ReusableFunc.VerifyTextOnPage(driver, "Sign Out");
	}
	
	@Test(dependsOnMethods = {"signIn"})
	public void rentalsFilter() throws Exception{
		ReusableFunc.ClickByLinkText(driver,"Rentals");
		ReusableFunc.VerifyTextOnPage(driver, "Rentals");
		
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

		//filter By cell number
		ReusableFunc.ClickByCss(driver, "#key > option:nth-child(2)");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("value"), "2348062899085");
	    ReusableFunc.ClickByXpath(driver, ".//*[@id='search']/div[3]/button", "search");
		//ReusableFunc.VerifyTextOnPage(driver, "Nigeria");
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/table/tbody"), "results table");
		                                                             
		//filter By customer number
		ReusableFunc.ClickByLinkText(driver,"Rentals");
		Thread.sleep(1000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='key']/option[4]", "customer number");
     	ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("value"), "89302855");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='search']/div[3]/button", "search");
		//ReusableFunc.VerifyTextOnPage(driver, "Zambia");
		//ReusableFunc.VerifyTextOnPage(driver, "Rental code exists in the system");
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/table/tbody"), "results table");

		
		//filter by rental code 
		ReusableFunc.ClickByLinkText(driver,"Rentals");
		Thread.sleep(1000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='key']/option[1]", "rental code number");
		ReusableFunc.clearTextBox(driver, By.id("value"));
     	ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("value"), "363673182073510535");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='search']/div[3]/button", "search");
		//ReusableFunc.VerifyTextOnPage(driver, "Zambia_UAT");
		//ReusableFunc.VerifyTextOnPage(driver, "AddToBill");
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/table/tbody"), "results table");

		
						
		//filter by  Smart card number
		ReusableFunc.ClickByLinkText(driver,"Rentals");
		Thread.sleep(1000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='key']/option[3]", "Smartcard number");
		ReusableFunc.clearTextBox(driver, By.id("value"));
     	ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("value"), " 	4290978273");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='search']/div[3]/button", "search");
		//ReusableFunc.VerifyTextOnPage(driver, "IS20");
		//ReusableFunc.VerifyTextOnPage(driver, "260977761506");
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/table/tbody"), "results table");

		

  }
	
	@Test(dependsOnMethods = {"signIn"})
	public void dateFilter() throws Exception{
		ReusableFunc.ClickByLinkText(driver,"Rentals");
		ReusableFunc.VerifyTextOnPage(driver, "Rentals");
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='searchDate']/div[1]/div/span", "From");
		ReusableFunc.ClickByXpath(driver, "html/body/div[2]/div[3]/table/tbody/tr[1]/td[2]", "date");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='searchDate']/div[2]/div/span", "To");
		ReusableFunc.ClickByXpath(driver, "html/body/div[3]/div[3]/table/tbody/tr[6]/td[7]", "date");
			
		//ReusableFunc.ClickByXpath(driver, "html/body/div[2]/div[3]/table/thead/tr[1]/th[2]", "click month");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='searchDate']/div[3]/button", "search");
		
	}
	
	
	@Test(dependsOnMethods = {"signIn"})
	public void rentalCodePopUp() throws Exception{
		ReusableFunc.ClickByLinkText(driver, "Rentals");
		//ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div[2]/div[1]/div/div[2]/table/tbody/tr[1]/td[2]/a", "popup");
		ReusableFunc.ClickByCss(driver, ".btn.btn-default.btn-xs");
		//driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);	
		List<WebElement> tables = driver.findElements(By.cssSelector(".modal-body>table"));
		System.out.println(tables.size());
		
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//org.testng.Assert.assertTrue(driver.findElement(By.xpath(".//*[@id='details']/div")).isDisplayed());
		     
	    //De-hash result table
		//WebElement table = driver.findElement(By.xpath(".//*[@id='draggable']/div[2]/table[1]/tbody/tr/td[1]/table"));
		/*WebElement table = driver.findElement(By.xpath(".//*[@id='draggable']/div[2]/table[1]"));
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		java.util.Iterator<WebElement> i = rows.iterator();
		while(i.hasNext()) {
	    WebElement row = i.next();
	    System.out.println(row.getText());
			  
			}
		System.out.println(rows.size());
		Assert.assertTrue(rows.size() ==3);*/
		    
		//Received SMS
		/*WebElement smstable = (new WebDriverWait(driver, 2))
				  .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".modal-body > table:nth-child(4) > tbody:nth-child(1)")));
		//WebElement smstable = driver.findElement(By.xpath(".//*[@id='draggable']/div[2]/table[1]/tbody"));
		List<WebElement> srows = smstable.findElements(By.tagName("tr"));
		java.util.Iterator<WebElement> j = srows.iterator();
		while(j.hasNext()) {
		}
		
		//System.out.println(srows.size());
		Assert.assertTrue(srows.size()==2);
		
		//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);	
		
		
	    //Rental
	   /* WebElement rtable = driver.findElement(By.xpath(".//*[@id='draggable']/div[2]/table[2]"));
		List<WebElement> rrows = rtable.findElements(By.tagName("tr"));
		java.util.Iterator<WebElement> k = rrows.iterator();
		while(k.hasNext()) {
			 WebElement rrow = k.next();
			 System.out.println(rrow.getText());
		}
			  
		System.out.println(rrows.size());
		Assert.assertTrue(rrows.size()==6);
		

		//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);		
		
		//Entitlement
		 WebElement etable = driver.findElement(By.xpath(".//*[@id='draggable']/div[2]/table[3]"));
		 List<WebElement> erows = etable.findElements(By.tagName("tr"));
		 java.util.Iterator<WebElement> l = erows.iterator();
			while(l.hasNext()) {
				 WebElement erow = l.next();
				 System.out.println(erow.getText());
			}
				  
			System.out.println(erows.size());
			Assert.assertTrue(erows.size()>=1);
		 
		//Transaction
		//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		WebElement ttable = driver.findElement(By.xpath(".//*[@id='draggable']/div[2]/table[4]"));
		List<WebElement> trows = ttable.findElements(By.tagName("tr"));
		java.util.Iterator<WebElement> m = trows.iterator();
			while(m.hasNext()) {
				 WebElement trow = m.next();
				 System.out.println(trow.getText());
			}
					  
			System.out.println(trows.size());
			Assert.assertTrue(trows.size()>=1);
		
		 //Sent SMS
				WebElement smtable = driver.findElement(By.xpath(".//*[@id='draggable']/div[2]/table[5]"));
				 List<WebElement> smrows = smtable.findElements(By.tagName("tr"));
				 java.util.Iterator<WebElement> n = smrows.iterator();
					while(n.hasNext()) {
						 WebElement smrow = n.next();
						 System.out.println(smrow.getText());
					}
						  
					System.out.println(smrows.size());
					Assert.assertTrue(smrows.size()<=8);
					

		
	    ReusableFunc.ClickByXpath(driver, ".//*[@id='closeDetails']", "close");*/
			
		    	
	}
	
	/*@Test(dependsOnMethods = {"signIn"})
	public void reentitleAsset() throws Exception{
		ReusableFunc.ClickByLinkText(driver, "Rentals");
		ReusableFunc.ClickByCss(driver, ".btn.btn-default.btn-xs");
		//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);		
		//ReusableFunc.ClickByID(driver, "reentitle");
		//ReusableFunc.VerifyTextOnPage(driver, "Enter confirmation code to re-entitle");
		//ReusableFunc.verifyElementIsDisplayed(driver, By.id("confcode"), "confirmation code text box");
		
		
		
	}*/
	
	
}
	
		
