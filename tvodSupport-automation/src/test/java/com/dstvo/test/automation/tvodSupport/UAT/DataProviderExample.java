package com.dstvo.test.automation.tvodSupport.UAT;

import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;
import com.thoughtworks.selenium.*;

import org.junit.AfterClass;
import org.testng.annotations.*;

import java.io.File;

import jxl.*; 


	public class DataProviderExample extends TestBase{
	    
	    @BeforeClass
	    @Override
		public void before() throws Exception {
			super.before();
			getWebDriver().get(getSiteURL());
		}
	    
		
		@Test
		public void signIn() throws Exception {
			ReusableFunc.VerifyTextOnPage(driver, "sign in");
			ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath("html/body/div[1]/div/div[1]/div/form/input[1]"), "admin");
			ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath("html/body/div[1]/div/div[1]/div/form/input[2]"), "Tv0dSupp0rt12345");
			ReusableFunc.CheckCheckBox(driver, driver.findElementByXPath("html/body/div[1]/div/div[1]/div/form/label/input"));
			ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div/div[1]/div/form/button", "sign in button");
			ReusableFunc.VerifyTextOnPage(driver, "Sign Out");
			ReusableFunc.ClickByLinkText(driver, "Create Login");
		}
	 
		    
	    @DataProvider(name = "DP1")
	    public Object[][] createData1() throws Exception{
	    	        Object[][] retObjArr=getTableArray("C:\\QA-automated-test_Workspace\\QA-automated-test\\tvodSupport-automation\\src\\test\\java\\com\\dstvo\\test\\automation\\tvodSupport\\Data1.xls",
	                "DataPool", "CSRTestdata");
	        return(retObjArr);
	    }
	    
	    	    
	    @Test (dependsOnMethods = {"signIn"},dataProvider = "DP1")
	    public void testDataProviderExample(String userName,String password, String retypePassword, String emailAddress) throws Exception {  
	    	ReusableFunc.VerifyTextOnPage(driver, "User Name");
	       	//enter user name
	        ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("username"), userName);
	        //enter Pasword
	        ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("password"), password);
	        Thread.sleep(2000);
	        //retype password
	        ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("passwordRetype"),retypePassword);
	        Thread.sleep(2000);
	        //enter email address
	        ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("emailAddress"), emailAddress);
	        Thread.sleep(2000);
	        //changeRole to CSR
	        ReusableFunc.ClickByXpath(driver, "/html/body/div/div[2]/section/article[2]/form/div[6]/div/select/option[2]","CSR");
	        
	        //click Country
	        ReusableFunc.ClickByXpath(driver, "/html/body/div/div[2]/section/article[2]/form/div[7]/div/select/option[6]", "Angola");
	        //signUp
	        ReusableFunc.ClickByXpath(driver, ".//*[@id='createUser']/div[8]/div/input", "signup");
	        Thread.sleep(2000);
	        ReusableFunc.ClickByLinkText(driver, "Create Login");
	        Thread.sleep(2000);
	        //login
	    	ReusableFunc.ClickByLinkText(driver, "Sign Out");
	    	Thread.sleep(2000);
			ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath("html/body/div[1]/div/div[1]/div/form/input[1]"), userName);
			ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath("html/body/div[1]/div/div[1]/div/form/input[2]"), password);
			ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div/div[1]/div/form/button", "sign in button");
			ReusableFunc.VerifyTextOnPage(driver, "Rentals");
			ReusableFunc.ClickByLinkText(driver, "Sign Out");
			this.signIn();
			
	        

	             
	      
	    }
	    
	    
	   /* @DataProvider(name = "DP2")
	    public Object[][] createData2() throws Exception{
	    	        Object[][] retObjArr=getTableArray("C:\\QA-automated-test_Workspace\\QA-automated-test\\tvodSupport-automation\\src\\test\\java\\com\\dstvo\\test\\automation\\tvodSupport\\Data1.xls",
	                "DataPool2", "CSRTestdata2");
	        return(retObjArr);
	    }
	    
	   @Test (dependsOnMethods = {"testDataProviderExample"}, dataProvider = "DP2")
	    public void loginCSR(String userName,String password) throws Exception { 
	    	Thread.sleep(2000);
	    	ReusableFunc.ClickByLinkText(driver, "Sign Out");
	    	Thread.sleep(5000);
			ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath("html/body/div[1]/div/div[1]/div/form/input[1]"), userName);
			ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath("html/body/div[1]/div/div[1]/div/form/input[2]"), password);
			ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div/div[1]/div/form/button", "sign in button");
			ReusableFunc.ClickByLinkText(driver, "Sign Out");

 	     	    	
	    	
	    }*/
	    
	
	    public String[][] getTableArray(String xlFilePath, String sheetName, String tableName) throws Exception{
	        String[][] tabArray=null;
	        
	            Workbook workbook = Workbook.getWorkbook(new File(xlFilePath));
	            Sheet sheet = workbook.getSheet(sheetName); 
	            int startRow,startCol, endRow, endCol,ci,cj;
	            Cell tableStart=sheet.findCell(tableName);
	            startRow=tableStart.getRow();
	            startCol=tableStart.getColumn();

	            Cell tableEnd= sheet.findCell(tableName, startCol+1,startRow+1, 100, 64000,  false);                

	            endRow=tableEnd.getRow();
	            endCol=tableEnd.getColumn();
	            System.out.println("startRow="+startRow+", endRow="+endRow+", " +
	                    "startCol="+startCol+", endCol="+endCol);
	            tabArray=new String[endRow-startRow-1][endCol-startCol-1];
	            ci=0;

	            for (int i=startRow+1;i<endRow;i++,ci++){
	                cj=0;
	                for (int j=startCol+1;j<endCol;j++,cj++){
	                    tabArray[ci][cj]=sheet.getCell(j,i).getContents();
	                }
	            }
	        

	        return(tabArray);
	    }
	    
	    
	}//end of class

	    