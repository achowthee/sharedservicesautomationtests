package com.dstvo.test.automation.tvodSupport.UAT;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.dstvo.test.automation.dstvfunctionlib.ReusableFunc;

public class ProfilesTest extends TestBase{
	@BeforeClass
	@Override
	public void before() throws Exception {
		super.before();
		getWebDriver().get(getSiteURL());
	}
	
	@Test
	public void signIn() throws Exception {
		ReusableFunc.VerifyTextOnPage(driver, "sign in");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath("html/body/div[1]/div/div[1]/div/form/input[1]"), "belindaAdmin@dstvo.com");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementByXPath("html/body/div[1]/div/div[1]/div/form/input[2]"), "123456");
		ReusableFunc.CheckCheckBox(driver, driver.findElementByXPath("html/body/div[1]/div/div[1]/div/form/label/input"));
		ReusableFunc.ClickByXpath(driver, "html/body/div[1]/div/div[1]/div/form/button", "sign in button");
		ReusableFunc.VerifyTextOnPage(driver, "Sign Out");
		

	}

	@Test(dependsOnMethods = {"signIn"})
	public void ProfilesFilter() throws Exception{
		ReusableFunc.ClickByLinkText(driver,"Profiles");
		ReusableFunc.VerifyTextOnPage(driver, "Profiles");
		//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	    //WebDriverWait wait= new WebDriverWait(driver,10 );
		
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("html/body/div/div[2]/div[1]/div/ul")));

		//ReusableFunc.ClickPagination(driver, By.xpath("html/body/div/div[2]/div[1]/div/ul"));
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		//filter By cell number
		ReusableFunc.ClickByCss(driver, "#key > option:nth-child(2)");
		ReusableFunc.ClickByXpath(driver,".//*[@id='key']/option[2]", "CellNumber");
		ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("value"), "566");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='search']/div[3]/button", "search");
		//ReusableFunc.VerifyTextOnPage(driver, "Namibia");
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("/html/body/div/div[2]/div[1]/div/div/table"), "results table");
		
		//filter By customer number
		ReusableFunc.ClickByLinkText(driver,"Profiles");
		Thread.sleep(1000);
     	ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("value"), "27791233037");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='search']/div[3]/button", "search");
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("/html/body/div/div[2]/div[1]/div/div/table"), "results table");

		
		//filter by cellphone number
		/*ReusableFunc.ClickByLinkText(driver,"Profiles");
		Thread.sleep(1000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='key']/option[2]", "cell number");
		ReusableFunc.clearTextBox(driver, By.id("value"));
     	ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("value"), "2348033940557");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='search']/div[3]/button", "search");
		ReusableFunc.VerifyTextOnPage(driver, "Nigeria");*/
		
		
		//filter By Account number
		ReusableFunc.ClickByLinkText(driver,"Profiles");
		Thread.sleep(1000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='key']/option[3]", "Account number");
		ReusableFunc.clearTextBox(driver, By.id("value"));
     	ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("value"), "234");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='search']/div[3]/button", "search");
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("/html/body/div/div[2]/div[1]/div/div/table"), "results table");
		
		//filter by  Smart card number
		ReusableFunc.ClickByLinkText(driver,"Profiles");
		Thread.sleep(1000);
		ReusableFunc.ClickByXpath(driver, ".//*[@id='key']/option[4]", "Smartcard number");
		ReusableFunc.clearTextBox(driver, By.id("value"));
     	ReusableFunc.EnterTextInTextBox(driver, driver.findElementById("value"), "446");
		ReusableFunc.ClickByXpath(driver, ".//*[@id='search']/div[3]/button", "search");
		ReusableFunc.verifyElementIsDisplayed(driver, By.xpath("/html/body/div/div[2]/div[1]/div/div/table"), "results table");

		//html/body/div[1]/div[2]/div[1]/div/div/table/tbody/tr[4]/td[6]

		
	}
	
	@Test(dependsOnMethods = {"signIn"})
	public void ProfilesPopUp() throws Exception{
		ReusableFunc.ClickByLinkText(driver,"Profiles");
		ReusableFunc.VerifyTextOnPage(driver, "Profiles");
		ReusableFunc.ClickByCss(driver, ".btn.btn-default.btn-xs");
	    driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		ReusableFunc.verifyElementIsDisplayed(driver,By.xpath(".//*[@id='draggable']/div[2]/table/tbody/tr/td[1]"),"popup");
		WebElement table = driver.findElement(By.xpath("//*[@id='draggable']"));
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		java.util.Iterator<WebElement> i = rows.iterator();
		while(i.hasNext()) {
		    WebElement row = i.next();
		    System.out.println(row.getText());
		  
		}
		System.out.println(rows.size());
	    Assert.assertTrue(rows.size() >=10);
	  
		ReusableFunc.ClickByXpath(driver, ".//*[@id='draggable']/div[3]/button", "close");
				
	}
	
/*	@Test(dependsOnMethods = {"signIn"})
	public void ProfilesReactivatePVR() throws Exception{
		ReusableFunc.ClickByLinkText(driver,"Profiles");
		ReusableFunc.VerifyTextOnPage(driver, "Profiles");
		ReusableFunc.ClickByCss(driver, ".btn.btn-default.btn-xs");
	    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	    ReusableFunc.ClickByID(driver,"reactivate");
	    ReusableFunc.VerifyTextOnPage(driver, "Service Results");
		ReusableFunc.ClickByID(driver, "goback");*/
		
	/*}
	//still to be implemented
	@Test(dependsOnMethods = {"SignIn"})
	public void ProfilesSyncFromIBS() throws Exception{
		ReusableFunc.ClickByLinkText(driver,"Profiles");
		ReusableFunc.VerifyTextOnPage(driver, "Profiles");
		ReusableFunc.ClickByCss(driver, ".btn.btn-default.btn-xs");
	    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	    ReusableFunc.ClickByID(driver,"sync");
	    ReusableFunc.VerifyTextOnPage(driver, "Service Results");
		ReusableFunc.ClickByID(driver, "goback");
		
	}*/
	
	
	
}
